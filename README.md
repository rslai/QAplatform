# QAplatform

#### 介绍
质量管理平台

#### 软件架构
软件架构说明


#### 安装教程

1. 进入项目目录
2. mvn 编译
    ```
    mvn clean package
    ```
3. 退出项目目录后，随便创建一个一目录
4. 将 项目目录中的 .env 和 docker-compose.yml 复制过来
5. 修改 docker-compose.yml 文件
   ```
      主要修改 "image:" 改成最新版本
   ```
6. 修改 .env 文件
   ```
   主要修改 MySQL Config
   ```
5. 启动 docker 容器
   ```
   docker-compose up -d
   ```

#### 本地调试

1. git 拉取最新代码
2. 找到 Application.java 点击运行
3. 点击 Edit Configurations 添加 VM Options 参数，内容如下
   ```
   --add-opens java.base/java.lang=ALL-UNNAMED
   ```
   原因：在 JsonValidator 中使用了反射修改 AssertionError 的 detailMessage 私有参数，在 jdk15 中会警告，在 jdk18中会抛异常（异常信息如下）。所以需要添加此参数。同样在 pom.xml 的 entryPoint 也添加了此参数。
   ```
   Unable to make field private java.lang.String java.lang.Throwable.detailMessage accessible: module java.base does not "opens java.lang" to unnamed module @5ffead27
   ```

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
