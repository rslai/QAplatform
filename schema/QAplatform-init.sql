/*
 QAplatform 数据库初始化脚本

 1、手动删除数据库 QAplatform
 2、在 mysql 客户端中执行以下 sql
 3、清除 redis 中 db10 下的 qap 缓存

 默认账号：admin
 默认密码：111111
*/

CREATE DATABASE IF NOT EXISTS QAplatform CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE QAplatform;

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for company
-- ----------------------------
DROP TABLE IF EXISTS `company`;
CREATE TABLE `company` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `name` varchar(50) NOT NULL COMMENT '公司名称',
  `is_delete` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否删除',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='公司信息表';

-- ----------------------------
-- Records of company
-- ----------------------------
BEGIN;
INSERT INTO `company` VALUES (1, '试验田', b'0', '2023-04-13 12:41:45', '2022-05-15 00:28:20');
COMMIT;

-- ----------------------------
-- Table structure for project
-- ----------------------------
DROP TABLE IF EXISTS `project`;
CREATE TABLE `project` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `company_id` bigint(20) NOT NULL COMMENT '所属公司 ID',
  `name` varchar(50) NOT NULL COMMENT '项目名称',
  `is_delete` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否删除',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `desc` varchar(255) DEFAULT NULL COMMENT '项目描述',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COMMENT='项目信息表';

-- ----------------------------
-- Records of project
-- ----------------------------
BEGIN;
INSERT INTO `project` VALUES (1, 1, '试验田', b'0', '2023-08-25 00:05:03', '2022-05-15 00:29:12', NULL);
COMMIT;

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `name` varchar(50) NOT NULL COMMENT '角色名称',
  `desc` varchar(50) NOT NULL COMMENT '角色描述',
  `is_delete` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否删除',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='角色信息表';

-- ----------------------------
-- Records of role
-- ----------------------------
BEGIN;
INSERT INTO `role` VALUES (1, 'admin', '超级管理员', b'0', '2023-08-15 12:44:54', '2023-07-19 09:01:06');
INSERT INTO `role` VALUES (2, 'user', '普通用户', b'0', '2023-08-22 00:54:03', '2023-07-19 09:01:38');
COMMIT;

-- ----------------------------
-- Table structure for role_route
-- ----------------------------
DROP TABLE IF EXISTS `role_route`;
CREATE TABLE `role_route` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `role_id` bigint(20) NOT NULL COMMENT '角色 id',
  `route_id` bigint(20) NOT NULL COMMENT '路由信息',
  `is_delete` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否删除',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=615 DEFAULT CHARSET=utf8 COMMENT='角色对应路由信息表';

-- ----------------------------
-- Records of role_route
-- ----------------------------
BEGIN;
INSERT INTO `role_route` VALUES (522, 3, 30, b'0', '2023-08-13 23:50:37', '2023-08-13 23:50:37');
INSERT INTO `role_route` VALUES (523, 3, 31, b'0', '2023-08-13 23:50:37', '2023-08-13 23:50:37');
INSERT INTO `role_route` VALUES (524, 3, 61, b'0', '2023-08-13 23:50:37', '2023-08-13 23:50:37');
INSERT INTO `role_route` VALUES (525, 3, 32, b'0', '2023-08-13 23:50:37', '2023-08-13 23:50:37');
INSERT INTO `role_route` VALUES (526, 3, 60, b'0', '2023-08-13 23:50:37', '2023-08-13 23:50:37');
INSERT INTO `role_route` VALUES (527, 1, 1, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (528, 1, 2, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (529, 1, 3, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (530, 1, 4, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (531, 1, 5, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (532, 1, 6, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (533, 1, 7, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (534, 1, 8, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (535, 1, 9, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (536, 1, 10, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (537, 1, 11, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (538, 1, 12, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (539, 1, 13, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (540, 1, 14, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (541, 1, 15, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (542, 1, 16, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (543, 1, 17, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (544, 1, 18, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (545, 1, 19, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (546, 1, 20, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (547, 1, 21, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (548, 1, 22, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (549, 1, 23, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (550, 1, 25, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (551, 1, 26, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (552, 1, 27, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (553, 1, 28, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (554, 1, 29, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (555, 1, 30, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (556, 1, 24, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (557, 1, 59, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (558, 1, 31, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (559, 1, 32, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (560, 1, 33, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (561, 1, 34, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (562, 1, 42, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (563, 1, 43, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (564, 1, 44, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (565, 1, 35, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (566, 1, 36, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (567, 1, 45, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (568, 1, 46, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (569, 1, 47, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (570, 1, 37, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (571, 1, 48, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (572, 1, 49, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (573, 1, 50, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (574, 1, 41, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (575, 1, 38, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (576, 1, 51, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (577, 1, 52, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (578, 1, 39, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (579, 1, 53, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (580, 1, 54, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (581, 1, 55, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (582, 1, 40, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (583, 1, 56, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (584, 1, 57, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (585, 1, 58, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (586, 1, 62, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (587, 1, 63, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (588, 2, 1, b'0', '2023-08-22 00:54:03', '2023-08-22 00:54:03');
INSERT INTO `role_route` VALUES (589, 2, 2, b'0', '2023-08-22 00:54:03', '2023-08-22 00:54:03');
INSERT INTO `role_route` VALUES (590, 2, 3, b'0', '2023-08-22 00:54:03', '2023-08-22 00:54:03');
INSERT INTO `role_route` VALUES (591, 2, 4, b'0', '2023-08-22 00:54:03', '2023-08-22 00:54:03');
INSERT INTO `role_route` VALUES (592, 2, 5, b'0', '2023-08-22 00:54:03', '2023-08-22 00:54:03');
INSERT INTO `role_route` VALUES (593, 2, 6, b'0', '2023-08-22 00:54:03', '2023-08-22 00:54:03');
INSERT INTO `role_route` VALUES (594, 2, 7, b'0', '2023-08-22 00:54:03', '2023-08-22 00:54:03');
INSERT INTO `role_route` VALUES (595, 2, 8, b'0', '2023-08-22 00:54:03', '2023-08-22 00:54:03');
INSERT INTO `role_route` VALUES (596, 2, 9, b'0', '2023-08-22 00:54:03', '2023-08-22 00:54:03');
INSERT INTO `role_route` VALUES (597, 2, 11, b'0', '2023-08-22 00:54:03', '2023-08-22 00:54:03');
INSERT INTO `role_route` VALUES (598, 2, 12, b'0', '2023-08-22 00:54:03', '2023-08-22 00:54:03');
INSERT INTO `role_route` VALUES (599, 2, 13, b'0', '2023-08-22 00:54:03', '2023-08-22 00:54:03');
INSERT INTO `role_route` VALUES (600, 2, 14, b'0', '2023-08-22 00:54:03', '2023-08-22 00:54:03');
INSERT INTO `role_route` VALUES (601, 2, 15, b'0', '2023-08-22 00:54:03', '2023-08-22 00:54:03');
INSERT INTO `role_route` VALUES (602, 2, 16, b'0', '2023-08-22 00:54:03', '2023-08-22 00:54:03');
INSERT INTO `role_route` VALUES (603, 2, 17, b'0', '2023-08-22 00:54:03', '2023-08-22 00:54:03');
INSERT INTO `role_route` VALUES (604, 2, 19, b'0', '2023-08-22 00:54:03', '2023-08-22 00:54:03');
INSERT INTO `role_route` VALUES (605, 2, 20, b'0', '2023-08-22 00:54:03', '2023-08-22 00:54:03');
INSERT INTO `role_route` VALUES (606, 2, 21, b'0', '2023-08-22 00:54:03', '2023-08-22 00:54:03');
INSERT INTO `role_route` VALUES (607, 2, 22, b'0', '2023-08-22 00:54:03', '2023-08-22 00:54:03');
INSERT INTO `role_route` VALUES (608, 2, 23, b'0', '2023-08-22 00:54:03', '2023-08-22 00:54:03');
INSERT INTO `role_route` VALUES (609, 2, 27, b'0', '2023-08-22 00:54:03', '2023-08-22 00:54:03');
INSERT INTO `role_route` VALUES (610, 2, 28, b'0', '2023-08-22 00:54:03', '2023-08-22 00:54:03');
INSERT INTO `role_route` VALUES (611, 2, 29, b'0', '2023-08-22 00:54:03', '2023-08-22 00:54:03');
INSERT INTO `role_route` VALUES (612, 2, 30, b'0', '2023-08-22 00:54:03', '2023-08-22 00:54:03');
INSERT INTO `role_route` VALUES (613, 2, 24, b'0', '2023-08-22 00:54:03', '2023-08-22 00:54:03');
INSERT INTO `role_route` VALUES (614, 2, 59, b'0', '2023-08-22 00:54:03', '2023-08-22 00:54:03');
COMMIT;

-- ----------------------------
-- Table structure for role_route
-- ----------------------------
DROP TABLE IF EXISTS `role_route`;
CREATE TABLE `role_route` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `role_id` bigint(20) NOT NULL COMMENT '角色 id',
  `route_id` bigint(20) NOT NULL COMMENT '路由信息',
  `is_delete` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否删除',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=615 DEFAULT CHARSET=utf8 COMMENT='角色对应路由信息表';

-- ----------------------------
-- Records of role_route
-- ----------------------------
BEGIN;
INSERT INTO `role_route` VALUES (522, 3, 30, b'0', '2023-08-13 23:50:37', '2023-08-13 23:50:37');
INSERT INTO `role_route` VALUES (523, 3, 31, b'0', '2023-08-13 23:50:37', '2023-08-13 23:50:37');
INSERT INTO `role_route` VALUES (524, 3, 61, b'0', '2023-08-13 23:50:37', '2023-08-13 23:50:37');
INSERT INTO `role_route` VALUES (525, 3, 32, b'0', '2023-08-13 23:50:37', '2023-08-13 23:50:37');
INSERT INTO `role_route` VALUES (526, 3, 60, b'0', '2023-08-13 23:50:37', '2023-08-13 23:50:37');
INSERT INTO `role_route` VALUES (527, 1, 1, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (528, 1, 2, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (529, 1, 3, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (530, 1, 4, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (531, 1, 5, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (532, 1, 6, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (533, 1, 7, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (534, 1, 8, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (535, 1, 9, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (536, 1, 10, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (537, 1, 11, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (538, 1, 12, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (539, 1, 13, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (540, 1, 14, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (541, 1, 15, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (542, 1, 16, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (543, 1, 17, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (544, 1, 18, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (545, 1, 19, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (546, 1, 20, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (547, 1, 21, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (548, 1, 22, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (549, 1, 23, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (550, 1, 25, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (551, 1, 26, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (552, 1, 27, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (553, 1, 28, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (554, 1, 29, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (555, 1, 30, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (556, 1, 24, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (557, 1, 59, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (558, 1, 31, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (559, 1, 32, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (560, 1, 33, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (561, 1, 34, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (562, 1, 42, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (563, 1, 43, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (564, 1, 44, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (565, 1, 35, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (566, 1, 36, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (567, 1, 45, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (568, 1, 46, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (569, 1, 47, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (570, 1, 37, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (571, 1, 48, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (572, 1, 49, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (573, 1, 50, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (574, 1, 41, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (575, 1, 38, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (576, 1, 51, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (577, 1, 52, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (578, 1, 39, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (579, 1, 53, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (580, 1, 54, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (581, 1, 55, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (582, 1, 40, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (583, 1, 56, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (584, 1, 57, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (585, 1, 58, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (586, 1, 62, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (587, 1, 63, b'0', '2023-08-15 12:44:54', '2023-08-15 12:44:54');
INSERT INTO `role_route` VALUES (588, 2, 1, b'0', '2023-08-22 00:54:03', '2023-08-22 00:54:03');
INSERT INTO `role_route` VALUES (589, 2, 2, b'0', '2023-08-22 00:54:03', '2023-08-22 00:54:03');
INSERT INTO `role_route` VALUES (590, 2, 3, b'0', '2023-08-22 00:54:03', '2023-08-22 00:54:03');
INSERT INTO `role_route` VALUES (591, 2, 4, b'0', '2023-08-22 00:54:03', '2023-08-22 00:54:03');
INSERT INTO `role_route` VALUES (592, 2, 5, b'0', '2023-08-22 00:54:03', '2023-08-22 00:54:03');
INSERT INTO `role_route` VALUES (593, 2, 6, b'0', '2023-08-22 00:54:03', '2023-08-22 00:54:03');
INSERT INTO `role_route` VALUES (594, 2, 7, b'0', '2023-08-22 00:54:03', '2023-08-22 00:54:03');
INSERT INTO `role_route` VALUES (595, 2, 8, b'0', '2023-08-22 00:54:03', '2023-08-22 00:54:03');
INSERT INTO `role_route` VALUES (596, 2, 9, b'0', '2023-08-22 00:54:03', '2023-08-22 00:54:03');
INSERT INTO `role_route` VALUES (597, 2, 11, b'0', '2023-08-22 00:54:03', '2023-08-22 00:54:03');
INSERT INTO `role_route` VALUES (598, 2, 12, b'0', '2023-08-22 00:54:03', '2023-08-22 00:54:03');
INSERT INTO `role_route` VALUES (599, 2, 13, b'0', '2023-08-22 00:54:03', '2023-08-22 00:54:03');
INSERT INTO `role_route` VALUES (600, 2, 14, b'0', '2023-08-22 00:54:03', '2023-08-22 00:54:03');
INSERT INTO `role_route` VALUES (601, 2, 15, b'0', '2023-08-22 00:54:03', '2023-08-22 00:54:03');
INSERT INTO `role_route` VALUES (602, 2, 16, b'0', '2023-08-22 00:54:03', '2023-08-22 00:54:03');
INSERT INTO `role_route` VALUES (603, 2, 17, b'0', '2023-08-22 00:54:03', '2023-08-22 00:54:03');
INSERT INTO `role_route` VALUES (604, 2, 19, b'0', '2023-08-22 00:54:03', '2023-08-22 00:54:03');
INSERT INTO `role_route` VALUES (605, 2, 20, b'0', '2023-08-22 00:54:03', '2023-08-22 00:54:03');
INSERT INTO `role_route` VALUES (606, 2, 21, b'0', '2023-08-22 00:54:03', '2023-08-22 00:54:03');
INSERT INTO `role_route` VALUES (607, 2, 22, b'0', '2023-08-22 00:54:03', '2023-08-22 00:54:03');
INSERT INTO `role_route` VALUES (608, 2, 23, b'0', '2023-08-22 00:54:03', '2023-08-22 00:54:03');
INSERT INTO `role_route` VALUES (609, 2, 27, b'0', '2023-08-22 00:54:03', '2023-08-22 00:54:03');
INSERT INTO `role_route` VALUES (610, 2, 28, b'0', '2023-08-22 00:54:03', '2023-08-22 00:54:03');
INSERT INTO `role_route` VALUES (611, 2, 29, b'0', '2023-08-22 00:54:03', '2023-08-22 00:54:03');
INSERT INTO `role_route` VALUES (612, 2, 30, b'0', '2023-08-22 00:54:03', '2023-08-22 00:54:03');
INSERT INTO `role_route` VALUES (613, 2, 24, b'0', '2023-08-22 00:54:03', '2023-08-22 00:54:03');
INSERT INTO `role_route` VALUES (614, 2, 59, b'0', '2023-08-22 00:54:03', '2023-08-22 00:54:03');
COMMIT;

-- ----------------------------
-- Table structure for route
-- ----------------------------
DROP TABLE IF EXISTS `route`;
CREATE TABLE `route` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `type` int(11) NOT NULL COMMENT '资源类型',
  `path` varchar(255) NOT NULL COMMENT '路由匹配的路径，注意根节点前要加/',
  `name` varchar(255) NOT NULL COMMENT '路由的名称',
  `component` varchar(255) NOT NULL COMMENT '对应加载的组件',
  `redirect` varchar(255) DEFAULT NULL COMMENT '重定向的路由路径',
  `hidden` bit(1) NOT NULL COMMENT '在菜单中是否隐藏。true：隐藏，默认 false',
  `always_show` bit(1) NOT NULL COMMENT '是否始终显示根菜单。true：无论有没有子节点都显示，false：必须有子节点才显示',
  `props` varchar(255) DEFAULT NULL COMMENT '传递数据的 props 属性',
  `route_meta_id` bigint(20) NOT NULL COMMENT '元信息 id',
  `parent_id` bigint(20) NOT NULL COMMENT '父分类的 id',
  `sort` int(11) NOT NULL COMMENT '排序',
  `resource_id` bigint(20) NOT NULL COMMENT '资源 id',
  `is_delete` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否删除',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8 COMMENT='路由信息表';

-- ----------------------------
-- Records of route
-- ----------------------------
BEGIN;
INSERT INTO `route` VALUES (1, 1, '/functional_test', 'functional_test', 'layout', '/functional_test/test_case', b'0', b'0', NULL, 1, 0, 1, 0, b'0', '2023-07-19 12:38:30', '2023-07-19 12:38:30');
INSERT INTO `route` VALUES (2, 1, 'test_case', 'test_case', 'test_case', NULL, b'0', b'0', '{ \"caseType\": 1 }', 2, 1, 1, 0, b'0', '2023-07-19 12:45:40', '2023-07-19 12:45:40');
INSERT INTO `route` VALUES (3, 1, 'edit_testsuite', 'edit_testsuite', 'edit_testsuite', NULL, b'1', b'0', NULL, 3, 1, 2, 0, b'0', '2023-07-19 12:54:06', '2023-07-19 12:54:06');
INSERT INTO `route` VALUES (4, 1, 'plan/execute', 'test_execute', 'test_execute', NULL, b'1', b'0', NULL, 4, 1, 3, 0, b'0', '2023-07-19 12:57:38', '2023-07-19 12:57:38');
INSERT INTO `route` VALUES (5, 1, 'test_plan', 'test_plan', 'test_plan', NULL, b'0', b'0', '{ \"caseType\": 1 }', 5, 1, 4, 0, b'0', '2023-07-19 12:59:22', '2023-07-19 12:59:22');
INSERT INTO `route` VALUES (6, 1, 'result_detail', 'result_detail', 'result_detail', NULL, b'1', b'0', NULL, 6, 1, 5, 0, b'0', '2023-07-19 13:02:11', '2023-07-19 13:02:11');
INSERT INTO `route` VALUES (7, 1, 'data_manage', 'data_manage', 'data_manage', NULL, b'0', b'0', NULL, 7, 1, 6, 0, b'0', '2023-07-19 13:03:56', '2023-07-19 13:03:56');
INSERT INTO `route` VALUES (8, 1, '/interface_test', 'interface_test', 'layout', '/interface_test/test_case', b'0', b'0', NULL, 8, 0, 2, 0, b'0', '2023-07-19 13:05:32', '2023-07-19 13:05:32');
INSERT INTO `route` VALUES (9, 1, 'test_case', 'test_case', 'test_case', NULL, b'0', b'0', '{ \"caseType\": 2 }', 9, 8, 1, 0, b'0', '2023-07-19 13:07:30', '2023-07-19 13:07:30');
INSERT INTO `route` VALUES (10, 1, 'public_step', 'public_step', 'public_step', NULL, b'0', b'0', NULL, 10, 8, 2, 0, b'0', '2023-07-19 13:10:03', '2023-07-19 13:10:03');
INSERT INTO `route` VALUES (11, 1, 'test_suite', 'test_suite', 'test_suite', NULL, b'0', b'0', '{ \"caseType\": 2 }', 11, 8, 3, 0, b'0', '2023-07-19 13:16:14', '2023-07-19 13:16:14');
INSERT INTO `route` VALUES (12, 1, 'edit_testsuite', 'edit_testsuite', 'edit_testsuite', NULL, b'1', b'0', NULL, 12, 8, 4, 0, b'0', '2023-07-19 13:43:56', '2023-07-19 13:43:56');
INSERT INTO `route` VALUES (13, 1, 'test_results', 'test_results', 'test_results', NULL, b'0', b'0', NULL, 13, 8, 5, 0, b'0', '2023-07-19 13:45:00', '2023-07-19 13:45:00');
INSERT INTO `route` VALUES (14, 1, 'result_detail', 'result_detail', 'result_detail', NULL, b'1', b'0', NULL, 14, 8, 6, 0, b'0', '2023-07-19 13:47:30', '2023-07-19 13:47:30');
INSERT INTO `route` VALUES (15, 1, 'interface_define', 'interface_define', 'interface_define', '/interface_test/interface_define/http', b'0', b'0', NULL, 15, 8, 7, 0, b'0', '2023-07-19 13:48:40', '2023-07-19 13:48:40');
INSERT INTO `route` VALUES (16, 1, 'http', 'http', 'http', NULL, b'0', b'0', NULL, 16, 15, 1, 0, b'0', '2023-07-19 13:50:07', '2023-07-19 13:50:07');
INSERT INTO `route` VALUES (17, 1, 'web_socket', 'web_socket', 'web_socket', NULL, b'0', b'0', NULL, 17, 15, 2, 0, b'0', '2023-07-19 21:57:00', '2023-07-19 21:57:00');
INSERT INTO `route` VALUES (18, 1, 'data_channel', 'data_channel', 'data_channel', NULL, b'0', b'0', NULL, 18, 15, 3, 0, b'0', '2023-07-19 21:58:05', '2023-07-19 21:58:05');
INSERT INTO `route` VALUES (19, 1, 'database', 'database', 'database', NULL, b'0', b'0', NULL, 19, 15, 4, 0, b'0', '2023-07-19 21:58:29', '2023-07-19 21:58:29');
INSERT INTO `route` VALUES (20, 1, 'ssh', 'ssh', 'ssh', NULL, b'0', b'0', NULL, 20, 15, 5, 0, b'0', '2023-07-19 21:59:24', '2023-07-19 21:59:24');
INSERT INTO `route` VALUES (21, 1, 'param_define', 'param_define', 'param_define', NULL, b'0', b'0', NULL, 21, 8, 8, 0, b'0', '2023-07-19 22:01:29', '2023-07-19 22:01:29');
INSERT INTO `route` VALUES (22, 1, 'global_param', 'global_param', 'global_param', NULL, b'0', b'0', NULL, 22, 21, 1, 0, b'0', '2023-07-19 22:01:57', '2023-07-19 22:01:57');
INSERT INTO `route` VALUES (23, 1, 'env_param', 'env_param', 'env_param', NULL, b'0', b'0', NULL, 23, 21, 2, 0, b'0', '2023-07-19 22:05:07', '2023-07-19 22:05:07');
INSERT INTO `route` VALUES (24, 1, 'job', 'job', 'job', NULL, b'0', b'0', NULL, 24, 30, 1, 0, b'0', '2023-08-05 01:52:49', '2023-07-19 22:06:17');
INSERT INTO `route` VALUES (25, 1, '/performance_test', 'performance_test', 'layout', '/performance_test/performance', b'0', b'0', NULL, 25, 0, 3, 0, b'0', '2023-07-19 22:08:09', '2023-07-19 22:08:09');
INSERT INTO `route` VALUES (26, 1, 'performance', 'performance', 'jobs', NULL, b'0', b'0', NULL, 26, 25, 1, 0, b'0', '2023-07-19 22:12:12', '2023-07-19 22:12:12');
INSERT INTO `route` VALUES (27, 1, '/tools', 'tools', 'layout', '/tools/ocr', b'0', b'0', NULL, 27, 0, 4, 0, b'0', '2023-07-19 22:15:53', '2023-07-19 22:15:53');
INSERT INTO `route` VALUES (28, 1, 'ocr', 'ocr', 'ocr', NULL, b'0', b'0', NULL, 28, 27, 1, 0, b'0', '2023-07-19 22:23:19', '2023-07-19 22:23:19');
INSERT INTO `route` VALUES (29, 1, 'fileUpload', 'fileUpload', 'fileUpload', NULL, b'0', b'0', NULL, 29, 27, 2, 0, b'0', '2023-07-19 22:24:13', '2023-07-19 22:24:13');
INSERT INTO `route` VALUES (30, 1, '/project_manage', 'project_manage', 'layout', '/project_manage/members', b'0', b'0', NULL, 30, 0, 5, 0, b'0', '2023-07-19 22:26:20', '2023-07-19 22:26:20');
INSERT INTO `route` VALUES (31, 1, 'members', 'members', 'members', NULL, b'0', b'0', NULL, 31, 30, 3, 0, b'0', '2023-07-19 22:28:01', '2023-07-19 22:28:01');
INSERT INTO `route` VALUES (32, 1, 'project_settings', 'project_settings', 'project_settings', NULL, b'0', b'0', NULL, 32, 30, 4, 0, b'0', '2023-07-19 22:28:19', '2023-07-19 22:28:19');
INSERT INTO `route` VALUES (33, 1, '/manage', 'manage', 'layout', NULL, b'0', b'0', NULL, 33, 0, 6, 0, b'0', '2023-07-19 22:30:12', '2023-07-19 22:30:12');
INSERT INTO `route` VALUES (34, 1, 'users', 'users', 'users', NULL, b'0', b'0', NULL, 34, 33, 1, 0, b'0', '2023-07-19 22:31:51', '2023-07-19 22:31:51');
INSERT INTO `route` VALUES (35, 1, 'users/edit_user', 'edit_user', 'edit_user', NULL, b'1', b'0', NULL, 35, 33, 2, 0, b'0', '2023-07-19 22:34:41', '2023-07-19 22:34:41');
INSERT INTO `route` VALUES (36, 1, 'company_list', 'company_list', 'company_list', NULL, b'0', b'0', NULL, 36, 33, 3, 0, b'0', '2023-07-19 22:35:05', '2023-07-19 22:35:05');
INSERT INTO `route` VALUES (37, 1, 'project_list', 'project_list', 'project_list', NULL, b'0', b'0', NULL, 37, 33, 4, 0, b'0', '2023-07-19 22:37:53', '2023-07-19 22:37:53');
INSERT INTO `route` VALUES (38, 1, 'resource', 'resource PagePermission', 'resource', NULL, b'0', b'0', NULL, 38, 41, 1, 0, b'0', '2023-07-20 00:18:36', '2023-07-20 00:18:36');
INSERT INTO `route` VALUES (39, 1, 'menu', 'menu', 'menu', NULL, b'0', b'0', NULL, 39, 41, 2, 0, b'0', '2023-07-20 00:20:09', '2023-07-20 00:20:09');
INSERT INTO `route` VALUES (40, 1, 'roles', 'roles', 'roles', NULL, b'0', b'0', NULL, 40, 41, 3, 0, b'0', '2023-07-20 00:21:04', '2023-07-20 00:21:04');
INSERT INTO `route` VALUES (41, 1, 'permission', 'permission', 'permission', NULL, b'0', b'0', NULL, 41, 33, 5, 0, b'0', '2023-07-28 20:29:13', '2023-07-28 20:29:13');
INSERT INTO `route` VALUES (59, 1, 'jobResult', 'jobResult', 'jobResult', '', b'0', b'0', '', 42, 30, 2, 0, b'0', '2023-08-05 01:53:51', '2023-08-05 01:53:51');
INSERT INTO `route` VALUES (62, 1, 'sysJob', 'sysJob', 'sysJob', '', b'0', b'0', '', 43, 33, 6, 0, b'0', '2023-08-15 12:44:15', '2023-08-15 12:44:15');
INSERT INTO `route` VALUES (63, 1, 'sysJobResult', 'sysJobResult', 'sysJobResult', '', b'0', b'0', '', 44, 33, 7, 0, b'0', '2023-08-15 12:47:58', '2023-08-15 12:44:34');
COMMIT;

-- ----------------------------
-- Table structure for route_meta
-- ----------------------------
DROP TABLE IF EXISTS `route_meta`;
CREATE TABLE `route_meta` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `title` varchar(255) NOT NULL COMMENT '页面标题',
  `icon` varchar(255) DEFAULT NULL COMMENT '图标',
  `no_cache` bit(1) NOT NULL COMMENT '是否禁用缓存页面。true：禁止缓存页面（默认值为false）',
  `affix` bit(1) NOT NULL COMMENT '标签视图中是否可关闭。true：不允许关闭（默认 false）',
  `breadcrumb` bit(1) NOT NULL COMMENT '是否显示面包屑。false：不在面包屑中显示（默认值为 true）',
  `active_menu` varchar(255) NOT NULL COMMENT '高亮菜单。设置后会高亮对应的侧边栏菜单',
  `is_delete` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否删除',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8 COMMENT='路由 Meta 信息表';

-- ----------------------------
-- Records of route_meta
-- ----------------------------
BEGIN;
INSERT INTO `route_meta` VALUES (1, '功能测试', 'el-icon-data-line', b'0', b'0', b'1', '', b'0', '2023-07-19 12:39:14', '2023-07-19 12:39:14');
INSERT INTO `route_meta` VALUES (2, '测试用例', NULL, b'0', b'0', b'1', '', b'0', '2023-07-19 12:44:56', '2023-07-19 12:44:56');
INSERT INTO `route_meta` VALUES (3, '编辑套件', NULL, b'0', b'0', b'1', '', b'0', '2023-07-19 12:53:21', '2023-07-19 12:53:21');
INSERT INTO `route_meta` VALUES (4, '执行套件', NULL, b'0', b'0', b'1', '', b'0', '2023-07-19 12:57:05', '2023-07-19 12:57:05');
INSERT INTO `route_meta` VALUES (5, '测试执行', NULL, b'0', b'0', b'1', '', b'0', '2023-07-19 12:59:49', '2023-07-19 12:59:49');
INSERT INTO `route_meta` VALUES (6, '报告详情', NULL, b'0', b'0', b'1', '', b'0', '2023-07-19 13:02:33', '2023-07-19 13:02:33');
INSERT INTO `route_meta` VALUES (7, '用例导入', NULL, b'0', b'0', b'1', '', b'0', '2023-07-19 13:03:20', '2023-07-19 13:03:20');
INSERT INTO `route_meta` VALUES (8, '接口测试', 'el-icon-data-line', b'0', b'0', b'1', '', b'0', '2023-07-19 13:05:55', '2023-07-19 13:05:55');
INSERT INTO `route_meta` VALUES (9, '测试用例', NULL, b'0', b'0', b'1', '', b'0', '2023-07-19 13:06:33', '2023-07-19 13:06:33');
INSERT INTO `route_meta` VALUES (10, '公共步骤', NULL, b'0', b'0', b'1', '', b'0', '2023-07-19 13:10:19', '2023-07-19 13:10:19');
INSERT INTO `route_meta` VALUES (11, '测试套件', NULL, b'0', b'0', b'1', '', b'0', '2023-07-19 13:15:32', '2023-07-19 13:15:32');
INSERT INTO `route_meta` VALUES (12, '编辑套件', NULL, b'0', b'0', b'1', '', b'0', '2023-07-19 13:43:10', '2023-07-19 13:43:10');
INSERT INTO `route_meta` VALUES (13, '测试报告', NULL, b'0', b'0', b'1', '', b'0', '2023-07-19 13:44:25', '2023-07-19 13:44:25');
INSERT INTO `route_meta` VALUES (14, '报告详情', NULL, b'0', b'0', b'1', '', b'0', '2023-07-19 13:46:58', '2023-07-19 13:46:58');
INSERT INTO `route_meta` VALUES (15, '接口定义', NULL, b'0', b'0', b'1', '', b'0', '2023-07-19 13:47:59', '2023-07-19 13:47:59');
INSERT INTO `route_meta` VALUES (16, 'HTTP', NULL, b'0', b'0', b'1', '', b'0', '2023-07-19 13:49:33', '2023-07-19 13:49:33');
INSERT INTO `route_meta` VALUES (17, 'WebSocket', NULL, b'0', b'0', b'1', '', b'0', '2023-07-19 21:56:20', '2023-07-19 21:56:20');
INSERT INTO `route_meta` VALUES (18, 'DataChannel', NULL, b'0', b'0', b'1', '', b'0', '2023-07-19 21:57:42', '2023-07-19 21:57:42');
INSERT INTO `route_meta` VALUES (19, 'Database', NULL, b'0', b'0', b'1', '', b'0', '2023-07-19 21:58:48', '2023-07-19 21:58:48');
INSERT INTO `route_meta` VALUES (20, 'SSH', NULL, b'0', b'0', b'1', '', b'0', '2023-07-19 21:59:01', '2023-07-19 21:59:01');
INSERT INTO `route_meta` VALUES (21, '参数定义', NULL, b'0', b'0', b'1', '', b'0', '2023-07-19 22:00:50', '2023-07-19 22:00:50');
INSERT INTO `route_meta` VALUES (22, '全局参数', NULL, b'0', b'0', b'1', '', b'0', '2023-07-19 22:02:13', '2023-07-19 22:02:13');
INSERT INTO `route_meta` VALUES (23, '环境参数', NULL, b'0', b'0', b'1', '', b'0', '2023-07-19 22:04:46', '2023-07-19 22:04:46');
INSERT INTO `route_meta` VALUES (24, '定时任务', NULL, b'0', b'0', b'1', '', b'0', '2023-08-05 01:52:49', '2023-07-19 22:05:36');
INSERT INTO `route_meta` VALUES (25, '性能测试', 'el-icon-s-unfold', b'0', b'0', b'1', '', b'0', '2023-07-19 22:07:23', '2023-07-19 22:07:23');
INSERT INTO `route_meta` VALUES (26, '性能测试 - 敬请期待', NULL, b'0', b'0', b'1', '', b'0', '2023-07-19 22:11:30', '2023-07-19 22:11:30');
INSERT INTO `route_meta` VALUES (27, '工具', 'wrench', b'0', b'0', b'1', '', b'0', '2023-07-19 22:14:38', '2023-07-19 22:14:38');
INSERT INTO `route_meta` VALUES (28, 'OCR 文字识别', NULL, b'0', b'0', b'1', '', b'0', '2023-07-19 22:23:35', '2023-07-19 22:23:35');
INSERT INTO `route_meta` VALUES (29, '文件上传', NULL, b'0', b'0', b'1', '', b'0', '2023-07-19 22:23:46', '2023-07-19 22:23:46');
INSERT INTO `route_meta` VALUES (30, '项目管理', 'el-icon-set-up', b'0', b'0', b'1', '', b'0', '2023-07-19 22:27:10', '2023-07-19 22:27:10');
INSERT INTO `route_meta` VALUES (31, '成员管理', NULL, b'0', b'0', b'1', '', b'0', '2023-07-19 22:27:23', '2023-07-19 22:27:23');
INSERT INTO `route_meta` VALUES (32, '项目设置', NULL, b'0', b'0', b'1', '', b'0', '2023-07-19 22:27:34', '2023-07-19 22:27:34');
INSERT INTO `route_meta` VALUES (33, '系统管理', 'el-icon-s-tools', b'0', b'0', b'1', '', b'0', '2023-07-19 22:31:03', '2023-07-19 22:31:03');
INSERT INTO `route_meta` VALUES (34, '用户管理', NULL, b'0', b'0', b'1', '', b'0', '2023-07-19 22:32:28', '2023-07-19 22:32:28');
INSERT INTO `route_meta` VALUES (35, '编辑用户', NULL, b'0', b'0', b'1', '/manage/users', b'0', '2023-07-19 22:34:02', '2023-07-19 22:34:02');
INSERT INTO `route_meta` VALUES (36, '公司管理', NULL, b'0', b'0', b'1', '', b'0', '2023-07-19 22:35:18', '2023-07-19 22:35:18');
INSERT INTO `route_meta` VALUES (37, '项目管理', NULL, b'0', b'0', b'1', '', b'0', '2023-07-19 22:37:29', '2023-07-19 22:37:29');
INSERT INTO `route_meta` VALUES (38, '资源管理', NULL, b'0', b'0', b'1', '', b'0', '2023-07-20 00:18:03', '2023-07-20 00:18:03');
INSERT INTO `route_meta` VALUES (39, '菜单管理', NULL, b'0', b'0', b'1', '', b'0', '2023-07-20 00:20:26', '2023-07-20 00:20:26');
INSERT INTO `route_meta` VALUES (40, '角色管理', NULL, b'0', b'0', b'1', '', b'0', '2023-07-20 00:20:39', '2023-07-20 00:20:39');
INSERT INTO `route_meta` VALUES (41, '权限管理', NULL, b'0', b'0', b'1', '', b'0', '2023-07-28 20:27:24', '2023-07-28 20:27:24');
INSERT INTO `route_meta` VALUES (42, '定时任务日志', '', b'0', b'0', b'1', '', b'0', '2023-08-05 01:53:51', '2023-08-05 01:53:51');
INSERT INTO `route_meta` VALUES (43, '系统定时任务', '', b'0', b'0', b'1', '', b'0', '2023-08-15 12:44:15', '2023-08-15 12:44:15');
INSERT INTO `route_meta` VALUES (44, '系统定时任务日志', '', b'0', b'0', b'1', '', b'0', '2023-08-15 12:47:58', '2023-08-15 12:44:34');
COMMIT;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `name` varchar(50) NOT NULL COMMENT '姓名',
  `avatar` varchar(255) DEFAULT NULL COMMENT '头像',
  `account` varchar(50) NOT NULL COMMENT '账号',
  `code` varchar(50) DEFAULT '' COMMENT '密码',
  `is_locked` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否锁定',
  `company_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '用户当前选中公司ID',
  `project_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '用户当前选中项目ID',
  `is_delete` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否删除',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8 COMMENT='用户表';

-- ----------------------------
-- Records of user
-- ----------------------------
BEGIN;
INSERT INTO `user` VALUES (1, 'admin', 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif', 'admin', '8caf1cb2d641c4be35f22e4ae14205a6', b'0', 2, 19, b'0', '2023-08-26 00:29:15', '2022-05-14 17:21:32');
COMMIT;

-- ----------------------------
-- Table structure for user_company_project
-- ----------------------------
DROP TABLE IF EXISTS `user_company_project`;
CREATE TABLE `user_company_project` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `user_id` bigint(20) NOT NULL COMMENT '用户 ID',
  `company_id` bigint(20) NOT NULL COMMENT '所属公司 id',
  `project_id` bigint(20) NOT NULL COMMENT '所属项目id',
  `is_delete` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否删除',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=943 DEFAULT CHARSET=utf8 COMMENT='用户对应公司项目表';

-- ----------------------------
-- Records of user_company_project
-- ----------------------------
BEGIN;
INSERT INTO `user_company_project` VALUES (1, 1, 1, 1, b'0', '2023-07-21 10:29:06', '2023-07-21 10:29:06');
COMMIT;

-- ----------------------------
-- Table structure for user_role
-- ----------------------------
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `role_id` bigint(20) NOT NULL COMMENT '角色 id',
  `user_id` bigint(20) NOT NULL COMMENT '用户信息',
  `is_delete` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否删除',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=114 DEFAULT CHARSET=utf8 COMMENT='用户对应角色信息表';

-- ----------------------------
-- Records of user_role
-- ----------------------------
BEGIN;
INSERT INTO `user_role` VALUES (1, 1, 1, b'0', '2023-08-02 00:49:44', '2023-08-02 00:49:44');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;