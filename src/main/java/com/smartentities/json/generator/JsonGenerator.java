package com.smartentities.json.generator;

/**
 * 引入第三方开源代码 JSON Generator，根据 JSON-Schema 生成 json
 *
 * github 地址：https://github.com/jignesh-dhua/json-generator
 */
public class JsonGenerator {

	private GeneratorConfig generatorConfig;

		
	public JsonGenerator(GeneratorConfig generatorConfig) {
		this.generatorConfig = generatorConfig;
	}

	public String generate() {
		return GeneratorFactory.getGenerator(generatorConfig.schema).generate().toString();
	}
}