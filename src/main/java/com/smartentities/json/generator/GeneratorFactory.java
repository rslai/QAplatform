package com.smartentities.json.generator;

import com.bjy.qa.exception.MyException;
import com.smartentities.json.generator.generators.*;
import org.everit.json.schema.*;

import java.util.Collection;

public class GeneratorFactory {

	public static JsonValueGenerator<?> getGenerator(Schema schema) {

		JsonValueGenerator<?> jsonValueGenerator = null;

		if (schema instanceof StringSchema) {
			jsonValueGenerator = new StringGenerator(schema);
		} else if (schema instanceof NumberSchema) {
			jsonValueGenerator = new NumberGenerator(schema);
		} else if (schema instanceof BooleanSchema) {
			jsonValueGenerator = new BooleanGenerator(schema);
		} else if (schema instanceof ObjectSchema) {
			jsonValueGenerator = new ObjectGenerator(schema);
		} else if (schema instanceof ArraySchema) {
			jsonValueGenerator = new ArrayGenerator(schema);
		} else if (schema instanceof EnumSchema) {
			jsonValueGenerator = new EnumGenerator(schema);
		} else if (schema instanceof ReferenceSchema) {
			jsonValueGenerator = new ReferenceGenerator(schema);
		} else if (schema instanceof EmptySchema) {
			jsonValueGenerator = new EmptyGenerator(schema);
		} else if (schema instanceof NullSchema) {
			jsonValueGenerator = new NullGenerator(schema);
		} else if (schema instanceof CombinedSchema) {
			Collection<Schema>  subschemas = ((CombinedSchema) schema).getSubschemas();
			for (Schema subschema : subschemas) {
				return getGenerator(subschema);
			}
			throw new MyException("CombinedSchema 类型 schema 中没有 Subschemas。");
		} else {
			throw new MyException("不支持的 schema 类型 schemaTyp:" + schema.getClass().getName());
		}

		return jsonValueGenerator;
	}
}
