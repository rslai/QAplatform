package com.smartentities.json.generator.generators;

import org.everit.json.schema.NullSchema;
import org.everit.json.schema.Schema;

public class NullGenerator extends JsonValueGenerator<Object> {
	public NullGenerator(Schema schema) {
		super(schema);
	}

	@Override
	public Object generate() {
		Object defaultValue = ((NullSchema) schema).getUnprocessedProperties().get("default");
		if (defaultValue != null) {
			return defaultValue.toString();
		}

		return null;
	}
}
