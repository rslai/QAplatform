package com.smartentities.json.generator.generators;

import org.everit.json.schema.BooleanSchema;
import org.everit.json.schema.Schema;

public class BooleanGenerator extends JsonValueGenerator<Boolean> {

	public BooleanGenerator(Schema schema) {
		super(schema);
	}

	@Override
	public Boolean generate() {
		Object defaultValue = ((BooleanSchema) schema).getUnprocessedProperties().get("default");
		if (defaultValue != null) {
			return Boolean.valueOf(defaultValue.toString());
		}

		return false;
	}
}