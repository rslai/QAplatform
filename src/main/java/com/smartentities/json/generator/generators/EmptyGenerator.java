package com.smartentities.json.generator.generators;

import org.everit.json.schema.EmptySchema;
import org.everit.json.schema.Schema;

public class EmptyGenerator extends JsonValueGenerator<String> {
	public EmptyGenerator(Schema schema) {
		super(schema);
	}

	@Override
	public String generate() {
		Object defaultValue = ((EmptySchema) schema).getUnprocessedProperties().get("default");
		if (defaultValue != null) {
			return defaultValue.toString();
		}

		return "";
	}
}