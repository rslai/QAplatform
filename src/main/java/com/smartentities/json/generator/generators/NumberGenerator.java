package com.smartentities.json.generator.generators;

import org.everit.json.schema.NumberSchema;
import org.everit.json.schema.Schema;

public class NumberGenerator extends JsonValueGenerator<Number> {

	public NumberGenerator(Schema schema) {
		super(schema);
	}

	@Override
	public Number generate() {
		Object defaultValue = ((NumberSchema) schema).getUnprocessedProperties().get("default");
		if (defaultValue != null) {
			try {
				if (defaultValue.toString().indexOf(".") > 0) {
					return Double.valueOf(defaultValue.toString());
				} else {
					return Long.valueOf(defaultValue.toString());
				}
			} catch (Exception e) {
			}
		}

		return 1;
	}
}