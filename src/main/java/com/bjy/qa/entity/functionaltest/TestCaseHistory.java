package com.bjy.qa.entity.functionaltest;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gitee.sunchenbin.mybatis.actable.annotation.Column;
import com.gitee.sunchenbin.mybatis.actable.annotation.Index;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@TableName("ft_test_case_history")
@TableComment("测试用例历史记录表")
@ApiModel("测试用例历史记录 Entity 模型")
public class TestCaseHistory extends TestCase {
    @Column(isNull = false, comment = "测试用例 ID")
    @Index(value = "IDX_CASE_ID")
    @ApiModelProperty(value = "测试用例 ID", required = true, example = "1")
    Long testCaseId;

    @Column(length = 50, isNull = false, comment = "用例历史记录设计人")
    @ApiModelProperty(value = "用例历史记录设计人", example = "rslai")
    String historyDesigner;

    @TableField(exist = false)
    List<StepHistory> stepHistories;
}
