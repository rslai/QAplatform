package com.bjy.qa.entity.functionaltest;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("手工执行测试计划提交数据DTO")
public class TestExecuteDataDTO {
    @ApiModelProperty(value = "测试计划 id", example = "1")
    Long suiteId;

    @ApiModelProperty(value = "测试用例 id", required = true, example = "1")
    Long cid;

    @ApiModelProperty(value = "测试用例 状态", required = true, example = "1")
    int caseStatus;
}
