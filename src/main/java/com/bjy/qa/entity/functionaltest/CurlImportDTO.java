package com.bjy.qa.entity.functionaltest;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("cURL Import DTO 模型")
public class CurlImportDTO {
    @ApiModelProperty(value = "是否忽略通用 header", example = "true")
    boolean ignoreGenericHeader;

    @ApiModelProperty(value = "curl", example = "{\"method\":\"GET\"......}")
    CurlDTO curl;
}
