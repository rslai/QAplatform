package com.bjy.qa.entity.functionaltest;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("api 请求响应结果 DTO 模型")
public class ApiLogDTO {
    @ApiModelProperty(value = "是否查询到 api 请求响应结果，true：查询到，false：未查询到")
    boolean result = false;

    @ApiModelProperty(value = "请求内容")
    ApiLog request = new ApiLog();

    @ApiModelProperty(value = "响应内容")
    ApiLog response = new ApiLog();
}
