package com.bjy.qa.entity.functionaltest;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.bjy.qa.entity.BaseEntity;
import com.bjy.qa.enumtype.CatalogType;
import com.bjy.qa.util.EnumUtil;
import com.gitee.sunchenbin.mybatis.actable.annotation.Column;
import com.gitee.sunchenbin.mybatis.actable.annotation.Index;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import com.gitee.sunchenbin.mybatis.actable.constants.MySqlTypeConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@TableName("ft_api")
@TableComment("api 定义表")
@ApiModel("api 定义的 Entity 模型")
public class Api extends BaseEntity<Api> {
    @Column(isNull = false, comment = "所属项目id")
    @Index(value = "IDX_PROJECT_ID")
    @ApiModelProperty(value = "项目信息id", example = "1")
    Long projectId;

    @Column(isNull = false, comment = "所属分类 ID")
    @NotNull(message = "catalogId: 所属分类 ID 必填")
    @ApiModelProperty(value = "所属分类 ID", required = true, example = "0")
    Long catalogId;

    @Column(isNull = false, type = MySqlTypeConstant.INT, comment = "分类类型")
    @Index(value = "IDX_TYPE")
    @NotNull(message = "type: 分类类型必填")
    @ApiModelProperty(value = "分类类型", required = true, example = "15")
    CatalogType type;

    @Column(isNull = false, comment = "api 定义名称")
    @NotNull(message = "name: 名称必填")
    @ApiModelProperty(value = "api 定义名称", example = "getVT")
    String name;

    @TableField(exist = false)
    @ApiModelProperty(value = "api 定义 URL", example = "http://172.20.1.10:80/getVT")
    String url;

    @Column(length = 100, comment = "api 定义 URL 中的 host")
    @ApiModelProperty(value = "api 定义 URL 中的 host", example = "http://172.20.1.10:80")
    String host;

    @Column(comment = "api 定义 URL 中的 URI")
    @ApiModelProperty(value = "api 定义 URL 中的 URI", example = "/getVT")
    String uri;

    @Column(length = 20, comment = "api 方法，http请求时为 POST/GET/PUT 等")
    @ApiModelProperty(value = "api 方法，http请求时为 POST/GET/PUT 等", example = "POST/GET")
    String method;

    @Column(comment = "扩展信息，http请求时为 JSON/FORM 等。WebSocket时为 {'send':{'interval':0,//间隔时间},'receive':{'ending':1,//结尾标记'lose':false//收到后是否即刻丢弃}}")
    @ApiModelProperty(value = "扩展信息，http请求时为 JSON/FORM 等", example = "JSON")
    String extra1;

    @Column(length = 50, isNull = false, comment = "api 设计人")
    @ApiModelProperty(value = "api 设计人", example = "rslai")
    String designer;

    @Column(isNull = false, comment = "排序")
    @ApiModelProperty(value = "排序", example = "0")
    Integer sort = 9999;

    @TableField(exist = false)
    List<ApiParam> apiParams;

    public void setType(int type) {
        this.type = EnumUtil.valueOf(CatalogType.class, type);
    }
}
