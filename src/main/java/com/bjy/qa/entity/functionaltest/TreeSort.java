package com.bjy.qa.entity.functionaltest;

import com.bjy.qa.enumtype.CatalogType;
import com.bjy.qa.enumtype.SortType;
import com.bjy.qa.util.EnumUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@ApiModel("Tree 排序 DTO 模型")
public class TreeSort {
    @NotNull(message = "project: 项目ID必填")
    @ApiModelProperty(value = "项目id", required = true, example = "1")
    Long projectId;

    @NotNull(message = "type: 分类类型必填")
    @ApiModelProperty(value = "分类类型", required = true, example = "1")
    CatalogType type;

    @NotNull(message = "source: 源对象必填")
    @ApiModelProperty(value = "源对象", required = true, example = "{ \"id\": 76, \"key\": \"e-76\", \"projectId\": 1, \"parentId\": 0, \"isCatalog\": false, \"name\": \"测试用例反反复复\" }")
    Tree source;

    @NotNull(message = "target: 目标对象必填")
    @ApiModelProperty(value = "目标对象", required = true, example = "{ \"id\": 103, \"key\": \"e-103\", \"projectId\": 1, \"parentId\": 0, \"isCatalog\": false, \"name\": \"测试用例\" }")
    Tree target;

    @NotNull(message = "sortType: 排序类型必填")
    @ApiModelProperty(value = "排序类型，1：BEFORE 目标节点前，2：AFTER 目标节点后，3： INNER 目标节点内-子节点", required = true, example = "INNER")
    SortType sortType;

    public void setType(int type) {
        this.type = EnumUtil.valueOf(CatalogType.class, type);
    }

    public void setSortType(int sortType) {
        this.sortType = EnumUtil.valueOf(SortType.class, sortType);
    }

}
