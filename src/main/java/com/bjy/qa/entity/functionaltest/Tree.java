package com.bjy.qa.entity.functionaltest;

import com.alibaba.fastjson.annotation.JSONField;
import com.bjy.qa.enumtype.EditStatus;
import com.bjy.qa.enumtype.Priority;
import com.bjy.qa.util.EnumUtil;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel("Tree DTO 模型")
public class Tree {
    @ApiModelProperty(value = "自增ID", required = true, example = "1")
    protected Long id;

    @ApiModelProperty(value = "tree中的唯一标识（key）", required = true, example = "c-1")
    private String key;

    @ApiModelProperty(value = "所属项目的id", required = true, example = "1")
    Integer projectId;

    @NotBlank
    @ApiModelProperty(value = "父分类的id", required = true, example = "1")
    Long parentId;

    @NotBlank
    @ApiModelProperty(value = "是否是分类", required = true, example = "true")
    Boolean isCatalog;

    @NotBlank
    @ApiModelProperty(value = "分类名称", required = true, example = "一级目录")
    String name;

    @JsonIgnore
    @JSONField(serialize = false)
    @ApiModelProperty(value = "排序", example = "100")
    Integer sort = 9999;

    @ApiModelProperty(value = "图标", example = "catalog")
    String icon;

    @ApiModelProperty(value = "优先级", example = "0")
    Priority priority;

    @ApiModelProperty(value = "用例编写状态", example = "1")
    EditStatus editStatus;

    public void setPriority(int priority) {
        this.priority = EnumUtil.valueOf(Priority.class, priority);
    }

    public void setEditStatus(int editStatus) {
        this.editStatus = EnumUtil.valueOf(EditStatus.class, editStatus);
    }
}
