package com.bjy.qa.entity.functionaltest;

import com.bjy.qa.enumtype.FileType;
import com.bjy.qa.util.EnumUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@ApiModel("导出 DTO 模型")
public class ExportDTO {
    @ApiModelProperty(value = "所属项目的 id", required = true, example = "1")
    Long projectId;

    @ApiModelProperty(value = "导出的文件类型", required = true, example = "1")
    @NotNull(message = "fileType: 文件类型 必填")
    FileType fileType;

    @ApiModelProperty(value = "需要导出的树节点", required = true, example = "[{\"id\":5,\"key\":\"c-5\",\"projectId\":1,\"parentId\":10,\"isCatalog\":true,\"name\":\"xxx\"}]")
    @NotNull(message = "tree: 需要导出的树节点 必填")
    List<FullTree> tree;

    public void setFileType(int type) {
        this.fileType = EnumUtil.valueOf(FileType.class, type);
    }
}
