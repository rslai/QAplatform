package com.bjy.qa.entity.functionaltest;

import com.bjy.qa.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@ApiModel("Api 调试请求 DTO 模型")
public class ApiDebugDTO extends BaseEntity<ApiDebugDTO> {
    @ApiModelProperty(value = "项目id", example = "1")
    @NotNull(message = "projectId: projectId 必填")
    Long projectId;

    @ApiModelProperty(value = "运行环境", example = "test")
    String env;

    @ApiModelProperty(value = "agent ID", example = "1")
    @NotNull(message = "agentId: agentId 必填")
    Long agentId;

    @ApiModelProperty(value = "api 定义", required = true, example = "{\"host\":}")
    Api api;
}
