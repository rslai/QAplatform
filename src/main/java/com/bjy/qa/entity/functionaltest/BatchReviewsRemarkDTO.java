package com.bjy.qa.entity.functionaltest;

import com.bjy.qa.entity.BaseEntity;
import com.bjy.qa.enumtype.EditStatus;
import com.bjy.qa.util.EnumUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel("批量用例评审备注 DTO")
public class BatchReviewsRemarkDTO extends BaseEntity<BatchReviewsRemarkDTO> {
    @ApiModelProperty(value = "评审计划 id", example = "1")
    Long reviewsId;

    @ApiModelProperty(value = "测试用例 id 列表", required = true, example = "[1, 2]")
    List<Long> testCaseIds;

    @ApiModelProperty(value = "测试用例 状态", required = true, example = "1")
    EditStatus caseStatus;

    @ApiModelProperty(value = "评审意见", example = "标题有问题：xxx")
    private String remark;

    public void setCaseStatus(int status) {
        this.caseStatus = EnumUtil.valueOf(EditStatus.class, status);
    }
}
