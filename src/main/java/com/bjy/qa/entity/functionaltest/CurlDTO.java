package com.bjy.qa.entity.functionaltest;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Map;

@Data
@ApiModel("cURL DTO 模型")
public class CurlDTO {
    @ApiModelProperty(value = "method", example = "GET")
    String method;

    @ApiModelProperty(value = "url", example = "https://www.baidu.com/dd?aa=1&bb=2")
    String url;

    @ApiModelProperty(value = "body", example = "aa=1&bb=2")
    String body;

    @ApiModelProperty(value = "header", example = "{\"Cache-Control\": \"max-age=0\", \"Connection\": \"keep-alive\"}")
    Map<String, Object> header;
}
