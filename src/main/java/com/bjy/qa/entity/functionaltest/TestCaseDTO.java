package com.bjy.qa.entity.functionaltest;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
@ApiModel("测试用例详情页")
public class TestCaseDTO {
    @ApiModelProperty(value = "测试用例名称", required = true, example = "登陆接口")
    String name;

    @ApiModelProperty(value = "测试结果 id", required = true, example = "1")
    Long rid;

    @ApiModelProperty(value = "测试用例 id", required = true, example = "1")
    Long cid;

    @ApiModelProperty(value = "DataProivder（数据驱动）", example = "[{\"A\":\"1\"},{\"A\":\"2\"},{}]")
    String dataProvider;

    @ApiModelProperty(value = "全局变量", example = "{'roomID':'12345'}")
    Map<String, String> gp;

    @TableField(exist = false)
    List<StepDTO> steps;

    @ApiModelProperty(value = "下发 debug 信令时作为唯一标识", example = "29002272-4659-4808-a804-08ce3388b136")
    String sessionId;
}
