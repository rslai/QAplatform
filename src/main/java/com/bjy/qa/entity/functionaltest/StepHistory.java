package com.bjy.qa.entity.functionaltest;

import com.baomidou.mybatisplus.annotation.TableName;
import com.gitee.sunchenbin.mybatis.actable.annotation.Column;
import com.gitee.sunchenbin.mybatis.actable.annotation.Index;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@TableName("ft_step_history")
@TableComment("测试用例步骤历史记录表")
@ApiModel("测试用例步骤历史记录 Entity 模型")
public class StepHistory extends Step {
    @Column(isNull = false, comment = "测试用例历史记录 ID")
    @Index(value = "IDX_CASE_HISTORY_ID")
    @ApiModelProperty(value = "测试用例历史记录 ID", required = true, example = "1")
    Long testCaseHistoryId;

    @Column(isNull = false, defaultValue = "0", comment = "step 历史记录表中的 父级id，一般父级都是条件步骤")
    @ApiModelProperty(value = "step 历史记录表中的 父级id，一般父级都是条件步骤", required = true, example = "0")
    Long historyParentId;

    @Column(isNull = false, comment = "测试用例步骤表 ID")
    @ApiModelProperty(value = "测试用例步骤表 ID", required = true, example = "1")
    Long stepId;
}
