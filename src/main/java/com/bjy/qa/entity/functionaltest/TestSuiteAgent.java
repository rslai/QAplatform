package com.bjy.qa.entity.functionaltest;

import com.baomidou.mybatisplus.annotation.TableName;
import com.bjy.qa.entity.BaseEntity;
import com.gitee.sunchenbin.mybatis.actable.annotation.Column;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@TableName("ft_test_suite_agents")
@TableComment("测试套件测试设备关系表")
@ApiModel("测试套件测试设备 Entity 模型")
public class TestSuiteAgent extends BaseEntity<TestSuiteAgent> {

    @Column(isNull = false, comment = "测试套件id")
    @ApiModelProperty(value = "套件id", example = "1")
    private Long testSuitesId;

    @Column(isNull = false, comment = "测试设备id")
    @ApiModelProperty(value = "agentId", example = "1")
    private Long agentId;

}
