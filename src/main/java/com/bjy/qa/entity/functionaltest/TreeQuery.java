package com.bjy.qa.entity.functionaltest;

import com.bjy.qa.enumtype.CatalogType;
import com.bjy.qa.util.EnumUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@ApiModel("Tree 查询 DTO 模型")
public class TreeQuery {
    @ApiModelProperty(value = "所属项目的id", required = true, example = "1")
    Long projectId;

    @ApiModelProperty(value = "父分类 ID", required = true, example = "0")
    Long parentId;

    @NotNull(message = "type: 分类类型必填")
    @ApiModelProperty(value = "分类类型", required = true, example = "1")
    CatalogType type;

    @ApiModelProperty(value = "用例类型", required = true, example = "1")
    CatalogType caseType;

    @ApiModelProperty(value = "测试套件 ID", required = true, example = "1")
    Long testSuitesId;

    @ApiModelProperty(value = "测试结果 ID", required = true, example = "1")
    Long testResultId;

    @ApiModelProperty(value = "评审计划 ID", required = true, example = "1")
    Long reviewsId;

    public void setType(int type) {
        this.type = EnumUtil.valueOf(CatalogType.class, type);
    }

    public void setCaseType(int caseType) {
        this.caseType = EnumUtil.valueOf(CatalogType.class, caseType);
    }
}
