package com.bjy.qa.entity.functionaltest;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.List;

@Data
@ApiModel("全局参数定义的 DTO 模型")
public class GlobalParamDTO {
    List<EnvParam> envParams; // 环境参数

    List<GlobalParam> globalParams; // 全局参数
}
