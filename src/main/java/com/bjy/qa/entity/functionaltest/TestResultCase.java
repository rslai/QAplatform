package com.bjy.qa.entity.functionaltest;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.bjy.qa.entity.BaseEntity;
import com.bjy.qa.enumtype.RunStatus;
import com.bjy.qa.util.EnumUtil;
import com.gitee.sunchenbin.mybatis.actable.annotation.Column;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import com.gitee.sunchenbin.mybatis.actable.constants.MySqlTypeConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;


@Data
@TableName("ft_test_result_case")
@TableComment("测试用例结果表")
@ApiModel("测试用例结果 Entity 模型")
public class TestResultCase extends BaseEntity<TestResultCase> {

    @Column(isNull = false, comment = "测试用例id")
    @ApiModelProperty(value = "测试用例id", example = "1")
    Long cid;

    @Column(isNull = false, type = MySqlTypeConstant.INT,comment = "状态")
    @ApiModelProperty(value = "状态", example = "1")
    RunStatus status;

    @Column(isNull = false, comment = "测试结果id")
    @ApiModelProperty(value = "测试结果id", example = "1")
    Long rid;

    @Column(isNull = false, comment = "运行设备")
    @ApiModelProperty(value = "运行设备", example = "1")
    Long agent;

    @TableField(exist = false)
    @ApiModelProperty(value = "测试用例名称", example = "xxx测试用例")
    String cidName;

    @TableField(exist = false)
    @ApiModelProperty(value = "运行设备名称", example = "武汉 agent")
    String agentName;

    @TableField(exist = false)
    @ApiModelProperty(value = "测试结果详情 list", example = "[{......}]")
    List<TestResultStep> testResultSteps;

    public void setStatus(int status) {
        this.status = EnumUtil.valueOf(RunStatus.class, status);
    }

}
