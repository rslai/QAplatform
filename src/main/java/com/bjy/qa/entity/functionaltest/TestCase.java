package com.bjy.qa.entity.functionaltest;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.bjy.qa.entity.BaseEntity;
import com.bjy.qa.enumtype.CatalogType;
import com.bjy.qa.enumtype.EditStatus;
import com.bjy.qa.enumtype.Priority;
import com.bjy.qa.util.EnumUtil;
import com.gitee.sunchenbin.mybatis.actable.annotation.Column;
import com.gitee.sunchenbin.mybatis.actable.annotation.Index;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import com.gitee.sunchenbin.mybatis.actable.constants.MySqlTypeConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@TableName("ft_test_case")
@TableComment("测试用例表")
@ApiModel("测试用例 Entity 模型")
public class TestCase extends BaseEntity<TestCase> {
    @Column(isNull = false, comment = "所属项目id")
    @Index(value = "IDX_PROJECT_ID")
    @ApiModelProperty(value = "项目信息id", example = "1")
    Long projectId;

    @Column(isNull = false, comment = "所属分类 id")
    @NotNull(message = "catalogId: 所属分类 ID 必填")
    @ApiModelProperty(value = "所属分类 ID", required = true, example = "0")
    Long catalogId;

    @Column(isNull = false, type = MySqlTypeConstant.INT, comment = "分类类型")
    @NotNull(message = "type: 分类类型必填")
    @ApiModelProperty(value = "分类类型", required = true, example = "15")
    CatalogType type;

    @Column(isNull = false, type = MySqlTypeConstant.INT, comment = "用例类型")
    @NotNull(message = "type: 用例类型必填")
    @ApiModelProperty(value = "用例类型", required = true, example = "1")
    CatalogType caseType;

    @Column(isNull = false, type = MySqlTypeConstant.INT, comment = "优先级")
    @NotNull(message = "priority: 优先级 必填")
    @ApiModelProperty(value = "优先级", example = "0")
    Priority priority;

    @Column(isNull = false, type = MySqlTypeConstant.INT, comment = "用例编写状态")
    @NotNull(message = "editStatus: 用例编写状态 必填")
    @ApiModelProperty(value = "用例编写状态", example = "1")
    EditStatus editStatus;

    @Column(length = 50, comment = "用例编号")
    @ApiModelProperty(value = "用例编号", example = "ADMIN-TCKS-IS-001")
    String number;

    @Column(length = 150, isNull = false, comment = "用例名称")
    @NotNull(message = "name: 用例名称必填")
    @ApiModelProperty(value = "用例名称", example = "测试 getVT 接口")
    String name;

    @Column(length = 500, comment = "前置（手工测试中是前提条件，自动化测试中是前置 case）")
    @TableField("`before`")
    @ApiModelProperty(value = "前置（手工测试中是前提条件，自动化测试中是前置 case）", example = "正常测试用例")
    String before;

    @Column(length = 500, comment = "后置（自动化测试中是后置 case")
    @ApiModelProperty(value = "后置（自动化测试中是后置 case", example = "正常测试用例")
    String after;

    @Column(type = MySqlTypeConstant.LONGTEXT, comment = "DataProivder（数据驱动）")
    @ApiModelProperty(value = "DataProivder（数据驱动）", example = "[{\"A\":\"1\"},{\"A\":\"2\"},{}]")
    String dataProvider;

    @Column(length = 50, isNull = false, comment = "用例设计人")
    @ApiModelProperty(value = "用例设计人", example = "rslai")
    String designer;

    @Column(isNull = false, comment = "排序")
    @ApiModelProperty(value = "排序", example = "100")
    Integer sort = 9999;

    @TableField(exist = false)
    List<Step> steps;

    public void setType(int type) {
        this.type = EnumUtil.valueOf(CatalogType.class, type);
    }

    public void setCaseType(int caseType) {
        this.caseType = EnumUtil.valueOf(CatalogType.class, caseType);
    }

    public void setPriority(int priority) {
        this.priority = EnumUtil.valueOf(Priority.class, priority);
    }

    public void setEditStatus(int editStatus) {
        this.editStatus = EnumUtil.valueOf(EditStatus.class, editStatus);
    }
}
