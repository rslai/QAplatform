package com.bjy.qa.entity.functionaltest;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.bjy.qa.entity.BaseEntity;
import com.bjy.qa.enumtype.CatalogType;
import com.bjy.qa.util.EnumUtil;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gitee.sunchenbin.mybatis.actable.annotation.Column;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import com.gitee.sunchenbin.mybatis.actable.constants.MySqlTypeConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@TableName("ft_catalog")
@TableComment("分类信息表")
@ApiModel("分类信息 Entity 模型")
public class Catalog extends BaseEntity<Catalog> {
    @Column(isNull = false, comment = "所属项目id")
    @ApiModelProperty(value = "所属项目的id", required = true, example = "1")
    Long projectId;

    @Column(isNull = false, comment = "父分类 ID")
    @NotNull(groups = CatalogAddGroup.class, message = "parentId: 父分类 ID 必填") // 创建分类校验
    @NotNull(groups = CatalogUpdateGroup.class, message = "parentId: 父分类 ID 必填") // 修改分类校验
    @ApiModelProperty(value = "父分类 ID", required = true, example = "0")
    Long parentId;

    @Column(isNull = false, type = MySqlTypeConstant.INT, comment = "分类类型")
    @NotNull(groups = CatalogAddGroup.class, message = "type: 分类类型必填") // 创建分类校验
    @NotNull(groups = CatalogUpdateGroup.class, message = "type: 分类类型必填") // 修改分类校验
    @ApiModelProperty(value = "分类类型", required = true, example = "1")
    CatalogType type;

    @Column(isNull = false, length = 100, comment = "分类名称")
    @NotBlank(groups = CatalogAddGroup.class, message = "name: 分类名称必填") // 创建分类校验
    @NotBlank(groups = CatalogUpdateGroup.class, message = "name: 分类名称必填") // 修改分类校验
    @ApiModelProperty(value = "分类名称", required = true, example = "一级目录")
    String name;

    @Column(isNull = false, comment = "排序")
    @ApiModelProperty(value = "排序", example = "100")
    Integer sort = 9999;

    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    List<Catalog> catalogs;

    public void setType(int type) {
        this.type = EnumUtil.valueOf(CatalogType.class, type);
    }

    public static interface CatalogAddGroup {
    }

    public static interface CatalogUpdateGroup {
    }
}
