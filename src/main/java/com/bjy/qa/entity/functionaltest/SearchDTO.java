package com.bjy.qa.entity.functionaltest;

import com.bjy.qa.enumtype.CatalogType;
import com.bjy.qa.util.EnumUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@ApiModel("搜索 DTO 模型")
public class SearchDTO {
    @NotNull(message = "projectId: 项目信息id必填")
    @ApiModelProperty(value = "项目信息id", example = "1")
    Long projectId;

    @NotNull(message = "type: 分类类型必填")
    @ApiModelProperty(value = "分类类型", required = true, example = "15")
    CatalogType type;

    @NotEmpty(message = "findStr: 搜索内容必填")
    @ApiModelProperty(value = "搜索内容", example = "新增")
    String findStr;

    public void setType(int type) {
        this.type = EnumUtil.valueOf(CatalogType.class, type);
    }

}
