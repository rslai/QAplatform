package com.bjy.qa.entity.functionaltest;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.bjy.qa.entity.BaseEntity;
import com.bjy.qa.enumtype.CatalogType;
import com.bjy.qa.enumtype.RunStatus;
import com.bjy.qa.util.EnumUtil;
import com.gitee.sunchenbin.mybatis.actable.annotation.Column;
import com.gitee.sunchenbin.mybatis.actable.annotation.Index;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import com.gitee.sunchenbin.mybatis.actable.constants.MySqlTypeConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@TableName("ft_test_result_step")
@TableComment("测试结果详情表")
@ApiModel("测试结果详情 Entity 模型")
public class TestResultStep extends BaseEntity<TestResultStep> {
    @TableField(exist = false)
    @ApiModelProperty(value = "分类类型 - 上报日志类型", example = "1")
    CatalogType ctype;

    @Column(isNull = false, comment = "测试用例id")
    @ApiModelProperty(value = "测试用例id", example = "1")
    Long cid;

    @Column(isNull = false, length=10, comment = "迭代次数")
    @ApiModelProperty(value = "迭代次数", example = "1/3")
    String ic;

    @Column(isNull = false, comment = "测试结果id")
    @ApiModelProperty(value = "测试结果id", example = "1")
    @Index(columns = "rid, cid")
    Long rid;

    @Column(length=100, comment = "描述")
    @TableField("`desc`")
    @ApiModelProperty(value = "描述", example = "执行成功！")
    String desc;

    @Column(isNull = false, length=100, comment = "类型(等待、休眠)")
    @ApiModelProperty(value = "类型(等待、休眠)", example = "等待")
    String type;

    @Column(isNull = false, type = MySqlTypeConstant.LONGTEXT, comment = "日志信息")
    @ApiModelProperty(value = "日志信息", example = "进入房间")
    String log;

    @Column(isNull = false, type = MySqlTypeConstant.INT, comment = "状态")
    @ApiModelProperty(value = "状态", example = "1")
    RunStatus status;

    @Column(isNull = false, length=10, comment = "msg (step、status)")
    @ApiModelProperty(value = "msg (step、status)", example = "status")
    String msg;

    public void setStatus(int status) {
        this.status = EnumUtil.valueOf(RunStatus.class, status);
    }

    public void setCtype(int type) {
        this.ctype = EnumUtil.valueOf(CatalogType.class, type);
    }

}
