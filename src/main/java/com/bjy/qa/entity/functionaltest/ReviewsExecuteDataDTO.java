package com.bjy.qa.entity.functionaltest;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("用例评审提交数据DTO")
public class ReviewsExecuteDataDTO {
    @ApiModelProperty(value = "评审计划 id", example = "1")
    Long reviewsId;

    @ApiModelProperty(value = "测试用例 id", required = true, example = "1")
    Long testCaseId;

    @ApiModelProperty(value = "测试用例 状态", required = true, example = "1")
    int caseStatus;
}
