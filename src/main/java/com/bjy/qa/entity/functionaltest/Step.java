package com.bjy.qa.entity.functionaltest;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.bjy.qa.entity.BaseEntity;
import com.bjy.qa.enumtype.CatalogType;
import com.bjy.qa.enumtype.ConditionType;
import com.bjy.qa.enumtype.ErrorHandlingType;
import com.bjy.qa.util.EnumUtil;
import com.gitee.sunchenbin.mybatis.actable.annotation.Column;
import com.gitee.sunchenbin.mybatis.actable.annotation.Index;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import com.gitee.sunchenbin.mybatis.actable.constants.MySqlTypeConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Data
@TableName("ft_step")
@TableComment("测试用例步骤表")
@ApiModel("测试用例步骤 Entity 模型")
public class Step extends BaseEntity<Step> {
    @Column(isNull = false, comment = "所属项目id")
    @Index(value = "IDX_PROJECT_ID")
    @ApiModelProperty(value = "项目信息id", example = "1")
    Long projectId;

    @Column(isNull = false, comment = "测试用例 ID")
    @Index(value = "IDX_CASE_ID")
    @NotNull(message = "testCaseId: 测试用例 ID 必填")
    @ApiModelProperty(value = "测试用例 ID", required = true, example = "1")
    Long testCaseId;

    @Column(isNull = false, defaultValue = "0", comment = "父级id，一般父级都是条件步骤")
    @ApiModelProperty(value = "父级id，一般父级都是条件步骤", required = true, example = "0")
    Long parentId;

    @Column(length=100, comment = "步骤描述")
    @TableField("`desc`")
    @ApiModelProperty(value = "步骤描述", required = true, example = "请求 getVT")
    String desc;

    @Column(comment = "API ID")
    @Index(value = "IDX_API_ID")
    @ApiModelProperty(value = "API ID", example = "1")
    Long apiId;

    @TableField(exist = false)
    @ApiModelProperty(value = "一对一的 Api")
    Api api;

    @Column(type = MySqlTypeConstant.INT, defaultValue = "0", comment = "步骤类型")
    @ApiModelProperty(value = "步骤类型", required = true, example = "3")
    CatalogType stepType;

    @Column(type = MySqlTypeConstant.INT, defaultValue = "0", comment = "异常处理类型")
    @ApiModelProperty(value = "异常处理类型", example = "1")
    ErrorHandlingType errorHandlingType;

    @Column(type = MySqlTypeConstant.INT, defaultValue = "0", comment = "条件类型")
    @ApiModelProperty(value = "条件类型", example = "0")
    ConditionType conditionType;

    @Column(isNull = false, comment = "排序")
    @ApiModelProperty(value = "排序", example = "0")
    Integer sort = 9999;

    @Column(type = MySqlTypeConstant.LONGTEXT, comment = "扩展信息（手工测试：操作步骤。自动化测试：head）")
    @ApiModelProperty(value = "扩展信息（手工测试：操作步骤。自动化测试：head）", example = "1、步骤一\n2、步骤二")
    String extra1;

    @Column(type = MySqlTypeConstant.LONGTEXT, comment = "扩展信息（手工测试：预期结果。自动化测试：body）")
    @ApiModelProperty(value = "扩展信息（手工测试：预期结果。自动化测试：body）", example = "\n正常进入房间")
    String extra2;

    @Column(type = MySqlTypeConstant.LONGTEXT, comment = "扩展信息（自动化测试：http url 参数）")
    @ApiModelProperty(value = "扩展信息（自动化测试：http url 参数）", example = "")
    String extra3;

    @Column(type = MySqlTypeConstant.LONGTEXT, comment = "扩展信息（自动化测试：cookie。WebSocket 中保存通道）")
    @ApiModelProperty(value = "扩展信息（自动化测试：cookie。WebSocket 中保存通道）", example = "")
    String extra4;

    @Column(type = MySqlTypeConstant.LONGTEXT, comment = "扩展信息（自动化测试：assert）")
    @ApiModelProperty(value = "扩展信息（自动化测试：assert）", example = "")
    String extra5;

    @TableField(exist = false)
    @ApiModelProperty(value = "包含的子步骤")
    List<Step> childSteps = new ArrayList<>();

    public void setStepType(int stepType) {
        this.stepType = EnumUtil.valueOf(CatalogType.class, stepType);
    }

    public void setErrorHandlingType(int errorHandlingType) {
        this.errorHandlingType = EnumUtil.valueOf(ErrorHandlingType.class, errorHandlingType);
    }

    public void setConditionType(int conditionType) {
        this.conditionType = EnumUtil.valueOf(ConditionType.class, conditionType);
    }
}
