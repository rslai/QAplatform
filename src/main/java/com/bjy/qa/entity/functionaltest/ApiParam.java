package com.bjy.qa.entity.functionaltest;

import com.baomidou.mybatisplus.annotation.TableName;
import com.bjy.qa.entity.BaseEntity;
import com.bjy.qa.enumtype.ParamKind;
import com.bjy.qa.enumtype.ParamType;
import com.bjy.qa.util.EnumUtil;
import com.gitee.sunchenbin.mybatis.actable.annotation.Column;
import com.gitee.sunchenbin.mybatis.actable.annotation.Index;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import com.gitee.sunchenbin.mybatis.actable.constants.MySqlTypeConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@TableName("ft_api_param")
@TableComment("api 定义中的参数表")
@ApiModel("api 定义中的参数 Entity 模型")
public class ApiParam extends BaseEntity<ApiParam> {
    @Column(isNull = false, comment = "api 定义 ID")
    @Index(value = "IDX_API_ID")
    @ApiModelProperty(value = "api 定义 ID", example = "1")
    Long apiId;

    @Column(type = MySqlTypeConstant.INT, defaultValue = "0", comment = "参数类型")
    @ApiModelProperty(value = "参数类型", required = true, example = "3")
    ParamType paramType;

    @Column(type = MySqlTypeConstant.INT, defaultValue = "0", comment = "参数分类")
    @ApiModelProperty(value = "参数分类", required = true, example = "1")
    ParamKind kind;

    @Column(isNull = false, length = 100, comment = "参数名称")
    @ApiModelProperty(value = "参数名称", example = "北京-proxy-172.20.2.1")
    String name;

    @Column(comment = "是否必须")
    @ApiModelProperty(value = "是否必须", example = "true")
    boolean isRequired;

    @Column(type = MySqlTypeConstant.LONGTEXT, comment = "参数说明")
    @ApiModelProperty(value = "参数说明", example = "北京区的 agent 服务器")
    String des;

    @Column(type = MySqlTypeConstant.LONGTEXT, comment = "示例值")
    @ApiModelProperty(value = "示例值", example = "a")
    String example;

    @Column(isNull = false, comment = "排序")
    @ApiModelProperty(value = "排序", example = "100")
    Integer sort;

    public void setParamType(int paramType) {
        this.paramType = EnumUtil.valueOf(ParamType.class, paramType);
    }

    public void setKind(int kind) {
        this.kind = EnumUtil.valueOf(ParamKind.class, kind);
    }
}
