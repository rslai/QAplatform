package com.bjy.qa.entity.functionaltest;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gitee.sunchenbin.mybatis.actable.annotation.Column;
import com.gitee.sunchenbin.mybatis.actable.annotation.Index;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@TableName("ft_api_history")
@TableComment("api 定义历史记录表表")
@ApiModel("api 定义历史记录表的 Entity 模型")
public class ApiHistory extends Api {
    @Column(isNull = false, comment = "api 定义 ID")
    @Index(value = "IDX_API_ID")
    @ApiModelProperty(value = "api 定义 ID", required = true, example = "1")
    Long apiId;

    @Column(length = 50, isNull = false, comment = "api 定义历史记录设计人")
    @ApiModelProperty(value = "api 定义历史记录设计人", example = "rslai")
    String historyDesigner;

    @TableField(exist = false)
    List<ApiParamHistory> apiParamHistories;
}
