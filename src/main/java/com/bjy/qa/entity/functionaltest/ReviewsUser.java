package com.bjy.qa.entity.functionaltest;

import com.baomidou.mybatisplus.annotation.TableName;
import com.bjy.qa.entity.BaseEntity;
import com.gitee.sunchenbin.mybatis.actable.annotation.Column;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@TableName("ft_reviews_user")
@TableComment("用例评审计划对应用户表")
@ApiModel("用例评审计划对应用户表 Entity 模型")
public class ReviewsUser extends BaseEntity<ReviewsUser> {
    @Column(isNull = false, comment = "项目 ID")
    @ApiModelProperty(value = "项目 ID", example = "1")
    @NotNull(message = "projectId: 项目 ID 字段字段必填")
    private Long projectId;

    @Column(isNull = false, comment = "用例评审计划 ID")
    @ApiModelProperty(value = "用例评审计划 ID", example = "1")
    @NotNull(message = "reviewsId: 用例评审计划 ID 字段字段必填")
    private Long reviewsId;

    @Column(isNull = false, comment = "评审者 ID")
    @ApiModelProperty(value = "评审者 ID", example = "1")
    @NotNull(message = "reviewerUserId: 评审者 ID 字段字段必填")
    private Long reviewerUserId;
}
