package com.bjy.qa.entity.functionaltest;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.bjy.qa.entity.BaseEntity;
import com.bjy.qa.enumtype.CatalogType;
import com.bjy.qa.enumtype.TaskStatus;
import com.bjy.qa.util.EnumUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.gitee.sunchenbin.mybatis.actable.annotation.Column;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import com.gitee.sunchenbin.mybatis.actable.constants.MySqlTypeConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import java.util.Date;
import java.util.List;

@Data
@TableName("ft_test_suite")
@TableComment("测试套件表")
@ApiModel("测试套件 Entity 模型")
public class TestSuite extends BaseEntity<TestSuite> {
    @Column(isNull = false, length = 50, comment = "套件名称")
    @NotBlank(groups = TestSuiteGroup.class, message = "name: 套件名称字段必填")
    @ApiModelProperty(value = "套件名称", example = "iOS测试")
    String name;

    @Column(isNull = false, comment = "所属项目 ID")
    @ApiModelProperty(value = "所属项目id", example = "1")
    Long projectId;

    @Column(isNull = false, type = MySqlTypeConstant.INT, comment = "用例类型")
    @ApiModelProperty(value = "套件类型", example = "1")
    CatalogType caseType;

    @Column(isNull = false, type = MySqlTypeConstant.INT,comment = "状态")
    @ApiModelProperty(value = "状态", example = "RUNNING")
    TaskStatus status;

    @Column(comment = "运行环境")
    @ApiModelProperty(value = "运行环境", example = "env")
    String env;

    @Column(comment = "测试负责人")
    @ApiModelProperty(value = "测试负责人", example = "张三")
    String owner;

    @Column(comment = "测试完成后发送通知的机器人 ID")
    @ApiModelProperty(value = "测试完成后发送通知的机器人 ID", example = "1")
    Long robotId;

    @Column(type = MySqlTypeConstant.TIMESTAMP, comment = "开始时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSS") // set
    @ApiModelProperty(value = "开始时间", example = "2022-05-10 10:10:30.112")
    protected Date startAt;

    @Column(type = MySqlTypeConstant.TIMESTAMP, comment = "结束时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSS") // set
    @ApiModelProperty(value = "结束时间", example = "2022-05-10 17:10:30.112")
    protected Date endAt;

    @TableField(exist = false)
    @ApiModelProperty(value = "包含的测试用例")
    List<TestCase> testCases;

    @TableField(exist = false)
    @ApiModelProperty(value = "包含的测试设备")
    List<Agent> agents;

    public static interface TestSuiteGroup {
    }

    public void setStatus(int value) {
        this.status = EnumUtil.valueOf(TaskStatus.class, value);
    }

    public void setCaseType(int caseType) {
        this.caseType = EnumUtil.valueOf(CatalogType.class, caseType);
    }
}
