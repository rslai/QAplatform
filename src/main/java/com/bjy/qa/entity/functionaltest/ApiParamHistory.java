package com.bjy.qa.entity.functionaltest;

import com.baomidou.mybatisplus.annotation.TableName;
import com.gitee.sunchenbin.mybatis.actable.annotation.Column;
import com.gitee.sunchenbin.mybatis.actable.annotation.Index;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@TableName("ft_api_param_history")
@TableComment("api 定义中的参数历史记录表表")
@ApiModel("api 定义中的参数历史记录表 Entity 模型")
public class ApiParamHistory extends ApiParam {
    @Column(isNull = false, comment = "api 定义历史记录 ID")
    @Index(value = "IDX_API_HISTORY_ID")
    @ApiModelProperty(value = "api 定义历史记录 ID", required = true, example = "1")
    Long apiHistoryId;

    @Column(isNull = false, comment = "api 定义中的参数表 ID")
    @ApiModelProperty(value = "api 定义中的参数表 ID", required = true, example = "1")
    Long apiParamId;
}
