package com.bjy.qa.entity.functionaltest;

import com.baomidou.mybatisplus.annotation.TableName;
import com.bjy.qa.entity.BaseEntity;
import com.bjy.qa.enumtype.EditStatus;
import com.bjy.qa.util.EnumUtil;
import com.gitee.sunchenbin.mybatis.actable.annotation.Column;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import com.gitee.sunchenbin.mybatis.actable.constants.MySqlTypeConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@TableName("ft_reviews_result")
@TableComment("评审结果表")
@ApiModel("评审结果表 Entity 模型")
public class ReviewsResult extends BaseEntity<ReviewsResult> {
    @Column(isNull = false, comment = "项目 ID")
    @ApiModelProperty(value = "项目 ID", example = "1")
    @NotNull(message = "projectId: 项目 ID 字段字段必填")
    private Long projectId;

    @Column(isNull = false, comment = "评审计划 ID")
    @ApiModelProperty(value = "评审计划 ID", example = "1")
    @NotNull(message = "reviewsId: 评审计划 ID 字段字段必填")
    private Long reviewsId;

    @Column(isNull = false, comment = "测试用例 ID")
    @ApiModelProperty(value = "测试用例 ID", example = "1")
    @NotNull(message = "testCaseId: 测试用例 ID 字段字段必填")
    private Long testCaseId;

    @Column(isNull = false, type = MySqlTypeConstant.INT,comment = "状态")
    @ApiModelProperty(value = "状态", example = "1")
    EditStatus status;

    public void setStatus(int status) {
        this.status = EnumUtil.valueOf(EditStatus.class, status);
    }
}
