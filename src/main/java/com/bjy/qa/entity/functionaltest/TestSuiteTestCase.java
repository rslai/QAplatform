package com.bjy.qa.entity.functionaltest;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.bjy.qa.entity.BaseEntity;
import com.gitee.sunchenbin.mybatis.actable.annotation.Column;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@TableName("ft_test_suite_test_case")
@TableComment("测试套件测试用例关系表")
@ApiModel("测试套件测试用例 Entity 模型")
public class TestSuiteTestCase extends BaseEntity<TestSuiteTestCase> {

    @TableField
    @Column(isNull = false, comment = "测试套件id")
    @ApiModelProperty(value = "套件id", example = "1")
    private Long testSuitesId;

    @TableField
    @Column(isNull = false, comment = "测试用例id")
    @ApiModelProperty(value = "用例id", example = "1")
    private Long testCasesId;

}
