package com.bjy.qa.entity.functionaltest;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel("运行测试套件模型")
public class RunTestSuite {

    @ApiModelProperty(value = "消息类型，suite:功能测试套件，debug:调试", required = true, example = "suite")
    private String msg;

    @ApiModelProperty(value = "用例类型，1:功能测试, 2:mock接口, 3:性能测试套件", required = true, example = "1")
    private int caseType;

    @ApiModelProperty(value = "执行的用例", required = true, example = "")
    private Object cases;

    @ApiModelProperty(value = "公共步骤（公共用例）", required = true, example = "")
    private List<TestCaseDTO> pubCases;
}
