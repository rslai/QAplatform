package com.bjy.qa.entity.functionaltest;

import com.bjy.qa.enumtype.EditStatus;
import com.bjy.qa.util.EnumUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel("批量用例评审提交数据 DTO")
public class BatchReviewsExecuteDataDTO {
    @ApiModelProperty(value = "评审计划 id", example = "1")
    Long reviewsId;

    @ApiModelProperty(value = "测试用例 id 列表", required = true, example = "[1, 2]")
    List<Long> testCaseIds;

    @ApiModelProperty(value = "测试用例 状态", required = true, example = "1")
    EditStatus caseStatus;

    public void setCaseStatus(int status) {
        this.caseStatus = EnumUtil.valueOf(EditStatus.class, status);
    }
}
