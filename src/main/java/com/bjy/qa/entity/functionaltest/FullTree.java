package com.bjy.qa.entity.functionaltest;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel("完整 Tree DTO 模型")
public class FullTree extends Tree {
    @ApiModelProperty(value = "树的子节点", required = true, example = "[{\"id\":5,\"key\":\"c-5\",\"projectId\":1,\"parentId\":10,\"isCatalog\":true,\"name\":\"xxx\"}]")
    protected List<FullTree> children;
}
