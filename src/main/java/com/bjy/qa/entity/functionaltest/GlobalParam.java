package com.bjy.qa.entity.functionaltest;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.bjy.qa.entity.BaseEntity;
import com.bjy.qa.enumtype.GlobalParamScope;
import com.bjy.qa.enumtype.GlobalParamType;
import com.bjy.qa.util.EnumUtil;
import com.gitee.sunchenbin.mybatis.actable.annotation.Column;
import com.gitee.sunchenbin.mybatis.actable.annotation.Index;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import com.gitee.sunchenbin.mybatis.actable.constants.MySqlTypeConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@TableName("ft_global_param")
@TableComment("全局参数定义表")
@ApiModel("全局参数定义的 Entity 模型")
public class GlobalParam extends BaseEntity<GlobalParam> {
    @Column(isNull = false, comment = "所属项目id")
    @Index(value = "IDX_PROJECT_ID")
    @ApiModelProperty(value = "项目id", example = "1")
    Long projectId;

    @Column(isNull = false, length = 50, comment = "参数名")
    @ApiModelProperty(value = "参数名", example = "roomId")
    String paramName;

    @Column(comment = "参数值", length = 1024)
    @ApiModelProperty(value = "参数值", example = "12345")
    String value;

    @Column(comment = "长度", defaultValue = "0")
    @ApiModelProperty(value = "定义参数长度", example = "8")
    Integer length;

    @Column(type = MySqlTypeConstant.INT, defaultValue = "0", comment = "类型")
    @ApiModelProperty(value = "定义参数类型", required = true, example = "1")
    GlobalParamType globalParamType;

    @Column(type = MySqlTypeConstant.INT, defaultValue = "0", comment = "作用域")
    @ApiModelProperty(value = "作用域", example = "1")
    GlobalParamScope globalParamScope;

    @Column(length = 20, defaultValue = "0", comment = "userId")
    @ApiModelProperty(value = "用户id", example = "1")
    Long userId;

    @Column(length=50, comment = "参数描述")
    @TableField("`desc`")
    @ApiModelProperty(value = "参数描述", required = true, example = "用户名")
    String desc;

    public void setGlobalParamType(int globalParamType) {
        this.globalParamType = EnumUtil.valueOf(GlobalParamType.class, globalParamType);
    }

    public void setGlobalParamScope(int globalParamScope) {
        this.globalParamScope = EnumUtil.valueOf(GlobalParamScope.class, globalParamScope);
    }

}
