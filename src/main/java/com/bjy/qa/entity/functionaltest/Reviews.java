package com.bjy.qa.entity.functionaltest;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.bjy.qa.entity.BaseEntity;
import com.bjy.qa.entity.user.User;
import com.bjy.qa.enumtype.EditStatus;
import com.bjy.qa.util.EnumUtil;
import com.gitee.sunchenbin.mybatis.actable.annotation.Column;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import com.gitee.sunchenbin.mybatis.actable.constants.MySqlTypeConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Data
@TableName("ft_reviews")
@TableComment("评审计划表")
@ApiModel("评审计划表 Entity 模型")
public class Reviews extends BaseEntity<Reviews> {
    @Column(isNull = false, comment = "项目 ID")
    @ApiModelProperty(value = "项目 ID", example = "1")
    @NotNull(message = "projectId: 项目 ID 字段字段必填")
    private Long projectId;

    @Column(isNull = false, length = 50, comment = "评审名称")
    @ApiModelProperty(value = "评审名称", example = "SDK 3.2用例评审")
    @NotNull(message = "name: 评审名称 字段字段必填")
    private String name;

    @Column(isNull = false, type = MySqlTypeConstant.INT, comment = "评审计划状态")
    @ApiModelProperty(value = "评审计划状态", example = "1")
    private EditStatus status;

    @Column(isNull = false,  comment = "待评审总人数")
    @ApiModelProperty(value = "待评审总人数", example = "2")
    private int totalReviewers;

    @Column(isNull = false,  comment = "已评人数")
    @ApiModelProperty(value = "已评人数", example = "2")
    private int reviewers;

    @Column(isNull = false,  comment = "待评审总用例数")
    @ApiModelProperty(value = "待评审总用例数", example = "2")
    private int totalTestCase;

    @Column(isNull = false,  comment = "评审通过总用例数")
    @ApiModelProperty(value = "评审通过总用例数", example = "2")
    private int passTestCases;

    @Column(isNull = false, comment = "评审通过率")
    @ApiModelProperty(value = "评审通过率", example = "25")
    private int passRate;

    @Column(isNull = false, length = 50, comment = "发起人 ID")
    @ApiModelProperty(value = "发起人 ID", example = "231")
    private Long initiatorUserId;

    @Column(isNull = false, comment = "开始时间")
    @ApiModelProperty(value = "开始时间", example = "2023-12-04")
    @NotNull(message = "startedAt: 开始时间 字段字段必填")
    private Date startedAt;

    @Column(isNull = false, comment = "截止时间")
    @ApiModelProperty(value = "截止时间", example = "2023-12-04")
    @NotNull(message = "endedAt: 截止时间 字段字段必填")
    private Date endedAt;

    @TableField(exist = false)
    @ApiModelProperty(value = "包含的测试用例")
    List<TestCase> testCases;

    @TableField(exist = false)
    @ApiModelProperty(value = "包含的评审的用户")
    List<User> user;

    public void setStatus(int status) {
        this.status = EnumUtil.valueOf(EditStatus.class, status);
    }
}
