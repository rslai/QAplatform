package com.bjy.qa.entity.functionaltest;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.bjy.qa.entity.BaseEntity;
import com.gitee.sunchenbin.mybatis.actable.annotation.Column;
import com.gitee.sunchenbin.mybatis.actable.annotation.Index;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.List;

@Data
@TableName("ft_env_param")
@TableComment("环境参数定义表")
@ApiModel("环境参数定义的 Entity 模型")
public class EnvParam extends BaseEntity<EnvParam> {
    @Column(isNull = false, comment = "所属项目id")
    @Index(value = "IDX_PROJECT_ID")
    @ApiModelProperty(value = "项目信息id", example = "1")
    Long projectId;

    @Column(isNull = false, length = 50, comment = "环境名")
    @NotEmpty(groups = AddEnvGroup.class, message = "envName: 环境名必填")
    @NotEmpty(groups = AddServerGroup.class, message = "envName: 环境名必填")
    @ApiModelProperty(value = "环境名", example = "TEST")
    String envName;

    @Column(length = 50, comment = "环境参数名")
    @NotEmpty(groups = AddServerGroup.class, message = "serverName: 服务名不能为空")
    @ApiModelProperty(value = "环境参数名", example = "tools")
    String serverName;

    @Column(comment = "环境参数值", length = 1024)
    @NotEmpty(groups = AddServerGroup.class, message = "value: 服务地址不能为空")
    @ApiModelProperty(value = "环境参数值", example = "https://test-tools.baijiayun.com/")
    String value;

    @Column(length=50, comment = "参数描述")
    @TableField("`desc`")
    @ApiModelProperty(value = "参数描述", required = true, example = "用户名")
    String desc;

    @TableField(exist = false)
    List<EnvParam> envParams;

    public interface AddEnvGroup {
    }

    public interface AddServerGroup {
    }
}
