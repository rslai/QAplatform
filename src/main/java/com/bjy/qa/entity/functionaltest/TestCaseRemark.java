package com.bjy.qa.entity.functionaltest;

import com.baomidou.mybatisplus.annotation.TableName;
import com.bjy.qa.entity.BaseEntity;
import com.bjy.qa.enumtype.RunStatus;
import com.bjy.qa.util.EnumUtil;
import com.gitee.sunchenbin.mybatis.actable.annotation.Column;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import com.gitee.sunchenbin.mybatis.actable.constants.MySqlTypeConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@TableName("ft_test_case_remark")
@TableComment("测试结果备注表")
@ApiModel("测试结果备注 Entity 模型")
public class TestCaseRemark extends BaseEntity<TestCaseRemark> {
    @Column(isNull = false, comment = "测试用例id")
    @ApiModelProperty(value = "测试用例id", example = "1")
    Long cid;

    @Column(isNull = false, comment = "测试套件id")
    @ApiModelProperty(value = "测试套件id", example = "1")
    Long suiteId;

    @Column(isNull = false, comment = "所属项目 ID")
    @ApiModelProperty(value = "所属项目id", example = "1")
    Long projectId;

    @Column(isNull = false, type = MySqlTypeConstant.INT, comment = "状态")
    @ApiModelProperty(value = "状态", example = "1")
    RunStatus status;

    @Column(isNull = false, type = MySqlTypeConstant.LONGTEXT, comment = "备注信息")
    @ApiModelProperty(value = "备注信息", example = "进入房间")
    String remark;

    @Column(isNull = false, type = MySqlTypeConstant.LONGTEXT, comment = "详细信息")
    @ApiModelProperty(value = "详细信息", example = "步骤1：预期结果，输入用户名密码。实际结果，进入房间成功")
    String details;

    @Column(comment = "操作人")
    @ApiModelProperty(value = "操作人", example = "张三")
    String operator;

    public void setStatus(int status) {
        this.status = EnumUtil.valueOf(RunStatus.class, status);
    }
}
