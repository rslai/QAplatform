package com.bjy.qa.entity.functionaltest;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel("测试步骤详情")
public class StepDTO {

    @TableField(exist = false)
    Step step;

}
