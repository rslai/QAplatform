package com.bjy.qa.entity.functionaltest;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.bjy.qa.entity.BaseEntity;
import com.bjy.qa.enumtype.AgentStatus;
import com.bjy.qa.util.EnumUtil;
import com.gitee.sunchenbin.mybatis.actable.annotation.Column;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import com.gitee.sunchenbin.mybatis.actable.constants.MySqlTypeConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@TableName("ft_agent")
@TableComment("代理信息表")
@ApiModel("代理信息 Entity 模型")
public class Agent extends BaseEntity<Agent> {
    @Column(isNull = false, length = 36, comment = "代理唯一标识")
    @TableField("`key`")
    @ApiModelProperty(value = "代理唯一标识", example = "29002272-4659-4808-a804-08ce3388b136")
    @NotBlank(groups = Agent.AgentAgentSignOnGroup.class, message = "key: key 字段必填")
    String key;

    @Column(isNull = false, length = 50, comment = "Agent 名称")
    @NotBlank(groups = Agent.AgentAddGroup.class, message = "name: 名称 字段必填")
    @ApiModelProperty(value = "名称", example = "public agent")
    private String name;

    @Column(length = 50, comment = "agent 版本")
    @ApiModelProperty(value = "版本", example = "v1.4.0-release")
    @NotBlank(groups = Agent.AgentAgentSignOnGroup.class, message = "version: 版本 字段必填")
    private String version;

    @Column(length = 50, comment = "运行系统")
    @ApiModelProperty(value = "运行系统", example = "Windows 11")
    @NotBlank(groups = Agent.AgentAgentSignOnGroup.class, message = "systemType: 运行系统 字段必填")
    private String systemType;

    @Column(length = 25, comment = "agent IP 地址")
    @ApiModelProperty(value = "agent IP 地址", example = "192.168.1.1")
    private String address;

    @Column(length = 50, comment = "mock base URL")
    @ApiModelProperty(value = "mock base URL", example = "http://127.0.0.1:6625/mock/前置url/")
    private String mockBaseUrl;

    @Column(type = MySqlTypeConstant.INT, comment = "agent 状态",defaultValue = "2")
    @ApiModelProperty(value = "状态", required = true, example = "OFF_LINE")
    private AgentStatus status;

    public void setStatus(int status) {
        this.status = EnumUtil.valueOf(AgentStatus.class, status);
    }

    public static interface AgentAddGroup {
    }

    public static interface AgentAgentSignOnGroup {
    }
}
