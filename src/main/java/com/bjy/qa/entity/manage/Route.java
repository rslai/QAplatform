package com.bjy.qa.entity.manage;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.bjy.qa.entity.BaseEntity;
import com.bjy.qa.enumtype.ResourceType;
import com.bjy.qa.util.EnumUtil;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gitee.sunchenbin.mybatis.actable.annotation.Column;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import com.gitee.sunchenbin.mybatis.actable.constants.MySqlTypeConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@TableName
@TableComment("路由信息表")
@ApiModel("菜单路由信息 Entity 模型")
public class Route extends BaseEntity<Route> {
    @Column(isNull = false, type = MySqlTypeConstant.INT, comment = "资源类型")
    @ApiModelProperty(value = "资源类型", required = true, example = "1")
    ResourceType type;

    @Column(isNull = false, comment = "路由匹配的路径，注意根节点前要加/")
    @ApiModelProperty(value = "路由匹配的路径，注意根节点前要加/", example = "/functional_test")
    String path;

    @Column(isNull = false, comment = "路由的名称")
    @ApiModelProperty(value = "路由的名称", example = "functional_test")
    String name;

    @Column(isNull = false, comment = "对应加载的组件")
    @ApiModelProperty(value = "对应加载的组件", example = "test_case")
    String component;

    @Column(comment = "重定向的路由路径")
    @ApiModelProperty(value = "重定向的路由路径", example = "/functional_test/test_case")
    String redirect;

    @Column(isNull = false, comment = "在菜单中是否隐藏。true：隐藏，默认 false")
    @ApiModelProperty(value = "在菜单中是否隐藏。true：隐藏，默认 false", example = "true")
    boolean hidden = false;

    @Column(isNull = false, comment = "是否始终显示根菜单。true：无论有没有子节点都显示，false：必须有子节点才显示")
    @ApiModelProperty(value = "是否始终显示根菜单。true：无论有没有子节点都显示，false：必须有子节点才显示", example = "true")
    boolean alwaysShow = false;

    @Column(comment = "传递数据的 props 属性")
    @ApiModelProperty(value = "传递数据的 props 属性", example = "{ 'caseType': 1 }")
    String props;

    @JsonIgnore
    @JSONField(serialize = false)
    @Column(isNull = false, comment = "元信息 id")
    @ApiModelProperty(value = "元信息 id", required = true, example = "1")
    Long routeMetaId;

    @ApiModelProperty(value = "元信息", required = true, example = "{ 'title': '编辑套件' }")
    @TableField(exist = false)
    RouteMeta meta;

    @Column(isNull = false, comment = "资源 id")
    @ApiModelProperty(value = "资源 id", required = true, example = "1")
    Long resourceId = 0L;

    @Column(isNull = false, comment = "父分类的 id")
    @ApiModelProperty(value = "父分类的 id", required = true, example = "1")
    Long parentId;

    @Column(isNull = false, comment = "排序")
    @ApiModelProperty(value = "排序", example = "100")
    Integer sort = 9999;

    @ApiModelProperty(value = "子路由", required = true, example = "[{......}, {......}]")
    @TableField(exist = false)
    List<Route> children;

    public void setType(int type) {
        this.type = EnumUtil.valueOf(ResourceType.class, type);
    }
}
