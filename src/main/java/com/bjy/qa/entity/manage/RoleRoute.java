package com.bjy.qa.entity.manage;

import com.baomidou.mybatisplus.annotation.TableName;
import com.bjy.qa.entity.BaseEntity;
import com.gitee.sunchenbin.mybatis.actable.annotation.Column;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@TableName
@TableComment("角色对应路由信息表")
@ApiModel("角色对应路由信息 Entity 模型")
public class RoleRoute extends BaseEntity<RoleRoute> {
    @Column(isNull = false, comment = "角色 id")
    Long roleId; // 角色 id

    @Column(isNull = false, comment = "路由信息")
    Long routeId; // 路由信息 id
}
