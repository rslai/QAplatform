package com.bjy.qa.entity.manage;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.bjy.qa.entity.BaseEntity;
import com.gitee.sunchenbin.mybatis.actable.annotation.Column;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

@TableName
@TableComment("公司信息表")
@Data
@ApiModel("公司信息 Entity 模型")
public class Company extends BaseEntity<Company> {
    @Column(isNull = false, length = 50, comment = "公司名称")
    @NotBlank(groups = CompanyGroup.class, message = "name: 公司名称字段必填")
    @ApiModelProperty(value = "公司名称", example = "A公司")
    String name;

    @TableField(exist = false)
    @ApiModelProperty(value = "公司下的所有项目列表")
    List<Project> projectList;

    public static interface CompanyGroup {
    }
}
