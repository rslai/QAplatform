package com.bjy.qa.entity.manage;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.bjy.qa.entity.BaseEntity;
import com.gitee.sunchenbin.mybatis.actable.annotation.Column;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@TableName
@TableComment("路由 Meta 信息表")
@ApiModel("菜单路由 Meta 信息 Entity 模型")
public class RouteMeta extends BaseEntity<RouteMeta> {
    @Column(isNull = false, comment = "页面标题")
    @ApiModelProperty(value = "页面标题", example = "")
    String title;

    @Column(comment = "图标")
    @ApiModelProperty(value = "图标", example = "")
    String icon;

    @Column(isNull = false, comment = "是否禁用缓存页面。true：禁止缓存页面（默认值为false）")
    @ApiModelProperty(value = "是否禁用缓存页面。true：禁止缓存页面（默认值为false）", example = "false")
    boolean noCache = false;

    @Column(isNull = false, comment = "标签视图中是否可关闭。true：不允许关闭（默认 false）")
    @ApiModelProperty(value = "标签视图中是否可关闭。true：不允许关闭（默认 false）", example = "false")
    boolean affix = false;

    @Column(isNull = false, comment = "是否显示面包屑。false：不在面包屑中显示（默认值为 true）")
    @ApiModelProperty(value = "是否显示面包屑。false：不在面包屑中显示（默认值为 true）", example = "true")
    boolean breadcrumb = true;

    @Column(isNull = false, comment = "高亮菜单。设置后会高亮对应的侧边栏菜单")
    @ApiModelProperty(value = "高亮菜单。设置后会高亮对应的侧边栏菜单", example = "/example/list")
    String activeMenu;

    @ApiModelProperty(value = "页面角色（可以设置多个角色）", required = true, example = "['admin','editor']")
    @TableField(exist = false)
    List<String> roles;
}
