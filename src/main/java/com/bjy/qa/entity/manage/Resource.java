package com.bjy.qa.entity.manage;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.bjy.qa.entity.BaseEntity;
import com.gitee.sunchenbin.mybatis.actable.annotation.Column;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@TableName
@TableComment("资源信息表")
@ApiModel("资源信息 Entity 模型")
public class Resource extends BaseEntity<Resource> {
    @Column(isNull = false, length=50, comment = "资源描述")
    @TableField("`desc`")
    @ApiModelProperty(value = "资源描述", required = true, example = "分页查询资源列表")
    String desc;

    @Column(isNull = false, length=50, comment = "资源名称")
    @ApiModelProperty(value = "资源路径", required = true, example = "/manage/resource/list")
    String path;
}
