package com.bjy.qa.entity.manage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel("角色对应所有权限的 DTO 模型")
public class RolePermissionDTO {
    @ApiModelProperty(value = "角色 id", example = "1")
    Long roleId;

    @ApiModelProperty(value = "角色 ids", example = "1")
    List<String> permissions;
}
