package com.bjy.qa.entity.manage;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.bjy.qa.entity.BaseEntity;
import com.bjy.qa.enumtype.ForwardMode;
import com.bjy.qa.util.EnumUtil;
import com.gitee.sunchenbin.mybatis.actable.annotation.Column;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import com.gitee.sunchenbin.mybatis.actable.constants.MySqlTypeConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@TableName
@TableComment("项目信息表")
@Data
@ApiModel("项目信息 Entity 模型")
public class Project extends BaseEntity<Project> {
    @Column(isNull = false, comment = "所属公司 ID")
    @NotNull(groups = ProjectGroup.class, message = "companyId: 公司ID字段必填")
    @ApiModelProperty(value = "所属公司 ID", example = "")
    Long companyId;

    @Column(isNull = false, length = 50, comment = "项目名称")
    @NotBlank(groups = ProjectGroup.class, message = "name: 项目名称字段必填")
    @ApiModelProperty(value = "项目名称", example = "云端课堂")
    String name;

    @Column(comment = "项目描述")
    @TableField("`desc`")
    @ApiModelProperty(value = "项目描述", required = true, example = "云端课堂 open api 测试")
    String desc;

    @Column(comment = "是否开启 mock")
    @ApiModelProperty(value = "是否开启 mock", example = "true")
    boolean mock;

    @Column(length = 20, comment = "mock 前置 url")
    @ApiModelProperty(value = "mock 前置 url", example = "abc")
    String mockPreUrl;

    @Column(isNull = false, defaultValue = "0", type = MySqlTypeConstant.INT, comment = "转发模式")
    @ApiModelProperty(value = "转发模式", required = true, example = "1")
    ForwardMode forwardMode;

    @Column(comment = "mock 转发 url")
    @ApiModelProperty(value = "mock 转发 url", example = "https://abc.com")
    String forwardUrl;

    public void setForwardMode(int forwardMode) {
        this.forwardMode = EnumUtil.valueOf(ForwardMode.class, forwardMode);
    }

    public static interface ProjectGroup {
    }
}
