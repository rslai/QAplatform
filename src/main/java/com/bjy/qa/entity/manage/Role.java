package com.bjy.qa.entity.manage;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.bjy.qa.entity.BaseEntity;
import com.gitee.sunchenbin.mybatis.actable.annotation.Column;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@TableName
@TableComment("角色信息表")
@ApiModel("角色信息 Entity 模型")
public class Role extends BaseEntity<Role> {
    @Column(isNull = false, length=50, comment = "角色名称")
    @NotBlank(message = "name: 角色名称字段必填")
    @ApiModelProperty(value = "角色名称", required = true, example = "admin")
    String name;

    @Column(isNull = false, length=50, comment = "角色描述")
    @TableField("`desc`")
    @NotBlank(message = "desc: 角色描述字段必填")
    @ApiModelProperty(value = "角色描述", required = true, example = "超级管理员")
    String desc;
}
