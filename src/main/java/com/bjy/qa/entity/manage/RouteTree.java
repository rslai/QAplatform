package com.bjy.qa.entity.manage;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
@ApiModel("路由 Tree DTO 模型")
public class RouteTree {
    @ApiModelProperty(value = "自增ID", required = true, example = "1")
    protected Long id;

    @ApiModelProperty(value = "tree中的唯一标识（key）", required = true, example = "c-1")
    private String key;

    @NotBlank
    @ApiModelProperty(value = "父分类的id", required = true, example = "1")
    Long parentId;

    @NotBlank
    @ApiModelProperty(value = "是否是路由", required = true, example = "true")
    Boolean isRoute;

    @NotBlank
    @ApiModelProperty(value = "名称", required = true, example = "菜单管理")
    String name;

    @ApiModelProperty(value = "在菜单中是否隐藏。true：隐藏，默认 false", example = "true")
    boolean hidden;

    @JsonIgnore
    @JSONField(serialize = false)
    @ApiModelProperty(value = "排序", example = "100")
    Integer sort = 9999;

    @ApiModelProperty(value = "路由 Tree 的子节点", required = true, example = "[{\"id\":51,\"key\":\"r-51\",\"parentId\":0,\"isRoute\":true,\"name\":\"xxxxxx\"}]")
    protected List<RouteTree> children;
}
