package com.bjy.qa.entity.dashboard;

import com.baomidou.mybatisplus.annotation.TableName;
import com.bjy.qa.entity.BaseEntity;
import com.gitee.sunchenbin.mybatis.actable.annotation.Column;
import com.gitee.sunchenbin.mybatis.actable.annotation.Index;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@TableName("dash_test_case_count")
@TableComment("数据统计-用户对应测试用例维度统计表")
@ApiModel("dash_test_case_count 定义的 Entity 模型")
public class TestCaseCount extends BaseEntity<TestCaseCount> {
    @Column(isNull = false, comment = "所属项目 id")
    @Index(value = "IDX_PROJECT_ID")
    @ApiModelProperty(value = "项目信息 id", example = "1")
    Long projectId;

    @Column(isNull = false, comment = "执行用例的用户")
    @ApiModelProperty(value = "执行用例的用户 designer", example = "张三")
    private String designer;

    @Column(comment = "新增的接口用例数")
    @ApiModelProperty(value = "新增接口用例数 addApiCases", example = "1")
    private int addApiCases;

    @Column(comment = "修改的接口用例数")
    @ApiModelProperty(value = "接口用例更新数据 updateApiCases", example = "1")
    private int updateApiCases;

    @Column(comment = "新增接口数")
    @ApiModelProperty(value = "新增接口用例数 addApiCount", example = "1")
    private int addApiCount;

    @Column(comment = "修改接口数")
    @ApiModelProperty(value = "接口用例更新数据 updateApiCount", example = "1")
    private int updateApiCount;

    @Column(comment = "新增功能用例数")
    @ApiModelProperty(value = "新增功能用例数 addFunCases", example = "1")
    private int addFunCases;

    @Column(comment = "更新功能用例数")
    @ApiModelProperty(value = "更新功能用例数 updateFunCases", example = "1")
    private int updateFunCases;

    @Column(comment = "执行功能用例数")
    @ApiModelProperty(value = "执行功能用例数 excFunCases", example = "1")
    private int excFunCases;

    @Column(comment = "当天执行用例总耗时")
    @ApiModelProperty(value = "当天执行用例总耗时 funTotalTime", example = "1")
    private long funTotalTime;

    @Column(comment = "执行每条用例平均耗时")
    @ApiModelProperty(value = "执行每条用例平均耗时 funAvgTime", example = "1")
    private double funAvgTime;

    @Column(comment = "执行用例时的离散度")
    @ApiModelProperty(value = "执行用例时的离散度 funVar", example = "1")
    private double funVar;
}
