package com.bjy.qa.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.gitee.sunchenbin.mybatis.actable.annotation.Column;
import com.gitee.sunchenbin.mybatis.actable.annotation.IsAutoIncrement;
import com.gitee.sunchenbin.mybatis.actable.constants.MySqlTypeConstant;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Base Entity 模型
 * @param <T>
 */
@Data
public abstract class BaseEntity<T extends Model<T>> extends Model<T> {
    @Column(isNull = false, comment = "自增id")
    @TableId(type = IdType.AUTO)
    @IsAutoIncrement
    @NotNull(groups = IdGroup.class, message = "id: id字段必填")
    @Min(value = 1, groups = IdGroup.class, message = "id: id字段必填")
    @ApiModelProperty(value = "自增ID", required = true, example = "1")
    protected Long id;

    @Column(isNull = false, defaultValue="false", comment = "是否删除")
    @TableLogic
    @ApiModelProperty(value = "是否已删除", example = "false")
    private boolean isDelete;

    @Column(isNull = false, type = MySqlTypeConstant.TIMESTAMP, defaultValue="CURRENT_TIMESTAMP", comment = "修改时间")
    @TableField(fill = FieldFill.INSERT_UPDATE) // INSERT_UPDATE 首次插入、其次更新时填充(或修改)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSS") // set
    @ApiModelProperty(value = "更新时间", example = "2022-05-10 17:10:30.112")
    protected Date updatedAt;

    @Column(isNull = false, type = MySqlTypeConstant.TIMESTAMP, defaultValue="CURRENT_TIMESTAMP", comment = "创建时间")
    @TableField(fill = FieldFill.INSERT) // INSERT代表只在插入时填充
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSS") // set
    @ApiModelProperty(value = "创建时间", example = "2022-05-10 17:10:30.112")
    protected Date createdAt;

    public static interface IdGroup {
    }
}
