package com.bjy.qa.entity;

import com.baomidou.mybatisplus.core.metadata.OrderItem;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

public class MyPage<T> {
    @ApiModelProperty(value = "页号", example = "1")
    private Long pageNum = 1L; // 当前页
    @ApiModelProperty(value = "每页显示的数据条数", example = "10")
    private Long pageSize = 10L; // 每页显示数据的条数
    private Long totalRecords; // 总记录条数
    private List<OrderItem> orders; // 排序
    private T query; // 查询对象

    public Long getPageNum() {
        return pageNum;
    }

    public void setPageNum(Long pageNum) {
        this.pageNum = pageNum;
    }

    public Long getPageSize() {
        return pageSize;
    }

    public void setPageSize(Long pageSize) {
        this.pageSize = pageSize;
    }

    public Long getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords(Long totalRecords) {
        this.totalRecords = totalRecords;
    }

    public List<OrderItem> getOrders() {
        return orders;
    }

    public void setOrders(List<OrderItem> orders) {
        this.orders = orders;
    }

    public T getQuery() {
        return query;
    }

    public void setQuery(T query) {
        this.query = query;
    }

    @Override
    public String toString() {
        return "MyPage{" +
                "pageNum=" + pageNum +
                ", pageSize=" + pageSize +
                ", totalRecords=" + totalRecords +
                ", orders=" + orders +
                ", query=" + query +
                '}';
    }
}
