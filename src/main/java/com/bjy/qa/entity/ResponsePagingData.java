package com.bjy.qa.entity;

import java.util.List;

/**
 * 返回带分页的 list 数据
 */
public class ResponsePagingData {
    MyPage page;
    List list;

    public ResponsePagingData(MyPage page, List list) {
        this.page = page;
        this.list = list;
    }

    public MyPage getPage() {
        return page;
    }

    public void setPage(MyPage page) {
        this.page = page;
    }

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "ResponseListData{" +
                "page=" + page +
                ", list=" + list +
                '}';
    }
}
