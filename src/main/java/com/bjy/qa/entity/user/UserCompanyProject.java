package com.bjy.qa.entity.user;

import com.baomidou.mybatisplus.annotation.TableName;
import com.bjy.qa.entity.BaseEntity;
import com.gitee.sunchenbin.mybatis.actable.annotation.Column;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import lombok.Data;

/**
 * 用户-公司-项目 对应表
 */
@TableName
@TableComment("用户对应公司项目表")
@Data
public class UserCompanyProject extends BaseEntity<UserCompanyProject> {
    @Column(isNull = false, comment = "用户 ID")
    Long userId; // user id

    @Column(isNull = false, comment = "所属公司 id")
    Long companyId; // 公司 id

    @Column(isNull = false, comment = "所属项目id")
    Long projectId; // 项目 id
}
