package com.bjy.qa.entity.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.ldap.odm.annotations.Attribute;
import org.springframework.ldap.odm.annotations.Entry;
import org.springframework.ldap.odm.annotations.Id;

import javax.naming.Name;

/**
 * LDAP 中的用户对象
 */
@Entry(base = "", objectClasses = "")
public class Person {
    @Id
    @JsonIgnore
    private Name id; // ID，包括组织（OU）和 常用名（CN）
    @Attribute(name = "cn")
    private String commonName; // 常用名
    @Attribute(name = "sn")
    private String suerName; // 姓
    private String givenName; // 名
    private String mail; // 邮箱
    private String mailNickname; // 邮箱昵称
    private String mobile; // 手机
    private String title; // 职位

    public Name getId() {
        return id;
    }

    public void setId(Name id) {
        this.id = id;
    }

    public String getCommonName() {
        return commonName;
    }

    public void setCommonName(String commonName) {
        this.commonName = commonName;
    }

    public String getSuerName() {
        return suerName;
    }

    public void setSuerName(String suerName) {
        this.suerName = suerName;
    }

    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getMailNickname() {
        return mailNickname;
    }

    public void setMailNickname(String mailNickname) {
        this.mailNickname = mailNickname;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", commonName='" + commonName + '\'' +
                ", suerName='" + suerName + '\'' +
                ", givenName='" + givenName + '\'' +
                ", mail='" + mail + '\'' +
                ", mailNickname='" + mailNickname + '\'' +
                ", mobile='" + mobile + '\'' +
                ", title='" + title + '\'' +
                '}';
    }
}
