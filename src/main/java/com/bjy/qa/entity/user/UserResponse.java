package com.bjy.qa.entity.user;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("用户返回 DTO 模型")
public class UserResponse {
    @ApiModelProperty(value = "token", example = "eyJ0eXAiOiJKV......")
    String token;
}
