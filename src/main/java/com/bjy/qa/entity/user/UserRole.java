package com.bjy.qa.entity.user;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.bjy.qa.entity.BaseEntity;
import com.bjy.qa.entity.manage.Role;
import com.gitee.sunchenbin.mybatis.actable.annotation.Column;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@TableName
@TableComment("用户对应角色信息表")
@ApiModel("用户对应角色信息表 Entity 模型")
public class UserRole extends BaseEntity<UserRole> {
    @Column(isNull = false, comment = "角色 id")
    Long roleId; // 角色 id

    @Column(isNull = false, comment = "用户信息")
    Long userId; // 用户信息 id

    @TableField(exist = false)
    Role role; // 角色信息
}
