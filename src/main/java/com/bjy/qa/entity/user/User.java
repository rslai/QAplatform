package com.bjy.qa.entity.user;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.bjy.qa.entity.BaseEntity;
import com.bjy.qa.entity.manage.Company;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import com.gitee.sunchenbin.mybatis.actable.annotation.Column;

import javax.validation.constraints.NotBlank;
import java.util.List;

@TableName
@TableComment("用户表")
@Data
@ApiModel("用户 Entity 模型")
public class User extends BaseEntity<User> {
    @Column(isNull = false, length=50, comment = "姓名")
    @NotBlank(groups = UserAddGroup.class, message = "name: 姓名字段必填") // 创建用户校验
    @NotBlank(groups = UserUpdateGroup.class, message = "name: 姓名字段必填") // 更新用户校验
    @ApiModelProperty(value = "姓名", required = true, example = "张三")
    String name;

    @Column(isNull = true, length=255, comment = "头像")
    @ApiModelProperty(value = "头像", required = true, example = "/image/pic.jpg")
    String avatar;

    @Column(isNull = false, length=50, comment = "账号")
    @NotBlank(groups = UserLoingGroup.class, message = "account: 账号字段必填") // 用户登录校验
    @NotBlank(groups = UserAddGroup.class, message = "account: 账号字段必填") // 创建用户校验
    @NotBlank(groups = UserUpdateGroup.class, message = "account: 账号字段必填") // 更新用户校验
    @ApiModelProperty(value = "账号", required = true, example = "zhangsan")
    String account;

    @Column(isNull = true, length=50, defaultValue = "", comment = "密码")
    @NotBlank(groups = UserLoingGroup.class, message = "code: 密码字段必填") // 用户登录校验
    @NotBlank(groups = UserAddGroup.class, message = "code: 密码字段必填") // 创建用户校验
    @ApiModelProperty(value = "密码", required = true, example = "111111")
    String code;

    @TableField(exist = false)
    @ApiModelProperty(value = "本地账号", required = true, example = "false")
    Boolean local = false;

    @TableField(exist = false)
    @ApiModelProperty(value = "角色", example = "[\"admin\", \"user\"]")
    List roles;

    @Column(isNull = false, defaultValue = "false", comment = "是否锁定")
    @ApiModelProperty(value = "是否锁定", example = "true")
    boolean isLocked;

    @Column(isNull = false, defaultValue = "0", comment = "用户当前选中公司ID")
    @ApiModelProperty(value = "用户当前选中公司ID", example = "1")
    long companyId;

    @Column(isNull = false, defaultValue = "0", comment = "用户当前选中项目ID")
    @ApiModelProperty(value = "用户当前选中项目ID", example = "1")
    long projectId;

    @TableField(exist = false)
    @ApiModelProperty(value = "用户所属公司列表")
    List<Company> companyList;

    public static interface UserAddGroup {
    }

    public static interface UserUpdateGroup {
    }

    public static interface UserLoingGroup {
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", avatar='" + avatar + '\'' +
                ", account='" + account + '\'' +
                ", code='********'" +
                ", local=" + local +
                ", roles=" + roles +
                ", isLocked=" + isLocked +
                ", companyId=" + companyId +
                ", projectId=" + projectId +
                ", companyList=" + companyList +
                "} " + super.toString();
    }
}
