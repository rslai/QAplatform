package com.bjy.qa.entity.user;

import com.bjy.qa.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 用户 对应 SSH服务器信息 的配置信息
 */
@Data
@ApiModel("用户 对应 SSH服务器信息 Entity 模型")
public class UserSshServerInfo extends BaseEntity<UserSshServerInfo> {
    @ApiModelProperty(value = "用户ID", example = "1")
    Long userId;

    @ApiModelProperty(value = "SSH服务器信息 ID", example = "10")
    Long sshServerInfoId;

    @ApiModelProperty(value = "用户名", example = "zhangsan")
    String sshUserName;

    @ApiModelProperty(value = "密码", example = "123456")
    String sshPassword;

    @Override
    public String toString() {
        return "UserSshServerInfo{" +
                "userId=" + userId +
                ", sshServerInfoId=" + sshServerInfoId +
                ", sshUserName='" + sshUserName + '\'' +
                ", sshPassword='******'" +
                "} " + super.toString();
    }
}
