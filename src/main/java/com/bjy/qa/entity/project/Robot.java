package com.bjy.qa.entity.project;

import com.baomidou.mybatisplus.annotation.TableName;
import com.bjy.qa.entity.BaseEntity;
import com.bjy.qa.enumtype.RobotType;
import com.bjy.qa.util.EnumUtil;
import com.gitee.sunchenbin.mybatis.actable.annotation.Column;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import com.gitee.sunchenbin.mybatis.actable.constants.MySqlTypeConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@TableName
@TableComment("机器人 表")
@Data
@ApiModel("机器人 Entity 模型")
public class Robot extends BaseEntity<Robot> {
    @Column(isNull = false, comment = "所属项目 ID")
    @NotNull(message = "projectId: 所属项目 ID 字段必填")
    @ApiModelProperty(value = "所属项目 ID", example = "1")
    private Long projectId;

    @Column(isNull = false, type = MySqlTypeConstant.INT, comment = "机器人类型")
    @NotNull(message = "type: 机器人类型字段必填")
    @ApiModelProperty(value = "机器人类型", required = true, example = "1")
    private RobotType type;

    @Column(isNull = false, length = 50, comment = "机器人名称")
    @NotBlank(message = "name: 机器人名称字段必填")
    @ApiModelProperty(value = "机器人名称", example = "测试环境机器人")
    private String name;

    @Column(isNull = false, comment = "机器人 token")
    @NotBlank(message = "name: 机器人 token字段必填")
    @ApiModelProperty(value = "机器人 token", example = "https://oapi.dingtalk.com/robot/send?access_token=07e102dddb5362xxxxxx7a3870e")
    String robotToken;

    @Column(isNull = false, comment = "机器人秘钥")
    @NotBlank(message = "name: 机器人秘钥字段必填")
    @ApiModelProperty(value = "机器人秘钥", example = "SEC096xxxxxx0ccfde891")
    String robotSecret;

    public void setType(int type) {
        this.type = EnumUtil.valueOf(RobotType.class, type);
    }
}
