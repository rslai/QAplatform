package com.bjy.qa.entity.project;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.bjy.qa.entity.BaseEntity;
import com.bjy.qa.enumtype.TaskStatus;
import com.bjy.qa.util.EnumUtil;
import com.gitee.sunchenbin.mybatis.actable.annotation.Column;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import com.gitee.sunchenbin.mybatis.actable.constants.MySqlTypeConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@TableName
@TableComment("定时任务执行结果 表")
@Data
@ApiModel("定时任务执行结果 Entity 模型")
public class JobResult extends BaseEntity<JobResult> {
    @Column(isNull = false, comment = "所属项目 ID")
    @ApiModelProperty(value = "所属项目 ID", example = "1")
    private Long projectId;

    @Column(isNull = false, comment = "定时任务 ID")
    @ApiModelProperty(value = "定时任务 ID", example = "1")
    private Long jobId;

    @TableField(exist = false)
    @ApiModelProperty(value = "定时任务")
    Job job;

    @Column(type = MySqlTypeConstant.LONGTEXT, comment = "定时任务执行结果")
    @ApiModelProperty(value = "定时任务执行结果", example = "执行成功")
    String result;

    @Column(isNull = false, type = MySqlTypeConstant.INT,comment = "定时任务执行结果状态")
    @ApiModelProperty(value = "定时任务执行结果状态", example = "RUNNING")
    TaskStatus status;

    public void setStatus(int status) {
        this.status = EnumUtil.valueOf(TaskStatus.class, status);
    }
}
