package com.bjy.qa.entity.project;

import com.baomidou.mybatisplus.annotation.TableName;
import com.bjy.qa.entity.BaseEntity;
import com.bjy.qa.enumtype.JobStatus;
import com.bjy.qa.enumtype.JobType;
import com.bjy.qa.util.EnumUtil;
import com.gitee.sunchenbin.mybatis.actable.annotation.Column;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import com.gitee.sunchenbin.mybatis.actable.constants.MySqlTypeConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@TableName
@TableComment("定时任务 表")
@Data
@ApiModel("定时任务 Entity 模型")
public class Job extends BaseEntity<Job> {
    @Column(isNull = false, comment = "所属项目 ID")
    @NotNull(groups = JobGroup.class, message = "projectId: 所属项目 ID 字段必填")
    @ApiModelProperty(value = "所属项目 ID", example = "1")
    private Long projectId;

    @Column(isNull = false, type = MySqlTypeConstant.INT, comment = "定时任务型")
    @NotNull(groups = JobGroup.class, message = "type: 定时任务型字段必填")
    @ApiModelProperty(value = "定时任务型", required = true, example = "1")
    private JobType type;

    @Column(isNull = false, length = 50, comment = "定时任务名称")
    @NotBlank(groups = JobGroup.class, message = "name: 定时任务名称字段必填")
    @ApiModelProperty(value = "定时任务名称", example = "后台接口定时任务")
    private String name;

    @Column(isNull = false, length = 50, comment = "cron 表达式")
    @NotBlank(groups = JobGroup.class, message = "cronExpression: cron 表达式字段必填")
    @ApiModelProperty(value = "cron 表达式", example = "0 0 3 * * ?")
    private String cronExpression;

    @Column(isNull = false, comment = "任务 ID")
    @NotNull(groups = JobGroup.class, message = "taskId: 任务 ID 字段必填")
    @ApiModelProperty(value = "任务 ID", example = "1")
    private Long taskId;

    @Column(isNull = false, type = MySqlTypeConstant.INT, comment = "任务状态")
    @NotNull(groups = JobGroup.class, message = "status: 任务状态字段必填")
    @ApiModelProperty(value = "任务状态", required = true, example = "1")
    private JobStatus status;

    @Column(isNull = false, comment = "用户（执行者） ID")
    @NotNull(groups = JobGroup.class, message = "userId: 用户 ID 字段必填")
    @ApiModelProperty(value = "用户 ID", example = "1")
    private Long userId;

    @Column(isNull = false, length = 50, comment = "执行者")
    @ApiModelProperty(value = "执行者", example = "rslai")
    String runUser;

    public void setType(int type) {
        this.type = EnumUtil.valueOf(JobType.class, type);
    }

    public void setStatus(int status) {
        this.status = EnumUtil.valueOf(JobStatus.class, status);
    }

    public static interface JobGroup {
    }

}
