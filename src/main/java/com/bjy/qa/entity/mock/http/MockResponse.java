package com.bjy.qa.entity.mock.http;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class MockResponse {
    private int status; // 返回的状态码
    private MockContentType contentType; // 请求的 Content-Type
    private Map<String, String> headers = new HashMap<>(); // 返回的 headers
    private Map<String, String> cookies = new HashMap<>(); // 返回的 cookies
    private String body = ""; // 返回的 body 参数
    private MockForward mockForward; // mock 转发
}
