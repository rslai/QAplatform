package com.bjy.qa.entity.mock;

import com.bjy.qa.entity.BaseEntity;
import com.bjy.qa.entity.mock.http.MockContentType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@ApiModel("mock DTO 模型")
public class MockDTO extends BaseEntity<MockDTO> {
    @ApiModelProperty(value = "期望名称", example = "name 传入 111")
    String name;

    @ApiModelProperty(value = "返回 Body", example = "{\"code\":0,\"message\":\"success\"}")
    String responseBody;

    @ApiModelProperty(value = "返回 Body 类型", example = "json")
    MockContentType responseType;

    @ApiModelProperty(value = "返回 HTTP 状态码", example = "200")
    String responseCode;

    @ApiModelProperty(value = "mock 期望", example = "[]")
    List<ConditionsDTO> conditions = new ArrayList<>();
}
