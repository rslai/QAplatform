package com.bjy.qa.entity.mock.http;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Mock ContentType 枚举类
 */
public enum MockMethod {
    UNKNOWN("UNKNOWN", 0),
    GET("GET", 1),
    POST("POST", 2),
    PUT("PUT", 3),
    PATCH("PATCH", 4),
    DELETE("DELETE", 5),
    COPY("COPY", 6),
    HEAD("HEAD", 7),
    OPTIONS("OPTIONS", 8),
    LINK("LINK", 9),
    UNLINK("UNLINK", 10),
    PURGE("PURGE", 11),
    LOCK("LOCK", 12),
    UNLOCK("UNLOCK", 13),
    PROPFIND("PROPFIND", 14),
    VIEW("VIEW", 15);

    private String name;

    @JsonValue
    private int value;

    MockMethod(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    @JSONField
    public int getValue() {
        return value;
    }
}
