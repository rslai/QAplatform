package com.bjy.qa.entity.mock.http;

import lombok.Data;

/**
 * mock 转发
 */
@Data
public class MockForward {
    private boolean enable; // 是否开启转发，默认不转发
    private String host; // URL 请求中的 host，host + uri 是完整的转发地址
    private String uri; // 请求的 URI，host + uri 是完整的转发地址
}
