package com.bjy.qa.entity.mock;

import com.bjy.qa.entity.BaseEntity;
import com.bjy.qa.enumtype.Comparison;
import com.bjy.qa.enumtype.Location;
import com.bjy.qa.util.EnumUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("mock 期望 DTO 模型")
public class ConditionsDTO extends BaseEntity<ConditionsDTO> {
    @ApiModelProperty(value = "参数位置", example = "query")
    Location location;

    @ApiModelProperty(value = "参数名", example = "name")
    String name;

    @ApiModelProperty(value = "比较", example = "equal")
    Comparison comparison;

    @ApiModelProperty(value = "参数值", example = "200")
    String value;

    public void setLocation(int location) {
        this.location = EnumUtil.valueOf(Location.class, location);
    }

    public void setComparison(int comparison) {
        this.comparison = EnumUtil.valueOf(Comparison.class, comparison);
    }
}
