package com.bjy.qa.entity.tools;

import com.bjy.qa.enumtype.TaskStatus;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("Task DTO 模型")
public class Task {
    @ApiModelProperty(value = "任务ID", example = "xxxx-xxxx-xxxx")
    String taskId;

    @ApiModelProperty(value = "执行任务状态", example = "TaskStatus.CREATE")
    TaskStatus status;

    @ApiModelProperty(value = "信息", example = "执行成功")
    String msg;

    @ApiModelProperty(value = "详细数据", example = "file:///home/ocr/xxxx-xxxx-xxxx/xxxx-xxxx-xxxx.txt")
    String detailData;
}
