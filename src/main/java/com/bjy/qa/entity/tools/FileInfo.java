package com.bjy.qa.entity.tools;

import com.bjy.qa.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.File;

@Data
@ApiModel("文件信息 DTO 模型")
public class FileInfo extends BaseEntity<FileInfo> {
    @ApiModelProperty(value = "文件类型", example = "jpg")
    String type = "";

    @ApiModelProperty(value = "文件名", example = "aa.jpg")
    String fileName;

    @ApiModelProperty(value = "获取文件路径+文件名", example = "/images/xxaaqdd.jpg")
    String absolutePath;

    @ApiModelProperty(value = "下载文件url", example = "https://www.aa.com/aa.jpg")
    String url;

    @ApiModelProperty(value = "文件的 java.io.File 对象")
    File file;
}
