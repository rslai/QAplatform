package com.bjy.qa.entity.tools;

import com.baomidou.mybatisplus.annotation.TableName;
import com.bjy.qa.entity.BaseEntity;
import com.bjy.qa.enumtype.OcrChannel;
import com.bjy.qa.enumtype.Status;
import com.bjy.qa.util.EnumUtil;
import com.gitee.sunchenbin.mybatis.actable.annotation.Column;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import com.gitee.sunchenbin.mybatis.actable.constants.MySqlTypeConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@TableName
@TableComment("图像识别表")
@Data
@ApiModel("图像识别 Entity 模型")
public class Ocr extends BaseEntity<Ocr> {
    @Column(isNull = false, type = MySqlTypeConstant.INT, comment = "OCR通道")
    @ApiModelProperty(value = "OCR通道", example = "1")
    OcrChannel channel = OcrChannel.QAP;

    @Column(isNull = false, length = 50, comment = "文件类型")
    @ApiModelProperty(value = "文件类型", example = "jpg")
    String fileType = "";

    @Column(isNull = false, length = 100, comment = "用户上传时文件名")
    @NotBlank(groups = AddOcr.class, message = "fileName: 文件名 必填")
    @ApiModelProperty(value = "用户上传时文件名", required = true, example = "abc.jpg")
    String fileName;

    @Column(isNull = false, comment = "服务器上保存的文件名")
    @NotBlank(groups = AddOcr.class, message = "serverFileName: 服务器文件名 必填")
    @ApiModelProperty(value = "服务器上保存的文件名", required = true, example = "/images/20231010/aadjnfkf111.jpg")
    String serverFileName;

    @Column(isNull = false, comment = "文件url")
    @NotBlank(groups = AddOcr.class, message = "url: URL 必填")
    @ApiModelProperty(value = "文件url", example = "http://127.0.0.1:8080/image/11.jpg")
    String url;

    @Column(isNull = false, type = MySqlTypeConstant.INT, comment = "状态")
    @ApiModelProperty(value = "状态", example = "true")
    Status status = Status.SUCCESS;

    @Column(type = MySqlTypeConstant.LONGTEXT, comment = "识别得到的文本")
    @ApiModelProperty(value = "识别得到的文本", example = "中文内容")
    String text;

    public static interface AddOcr {
    }

    public void setChannel(int channel) {
        this.channel = EnumUtil.valueOf(OcrChannel.class, channel);
    }
}
