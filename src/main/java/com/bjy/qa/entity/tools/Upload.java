package com.bjy.qa.entity.tools;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.bjy.qa.entity.BaseEntity;
import com.gitee.sunchenbin.mybatis.actable.annotation.Column;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@TableName
@TableComment("文件上传表")
@Data
@ApiModel("文件上传 Entity 模型")
public class Upload extends BaseEntity<Upload> {
    @Column(length = 50, comment = "文件类型")
    @ApiModelProperty(value = "文件类型", example = "jpg")
    String type = "";

    @Column(isNull = false, length = 100, comment = "用户上传时文件名")
    @NotBlank(groups = Upload.class, message = "fileName: 文件名 必填")
    @ApiModelProperty(value = "用户上传时文件名", example = "aa.jpg")
    String fileName;

    @Column(isNull = false, comment = "服务器上保存的文件名")
    @NotBlank(groups = Upload.class, message = "serverFileName: 服务器文件名 必填")
    @ApiModelProperty(value = "服务器上保存的文件名", example = "/images/xxaaqdd.jpg")
    String serverFileName;

    @Column(length=50, comment = "文件描述")
    @TableField("`desc`")
    @ApiModelProperty(value = "文件描述", required = true, example = "测试 csv 文件")
    String desc;

    @Column(isNull = false, comment = "文件url")
    @NotBlank(groups = AddUpload.class, message = "url: URL 必填")
    @ApiModelProperty(value = "文件url", example = "https://www.aa.com/aa.jpg")
    String url;

    public static interface AddUpload {
    }
}
