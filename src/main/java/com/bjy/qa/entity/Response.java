package com.bjy.qa.entity;

import com.bjy.qa.enumtype.ErrorCode;
import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel("响应 DTO 模型")
public class Response<T> {
    int code;
    String msg;
    T data;

    public static Response successAsync(String msg) {
        Response response = new Response();
        response.code = ErrorCode.REQUEST_SUCCEEDED_ASYNC.getCode();
        response.msg = ErrorCode.REQUEST_SUCCEEDED_ASYNC.getMsg() + msg;
        return response;
    }

    public static Response successAsync(Object obj) {
        Response response = new Response();
        response.code = ErrorCode.REQUEST_SUCCEEDED_ASYNC.getCode();
        response.msg = ErrorCode.REQUEST_SUCCEEDED_ASYNC.getMsg();
        response.data = obj;
        return response;
    }

    public static Response success(Object obj) {
        Response response = new Response();
        response.code = ErrorCode.REQUEST_SUCCEEDED.getCode();
        response.msg = ErrorCode.REQUEST_SUCCEEDED.getMsg();
        response.data = obj;
        return response;
    }

    public static Response success(ErrorCode errorCode, Object obj) {
        Response response = new Response();
        response.code = errorCode.getCode();
        response.msg = errorCode.getMsg();
        response.data = obj;
        return response;
    }

    public static Response fail(Object obj) {
        Response response = new Response();
        response.code = ErrorCode.REQUEST_FAILED.getCode();
        response.msg = ErrorCode.REQUEST_FAILED.getMsg();
        response.data = obj;
        return response;
    }

    public static Response fail(ErrorCode errorCode, Object obj) {
        Response response = new Response();
        response.code = errorCode.getCode();
        response.msg = errorCode.getMsg();
        response.data = obj;
        return response;
    }
}
