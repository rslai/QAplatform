package com.bjy.qa.entity.performancetest;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.bjy.qa.entity.BaseEntity;
import com.bjy.qa.enumtype.CatalogType;
import com.bjy.qa.util.EnumUtil;
import com.gitee.sunchenbin.mybatis.actable.annotation.Column;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import com.gitee.sunchenbin.mybatis.actable.constants.MySqlTypeConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
@TableName("pt_test_suite")
@TableComment("性能测试套件表")
@ApiModel("性能测试套件 Entity 模型")
public class PerfTestSuite extends BaseEntity<PerfTestSuite> {
    @Column(isNull = false, comment = "所属项目 ID")
    @ApiModelProperty(value = "所属项目id", example = "1")
    Long projectId;

    @Column(isNull = false, length = 50, comment = "性能测试套件名称")
    @NotBlank(message = "name: 性能测试套件名称 字段必填")
    @ApiModelProperty(value = "性能测试套件名称", example = "用户登录接口压测")
    String name;

    @Column(isNull = false, type = MySqlTypeConstant.INT, comment = "用例类型")
    @ApiModelProperty(value = "套件类型", example = "1")
    CatalogType caseType;

    @Column(isNull = false, comment = "执行压测的 agent ID")
    @ApiModelProperty(value = "执行压测的 agent ID", example = "1")
    Long agentId;

    @Column(comment = "运行环境")
    @ApiModelProperty(value = "运行环境", example = "env")
    String env;

    @TableField(updateStrategy = FieldStrategy.IGNORED) // 设置此字段可以被更新为 null
    @Column(defaultValue = "0", comment = "测试完成后发送通知的机器人 ID")
    @ApiModelProperty(value = "测试完成后发送通知的机器人 ID", example = "1")
    Long robotId;

    @TableField(exist = false)
    @ApiModelProperty(value = "包含的测试用例")
    List<TestScript> testScripts;

    public void setCaseType(int caseType) {
        this.caseType = EnumUtil.valueOf(CatalogType.class, caseType);
    }
}
