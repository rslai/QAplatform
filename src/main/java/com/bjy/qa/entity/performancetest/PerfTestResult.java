package com.bjy.qa.entity.performancetest;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.bjy.qa.entity.BaseEntity;
import com.bjy.qa.enumtype.CatalogType;
import com.bjy.qa.enumtype.RunStatus;
import com.bjy.qa.util.EnumUtil;
import com.gitee.sunchenbin.mybatis.actable.annotation.Column;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import com.gitee.sunchenbin.mybatis.actable.constants.MySqlTypeConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

@Data
@TableName("pt_test_result")
@TableComment("性能测试结果表")
@ApiModel("性能测试结果 Entity 模型")
public class PerfTestResult extends BaseEntity<PerfTestResult> {
    @Column(isNull = false, comment = "性能测试套件id")
    @ApiModelProperty(value = "性能测试套件id", example = "1")
    Long suiteId;

    @Column(isNull = false, comment = "性能测试套件名称")
    @ApiModelProperty(value = "性能测试套件名称", example = "测试套件A")
    String suiteName;

    @Column(isNull = false, defaultValue="3", type = MySqlTypeConstant.INT, comment = "用例类型")
    @ApiModelProperty(value = "套件类型", example = "1")
    CatalogType caseType;

    @Column(isNull = false, comment = "项目id")
    @ApiModelProperty(value = "项目id", example = "1")
    Long projectId;

    @Column(isNull = false, length = 50, comment = "执行者")
    @ApiModelProperty(value = "执行者", example = "rslai")
    String runUser;

    @Column(isNull = false, type = MySqlTypeConstant.INT, comment = "状态")
    @ApiModelProperty(value = "状态", example = "RUNNING")
    RunStatus status;

    @Column(isNull = false, defaultValue="false", comment = "基线")
    @ApiModelProperty(value = "基线", example = "true")
    Boolean baseLine;

    @Column(comment = "总耗时")
    @ApiModelProperty(value = "总耗时", example = "2小时38分")
    String totalTime;

    @Column(type = MySqlTypeConstant.TIMESTAMP, comment = "结束时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSS") // set
    @ApiModelProperty(value = "结束时间", example = "2022-05-10 17:10:30.112")
    protected Date endAt;

    @TableField(exist = false)
    @ApiModelProperty(value = "性能套件关联的用例集", example = "")
    List<PerfTestResultTestScript> perfTestResultTestScriptList;

    public void setStatus(int status) {
        this.status = EnumUtil.valueOf(RunStatus.class, status);
    }

    public void setCaseType(int caseType) {
        this.caseType = EnumUtil.valueOf(CatalogType.class, caseType);
    }
}
