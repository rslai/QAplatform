package com.bjy.qa.entity.performancetest;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel("性能测试实时图表页签 - Chart 中显示的数据 DTO 模型")

public class ChartDTO {
    @ApiModelProperty(value = "测试结果 id", required = true, example = "1")
    Long resultId;

    @ApiModelProperty(value = "测试脚本 id", required = true, example = "1")
    Long testScriptId;

    @ApiModelProperty(value = "时间（x轴）", required = true, example = "['11:10:02', '11:10:07']")
    List<String> time;

    @ApiModelProperty(value = "用户数", required = true, example = "[0,5]")
    List<Integer> userCount;

    @ApiModelProperty(value = "每秒请求数", required = true, example = "[0,0.1]")
    List<Float> currentRequestsPerSec;

    @ApiModelProperty(value = "每秒失败数", required = true, example = "[0,0.1]")
    List<Float> currentFailPerSec;

    @ApiModelProperty(value = "50% 响应时间（中位数）", required = true, example = "[0,0.1]")
    List<Float> responseTimePercentile50;

    @ApiModelProperty(value = "95% 响应时间", required = true, example = "[0,0.1]")
    List<Float> responseTimePercentile95;
}
