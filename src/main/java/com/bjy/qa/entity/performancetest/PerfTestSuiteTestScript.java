package com.bjy.qa.entity.performancetest;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.bjy.qa.entity.BaseEntity;
import com.gitee.sunchenbin.mybatis.actable.annotation.Column;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@TableName("pt_test_suite_test_script")
@TableComment("性能测试套件 对应 性能测试脚本 关系表")
@ApiModel("性能测试套件 对应 性能测试脚本 Entity 模型")
public class PerfTestSuiteTestScript extends BaseEntity<PerfTestSuiteTestScript> {
    @TableField
    @Column(isNull = false, comment = "性能测试套件 id")
    @ApiModelProperty(value = "性能测试套件 id", example = "1")
    private Long perfTestSuiteId;

    @TableField
    @Column(isNull = false, comment = "性能测试脚本 id")
    @ApiModelProperty(value = "性能测试脚本 id", example = "1")
    private Long testScriptId;

    @TableField
    @Column(isNull = false, comment = "性能测试脚本 id")
    @ApiModelProperty(value = "性能测试脚本 id", example = "1")
    private Long scenarioId;

    @Column(isNull = false, comment = "排序")
    @ApiModelProperty(value = "排序", example = "100")
    Integer sort = 9999;
}
