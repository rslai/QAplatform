package com.bjy.qa.entity.performancetest;

import com.bjy.qa.enumtype.ScriptType;
import com.bjy.qa.util.EnumUtil;
import com.gitee.sunchenbin.mybatis.actable.annotation.Column;
import com.gitee.sunchenbin.mybatis.actable.constants.MySqlTypeConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
@ApiModel("性能测试脚本 DTO 模型")
public class TestScriptDTO {
    @ApiModelProperty(value = "测试结果 id", required = true, example = "1")
    Long rid;

    @ApiModelProperty(value = "测试用例 id", required = true, example = "1")
    Long cid;

    @ApiModelProperty(value = "项目信息 id", required = true, example = "1")
    Long pid;

    @ApiModelProperty(value = "性能测试脚本类型", required = true, example = "1")
    ScriptType scriptType;

    @ApiModelProperty(value = "用例名称", required = true, example = "测试 getVT 接口")
    String name;

    @Column(type = MySqlTypeConstant.LONGTEXT, comment = "DataProivder（数据驱动）")
    @ApiModelProperty(value = "DataProivder（数据驱动）", example = "[{\"A\":\"1\"},{\"A\":\"2\"},{}]")
    String dataProvider;

    @ApiModelProperty(value = "脚本内容", example = "脚本内容")
    String content;

    @ApiModelProperty(value = "使用的压力机")
    List<Generator> generators;

    @ApiModelProperty(value = "包含性能测试场景")
    Scenario scenario;

    @ApiModelProperty(value = "全局变量", example = "{'roomID':'12345'}")
    Map<String, String> gp;

    @ApiModelProperty(value = "下发 debug 信令时作为唯一标识", example = "29002272-4659-4808-a804-08ce3388b136")
    String sessionId;

    public void setScriptType(int scriptType) {
        this.scriptType = EnumUtil.valueOf(ScriptType.class, scriptType);
    }
}
