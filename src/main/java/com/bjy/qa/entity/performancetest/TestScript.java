package com.bjy.qa.entity.performancetest;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.bjy.qa.entity.BaseEntity;
import com.bjy.qa.enumtype.CatalogType;
import com.bjy.qa.enumtype.EditStatus;
import com.bjy.qa.enumtype.Priority;
import com.bjy.qa.enumtype.ScriptType;
import com.bjy.qa.util.EnumUtil;
import com.gitee.sunchenbin.mybatis.actable.annotation.Column;
import com.gitee.sunchenbin.mybatis.actable.annotation.Index;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import com.gitee.sunchenbin.mybatis.actable.constants.MySqlTypeConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@TableName("pt_test_script")
@TableComment("性能测试脚本表")
@ApiModel("性能测试脚本 Entity 模型")
public class TestScript extends BaseEntity<TestScript> {
    @Column(isNull = false, comment = "所属项目id")
    @Index(value = "IDX_PROJECT_ID")
    @NotNull(message = "projectId: 项目信息 id 必填")
    @ApiModelProperty(value = "项目信息 id", example = "1")
    Long projectId;

    @Column(isNull = false, type = MySqlTypeConstant.INT, comment = "性能测试脚本类型")
    @NotNull(message = "scriptType: 性能测试脚本类型必填")
    @ApiModelProperty(value = "性能测试脚本类型", required = true, example = "1")
    ScriptType scriptType;

    @Column(isNull = false, comment = "所属分类 id")
    @NotNull(message = "catalogId: 所属分类 ID 必填")
    @ApiModelProperty(value = "所属分类 ID", required = true, example = "0")
    Long catalogId;

    @Column(isNull = false, type = MySqlTypeConstant.INT, comment = "分类类型")
    @NotNull(message = "type: 分类类型必填")
    @ApiModelProperty(value = "分类类型", required = true, example = "15")
    CatalogType type;

    @Column(isNull = false, type = MySqlTypeConstant.INT, comment = "优先级")
    @NotNull(message = "priority: 优先级 必填")
    @ApiModelProperty(value = "优先级", example = "0")
    Priority priority;

    @Column(isNull = false, type = MySqlTypeConstant.INT, comment = "用例编写状态")
    @NotNull(message = "editStatus: 用例编写状态 必填")
    @ApiModelProperty(value = "用例编写状态", example = "1")
    EditStatus editStatus;

    @Column(length = 150, isNull = false, comment = "用例名称")
    @NotNull(message = "name: 用例名称必填")
    @ApiModelProperty(value = "用例名称", example = "测试 getVT 接口")
    String name;

    @Column(type = MySqlTypeConstant.LONGTEXT, comment = "DataProivder（数据驱动）")
    @ApiModelProperty(value = "DataProivder（数据驱动）", example = "[{\"A\":\"1\"},{\"A\":\"2\"},{}]")
    String dataProvider;

    @Column(length = 50, isNull = false, comment = "用例设计人")
    @ApiModelProperty(value = "用例设计人", example = "1")
    Long userId;

    @Column(isNull = false, comment = "排序")
    @ApiModelProperty(value = "排序", example = "100")
    Integer sort = 9999;

    @Column(isNull = false, type = MySqlTypeConstant.LONGTEXT, comment = "脚本内容")
    @NotNull(message = "content: 脚本内容 必填")
    @ApiModelProperty(value = "脚本内容", example = "脚本内容")
    String content;

    @TableField(exist = false)
    @ApiModelProperty(value = "使用的压力机")
    List<Generator> generators;

    @TableField(exist = false)
    @ApiModelProperty(value = "包含性能测试场景")
    Scenario scenario;

    public void setType(int type) {
        this.type = EnumUtil.valueOf(CatalogType.class, type);
    }

    public void setPriority(int priority) {
        this.priority = EnumUtil.valueOf(Priority.class, priority);
    }

    public void setEditStatus(int editStatus) {
        this.editStatus = EnumUtil.valueOf(EditStatus.class, editStatus);
    }

    public void setScriptType(int scriptType) {
        this.scriptType = EnumUtil.valueOf(ScriptType.class, scriptType);
    }
}
