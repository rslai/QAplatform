package com.bjy.qa.entity.performancetest;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.bjy.qa.entity.BaseEntity;
import com.bjy.qa.enumtype.SystemType;
import com.bjy.qa.util.EnumUtil;
import com.gitee.sunchenbin.mybatis.actable.annotation.Column;
import com.gitee.sunchenbin.mybatis.actable.annotation.Index;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import com.gitee.sunchenbin.mybatis.actable.constants.MySqlTypeConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@TableName("pt_generator")
@TableComment("压力机（load generators）表")
@ApiModel("压力机（load generators）Entity 模型")
public class Generator extends BaseEntity<Generator> {
    @Column(isNull = false, comment = "所属项目id")
    @Index(value = "IDX_PROJECT_ID")
    @NotNull(message = "projectId: 项目信息 id 必填")
    @ApiModelProperty(value = "项目信息 id", example = "1")
    Long projectId;

    @Column(type = MySqlTypeConstant.INT, comment = "运行系统")
    @ApiModelProperty(value = "运行系统", example = "2")
    @NotNull(message = "systemType: 操作系统类型 字段必填")
    private SystemType systemType;

    @Column(length = 50, comment = "主机名")
    @ApiModelProperty(value = "主机名", example = "test-01")
    private String hostname;

    @Column(length = 50, comment = "IP 地址")
    @ApiModelProperty(value = "IP 地址", example = "192.168.1.1")
    @NotBlank(message = "ip: IP地址 字段必填")
    private String ip;

    @Column(comment = "端口号")
    @ApiModelProperty(value = "IP 地址", example = "65535")
    @NotNull(message = "port: 端口号 字段必填")
    private int port;

    @Column(length = 25, comment = "工作目录")
    @ApiModelProperty(value = "工作目录", example = "/")
    private String workDir;

    @Column(length = 1024, comment = "预执行命令")
    @ApiModelProperty(value = "预执行命令，登录成功后需要先执行的命令", example = "sudo su -")
    private String runCmd;

    @Column(type = MySqlTypeConstant.LONGTEXT, comment = "扩展信息（用户名、密码等）")
    @ApiModelProperty(value = "扩展信息（用户名、密码等）", example = "{\"username\":\"aaa\",\"password\":\"xxx\"}")
    String extra;

    @Column(isNull = false, length=100, comment = "描述")
    @TableField("`desc`")
    @ApiModelProperty(value = "描述", example = "执行成功！")
    String desc;

    public void setSystemType(int systemType) {
        this.systemType = EnumUtil.valueOf(SystemType.class, systemType);
    }
}
