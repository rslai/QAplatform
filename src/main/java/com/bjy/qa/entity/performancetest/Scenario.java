package com.bjy.qa.entity.performancetest;

import com.baomidou.mybatisplus.annotation.TableName;
import com.bjy.qa.entity.BaseEntity;
import com.bjy.qa.enumtype.ControlMode;
import com.bjy.qa.enumtype.LoadTestMode;
import com.bjy.qa.enumtype.LogMode;
import com.bjy.qa.util.EnumUtil;
import com.gitee.sunchenbin.mybatis.actable.annotation.Column;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import com.gitee.sunchenbin.mybatis.actable.constants.MySqlTypeConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@TableName("pt_scenario")
@TableComment("性能测试场景 表")
@ApiModel("性能测试场景 Entity 模型")
public class Scenario extends BaseEntity<Scenario> {
    @Column(isNull = false, comment = "所属项目 ID")
    @ApiModelProperty(value = "所属项目id", example = "1")
    Long projectId;

    @Column(isNull = false, type = MySqlTypeConstant.INT, comment = "日志模式")
    @NotNull(message = "type: 日志模式 必填")
    @ApiModelProperty(value = "日志模式", required = true, example = "1")
    LogMode logMode;

    @Column(isNull = false, type = MySqlTypeConstant.INT, comment = "控制模式")
    @NotNull(message = "type: 控制模式 必填")
    @ApiModelProperty(value = "控制模式", required = true, example = "1")
    ControlMode controlMode;

    @Column(isNull = false, type = MySqlTypeConstant.INT, comment = "压测模式")
    @NotNull(message = "type: 压测模式 必填")
    @ApiModelProperty(value = "压测模式", required = true, example = "1")
    LoadTestMode loadTestMode;

    @Column(isNull = false, type = MySqlTypeConstant.LONGTEXT, comment = "测试场景内容")
    @NotNull(message = "content: 测试场景内容 必填")
    @ApiModelProperty(value = "测试场景内容", example = "测试场景内容")
    String content;

    public void setLogMode(int mode) {
        this.logMode = EnumUtil.valueOf(LogMode.class, mode);
    }

    public void setControlMode(int mode) {
        this.controlMode = EnumUtil.valueOf(ControlMode.class, mode);
    }

    public void setLoadTestMode(int mode) {
        this.loadTestMode = EnumUtil.valueOf(LoadTestMode.class, mode);
    }
}
