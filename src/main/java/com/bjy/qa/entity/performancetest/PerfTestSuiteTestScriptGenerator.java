package com.bjy.qa.entity.performancetest;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.bjy.qa.entity.BaseEntity;
import com.gitee.sunchenbin.mybatis.actable.annotation.Column;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@TableName("pt_test_suite_test_script_generator")
@TableComment("性能测试套件 中 性能测试脚本 对应 压力机 关系表")
@ApiModel("性能测试套件 中 性能测试脚本 对应 压力机 Entity 模型")
public class PerfTestSuiteTestScriptGenerator extends BaseEntity<PerfTestSuiteTestScriptGenerator> {
    @TableField
    @Column(isNull = false, comment = "性能测试套件 对应 性能测试脚本 id")
    @ApiModelProperty(value = "性能测试套件 对应 性能测试脚本 id", example = "1")
    private Long perfTestSuiteTestScriptId;

    @TableField
    @Column(isNull = false, comment = "压力机 id")
    @ApiModelProperty(value = "压力机 id", example = "1")
    private Long generatorId;
}
