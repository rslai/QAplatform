package com.bjy.qa.entity.performancetest;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.bjy.qa.entity.BaseEntity;
import com.bjy.qa.enumtype.PerfLogType;
import com.bjy.qa.util.EnumUtil;
import com.gitee.sunchenbin.mybatis.actable.annotation.Column;
import com.gitee.sunchenbin.mybatis.actable.annotation.Index;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import com.gitee.sunchenbin.mybatis.actable.constants.MySqlTypeConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@TableName("pt_test_result_report")
@TableComment("性能测试结果报告表")
@ApiModel("性能测试结果报告 Entity 模型")
public class PerfTestResultReport extends BaseEntity<PerfTestResultReport> {
    @Column(isNull = false, comment = "测试结果 id")
    @ApiModelProperty(value = "测试结果 id", example = "1")
    @Index(columns = "result_id, test_script_id")
    Long resultId;

    @Column(isNull = false, comment = "测试脚本 id")
    @ApiModelProperty(value = "测试脚本 id", example = "1")
    Long testScriptId;

    @Column(isNull = false, type = MySqlTypeConstant.INT, comment = "性能测试日志类型")
    @ApiModelProperty(value = "性能测试日志类型", example = "1")
    PerfLogType logType;

    @Column(length=100, comment = "类型(/stats/requests/csv、/exceptions/csv、/stats/failures/csv)")
    @ApiModelProperty(value = "类型(/stats/requests/csv、/exceptions/csv、/stats/failures/csv)", example = "/stats/failures/csv")
    @TableField("`desc`")
    String desc;

    @Column(isNull = false, type = MySqlTypeConstant.LONGTEXT, comment = "数据详情")
    @ApiModelProperty(value = "数据详情", example = "{\"headers\":{\"nodes\":\"Nodes\",\"count\":\"Count\",\"message\":\"Message\",\"traceback\":\"Traceback\"},\"body\":[]}")
    String data;

    public void setType(int type) {
        this.logType = EnumUtil.valueOf(PerfLogType.class, type);
    }
}
