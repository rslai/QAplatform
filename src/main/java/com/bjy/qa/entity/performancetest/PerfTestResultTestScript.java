package com.bjy.qa.entity.performancetest;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.bjy.qa.entity.BaseEntity;
import com.bjy.qa.enumtype.RunStatus;
import com.bjy.qa.util.EnumUtil;
import com.gitee.sunchenbin.mybatis.actable.annotation.Column;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import com.gitee.sunchenbin.mybatis.actable.constants.MySqlTypeConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@TableName("pt_test_result_test_script")
@TableComment("性能测试结果 对应 性能测试脚本 表")
@ApiModel("性能测试结果 对应 性能测试脚本 表 Entity 模型")
public class PerfTestResultTestScript extends BaseEntity<PerfTestResultTestScript> {
    @Column(isNull = false, type = MySqlTypeConstant.INT,comment = "状态")
    @ApiModelProperty(value = "状态", example = "1")
    RunStatus status;

    @Column(isNull = false, comment = "性能测试结果 id")
    @ApiModelProperty(value = "性能测试结果 id", example = "1")
    Long resultId;

    @Column(isNull = false, comment = "测试脚本 id")
    @ApiModelProperty(value = "测试脚本 id", example = "1")
    Long  testScriptId;

    @TableField(exist = false)
    @ApiModelProperty(value = "测试脚本名称", example = "xxx测试用例")
    String testScriptName;

    @Column(isNull = false, comment = "运行设备")
    @ApiModelProperty(value = "运行设备", example = "1")
    Long agent;

    @TableField(exist = false)
    @ApiModelProperty(value = "运行设备名称", example = "武汉 agent")
    String agentName;

    public void setStatus(int status) {
        this.status = EnumUtil.valueOf(RunStatus.class, status);
    }
}
