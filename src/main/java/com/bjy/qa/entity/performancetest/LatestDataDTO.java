package com.bjy.qa.entity.performancetest;

import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("性能测试实时图表页签 - 表格中显示的数据 DTO 模型")
public class LatestDataDTO {
    @ApiModelProperty(value = "测试结果 id", required = true, example = "1")
    Long resultId;

    @ApiModelProperty(value = "测试脚本 id", required = true, example = "1")
    Long testScriptId;

    @ApiModelProperty(value = "请求相关数据", required = true, example = "{}")
    JSONObject requests = new JSONObject();

    @ApiModelProperty(value = "异常数据", required = true, example = "{}")
    JSONObject exceptions = new JSONObject();

    @ApiModelProperty(value = "task 数据", required = true, example = "{}")
    JSONObject tasks = new JSONObject();
}
