package com.bjy.qa.entity.ssh;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("SSH 运行结果 Entity 模型")
public class Results {
    @ApiModelProperty(value = "状态", example = "true")
    boolean status;

    @ApiModelProperty(value = "执行的命令", example = "ls")
    String cmd;

    @ApiModelProperty(value = "执行结果", example = "db     audit")
    String body;

    @ApiModelProperty(value = "提示符", example = "张三 /home %")
    String prompt;
}
