package com.bjy.qa.entity.ssh;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel("SSH服务器 Entity 模型")
public class SSHServer {
    @ApiModelProperty(value = "SSH 服务器 host 或 ip", example = "172.20.1.10")
    String host;

    @ApiModelProperty(value = "用户名", example = "zhangsan")
    String userName;

    @ApiModelProperty(value = "密码", example = "111111")
    String password;

    @ApiModelProperty(value = "端口", example = "2223")
    int port;

    @ApiModelProperty(value = "是否登录的是 jumpServer", example = "true")
    boolean jumpServer;

    @ApiModelProperty(value = "需要执行的命令 List", example = "[\"cmd\", \"ls -l\"]")
    List<String> cmdList;

    @Override
    public String toString() {
        return "SSHServer{" +
                "host='" + host + '\'' +
                ", userName='" + userName + '\'' +
                ", password='******'" +
                ", port=" + port +
                ", jumpServer=" + jumpServer +
                ", cmdList=" + cmdList +
                '}';
    }
}
