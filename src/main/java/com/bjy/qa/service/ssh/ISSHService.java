package com.bjy.qa.service.ssh;

import com.bjy.qa.entity.ssh.Results;
import com.bjy.qa.entity.ssh.SSHServer;
import com.jcraft.jsch.JSchException;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeoutException;

public interface ISSHService {

    List<Results> executeCmd(SSHServer sshServer) throws JSchException, IOException, TimeoutException;

}
