package com.bjy.qa.service.functionaltest;

import com.bjy.qa.entity.MyPage;
import com.bjy.qa.entity.ResponsePagingData;
import com.bjy.qa.entity.functionaltest.Agent;

import java.util.List;
import java.util.Map;

public interface IAgentService {
    /**
     * agent 登录并返回 agent 的配置信息
     * @param agent agent 对象
     * @return
     */
    Map<String, Object> agentSignOn(Agent agent);

    /**
     * 返回所有记录
     * @return
     */
    List<Agent> listAll();

    /**
     * 分页查询 agent 列表
     * @param myPage 分页查询参数对象，包括查询条件、分页信息等
     * @return
     */
    ResponsePagingData list(MyPage<Agent> myPage);

    /**
     * 新增 agent
     * @param agent agent 对象
     * @return
     */
    Object add(Agent agent);

    /**
     * 修改 agent
     * @param agent agent 对象
     * @return
     */
    int update(Agent agent);

    /**
     * 重启 agent
     * @param key agentKey
     * @return
     */
    Object reboot(String key);

    /**
     * 获取一个在线的 agent
     * @return 返回一个在线的 agent 或 null
     */
    Agent getOnlineAgent();

    /**
     * 给 agent 下发一个命令
     * @param agentKey agentKey
     * @param cmdStr 命令字符串
     * @param msg 要发送的内容
     * @return
     */
    boolean pubCmd(String agentKey, String cmdStr, String msg);
}
