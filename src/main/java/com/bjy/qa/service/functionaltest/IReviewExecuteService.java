package com.bjy.qa.service.functionaltest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bjy.qa.entity.Response;
import com.bjy.qa.entity.functionaltest.BatchReviewsExecuteDataDTO;
import com.bjy.qa.entity.functionaltest.FullTree;
import com.bjy.qa.entity.functionaltest.Reviews;
import com.bjy.qa.entity.functionaltest.ReviewsExecuteDataDTO;
import com.bjy.qa.enumtype.CatalogType;

import java.util.List;

public interface IReviewExecuteService extends IService<Reviews> {

    /**
     * 测试执行中查询 tree 完整路径，只返回在测试套件中被选中的 测试用例
     * @param type 分类类型 枚举类
     * @param caseType 用例类型 枚举类
     * @param projectId 项目ID
     * @param reviewsId 评审详情表 ID
     * @param parentId tree 节点 ID
     * @return
     */
    List<FullTree> listTree(CatalogType type, CatalogType caseType, Long projectId, Long reviewsId, Long parentId);

    /**
     * 更新测试用例执行状态
     * @param reviewsExecuteDataDTO
     * @param runUser
     * @return
     */
    Response<String> updateCaseStatus(ReviewsExecuteDataDTO reviewsExecuteDataDTO, String runUser);

    /**
     * 批量更新测试用例执行状态
     * @param batchReviewsExecuteDataDTO 批量更新用例执行状态的DTO
     * @param runUser 编辑用户名
     * @return
     */
    Response<String> batchUpdateCaseStatus(BatchReviewsExecuteDataDTO batchReviewsExecuteDataDTO, String runUser);
}
