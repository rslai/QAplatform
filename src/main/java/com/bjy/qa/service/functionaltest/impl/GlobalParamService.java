package com.bjy.qa.service.functionaltest.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bjy.qa.dao.functionaltest.EnvParamDao;
import com.bjy.qa.dao.functionaltest.GlobalParamDao;
import com.bjy.qa.entity.functionaltest.EnvParam;
import com.bjy.qa.entity.functionaltest.GlobalParam;
import com.bjy.qa.entity.functionaltest.GlobalParamDTO;
import com.bjy.qa.exception.MyException;
import com.bjy.qa.service.functionaltest.IGlobalParamService;
import com.bjy.qa.util.security.SecurityUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import static com.bjy.qa.enumtype.GlobalParamScope.SUIT_UNIQUENESS;
import static com.bjy.qa.enumtype.GlobalParamType.*;
import static com.bjy.qa.util.security.TripleDESUtil.decrypt;
import static com.bjy.qa.util.security.TripleDESUtil.encrypt;


@Service
public class GlobalParamService extends ServiceImpl<GlobalParamDao, GlobalParam> implements IGlobalParamService {
    @Resource
    EnvParamDao envParamDao;

    @Resource
    GlobalParamDao globalParamDao;

    @Value("${qa-platform.secret-key}")
    String secretKey;

    @Override
    public List<GlobalParam> list(Long projectId, Long userId) throws IllegalBlockSizeException, NoSuchPaddingException, BadPaddingException, NoSuchAlgorithmException, InvalidKeyException {
        List<GlobalParam> globalParams = globalParamDao.list(projectId, userId);
        for (GlobalParam globalParam: globalParams) {
            if (globalParam.getGlobalParamType() == USER_PARAMETER) {
                globalParam.setValue(decrypt(globalParam.getValue(), secretKey));
            }
        }
        return globalParams;
    }

    @Override
    public boolean createOrUpdate(GlobalParam globalParam) throws Exception {
        if (globalParamDao.selectUserParamByName(globalParam.getProjectId(), globalParam.getId(), globalParam.getParamName(), SecurityUtils.getCurrentUserId()) != null) {
            throw new MyException("操作失败,该参数已存在,不可重复添加");
        }
        if (globalParam.getGlobalParamType() == STATIC_PARAMETER ) {
            globalParam.setLength(0);
            globalParam.setUserId(0L);
            globalParam.setGlobalParamScope(SUIT_UNIQUENESS.getValue());
        } else if (globalParam.getGlobalParamType() == DYNAMIC_PARAMETER) {
            globalParam.setUserId(0L);
        } else if (globalParam.getGlobalParamType() == USER_PARAMETER) {
            globalParam.setLength(0);
            globalParam.setUserId(SecurityUtils.getCurrentUserId());
            globalParam.setValue(encrypt(globalParam.getValue(), secretKey));
            globalParam.setGlobalParamScope(SUIT_UNIQUENESS.getValue());
        }
        if (globalParam.getId() == null || globalParam.getId() == 0) {
            globalParamDao.insert(globalParam);
        } else {
            globalParamDao.updateById(globalParam);
        }
        return true;
    }

    @Override
    public GlobalParamDTO getAllGlobalParam(Long projectId, String envName, Long userId) {
        GlobalParamDTO globalParamDto = new GlobalParamDTO();

        // 不传 用户ID 从当前登录用户中获取。传了，用传的用户ID
        if (userId == null) {
            userId = SecurityUtils.getCurrentUserId();
        }

        // 查询环境参数
        if (StringUtils.isNotBlank(envName)) {
            List<EnvParam> envParams = envParamDao.listEnvParamsByName(projectId, envName);
            if (envParams.size() != 0) {
                globalParamDto.setEnvParams(envParams.get(0).getEnvParams());
            }
        }

        // 查询全局参数
        globalParamDto.setGlobalParams(globalParamDao.list(projectId, userId));
        return globalParamDto;
    }
}
