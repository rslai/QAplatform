package com.bjy.qa.service.functionaltest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bjy.qa.entity.MyPage;
import com.bjy.qa.entity.ResponsePagingData;
import com.bjy.qa.entity.functionaltest.*;

import java.util.List;
import java.util.Map;

public interface ITestSuiteService extends IService<TestSuite> {

    /**
     * 新增套件
     * @param testSuit
     * @return
     */
    int add(TestSuite testSuit);

    /**
     * 更新套件
     * @param testSuit
     * @return
     */
    int update(TestSuite testSuit);

    /**
     * 返回所有记录
     * @return
     */
    List<TestSuite> selectAll(TestSuite testSuit);

    /**
     * 分页查询套件列表
     * @param myPage
     * @return
     */
    ResponsePagingData list(MyPage<TestSuite> myPage);

    /**
     * 根据id查找套件信息
     * @param id 套件id
     * @return
     */
    TestSuite findById(Long id);

    /**
     * 删除一条数据
     * @param id 套件id
     * @return
     */
    int delete(Long id);

    /**
     * 执行测试套件
     * @param id 套件id
     * @param userId 不传 用户ID 从当前登录用户中获取。传了，用传的 用户ID
     * @param runUser  执行者
     * @return
     */
    TestResultSuite runSuite(Long id, Long userId, String runUser);

    /**
     * 停止测试套件
     * @param id 套件id
     * @param runUser  执行者
     * @return
     */
    boolean forceStopSuite(Long id, String runUser);

    /**
     * 执行测试套件
     * @param testSuite 测试套件
     * @param testCasesList 测试用例列表
     * @param agentsList agent 列表
     * @param userId 用户id
     * @param runUser 运行用户
     * @return
     */
    TestResultSuite runSuite(TestSuite testSuite, List<TestCase> testCasesList, List<Agent> agentsList, Long userId, String runUser);

    /**
     * 根据 项目id 和 环境，得到参数 map
     * 得到两个数据对象：
     *      globalParamMap：全局参数（环境参数 + 静态参数 + 用户参数 + 套件唯一参数）
     *      caseUniqUniquenessParamMap：用例唯一参数
     * @param projectID 项目id
     * @param env 环境
     * @param userId 不传 用户ID 从当前登录用户中获取。传了，用传的 用户ID
     * @param globalParamMap 全局参数（环境参数 + 静态参数 + 用户参数 + 套件唯一参数）
     * @param caseUniqUniquenessParams 用例唯一参数列表
     * @return 唯一值（由于动态参数有时间戳，短时间产生多个时间戳会有重复现象，所以每产生一个动态参数都会给这个 int 值 + 1）
     * @throws Exception
     */
    int getParamMap(Long projectID, String env, Long userId, Map<String, String> globalParamMap, List<GlobalParam> caseUniqUniquenessParams);

    /**
     * 设为 或 取消 保留
     * @param id 测试结果 ID
     * @return
     */
    TestResultSuite updateReserved(Long id);
}
