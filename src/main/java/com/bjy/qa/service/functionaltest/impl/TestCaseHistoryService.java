package com.bjy.qa.service.functionaltest.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bjy.qa.dao.functionaltest.TestCaseHistoryDao;
import com.bjy.qa.entity.functionaltest.Step;
import com.bjy.qa.entity.functionaltest.StepHistory;
import com.bjy.qa.entity.functionaltest.TestCase;
import com.bjy.qa.entity.functionaltest.TestCaseHistory;
import com.bjy.qa.service.functionaltest.IStepHistoryService;
import com.bjy.qa.service.functionaltest.ITestCaseHistoryService;
import com.bjy.qa.util.security.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class TestCaseHistoryService extends ServiceImpl<TestCaseHistoryDao, TestCaseHistory> implements ITestCaseHistoryService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    TestCaseHistoryDao testCaseHistoryDao;

    @Resource
    IStepHistoryService iStepHistoryService;

    @Override
    public boolean add(TestCase testCase) {
        TestCaseHistory testCaseHistory = JSON.parseObject(JSON.toJSONString(testCase), TestCaseHistory.class);
        testCaseHistory.setTestCaseId(testCase.getId());
        testCaseHistory.setHistoryDesigner(SecurityUtils.getCurrentUserName());
        testCaseHistory.setUpdatedAt(null);
        testCaseHistory.setCreatedAt(null);
        testCaseHistory.setId(null);
        testCaseHistoryDao.insert(testCaseHistory);

        this.recursionSaveStepHistory(testCaseHistory.getId(), 0L, testCase.getSteps()); // 递归 保存 步骤历史记录列表

        return true;
    }

    /**
     * 递归 保存 步骤历史记录列表
     * @param historyTestCaseId 测试用例在历史表中的 ID（这些 step 属于那个 测试用例）
     * @param historyParentId step 在历史表中的的 父 ID
     * @param stepList 步骤列表
     * @return
     */
    private boolean recursionSaveStepHistory(Long historyTestCaseId, Long historyParentId, List<Step> stepList) {
        /**
         * 保存或更新 当前层级的 步骤列表
         */
        List<StepHistory> stepHistories = JSON.parseArray(JSON.toJSONString(stepList), StepHistory.class);
        for (StepHistory stepHistory : stepHistories) {
            stepHistory.setTestCaseHistoryId(historyTestCaseId);
            stepHistory.setHistoryParentId(historyParentId);
            stepHistory.setStepId(stepHistory.getId());
            stepHistory.setId(null);
            stepHistory.setUpdatedAt(null);
            stepHistory.setCreatedAt(null);
        }
        iStepHistoryService.saveBatch(stepHistories);

        /**
         * 循环 保存或更新 下一层级的 步骤列表
         */
        for (StepHistory stepHistory : stepHistories) {
            if (stepHistory.getChildSteps() != null && stepHistory.getChildSteps().size() > 0) {
                this.recursionSaveStepHistory(historyTestCaseId, stepHistory.getId(), stepHistory.getChildSteps());
            }
        }

        return true;
    }

    @Override
    public List<TestCaseHistory> selectByTestCaseId(Long projectId, Long testCaseId) {
        return testCaseHistoryDao.selectByTestCaseId(projectId, testCaseId);
    }
}
