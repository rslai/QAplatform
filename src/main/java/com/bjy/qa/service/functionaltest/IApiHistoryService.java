package com.bjy.qa.service.functionaltest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bjy.qa.entity.functionaltest.Api;
import com.bjy.qa.entity.functionaltest.ApiHistory;

import java.util.List;

public interface IApiHistoryService extends IService<ApiHistory> {
    /**
     * 添加 api 历史记录
     * @param api api
     * @param userName 用户名（作者）
     * @return
     */
    ApiHistory add(Api api, String userName);

    /**
     * 根据 apiId 查询历史记录列表
     * @param projectId 项目 id
     * @param apiId 接口 id
     * @return
     */
    List<ApiHistory> selectByApiId(Long projectId, Long apiId);
}
