package com.bjy.qa.service.functionaltest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bjy.qa.entity.functionaltest.TestSuiteAgent;

public interface ITestSuiteAgentService extends IService<TestSuiteAgent> {

}
