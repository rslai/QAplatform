package com.bjy.qa.service.functionaltest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bjy.qa.entity.functionaltest.EnvParam;

import java.util.List;

public interface IEnvParamService extends IService<EnvParam> {

    Boolean saveOrUpdateServer(EnvParam envParam);

    Boolean saveOrUpdateEnv(EnvParam envParam);

    EnvParam selectIdByEnvName(Long projectId, String envName);

    int deleteEnvByName(Long projectId, String envName);

    List<EnvParam> listEnvParamsByName(Long projectId, String envName);

}
