package com.bjy.qa.service.functionaltest.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bjy.qa.dao.functionaltest.ReviewTestCaseDao;
import com.bjy.qa.entity.functionaltest.ReviewTestCase;
import org.springframework.stereotype.Service;

@Service
public class ReviewTestCaseService extends ServiceImpl<ReviewTestCaseDao,ReviewTestCase> {
}
