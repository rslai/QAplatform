package com.bjy.qa.service.functionaltest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bjy.qa.entity.functionaltest.TestCase;
import com.bjy.qa.entity.functionaltest.TestCaseHistory;

import java.util.List;

public interface ITestCaseHistoryService extends IService<TestCaseHistory> {
    /**
     * 添加测试用例历史记录
     * @param testCase 测试用例
     * @return
     */
    boolean add(TestCase testCase);

    /**
     * 根据测试用例 id 查询历史记录列表
     * @param projectId 项目 id
     * @param testCaseId 测试用例 id
     * @return
     */
    List<TestCaseHistory> selectByTestCaseId(Long projectId, Long testCaseId);
}
