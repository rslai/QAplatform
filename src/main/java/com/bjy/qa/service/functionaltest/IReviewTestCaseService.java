package com.bjy.qa.service.functionaltest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bjy.qa.entity.functionaltest.ReviewTestCase;

public interface IReviewTestCaseService extends IService<ReviewTestCase> {
}
