package com.bjy.qa.service.functionaltest;

import com.bjy.qa.entity.functionaltest.*;
import com.bjy.qa.enumtype.FileType;

import java.util.List;

public interface IApiService {
    /**
     * 返回所有记录
     * @return
     */
    List<Api> selectAll();

    /**
     * 根据 id 查询数据
     * @param type
     * @param id
     * @return
     */
    Api selectById(int type, Long id);

    /**
     * 新增一条数据
     * @param api api
     * @param userName 用户名（作者）
     * @return
     */
    Api add(Api api, String userName);

    /**
     * 更新一条数据
     * @param api api
     * @param userName 用户名（作者）
     * @return
     */
    Api update(Api api, String userName);

    /**
     * 删除一条数据
     * @param id ID
     * @return
     */
    int delete(Long id);

    /**
     * 搜索 api 定义信息
     * @param searchDTO 搜索 DTO 模型
     * @return
     */
    List<Api> search(SearchDTO searchDTO);

    /**
     * 导入接口定义
     * @param taskId 任务 ID
     * @param fileType 文件类型
     * @param filePath 文件路径
     * @param projectId 项目 ID
     * @param userName 用户名
     * @return
     */
    Object apiFileImport(String taskId, FileType fileType, String filePath, Long projectId, String userName);

    /**
     * 发送 Api 调试请求
     * @param apiDebugDTO Api 调试 DTO 模型
     * @return
     */
    Object apiDebug(ApiDebugDTO apiDebugDTO);

    /**
     * 获取 Api 调试 结果
     * @param uuid uuid
     * @param timestamp debug 任务下发的时间戳
     * @return
     */
    ApiLogDTO getApiDebugStatus(String uuid, Long timestamp);

    /**
     * cURL 导入接口定义
     * @param curlImportDTO cURL Import DTO 模型
     * @return
     */
    Api apiCurlImport(CurlImportDTO curlImportDTO);

    /**
     * API 导出
     * @param type api 类型
     * @param id API id
     * @return
     */
    String apiExport(int type, Long id);
}