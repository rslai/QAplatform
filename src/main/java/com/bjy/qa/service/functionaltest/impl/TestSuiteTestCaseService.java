package com.bjy.qa.service.functionaltest.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bjy.qa.dao.functionaltest.TestSuiteTestCaseDao;
import com.bjy.qa.entity.functionaltest.TestSuiteTestCase;
import com.bjy.qa.service.functionaltest.ITestSuiteTestCaseService;
import org.springframework.stereotype.Service;

@Service
public class TestSuiteTestCaseService extends ServiceImpl<TestSuiteTestCaseDao, TestSuiteTestCase> implements ITestSuiteTestCaseService {

}
