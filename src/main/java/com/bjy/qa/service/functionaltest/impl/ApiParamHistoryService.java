package com.bjy.qa.service.functionaltest.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bjy.qa.dao.functionaltest.ApiParamHistoryDao;
import com.bjy.qa.entity.functionaltest.ApiParamHistory;
import com.bjy.qa.service.functionaltest.IApiParamHistoryService;
import org.springframework.stereotype.Service;

@Service
public class ApiParamHistoryService extends ServiceImpl<ApiParamHistoryDao, ApiParamHistory> implements IApiParamHistoryService {

}
