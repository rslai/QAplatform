package com.bjy.qa.service.functionaltest.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bjy.qa.dao.functionaltest.ReviewsResultDao;
import com.bjy.qa.entity.functionaltest.ReviewsResult;
import com.bjy.qa.service.functionaltest.IReviewsResultService;
import org.springframework.stereotype.Service;

@Service
public class ReviewsResultService extends ServiceImpl<ReviewsResultDao, ReviewsResult> implements IReviewsResultService {

}
