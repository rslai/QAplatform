package com.bjy.qa.service.functionaltest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bjy.qa.entity.Response;
import com.bjy.qa.entity.functionaltest.FullTree;
import com.bjy.qa.entity.functionaltest.TestExecuteDataDTO;
import com.bjy.qa.entity.functionaltest.TestSuite;
import com.bjy.qa.enumtype.CatalogType;

import java.util.List;

public interface ITestExecuteService extends IService<TestSuite> {

    /**
     * 测试执行中查询 tree 完整路径，只返回在测试套件中被选中的 测试用例
     * @param type 分类类型 枚举类
     * @param caseType 用例类型 枚举类
     * @param projectId 项目ID
     * @param testSuitesId 测试套件表 ID
     * @param testResultId 测试结果表 ID
     * @param parentId tree 节点 ID
     * @return
     */
    List<FullTree> listTree(CatalogType type, CatalogType caseType, Long projectId, Long testSuitesId, Long testResultId, Long parentId);

    /**
     * 更新测试用例执行状态
     * @param testExecuteDataDTO
     * @param runUser
     * @return
     */
    Response<String> updateCaseStatus(TestExecuteDataDTO testExecuteDataDTO , String runUser);
}
