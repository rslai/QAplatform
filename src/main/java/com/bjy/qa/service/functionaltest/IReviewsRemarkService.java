package com.bjy.qa.service.functionaltest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bjy.qa.entity.functionaltest.BatchReviewsRemarkDTO;
import com.bjy.qa.entity.functionaltest.ReviewsRemark;

import java.util.List;

public interface IReviewsRemarkService extends IService<ReviewsRemark> {
    /**
     * 获取用例评审的备注信息
     * @param reviewId 评审计划 ID
     * @param cid 用例 ID
     * @return
     */
    List<ReviewsRemark> getCaseRemark(Long reviewId, Long cid);

    /**
     * 添加备注
     * @param reviewsRemark
     * @return
     */
    ReviewsRemark add(ReviewsRemark reviewsRemark);

    /**
     * 批量添加备注
     * @param batchReviewsRemarkDTO 批量用例评审备注 DTO
     * @param reviewerUserId 评审者 ID
     * @return
     */
    String batchAdd(BatchReviewsRemarkDTO batchReviewsRemarkDTO, Long reviewerUserId);
}
