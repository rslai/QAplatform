package com.bjy.qa.service.functionaltest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bjy.qa.entity.MyPage;
import com.bjy.qa.entity.ResponsePagingData;
import com.bjy.qa.entity.functionaltest.Reviews;

public interface IReviewsService extends IService<Reviews> {
    /**
     * 分页查询测试计划列表
     * @param myPage 分页参数
     * @return
     */
    ResponsePagingData list(MyPage<Reviews> myPage);

    /**
     * 创建评审
     * @param reviews 评审计划
     * @return
     */
    int add(Reviews reviews);

    /**
     * 编辑评审
     * @param reviews 评审计划
     * @return
     */
    int update(Reviews reviews);

    /**
     * 删除评审
     * @param id 评审计划 ID
     * @return
     */
    int delete(Long id);

    /**
     * 通过评审计划 ID 来查找关联的 user
     * @param id 评审计划 ID
     * @return
     */
    Reviews findById(Long id);

    /**
     * 结束评审
     * @param id
     * @return
     */
    Object stop(Long id);
}
