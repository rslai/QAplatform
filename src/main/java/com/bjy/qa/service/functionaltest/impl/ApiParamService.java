package com.bjy.qa.service.functionaltest.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bjy.qa.dao.functionaltest.ApiParamDao;
import com.bjy.qa.entity.functionaltest.ApiParam;
import com.bjy.qa.service.functionaltest.IApiParamService;
import org.springframework.stereotype.Service;

@Service
public class ApiParamService extends ServiceImpl<ApiParamDao, ApiParam> implements IApiParamService {

}
