package com.bjy.qa.service.functionaltest.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bjy.qa.dao.functionaltest.StepDao;
import com.bjy.qa.entity.functionaltest.Step;
import com.bjy.qa.service.functionaltest.IStepService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class StepService extends ServiceImpl<StepDao, Step> implements IStepService {
    @Resource
    StepDao stepDao;

    @Override
    public int deletePhysically(List<Long> ids) {
        return stepDao.deletePhysically(ids);
    }
}
