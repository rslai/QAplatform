package com.bjy.qa.service.functionaltest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bjy.qa.entity.functionaltest.*;

import java.util.List;

public interface ITestCaseService extends IService<TestCase> {
    /**
     * 添加测试用例
     * @param testCase 测试用例
     * @return
     */
    TestCase add(TestCase testCase);

    /**
     * 更新测试用例
     * @param testCase 测试用例
     * @return
     */
    TestCase update(TestCase testCase);

    /**
     * 批量新增和修改测试用例
     * @param testCases 测试用例
     * @return
     */
    List<TestCase> saveOrUpdateBatch(List<TestCase> testCases);

    /**
     * 根据 id 查询数据
     * @param id
     * @return
     */
    TestCase selectById(Long id);

    /**
     * 根据 目录ID 查询该目录下的测试用例
     * @param projectId 项目id
     * @param catalogId 目录id
     * @return
     */
    List<TestCase> selectByCatalogId(Long projectId, Long catalogId);

    /**
     * 删除一条数据
     * @param id ID
     * @return
     */
    int delete(Long id);

    /**
     * 导入测试用例
     * @param taskId 任务 ID
     * @param filePath 文件路径
     * @param projectId 项目 ID
     * @param userName 用户名
     * @return
     */
    Object fileImport(String taskId, String filePath, Long projectId, String userName);

    /**
     * 导出测试用例
     * @param taskId 任务 ID
     * @param exportDTO 导出 DTO 模型
     * @return
     */
    Boolean export(String taskId, ExportDTO exportDTO);

    /**
     * 搜索测试用例
     * @param searchDTO 搜索 DTO 模型
     * @return
     */
    List<TestCase> search(SearchDTO searchDTO);

    /**
     * 发送 TestCase 调试请求
     * @param testCaseDebugDTO TestCase 调试请求 DTO 模型
     * @param runUser 执行者
     * @return
     */
    TestResultSuite testCaseDebug(TestCaseDebugDTO testCaseDebugDTO, String runUser);
}
