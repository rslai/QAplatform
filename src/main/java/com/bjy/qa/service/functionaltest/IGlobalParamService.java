package com.bjy.qa.service.functionaltest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bjy.qa.entity.functionaltest.GlobalParam;
import com.bjy.qa.entity.functionaltest.GlobalParamDTO;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

public interface IGlobalParamService extends IService<GlobalParam> {
    /**
     * 查询指定用户下的所有全局参数
     * @param userId 用户id
     * @param projectId 项目ID
     * @return
     */
    List<GlobalParam> list(Long projectId, Long userId) throws IllegalBlockSizeException, NoSuchPaddingException, BadPaddingException, NoSuchAlgorithmException, InvalidKeyException;

    /**
     * 新增或更新全局参数
     * @param globalParam
     * @return
     */
    boolean createOrUpdate(GlobalParam globalParam) throws Exception;

    /**
     * 返回所有参数（环境 + 全局）
     * @param projectId 项目 id
     * @param envName 环境 name
     * @param userId 不传 用户ID 从当前登录用户中获取。传了，用传的 用户ID
     * @return
     */
    GlobalParamDTO getAllGlobalParam(Long projectId, String envName, Long userId);
}
