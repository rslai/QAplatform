package com.bjy.qa.service.functionaltest.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bjy.qa.dao.functionaltest.TestCaseRemarkDao;
import com.bjy.qa.entity.functionaltest.TestCaseRemark;
import com.bjy.qa.service.functionaltest.ITestCaseRemarkService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class TestCaseRemarkService extends ServiceImpl<TestCaseRemarkDao, TestCaseRemark> implements ITestCaseRemarkService {
    @Resource
    TestCaseRemarkDao testCaseRemarkDao;

    @Override
    public TestCaseRemark add(TestCaseRemark testCaseRemark) {
        testCaseRemarkDao.insert(testCaseRemark);
        return testCaseRemark;
    }

    @Override
    public List<TestCaseRemark> getCaseRemark(Long sid, Long cid) {
        return testCaseRemarkDao.getCaseRemark(sid, cid);
    }
}
