package com.bjy.qa.service.functionaltest.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bjy.qa.dao.functionaltest.ReviewsDao;
import com.bjy.qa.dao.functionaltest.ReviewsRemarkDao;
import com.bjy.qa.entity.functionaltest.BatchReviewsRemarkDTO;
import com.bjy.qa.entity.functionaltest.Reviews;
import com.bjy.qa.entity.functionaltest.ReviewsRemark;
import com.bjy.qa.enumtype.EditStatus;
import com.bjy.qa.exception.MyException;
import com.bjy.qa.service.functionaltest.IReviewsRemarkService;
import com.bjy.qa.util.security.SecurityUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class ReviewsRemarkService extends ServiceImpl<ReviewsRemarkDao, ReviewsRemark> implements IReviewsRemarkService {
    @Resource
    ReviewsDao reviewsDao;
    @Resource
    ReviewsRemarkDao reviewsRemarkDao;

    @Override
    public List<ReviewsRemark> getCaseRemark(Long reviewId, Long cid) {
        return reviewsRemarkDao.getCaseRemark(reviewId, cid);
    }

    @Override
    public ReviewsRemark add(ReviewsRemark reviewsRemark){
        reviewsRemark.setReviewerUserId(SecurityUtils.getCurrentUserId());
        reviewsRemarkDao.insert(reviewsRemark);
        return reviewsRemark;
    }

    @Override
    public String batchAdd(BatchReviewsRemarkDTO batchReviewsRemarkDTO, Long reviewerUserId) {
        Reviews reviews = reviewsDao.selectById(batchReviewsRemarkDTO.getReviewsId());
        if (reviews != null) {
            if (reviews.getStatus().getValue() == EditStatus.FINISH.getValue()) {
                throw new MyException("该评审已经结束，不能再批量添加评审备注！");
            }

            List<ReviewsRemark> reviewsRemarkList = new ArrayList<>();
            for (Long testCaseId : batchReviewsRemarkDTO.getTestCaseIds()) {
                ReviewsRemark reviewsRemark = new ReviewsRemark();
                reviewsRemark.setProjectId(reviews.getProjectId());
                reviewsRemark.setReviewsId(batchReviewsRemarkDTO.getReviewsId());
                reviewsRemark.setTestCaseId(testCaseId);
                reviewsRemark.setRemark(batchReviewsRemarkDTO.getRemark());
                reviewsRemark.setReviewerUserId(reviewerUserId);
                reviewsRemark.setStatus(batchReviewsRemarkDTO.getCaseStatus().getValue());
                reviewsRemarkList.add(reviewsRemark);
            }
            if (reviewsRemarkList.size() > 0) {
                this.saveBatch(reviewsRemarkList);
            }
        }

        return "批量添加评审备注成功!";
    }
}
