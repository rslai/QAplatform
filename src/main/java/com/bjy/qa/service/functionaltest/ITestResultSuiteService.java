package com.bjy.qa.service.functionaltest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bjy.qa.entity.MyPage;
import com.bjy.qa.entity.ResponsePagingData;
import com.bjy.qa.entity.functionaltest.TestResultSuite;

public interface ITestResultSuiteService extends IService<TestResultSuite> {
    /**
     * 分页查询套件列表
     * @param myPage
     * @return
     */
    ResponsePagingData list(MyPage<TestResultSuite> myPage);

    /**
     * 根据id查找套件信息
     * @param id 套件id
     * @return
     */
    TestResultSuite getResultInfoById(Long id);
}
