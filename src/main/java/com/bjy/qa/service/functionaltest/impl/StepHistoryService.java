package com.bjy.qa.service.functionaltest.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bjy.qa.dao.functionaltest.StepHistoryDao;
import com.bjy.qa.entity.functionaltest.StepHistory;
import com.bjy.qa.service.functionaltest.IStepHistoryService;
import org.springframework.stereotype.Service;

@Service
public class StepHistoryService extends ServiceImpl<StepHistoryDao, StepHistory> implements IStepHistoryService {

}
