package com.bjy.qa.service.functionaltest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bjy.qa.entity.functionaltest.Catalog;
import com.bjy.qa.entity.functionaltest.FullTree;
import com.bjy.qa.entity.functionaltest.Tree;
import com.bjy.qa.entity.functionaltest.TreeSort;
import com.bjy.qa.enumtype.CatalogType;

import java.util.List;

public interface ICatalogService extends IService<Catalog> {
    /**
     * 查询分类列表
     * @param type 分类类型 枚举类
     * @param projectId 项目ID
     * @param parentId 父分类ID
     * @return 列表
     */
    List<Catalog> listCatalogByParentId(CatalogType type, Long projectId, Long parentId);

    Catalog selectByName(CatalogType type, Long projectId, Long parentId, String name);

    /**
     * 查询 tree 上某个子节点的所有数据
     * @param type 分类类型 枚举类
     * @param projectId 项目ID
     * @param parentId tree 节点 ID
     * @return
     */
    List<Tree> listTreeContent(CatalogType type, Long projectId, Long parentId);

    /**
     * 查询 tree 上某个type的所有数据
     * @param type 分类类型 枚举类
     * @param caseType 用例类型 枚举类
     * @param projectId 项目ID
     * @param parentId tree 节点 ID
     * @return
     */
    List<FullTree> listTree(CatalogType type, CatalogType caseType, Long projectId, Long parentId);

    /**
     * 查询 tree 完整路径 - 只包含目录
     * @param type 分类类型 枚举类
     * @param caseType 用例类型 枚举类
     * @param projectId 项目ID
     * @param parentId tree 节点 ID
     * @return
     */
    List<FullTree> listTreeCatalog(CatalogType type, CatalogType caseType, Long projectId, Long parentId);

    /**
     * 分类类型 枚举类
     * @param treeSort tree 排序 DTO 模型
     * @return
     */
    Integer treeSort(Long projectId, TreeSort treeSort);

    /**
     * 根据当前元素id，递归查找控件元素以及父分类列表
     * 例如：一个用例A他在分类A下，分类A又在分类B下。那返回就是 [分类B, 分类A 用例A]
     * @param id id
     * @return
     */
    List<Tree> getElementParent(CatalogType type, Long id);

    /**
     * 根据 id 查找分类
     * @param id id
     * @return
     */
    Catalog selectById(Long id);

    /**
     * 新增分类信息
     * @param catalog
     * @return
     */
    Catalog add(Catalog catalog);

    /**
     * 更新分类信息
     * @param catalog
     * @return
     */
    Catalog update(Catalog catalog);

    /**
     * 根据 id 删除分类
     * @param type 分类类型 枚举类
     * @param projectId 项目ID
     * @param id id
     * @return
     */
    int delete(CatalogType type, Long projectId, Long id);

    /**
     * 递归复制分类信息
     * @param type 分类类型 枚举类
     * @param projectId 项目ID
     * @param id id
     * @return
     */
    Tree copy(CatalogType type, Long projectId, Long id);
}
