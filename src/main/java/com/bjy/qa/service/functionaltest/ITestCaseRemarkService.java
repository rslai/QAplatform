package com.bjy.qa.service.functionaltest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bjy.qa.entity.functionaltest.TestCaseRemark;

import java.util.List;

public interface ITestCaseRemarkService extends IService<TestCaseRemark> {

    /**
     * 添加备注
     * @param testCaseRemark
     * @return
     */
    TestCaseRemark add(TestCaseRemark testCaseRemark);

    /**
     * 通过 suite id 和 case id获取备注信息
     * @param sid
     * @param cid
     * @return
     */
    List<TestCaseRemark> getCaseRemark(Long sid, Long cid);
}
