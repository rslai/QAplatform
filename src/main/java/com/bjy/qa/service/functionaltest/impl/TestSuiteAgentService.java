package com.bjy.qa.service.functionaltest.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bjy.qa.dao.functionaltest.TestSuiteAgentDao;
import com.bjy.qa.entity.functionaltest.TestSuiteAgent;
import com.bjy.qa.service.functionaltest.ITestSuiteAgentService;
import org.springframework.stereotype.Service;

@Service
public class TestSuiteAgentService extends ServiceImpl<TestSuiteAgentDao, TestSuiteAgent> implements ITestSuiteAgentService {

}
