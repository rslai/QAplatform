package com.bjy.qa.service.functionaltest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bjy.qa.entity.functionaltest.TestResultCase;
import com.bjy.qa.entity.functionaltest.TestResultStep;

import java.util.List;
import java.util.Map;

public interface ITestResultCaseService extends IService<TestResultCase> {

    /**
     * 查询测试结果中的某个测试用例结果
     * @param cid caseId
     * @param rid ResultId
     * @return
     */
    Map<String, TestResultCase> getCaseResultDetails(Long cid, Long rid);

    /**
     * 通过suite id 和 case id来查询用例信息
     * @param cid
     * @param sid
     * @return
     */
    List<TestResultCase> getCaseResult(List<Long> cid,Long sid);
    /**
     * 根据 测试结果id 和 测试用例id 查询 测试用例结果
     * @param cid rid
     * @return
     */
    TestResultCase getTestResultCase(Long rid, Long cid);

    /**
     * 处理 agent 的测试结果上报
     * @return
     */
    String handlerResutl(TestResultStep testResultStep);
}
