package com.bjy.qa.service.functionaltest.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bjy.qa.dao.functionaltest.ReviewsUserDao;
import com.bjy.qa.entity.functionaltest.ReviewsUser;
import org.springframework.stereotype.Service;

@Service
public class ReviewsUserService extends ServiceImpl<ReviewsUserDao, ReviewsUser> {
}
