package com.bjy.qa.service.functionaltest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bjy.qa.entity.functionaltest.ReviewsResult;

public interface IReviewsResultService extends IService<ReviewsResult> {

}
