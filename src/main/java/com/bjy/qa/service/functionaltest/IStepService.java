package com.bjy.qa.service.functionaltest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bjy.qa.entity.functionaltest.Step;

import java.util.List;

public interface IStepService extends IService<Step> {
    /**
     * 物理删除一条数据
     * @param id ID
     * @return
     */
    int deletePhysically(List<Long> id);
}
