package com.bjy.qa.service.functionaltest.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bjy.qa.dao.functionaltest.AgentDao;
import com.bjy.qa.dao.functionaltest.TestCaseDao;
import com.bjy.qa.dao.functionaltest.TestResultCaseDao;
import com.bjy.qa.dao.functionaltest.TestResultSuiteDao;
import com.bjy.qa.entity.MyPage;
import com.bjy.qa.entity.ResponsePagingData;
import com.bjy.qa.entity.functionaltest.Agent;
import com.bjy.qa.entity.functionaltest.TestCase;
import com.bjy.qa.entity.functionaltest.TestResultCase;
import com.bjy.qa.entity.functionaltest.TestResultSuite;
import com.bjy.qa.exception.MyException;
import com.bjy.qa.service.functionaltest.ITestResultSuiteService;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service
public class TestResultSuiteService extends ServiceImpl<TestResultSuiteDao, TestResultSuite>  implements ITestResultSuiteService {
    @Resource
    TestResultSuiteDao testResultSuiteDao;

    @Resource
    TestResultCaseDao testResultCaseDao;

    @Resource
    TestCaseDao testCaseDao;

    @Resource
    AgentDao agentDao;

    @Override
    public ResponsePagingData list(MyPage<TestResultSuite> myPage) {
        Page<TestResultSuite> page = new Page<>(myPage.getPageNum(), myPage.getPageSize()); // 构造待查询 page 对象
        page.setOrders(OrderItem.descs("id")); // 按照id倒序排

        TestResultSuite testResultSuite = myPage.getQuery();
        QueryWrapper<TestResultSuite> queryWrapper = new QueryWrapper<>();

        if (testResultSuite != null) {
            if (StringUtils.isNotBlank(testResultSuite.getProjectId().toString())) {
                queryWrapper.eq("project_id", testResultSuite.getProjectId());
            }
            if (testResultSuite.getCaseType().getValue() != 0) {
                queryWrapper.eq("case_type", testResultSuite.getCaseType());
            }
            if (testResultSuite.getSuiteId() != null) {
                queryWrapper.eq("suite_id", testResultSuite.getSuiteId());
            }
        }
        page = testResultSuiteDao.selectPage(page, queryWrapper);
        myPage.setTotalRecords(page.getTotal());
        return new ResponsePagingData(myPage, page.getRecords());
    }

    @Override
    public TestResultSuite getResultInfoById(Long resultId) {
        TestResultSuite testResultSuite = testResultSuiteDao.selectById(resultId);
        if (testResultSuite == null) {
            throw new MyException("测试报告不存在，或已清除。 resultId:" + resultId);
        }

        List<TestResultCase> testResultCaseList = testResultCaseDao.selecByResultId(resultId);

        for (int i = 0; i < testResultCaseList.size(); i++) {
            // 设置 case name
            TestCase testCase = testCaseDao.findById(testResultCaseList.get(i).getCid()); // 查找测试用例（包含已删除测试用例），比如在测试报告中也应该能显示出来
            if (testCase == null) {
                testResultCaseList.get(i).setCidName("== 此用例已删除 ==");
            } else {
                testResultCaseList.get(i).setCidName(testCase.getName());
            }

            // 设置 agent name
            Agent agent = agentDao.selectById(testResultCaseList.get(i).getAgent());
            testResultCaseList.get(i).setAgentName(agent.getName());
        }
        testResultSuite.setCidList(testResultCaseDao.selecByResultId(resultId));

        return testResultSuite;
    }
}
