package com.bjy.qa.service.functionaltest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bjy.qa.entity.functionaltest.ApiParam;

public interface IApiParamService extends IService<ApiParam> {
}
