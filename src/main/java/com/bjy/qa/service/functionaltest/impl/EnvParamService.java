package com.bjy.qa.service.functionaltest.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bjy.qa.dao.functionaltest.EnvParamDao;
import com.bjy.qa.dao.functionaltest.GlobalParamDao;
import com.bjy.qa.entity.functionaltest.EnvParam;
import com.bjy.qa.exception.MyException;
import com.bjy.qa.service.functionaltest.IEnvParamService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service
public class EnvParamService extends ServiceImpl<EnvParamDao, EnvParam> implements IEnvParamService {
    @Resource
    EnvParamDao envParamDao;

    @Resource
    GlobalParamDao globalParamDao;

    @Override
    public EnvParam selectIdByEnvName(Long projectId, String envName) {
        return envParamDao.selectIdByEnvName(projectId, null, envName);
    }

    @Override
    public int deleteEnvByName(Long projectId, String envName) {
        QueryWrapper<EnvParam> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("project_id", projectId);
        queryWrapper.eq("env_name", envName);
        return envParamDao.delete(queryWrapper);
    }

    @Override
    public List<EnvParam> listEnvParamsByName(Long projectId, String envName) {
        return envParamDao.listEnvParamsByName(projectId, envName);
    }

    @Override
    public Boolean saveOrUpdateServer(EnvParam envParam) {
        if (envParamDao.selectServerByName(envParam.getProjectId(), envParam.getId(), envParam.getEnvName(), envParam.getServerName()) != null) {
            throw new MyException("服务名已存在，不可重复添加");
        }
        if (envParam.getId() == null) {
            envParamDao.insert(envParam);
        } else {
            envParamDao.updateById(envParam);
        }
        return true;
    }

    @Override
    public Boolean saveOrUpdateEnv(EnvParam envParam) {
        if (envParamDao.selectIdByEnvName(envParam.getProjectId(), envParam.getId(), envParam.getEnvName()) != null){
            throw new MyException("当前环境已存在，请检查后，重新添加");
        }
        if (envParam.getId() == null) {
            envParamDao.insert(envParam);
        } else {
            envParamDao.updateById(envParam);
        }
        return true;
    }
}
