package com.bjy.qa.service.mock;

import com.bjy.qa.entity.mock.http.MockRequest;
import com.bjy.qa.entity.mock.http.MockResponse;

public interface IMockService {
    /**
     * 获取 mock 请求的响应信息
     * @param mockRequest mock 请求
     * @return mock 请求的响应信息
     */
    MockResponse getMockResponse(MockRequest mockRequest);
}
