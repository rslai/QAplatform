package com.bjy.qa.service.dashboard;

import java.text.ParseException;
import java.util.Map;

public interface ITestCaseCountService {
    /**
     * 数据统计-按用户对应测试用例维度统计
     * @return
     */
    Object testCaseCount();

    /**
     * 数据统计-返回用户对应测试用例维度
     * @param projectId 项目 ID
     * @param startDate 开始日期
     * @param endDate 结束日期
     * @return 返回统计到的数据
     */
    Map<String, Object> getTestCaseCount(Long projectId, String startDate, String endDate) throws ParseException;

    /**
     * 数据统计-返回用例总数和套件总数
     * @param projectId 项目 ID
     * @return 返回统计到的数据
     */
    Map<String, Object> getTotalCount(Long projectId) throws ParseException;
}
