package com.bjy.qa.service.user;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bjy.qa.entity.user.UserRole;

public interface IUserRoleService extends IService<UserRole> {

}
