package com.bjy.qa.service.user.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bjy.qa.dao.user.UserRoleDao;
import com.bjy.qa.entity.user.UserRole;
import com.bjy.qa.service.user.IUserRoleService;
import org.springframework.stereotype.Service;

@Service
public class UserRoleService extends ServiceImpl<UserRoleDao, UserRole> implements IUserRoleService {

}
