package com.bjy.qa.service.user;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bjy.qa.entity.user.UserCompanyProject;

public interface IUserCompanyProjectService extends IService<UserCompanyProject>  {
    /**
     * 校验当前用户是否有指定项目权限
     * @param companyId 公司 id。可以传空，空只校验 projectId
     * @param projectId 项目 id
     * @return true: 有权限 false: 无权限
     */
    boolean hasUserProjectPermissions(Long companyId, Long projectId);
}
