package com.bjy.qa.service.user;

import com.bjy.qa.entity.MyPage;
import com.bjy.qa.entity.ResponsePagingData;
import com.bjy.qa.entity.user.User;
import com.bjy.qa.entity.user.UserResponse;
import java.util.List;
import java.util.Map;

public interface IUserService {

    /**
     * 登录
     * @param user
     * @return
     */
    UserResponse login(User user);

    /**
     * 根据用户id查找用户信息
     * @param id 用户信息
     * @return User对象
     */
    User getInfo(Long id);

    /**
     * 新增用户信息
     * @param user
     * @return
     */
    int add(User user);

    /**
     * 更新用户信息
     * @param user
     * @return
     */
    int update(User user);

    /**
     * 更新用户当前 产品-项目
     * @param user
     * @return
     */
    int updateCurrentCompanyProject(User user) throws Exception;

    /**
     * 锁定用户
     * @param user
     * @return
     * @throws Exception
     */
    int locking(User user) throws Exception;

    /**
     * 修改用户密码
     * @param user 用户信息
     * @return
     */
    Boolean changeCode(User user);

    /**
     * 分页查询用户列表
     * @param myPage
     * @return
     */
    ResponsePagingData list(MyPage<User> myPage);

    /**
     * 按 项目 ID 分页查询用户列表
     * @param myPage
     * @return
     */
    ResponsePagingData listByProjectId(MyPage<User> myPage);

    /**
     * 查询用户完整信息，包括公司及项目列表（需要关联多个表一般不要使用）
     * @param id 用户id
     * @return
     */
    User selectWholeInfoById(Long id);

    /**
     * 通过 项目 ID 查询所有拥有该项目权限的用户
     * @param id 项目 ID
     * @return
     */
    List<User> selectUserByProjectId(Long id);

    /**
     * 通过 项目 ID 查询所有不包含该项目权限的用户
     * @param projectId 项目 ID
     * @return
     */
    Object selectUserByNotIncludeProjectId(Long projectId);

    /**
     * 生成用户对外 Token，例如：wehhook 时用到的 token
     * @param user 用户信息
     * @param extMap 扩展信息
     * @param day 有效期天数
     * @return
     * @throws Exception
     */
    Object generateToken(User user, Map<String, Object> extMap, int day);

    /**
     * 将用户从项目中移除
     * @param userId 用户id
     * @param projectId 项目id
     * @return
     */
    Object removeUserProject(Long userId, Long projectId);
}
