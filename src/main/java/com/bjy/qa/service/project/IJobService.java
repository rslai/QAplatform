package com.bjy.qa.service.project;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bjy.qa.entity.MyPage;
import com.bjy.qa.entity.ResponsePagingData;
import com.bjy.qa.entity.project.Job;
import com.bjy.qa.enumtype.JobStatus;
import org.quartz.SchedulerException;

public interface IJobService extends IService<Job> {
    /**
     * 分页查询定时任务列表
     * @param myPage
     * @return
     */
    ResponsePagingData list(MyPage<Job> myPage);

    /**
     * 新增定时任务信息
     * @param job
     * @return
     */
    Object add(Job job);

    /**
     * 更新定时任务信息
     * @param job
     * @return
     */
    Object update(Job job);

    /**
     * 删除一条数据
     * @param id ID
     * @return
     */
    int delete(Long id);

    /**
     * 运行定时任务
     * @param projectId 项目ID
     * @param id 定时任ID
     * @param status 定时任务状态
     * @return
     */
    Object updateStatus(Long projectId, Long id, JobStatus status) throws SchedulerException;

    /**
     * 初始化系统定时任务
     * @return
     * @throws SchedulerException
     */
    Object initSystemJob() throws SchedulerException;
}
