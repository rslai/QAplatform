package com.bjy.qa.service.project;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bjy.qa.entity.project.Robot;

public interface IRobotService extends IService<Robot> {
    /**
     * 查询定时任务列表
     * @return
     */
    Object listAll(Long projectId);

    /**
     * 新增定时任务信息
     * @param robot
     * @return
     */
    Object add(Robot robot);

    /**
     * 更新定时任务信息
     * @param robot
     * @return
     */
    Object update(Robot robot);

    /**
     * 删除一条数据
     * @param id ID
     * @return
     */
    int delete(Long id);
}
