package com.bjy.qa.service.project.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bjy.qa.dao.project.JobDao;
import com.bjy.qa.dao.project.JobResultDao;
import com.bjy.qa.entity.MyPage;
import com.bjy.qa.entity.ResponsePagingData;
import com.bjy.qa.entity.project.JobResult;
import com.bjy.qa.service.project.IJobResultService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class JobResultService extends ServiceImpl<JobResultDao, JobResult> implements IJobResultService {
    @Resource
    JobResultDao jobResultDao;

    @Resource
    JobDao jobDao;

    @Override
    public ResponsePagingData list(MyPage<JobResult> myPage) {
        Page<JobResult> page = new Page(myPage.getPageNum(), myPage.getPageSize()); // 构造待查询 page 对象
        page.setOrders(myPage.getOrders()); // 设置排序字段

        // 构造待查询 QueryWrapper
        JobResult jobResult = myPage.getQuery();
        QueryWrapper<JobResult> queryWrapper = new QueryWrapper<>();
        if (jobResult != null) {
            if (jobResult.getProjectId() == null) { // 查询系统 job
                queryWrapper.eq("project_id", 0L);
            } else { // 查询 project job
                queryWrapper.eq("project_id", jobResult.getProjectId());
            }
        }
        queryWrapper.orderByDesc("id");

        page = jobResultDao.selectPage(page, queryWrapper); // 按分页查询

        myPage.setTotalRecords(page.getTotal()); // 设置返回总记录数

        page.getRecords().forEach(item -> {
            System.out.println(item.getJobId());
            item.setJob(jobDao.selectById(item.getJobId()));
        });

        return new ResponsePagingData(myPage, page.getRecords());
    }
}
