package com.bjy.qa.service.project;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bjy.qa.entity.MyPage;
import com.bjy.qa.entity.ResponsePagingData;
import com.bjy.qa.entity.project.JobResult;

public interface IJobResultService extends IService<JobResult> {
    /**
     * 分页查询定时任务执行结果列表
     * @param myPage
     * @return
     */
    ResponsePagingData list(MyPage<JobResult> myPage);
}
