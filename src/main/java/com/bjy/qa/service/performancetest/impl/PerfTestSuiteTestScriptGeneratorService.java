package com.bjy.qa.service.performancetest.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bjy.qa.dao.performancetest.PerfTestSuiteTestScriptGeneratorDao;
import com.bjy.qa.entity.performancetest.PerfTestSuiteTestScriptGenerator;
import com.bjy.qa.service.performancetest.IPerfTestSuiteTestScriptGeneratorService;
import org.springframework.stereotype.Service;

@Service
public class PerfTestSuiteTestScriptGeneratorService extends ServiceImpl<PerfTestSuiteTestScriptGeneratorDao, PerfTestSuiteTestScriptGenerator> implements IPerfTestSuiteTestScriptGeneratorService {

}
