package com.bjy.qa.service.performancetest.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bjy.qa.dao.performancetest.PerfTestSuiteTestScriptDao;
import com.bjy.qa.entity.performancetest.PerfTestSuiteTestScript;
import com.bjy.qa.service.performancetest.IPerfTestSuiteTestScriptService;
import org.springframework.stereotype.Service;

@Service
public class PerfTestSuiteTestScriptService extends ServiceImpl<PerfTestSuiteTestScriptDao, PerfTestSuiteTestScript> implements IPerfTestSuiteTestScriptService {

}
