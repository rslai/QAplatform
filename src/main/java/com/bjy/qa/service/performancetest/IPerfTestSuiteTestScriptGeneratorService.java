package com.bjy.qa.service.performancetest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bjy.qa.entity.performancetest.PerfTestSuiteTestScriptGenerator;

/**
 * 性能测试套件 中 性能测试脚本 对应 压力机 服务类
 */
public interface IPerfTestSuiteTestScriptGeneratorService extends IService<PerfTestSuiteTestScriptGenerator> {

}
