package com.bjy.qa.service.performancetest.impl;

import com.bjy.qa.dao.performancetest.TestScriptDao;
import com.bjy.qa.entity.functionaltest.SearchDTO;
import com.bjy.qa.entity.performancetest.TestScript;
import com.bjy.qa.enumtype.CatalogType;
import com.bjy.qa.service.performancetest.ITestScriptService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class TestScriptService implements ITestScriptService {
    @Resource
    TestScriptDao testScriptDao;

    @Override
    public TestScript selectById(Long id) {
        return testScriptDao.selectById(id);
    }

    @Override
    public List<TestScript> selectByCatalogId(Long projectId, Long catalogId) {
        return testScriptDao.selectByCatalogId(projectId, CatalogType.TEST_CASE, catalogId);
    }

    @Override
    public List<TestScript> search(SearchDTO searchDTO) {
        return testScriptDao.search(searchDTO.getProjectId(), searchDTO.getType(), searchDTO.getFindStr(), "%" + searchDTO.getFindStr() + "%");
    }

    @Override
    public TestScript add(TestScript testScript) {
        testScriptDao.insert(testScript);
        return testScript;
    }

    @Override
    public TestScript update(TestScript testScript) {
        // 更新数据需要清除 UpdatedTime 和 CreatedTime，否则不会自动更新修改时间
        testScript.setUpdatedAt(null);
        testScript.setCreatedAt(null);
        testScriptDao.updateById(testScript);
        return testScript;
    }

    @Override
    public int delete(Long id) {
        return testScriptDao.deleteById(id); // 删除 性能测试脚本
    }
}
