package com.bjy.qa.service.performancetest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bjy.qa.entity.performancetest.PerfTestResultTestScript;

/**
 * 性能测试结果 对应 性能测试脚本 服务类
 */
public interface IPerfTestResultTestScriptService extends IService<PerfTestResultTestScript> {

}
