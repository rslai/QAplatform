package com.bjy.qa.service.performancetest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bjy.qa.entity.performancetest.PerfTestSuiteTestScript;

/**
 * 性能测试套件 对应 性能测试脚本 服务类
 */
public interface IPerfTestSuiteTestScriptService extends IService<PerfTestSuiteTestScript> {

}
