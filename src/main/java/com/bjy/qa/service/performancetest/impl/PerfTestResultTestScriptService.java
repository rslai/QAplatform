package com.bjy.qa.service.performancetest.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bjy.qa.dao.performancetest.PerfTestResultTestScriptDao;
import com.bjy.qa.entity.performancetest.PerfTestResultTestScript;
import com.bjy.qa.service.performancetest.IPerfTestResultTestScriptService;
import org.springframework.stereotype.Service;

@Service
public class PerfTestResultTestScriptService extends ServiceImpl<PerfTestResultTestScriptDao, PerfTestResultTestScript> implements IPerfTestResultTestScriptService {

}
