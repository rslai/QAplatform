package com.bjy.qa.service.performancetest;

import com.bjy.qa.entity.MyPage;
import com.bjy.qa.entity.ResponsePagingData;
import com.bjy.qa.entity.performancetest.Generator;

import java.util.List;

/**
 * 压力机（load generators） 服务类
 */
public interface IGeneratorService {
    /**
     * 返回所有记录
     * @return
     */
    List<Generator> listAll(Long projectId);

    /**
     * 分页查询压力机（load generators）列表
     * @param myPage 分页参数
     * @return
     */
    ResponsePagingData list(MyPage<Generator> myPage);

    /**
     * 根据id查找压力机（load generators）信息
     * @param id 公司id
     * @return
     */
    Generator selectById(Long id);

    /**
     * 新增压力机（load generators）
     * @param generator 压力机（load generators）
     * @return
     */
    int add(Generator generator);

    /**
     * 更新压力机（load generators）信息
     * @param generator 压力机（load generators）
     * @return
     */
    int update(Generator generator);

    /**
     * 删除一条数据
     * @param id ID
     * @return
     */
    int delete(Long id);
}
