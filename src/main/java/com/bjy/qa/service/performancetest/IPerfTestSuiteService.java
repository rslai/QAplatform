package com.bjy.qa.service.performancetest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bjy.qa.entity.MyPage;
import com.bjy.qa.entity.ResponsePagingData;
import com.bjy.qa.entity.performancetest.PerfTestResult;
import com.bjy.qa.entity.performancetest.PerfTestSuite;

import java.util.List;

/**
 * 性能测试套件 服务类
 */
public interface IPerfTestSuiteService extends IService<PerfTestSuite> {
    /**
     * 分页查询 性能测试套件 列表
     * @param myPage 分页参数
     * @return
     */
    ResponsePagingData list(MyPage<PerfTestSuite> myPage);

    /**
     * 返回所有记录
     * @return
     */
    List<PerfTestSuite> listAll(Long projectId);

    /**
     * 根据 id 查询 性能测试套件 全部信息（包括套件下的所有用例、压力机、场景）
     * @param id 套件 id
     * @return
     */
    PerfTestSuite findById(Long id);

    /**
     * 新增 性能测试套件
     * @param perfTestSuite 性能测试套件
     * @return
     */
    int add(PerfTestSuite perfTestSuite);

    /**
     * 更新 性能测试套件 信息
     * @param perfTestSuite 性能测试套件
     * @return
     */
    int update(PerfTestSuite perfTestSuite);

    /**
     * 删除一条数据
     * @param id ID
     * @return
     */
    Boolean delete(Long id);

    /**
     * 执行性能测试套件
     * @param projectId 项目 ID
     * @param userId 执行套件 用户 ID
     * @param id 性能测试套件 ID
     * @return
     */
    boolean run(Long projectId, Long userId, Long id);

    /**
     * 停止性能测试套件
     * @param id 套件id
     * @param runUser  执行者
     * @return
     */
    boolean forceStop(Long id, String runUser);

    /**
     * 设为 或 取消 基线
     * @param id 测试结果 ID
     * @return
     */
    PerfTestResult updateBaseLine(Long id);
}
