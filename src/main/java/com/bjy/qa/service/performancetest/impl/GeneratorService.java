package com.bjy.qa.service.performancetest.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bjy.qa.dao.performancetest.GeneratorDao;
import com.bjy.qa.entity.MyPage;
import com.bjy.qa.entity.ResponsePagingData;
import com.bjy.qa.entity.performancetest.Generator;
import com.bjy.qa.exception.MyException;
import com.bjy.qa.service.performancetest.IGeneratorService;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class GeneratorService implements IGeneratorService {
    @Resource
    GeneratorDao generatorDao;

    @Override
    public List<Generator> listAll(Long projectId) {
        // 构造待查询 QueryWrapper
        QueryWrapper<Generator> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("project_id", projectId);
        queryWrapper.orderByAsc("id");

        return generatorDao.selectList(queryWrapper);
    }

    @Override
    public ResponsePagingData list(MyPage<Generator> myPage) {
        Page<Generator> page = new Page(myPage.getPageNum(), myPage.getPageSize()); // 构造待查询 page 对象
        page.setOrders(myPage.getOrders()); // 设置排序字段

        // 构造待查询 QueryWrapper
        Generator generator = myPage.getQuery();
        QueryWrapper<Generator> queryWrapper = new QueryWrapper<>();
        if (generator != null) {
            if (generator.getProjectId() == null) {
                throw new MyException("projectId: 项目信息 id 必填!");
            } else {
                queryWrapper.eq("project_id", generator.getProjectId());
            }

            if (StringUtils.isNotBlank(generator.getHostname())) {
                queryWrapper.like("hostname", generator.getHostname());
            }
            if (StringUtils.isNotBlank(generator.getIp())) {
                queryWrapper.like("ip", generator.getIp());
            }
        }
        queryWrapper.orderByDesc("id");

        page = generatorDao.selectPage(page, queryWrapper); // 按分页查询

        myPage.setTotalRecords(page.getTotal()); // 设置返回总记录数

        return new ResponsePagingData(myPage, page.getRecords());
    }

    @Override
    public Generator selectById(Long id) {
        return generatorDao.selectById(id);
    }

    @Override
    public int add(Generator generator) {
        return generatorDao.insert(generator);
    }

    @Override
    public int update(Generator generator) {
        // 更新数据需要清除 UpdatedTime 和 CreatedTime，否则不会自动更新修改时间
        generator.setUpdatedAt(null);
        generator.setCreatedAt(null);

        return generatorDao.updateById(generator);
    }

    @Override
    public int delete(Long id) {
//        // 删除 company 前，先判断此 company 下的 project 是否都已删除
//        QueryWrapper<Project> queryWrapper = new QueryWrapper<>();
//        queryWrapper.eq("company_id", id);
//        if (projectDao.selectCount(queryWrapper) > 0) {
//            throw new MyException("删除错误，请先删除此公司下的所有项目！");
//        }
//
//        return generatorDao.deleteById(id);
        // TODO: 2023/12/22 暂时不支持删除
        throw new MyException("todo：暂时不支持删除！");
    }
}
