package com.bjy.qa.service.performancetest;

import com.bjy.qa.entity.functionaltest.SearchDTO;
import com.bjy.qa.entity.performancetest.TestScript;

import java.util.List;

/**
 * 性能测试脚本 服务类
 */
public interface ITestScriptService {
    /**
     * 根据 id 查找 性能测试脚本 信息
     * @param id 公司id
     * @return
     */
    TestScript selectById(Long id);

    /**
     * 根据 目录ID 查询该目录下的 性能测试脚本
     * @param projectId 项目id
     * @param catalogId 目录id
     * @return
     */
    List<TestScript> selectByCatalogId(Long projectId, Long catalogId);

    /**
     * 搜索 性能测试脚本
     * @param searchDTO 搜索 DTO 模型
     * @return
     */
    List<TestScript> search(SearchDTO searchDTO);

    /**
     * 新增 性能测试脚本
     * @param testScript 性能测试脚本
     * @return
     */
    TestScript add(TestScript testScript);

    /**
     * 更新 性能测试脚本 信息
     * @param testScript 性能测试脚本
     * @return
     */
    TestScript update(TestScript testScript);

    /**
     * 删除一条数据
     * @param id ID
     * @return
     */
    int delete(Long id);
}
