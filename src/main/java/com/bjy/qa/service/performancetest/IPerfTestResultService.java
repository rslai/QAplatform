package com.bjy.qa.service.performancetest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bjy.qa.entity.MyPage;
import com.bjy.qa.entity.ResponsePagingData;
import com.bjy.qa.entity.performancetest.*;

import java.util.List;

/**
 * 性能测试结果 服务类
 */
public interface IPerfTestResultService extends IService<PerfTestResult> {
    /**
     * 分页查询 性能测试结果 列表
     * @param myPage
     * @return
     */
    ResponsePagingData list(MyPage<PerfTestResult> myPage);

    /**
     * 根据 id 查询 性能测试结果信息
     * @param id 测试结果 id
     * @return
     */
    PerfTestResult getPerfTestResultInfoById(Long id);

    /**
     * 查询测试结果中的某个测试脚本的日志
     * @param resultId 测试结果 ID
     * @param testScriptId 测试脚本 ID
     * @return
     */
    List<PerfTestResultLog> getCaseResultDetails(Long resultId, Long testScriptId);

    /**
     * 查询测试结果 - 测试报告
     * @param resultId 测试结果 ID
     * @param testScriptId 测试脚本 ID
     * @return
     */
    ReportDTO getReport(Long resultId, Long testScriptId);

    /**
     * 查询 性能测试实时图表页签 - 表格中显示的数据
     * @param resultId 测试结果 ID
     * @param testScriptId 测试脚本 ID
     * @return
     */
    LatestDataDTO getLatestData(Long resultId, Long testScriptId);

    /**
     * 查询 性能测试实时图表页签 - chart 绘图需要的数据
     * @param resultId 测试结果 ID
     * @param testScriptId 测试脚本 ID
     * @return
     */
    ChartDTO getChart(Long resultId, Long testScriptId);

    /**
     * 异步计算性能测试实时图表页签上的数据（LatestData、Chart）
     * @param resultId 测试结果 ID
     * @param testScriptId 测试脚本 ID
     * @return
     */
    void asyncCountChartData(Long resultId, Long testScriptId);

    /**
     * 性能测试结果上报
     * @param perfTestResultLog 性能测试结果日志
     * @return
     */
    String log(PerfTestResultLog perfTestResultLog);
}
