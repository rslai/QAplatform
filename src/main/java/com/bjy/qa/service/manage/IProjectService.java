package com.bjy.qa.service.manage;

import com.bjy.qa.entity.MyPage;
import com.bjy.qa.entity.ResponsePagingData;
import com.bjy.qa.entity.manage.Project;

import java.util.List;

public interface IProjectService {

    /**
     * 返回所有记录
     * @return
     */
    List<Project> selectAll();

    /**
     * 分页查询项目列表
     * @param myPage
     * @return
     */
    ResponsePagingData list(MyPage<Project> myPage);

    /**
     * 根据id查找项目信息
     * @param id 公司id
     * @return
     */
    Project selectById(Long id);

    /**
     * 新增项目信息
     * @param project
     * @return
     */
    int add(Project project);

    /**
     * 更新项目信息
     * @param project
     * @return
     */
    int update(Project project);

    /**
     * 删除一条数据
     * @param id ID
     * @return
     */
    int delete(Long id);
}
