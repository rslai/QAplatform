package com.bjy.qa.service.manage;

import com.bjy.qa.entity.MyPage;
import com.bjy.qa.entity.ResponsePagingData;
import com.bjy.qa.entity.manage.Company;

import java.util.List;

public interface ICompanyService {

    /**
     * 返回所有记录
     * @return
     */
    List<Company> selectAll();

    /**
     * 分页查询公司列表
     * @param myPage
     * @return
     */
    ResponsePagingData list(MyPage<Company> myPage);

    /**
     * 根据id查找公司信息
     * @param id 公司id
     * @return
     */
    Company selectById(Long id);

    /**
     * 新增公司信息
     * @param company
     * @return
     */
    int add(Company company);

    /**
     * 更新公司信息
     * @param company
     * @return
     */
    int update(Company company);

    /**
     * 删除一条数据
     * @param id ID
     * @return
     */
    int delete(Long id);
}
