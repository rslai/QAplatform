package com.bjy.qa.service.manage;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bjy.qa.entity.MyPage;
import com.bjy.qa.entity.ResponsePagingData;
import com.bjy.qa.entity.manage.Role;
import com.bjy.qa.entity.manage.RolePermissionDTO;

import java.util.List;
import java.util.Map;

public interface IRoleService extends IService<Role> {
    /**
     * 得到所有权限（Route 菜单 + Permission 按钮）
     * @return
     */
    Map<String, Object> getPermission();

    /**
     * 得到角色的所有权限（Route 菜单 + Permission 按钮）
     * @param roleId 角色 ID
     * @return
     */
    List<String> getRolePermission(Long roleId);

    /**
     * 保存角色的所有权限（Route 菜单 + Permission 按钮）
     * @param rolePermission 角色权限 DTO
     * @return
     */
    Boolean saveRolePermission(RolePermissionDTO rolePermission);

    /**
     * 分页查询角色列表
     * @param myPage
     * @return
     */
    ResponsePagingData list(MyPage<Role> myPage);

    /**
     * 查询所有角色列表
     * @return
     */
    List<Role> listAll();

    /**
     * 查询所有项目角色
     * @return
     */
    List<Role> listAllProjectRoles();

    /**
     * 新增角色信息
     * @param role
     * @return
     */
    Object add(Role role);

    /**
     * 更新角色信息
     * @param role
     * @return
     */
    int update(Role role);

    /**
     * 删除一条数据
     * @param id ID
     * @return
     */
    int delete(Long id);
}
