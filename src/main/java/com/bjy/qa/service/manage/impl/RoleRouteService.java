package com.bjy.qa.service.manage.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bjy.qa.dao.manage.RoleRouteDao;
import com.bjy.qa.entity.manage.RoleRoute;
import com.bjy.qa.service.manage.IRoleRouteService;
import org.springframework.stereotype.Service;

@Service
public class RoleRouteService extends ServiceImpl<RoleRouteDao, RoleRoute> implements IRoleRouteService {

}
