package com.bjy.qa.service.manage;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bjy.qa.entity.manage.Route;
import com.bjy.qa.entity.manage.RouteTree;

import java.util.List;

public interface IRouteService extends IService<Route> {
    /**
     * 查询 路由 Tree 数据
     * @return
     */
    List<RouteTree> listTree();

    /**
     * 根据 id 查找路由信息
     * @param id 路由 id
     * @return
     */
    Object selectById(Long id);

    /**
     * 新增路由信息
     * @param route
     * @return
     */
    Object add(Route route);

    /**
     * 更新路由信息
     * @param route
     * @return
     */
    Object update(Route route);

    /**
     * 删除一条数据
     * @param id ID
     * @return
     */
    Boolean delete(Long id);
}
