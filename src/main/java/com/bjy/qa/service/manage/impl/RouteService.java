package com.bjy.qa.service.manage.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bjy.qa.dao.manage.ResourceDao;
import com.bjy.qa.dao.manage.RoleRouteDao;
import com.bjy.qa.dao.manage.RouteDao;
import com.bjy.qa.dao.manage.RouteMetaDao;
import com.bjy.qa.entity.manage.Route;
import com.bjy.qa.entity.manage.RouteTree;
import com.bjy.qa.enumtype.ResourceType;
import com.bjy.qa.service.manage.IRouteService;
import org.springframework.cache.annotation.CacheEvict;

import javax.annotation.Resource;
import java.util.List;

@org.springframework.stereotype.Service
public class RouteService extends ServiceImpl<RouteDao, Route> implements IRouteService {
    @Resource
    RouteDao routeDao;

    @Resource
    RouteMetaDao routeMetaDao;

    @Resource
    RoleRouteDao roleRouteDao;

    @Resource
    ResourceDao resourceDao;

    @Override
    public List<RouteTree> listTree() {
        return listTree(0L);
    }

    /**
     * 递归查询 路由 Tree 数据
     * @param parentId 父 ID
     * @return
     */
    private List<RouteTree> listTree(Long parentId) {
        List<RouteTree> routeTreeList = routeDao.listTree(parentId);
        for (RouteTree routeTree : routeTreeList) {
            routeTree.setChildren(listTree(routeTree.getId()));
        }
        return routeTreeList;
    }

    @Override
    public Object selectById(Long id) {
        return routeDao.selectById(id);
    }

    @Override
    public Object add(Route route) {
        if (route.getType() == ResourceType.MENU) { // 菜单 路由，添加 route、routeMeta 表
            routeMetaDao.insert(route.getMeta());
            route.setRouteMetaId(route.getMeta().getId());
            routeDao.insert(route);
        } else { // 指令 路由，添加 route 表，routeMetaId 设置成 0
            route.setRouteMetaId(0L);
            routeDao.insert(route);

            // 指令 路由，名称设置成 资源描述
            com.bjy.qa.entity.manage.Resource resource = resourceDao.selectById(route.getResourceId());
            route.setName(resource.getDesc());
        }
        return route;
    }

    @CacheEvict(cacheNames = "qap:roleService.getPermission", allEntries = true)
    @Override
    public Object update(Route route) {
        // 更新数据需要清除 UpdatedTime 和 CreatedTime，否则不会自动更新修改时间
        route.setUpdatedAt(null);
        route.setCreatedAt(null);
        routeDao.updateById(route);

        route.getMeta().setUpdatedAt(null);
        route.getMeta().setCreatedAt(null);
        routeMetaDao.updateById(route.getMeta());

        return route;
    }

    @CacheEvict(cacheNames = "qap:roleService.getPermission", allEntries = true)
    @Override
    public Boolean delete(Long id) {
        Route children = lambdaQuery()
                .eq(Route::getParentId, id)
                .last("limit 1")
                .one();
        if (children != null) {
            throw new RuntimeException("菜单删除失败，该菜单下还有子菜单或子指令！");
        }

        // 查询要删除的 路由 信息
        Route route = routeDao.selectById(id);
        if (route == null) {
            return true;
        }

        if (route.getType() == ResourceType.MENU) { // 菜单 路由，删除 meta 数据
            routeMetaDao.deleteById(route.getMeta().getId());
        }
        roleRouteDao.deleteByRouteId(id); // 删除 角色、路由 关系表 信息
        routeDao.deleteById(id); // 删除 路由 信息
        return true;
    }
}
