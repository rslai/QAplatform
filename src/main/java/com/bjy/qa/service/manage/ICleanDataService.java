package com.bjy.qa.service.manage;

public interface ICleanDataService {
    /**
     * 清除已删除数据
     * @return
     */
    Object cleanDeleted();

    /**
     * 清除测试报告
     * @return
     */
    Object cleanTestResult();
}
