package com.bjy.qa.service.manage;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bjy.qa.entity.manage.RoleRoute;

public interface IRoleRouteService extends IService<RoleRoute> {

}
