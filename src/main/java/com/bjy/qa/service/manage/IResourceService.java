package com.bjy.qa.service.manage;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bjy.qa.entity.MyPage;
import com.bjy.qa.entity.ResponsePagingData;
import com.bjy.qa.entity.manage.Resource;

import java.util.List;

public interface IResourceService extends IService<Resource> {
    /**
     * 分页查询资源列表
     * @param myPage
     * @return
     */
    ResponsePagingData list(MyPage<Resource> myPage);

    /**
     * 查询资源列表
     * @return
     */
    List<Resource> listAll();

    /**
     * 删除一条数据
     * @param id ID
     * @return
     */
    int delete(Long id);

    /**
     * 同步资源信息
     * @return
     */
    Boolean sync();
}
