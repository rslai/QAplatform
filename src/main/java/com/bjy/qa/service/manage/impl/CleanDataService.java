package com.bjy.qa.service.manage.impl;

import com.bjy.qa.dao.manage.CleanDataDao;
import com.bjy.qa.service.manage.ICleanDataService;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@org.springframework.stereotype.Service
public class CleanDataService implements ICleanDataService {
    @Resource
    CleanDataDao cleanDataDao;

    @Override
    public Object cleanDeleted() {
        List<Map> allTableSchema = cleanDataDao.getAllTableSchema();
        for (Map map : allTableSchema) {
            String tableName = (String) map.get("TABLE_NAME");
            if (tableName.toUpperCase().indexOf("QRTZ") < 0) {
                cleanDataDao.cleanDeleted(tableName);
            }
        }
        return true;
    }

    @Override
    public Object cleanTestResult() {
        /**
         * 清理(功能、接口)测试结果数据
         */
        // 清理 临时（不发钉钉通知的）测试报告
        cleanDataDao.cleanTestResultStep(false, 1); // 清理 测试结果详情表 ft_test_result_step
        cleanDataDao.cleanTestResultCase(false, 1); // 清理 测试用例结果表 ft_test_result_case
        cleanDataDao.cleanTestResultSuite(false, 1); // 清理 测试结果表 ft_test_result_suite
        // 清理 永久（发钉钉通知的）测试报告
        cleanDataDao.cleanTestResultStep(true, 7); // 清理 测试结果详情表 ft_test_result_step
        cleanDataDao.cleanTestResultCase(true, 7); // 清理 测试用例结果表 ft_test_result_case
        cleanDataDao.cleanTestResultSuite(true, 7); // 清理 测试结果表 ft_test_result_suite
        // 清理 悬空（没有测试套件）的测试报告
        cleanDataDao.cleanHangAirTestResultStep(); // 清理 悬空（没有测试套件）的测试结果详情表 ft_test_result_step
        cleanDataDao.cleanHangAirTestResultCase(); // 清理 悬空（没有测试套件）的测试用例结果表 ft_test_result_case
        cleanDataDao.cleanHangAirTestResultSuite(); // 清理 悬空（没有测试套件）的测试结果表 ft_test_result_suite

        /**
         * 清理(性能)测试结果数据
         */
        // 清理 临时（不发钉钉通知的）测试报告
        cleanDataDao.cleanPTTestResultLog(false, 1); // 清理 性能测试结果日志表 pt_test_result_log
        cleanDataDao.cleanPTTestResultReport(false, 1); // 清理 性能测试结果报告 pt_test_result_report
        cleanDataDao.cleanPTTestResultTestScript(false, 1); // 清理 性能测试结果 对应 性能测试脚本 表 pt_test_result_test_script
        cleanDataDao.cleanPTTestResult(false, 1); // 清理 性能测试结果表 pt_test_result
        // 清理 永久（发钉钉通知的）测试报告
        cleanDataDao.cleanPTTestResultLog(true, 7); // 清理 性能测试结果日志表 pt_test_result_log
        cleanDataDao.cleanPTTestResultReport(true, 7); // 清理 性能测试结果报告 pt_test_result_report
        cleanDataDao.cleanPTTestResultTestScript(true, 7); // 清理 性能测试结果 对应 性能测试脚本 表 pt_test_result_test_script
        cleanDataDao.cleanPTTestResult(true, 7); // 清理 性能测试结果表 pt_test_result
        // 清理 悬空（没有测试套件）的测试报告
        cleanDataDao.cleanHangAirPTTestResultLog(); // 清理 悬空（没有测试套件）性能测试结果日志表 pt_test_result_log
        cleanDataDao.cleanHangAirPTTestResultReport(); // 清理 悬空（没有测试套件）性能测试结果报告 pt_test_result_report
        cleanDataDao.cleanHangAirPTTestResultTestScript(); // 清理 悬空（没有测试套件）性能测试结果 对应 性能测试脚本 表 pt_test_result_test_script
        cleanDataDao.cleanHangAirPTTestResult(); // 清理 悬空（没有测试套件）性能测试结果表 pt_test_result

        // 释放表空间
        cleanDataDao.optimizeTableTestResultStep(); // 释放 ft_test_result_step 表空间
        cleanDataDao.optimizeTableTestResultCase(); // 释放 ft_test_result_case 表空间
        cleanDataDao.optimizeTableTestResultSuite(); // 释放 ft_test_result_suite 表空间
        return true;
    }
}
