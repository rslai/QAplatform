package com.bjy.qa.service.tools;

import com.bjy.qa.entity.MyPage;
import com.bjy.qa.entity.ResponsePagingData;
import com.bjy.qa.entity.tools.Upload;

public interface IUploadService {

    /**
     * 分页查询上传问价列表
     * @param myPage
     * @return
     */
    ResponsePagingData list(MyPage<Upload> myPage) throws Exception;

    /**
     * 根据 id 查询数据
     * @param id
     * @return
     */
    Upload select(Long id);

    /**
     * 新增一条数据
     * @param upload
     * @return
     */
    int add(Upload upload);

    /**
     * 更新一条数据
     * @param upload
     * @return
     */
    int update(Upload upload);

    /**
     * 删除一条数据
     * @param id ID
     * @return
     */
    int delete(Long id);

}
