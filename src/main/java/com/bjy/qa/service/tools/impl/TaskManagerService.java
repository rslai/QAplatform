package com.bjy.qa.service.tools.impl;

import com.bjy.qa.entity.tools.Task;
import com.bjy.qa.enumtype.TaskStatus;
import com.bjy.qa.service.tools.ITaskManagerService;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Service
public class TaskManagerService implements ITaskManagerService {
    public static Map<String, Task> taskMap = new HashMap<>(); // 保存所有任务列表

    @Override
    public Task newTask() {
        Task task = new Task();
        task.setTaskId(UUID.randomUUID().toString());
        task.setStatus(TaskStatus.CREATE);

        TaskManagerService.taskMap.put(task.getTaskId(), task);
        return task;
    }

    @Override
    public boolean changeTaskStatus(String taskId, TaskStatus taskStatus) {
        Task task = TaskManagerService.taskMap.get(taskId);
        if (task != null) {
            task.setStatus(taskStatus);
        }
        return true;
    }

    @Override
    public boolean changeTaskStatus(String taskId, TaskStatus taskStatus, String msg) {
        Task task = TaskManagerService.taskMap.get(taskId);
        if (task != null) {
            task.setStatus(taskStatus);
            task.setMsg(msg);
        }
        return true;
    }

    @Override
    public boolean changeTaskStatus(String taskId, TaskStatus taskStatus, String msg, String detailData) {
        Task task = TaskManagerService.taskMap.get(taskId);
        if (task != null) {
            task.setStatus(taskStatus);
            task.setMsg(msg);
            task.setDetailData(detailData);
        }
        return true;
    }

    @Override
    public Task getTask(String taskId) {
        Task task = TaskManagerService.taskMap.get(taskId);
        if (task == null) {
            task = new Task();
            task.setTaskId(taskId);
            task.setStatus(TaskStatus.END_SUCCESS);
        }
        return task;
    }

    @Override
    public Map<String, Task> getTasks() {
        return TaskManagerService.taskMap;
    }

    @Override
    public boolean deleteTask(String taskId) {
        TaskManagerService.taskMap.remove(taskId);
        return true;
    }
}
