package com.bjy.qa.service.tools;

import com.bjy.qa.entity.MyPage;
import com.bjy.qa.entity.ResponsePagingData;
import com.bjy.qa.entity.tools.Ocr;

public interface IOcrService {

    /**
     * 分页查询上传问价列表
     * @param myPage
     * @return
     */
    ResponsePagingData list(MyPage<Ocr> myPage) throws Exception;

    /**
     * 根据 id 查询数据
     * @param id
     * @return
     */
    Ocr select(Long id);

    /**
     * 新增一条数据
     * @param ocr
     * @return
     */
    Object add(Ocr ocr);

    /**
     * 删除一条数据
     * @param id ID
     * @return
     */
    int delete(Long id);

}
