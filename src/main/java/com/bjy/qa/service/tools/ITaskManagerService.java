package com.bjy.qa.service.tools;

import com.bjy.qa.entity.tools.Task;
import com.bjy.qa.enumtype.TaskStatus;

import java.util.Map;

public interface ITaskManagerService {
    /**
     * 创建任务
     * @return
     */
    Task newTask();

    /**
     * 修改任务状态
     * @param taskId 任务 ID
     * @param taskStatus 任务状态
     * @return
     */
    boolean changeTaskStatus(String taskId, TaskStatus taskStatus);

    /**
     * 修改任务状态
     * @param taskId 任务 ID
     * @param taskStatus 任务状态
     * @param msg 信息（如果有想要前端展示的信息可以放在这里）
     * @return
     */
    boolean changeTaskStatus(String taskId, TaskStatus taskStatus, String msg);

    /**
     * 修改任务状态
     * @param taskId 任务 ID
     * @param taskStatus 任务状态
     * @param msg 信息（如果有想要前端展示的信息，可以放在这里）
     * @param detailData 详细数据（如果要显示更详细的信息或者 json 数据可以放在这里）
     * @return
     */
    boolean changeTaskStatus(String taskId, TaskStatus taskStatus, String msg, String detailData);

    /**
     * 查询任务
     * @param taskId 任务 ID
     * @return
     */
    Task getTask(String taskId);

    /**
     * 获取所有任务
     * @return
     */
    Map<String, Task> getTasks();

    /**
     * 删除任务
     * @param taskId 任务 ID
     * @return
     */
    boolean deleteTask(String taskId);
}
