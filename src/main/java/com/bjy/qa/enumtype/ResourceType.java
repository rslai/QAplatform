package com.bjy.qa.enumtype;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * 资源类型 枚举类
 */
public enum ResourceType {
    NONE("none", 0),
    MENU("菜单资源", 1),
    DIRECTIVE("指令资源", 2);

    private String name;

    @JsonValue
    @EnumValue
    private int value;

    ResourceType(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    @JSONField
    public int getValue() {
        return value;
    }
}
