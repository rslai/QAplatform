package com.bjy.qa.enumtype;

/**
 * 参数比较 枚举类
 */
public enum Comparison {
    EQUAL("等于"),
    NOT_EQUAL("不等于"),
    IS_ABOVE("大于"),
    IS_ATLEAST("大于或等于"),
    IS_BELOW("小于"),
    IS_AT_MOST("小于或等于"),
    INCLUDE("包含"),
    NOT_INCLUDE("不包含"),
    MATCH("正则匹配"),
    EXISTS("存在"),
    NOT_EXIST("不存在");

    private String name;

    Comparison(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
