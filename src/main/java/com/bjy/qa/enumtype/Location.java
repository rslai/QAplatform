package com.bjy.qa.enumtype;

/**
 * 参数位置 枚举类
 */
public enum Location {
    QUERY("query"),
    PATH("path"),
    HEADER("header"),
    COOKIE("cookie"),
    BODY("body");

    private String name;

    Location(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
