package com.bjy.qa.enumtype;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * 执行任务状态枚举类
 */
public enum TaskStatus {
    NONE("none", 0),
    CREATE("创建", 1),
    RUN("运行中", 2),
    END_BREAK("结束-中断", 3),
    END_FAILED("结束-失败", 4),
    END_SUCCESS("结束-成功", 5);

    private String name;
    @JsonValue
    @EnumValue
    private int value;

    TaskStatus(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    @JSONField
    public int getValue() {
        return value;
    }
}
