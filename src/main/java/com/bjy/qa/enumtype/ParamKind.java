package com.bjy.qa.enumtype;
import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * api 参数 类型 枚举类
 */
public enum ParamKind {
    NONE("none", 0),
    KIND_HEADER("header", 1),
    KIND_URL_PARAM("params", 2),
    KIND_BODY("body", 3),
    KIND_COOKIE("cookies", 4),
    KIND_ASSERT("assert", 5),
    KIND_MOCK("mock", 6);

    private String name;

    @JsonValue
    @EnumValue
    private int value;

    ParamKind(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    @JSONField
    public int getValue() {
        return value;
    }
}
