package com.bjy.qa.enumtype;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * OCR通道枚举类
 */
public enum OcrChannel {
    QAP("QAplatform ocr", 1),
    BAIDU("百度ocr", 2);

    private String name;

    @JsonValue
    @EnumValue
    private int value;

    OcrChannel(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    @JSONField
    public int getValue() {
        return value;
    }
}
