package com.bjy.qa.enumtype;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * 机器人类型 枚举类
 */
public enum RobotType {
    DING_TALK("钉钉机器人", 1),
    WE_CHAT("企业微信机器人", 2),
    FEI_SHU("飞书机器人", 3),
    YOU_SPACE("友空间机器人", 4),
    EMAIL("邮件", 5);

    private String name;

    @JsonValue
    @EnumValue
    private int value;

    RobotType(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    @JSONField
    public int getValue() {
        return value;
    }
}
