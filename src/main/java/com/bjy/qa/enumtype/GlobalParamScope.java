package com.bjy.qa.enumtype;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;

public enum GlobalParamScope {
    NONE("none", 0),
    CASE_UNIQUENESS("case唯一", 1),
    SUIT_UNIQUENESS("suit唯一", 2);

    private String name;

    @JsonValue
    @EnumValue
    private int value;

    GlobalParamScope(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getType() {
        return name;
    }

    @JSONField
    public int getValue() {
        return value;
    }
}
