package com.bjy.qa.enumtype;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * 用例编辑状态 枚举类
 */
public enum EditStatus {
    NONE("none", 0),
    WRITING("编写中", 1),
    WAIT_REVIEW("等待评审", 2),
    REVIEW_REFUSE("评审不通过", 3),
    FINISH("评审通过", 4);

    private String name;

    @JsonValue
    @EnumValue
    private int value;

    EditStatus(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    @JSONField
    public int getValue() {
        return value;
    }
}
