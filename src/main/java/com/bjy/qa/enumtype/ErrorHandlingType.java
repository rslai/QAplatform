package com.bjy.qa.enumtype;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * 错误处理类型 枚举类
 */
public enum ErrorHandlingType {
    NONE("none", 0),
    IGNORE("忽略", 1),
    WARNING("警告", 2),
    STOP("中断", 3);

    private String name; // 状态名称

    @JsonValue
    @EnumValue
    private int value; // 状态值

    ErrorHandlingType(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    @JSONField
    public int getValue() {
        return value;
    }
}
