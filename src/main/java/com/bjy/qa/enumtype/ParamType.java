package com.bjy.qa.enumtype;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * api 参数 类型 枚举类
 */
public enum ParamType {
    String("string", 0),
    Int("int", 1),
    Bigint("bigint", 2),
    File("file", 3),
    Json("json", 4),
    Text("text", 5),
    Array("array", 6);

    private String name;

    @JsonValue
    @EnumValue
    private int value;

    ParamType(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    @JSONField
    public int getValue() {
        return value;
    }
}
