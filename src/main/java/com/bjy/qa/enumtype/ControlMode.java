package com.bjy.qa.enumtype;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * 控制模式 枚举类
 */
public enum ControlMode {
    CENTRAL("集中模式", 1), // 集中模式（同时启动所有并发（设置的并发数/线程/协程），当设置的并发数全部结束后（某个线程（协程）完成后需要等待其他的线程（协程）完成），再次启动所设置的并发进行施压。）
    INDIVIDUAL("单独模式", 2); // 单独模式（同时启动所有并发（设置的并发数/线程/协程），当其中的某个或某些线程（协程）完成后，立即再次启动完成的线程（协程），不等待其他的线程（协程）。）

    private String name;

    @JsonValue
    @EnumValue
    private int value;

    ControlMode(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    @JSONField
    public int getValue() {
        return value;
    }
}
