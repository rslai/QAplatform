package com.bjy.qa.enumtype;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * 转发模式
 */
public enum ForwardMode {
    NOT_FORWARD("不转发", 0),
    API_NOT_DEFINE("接口未定义", 1), // 未定义接口 url，转发
    CONDITIONS_NOT_DEFINE("条件未定义", 2); // 未定义接口 url + 定义了 url 但没有匹配上条件，转发

    private String name;

    @JsonValue
    @EnumValue
    private int value;

    ForwardMode(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    @JSONField
    public int getValue() {
        return value;
    }
}
