package com.bjy.qa.enumtype;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * 定时任务状态 枚举类
 */
public enum JobStatus {
    ON("启用", 1),
    OFF("关闭", 2),
    ONCE("运行一次", 3);

    private String name;

    @JsonValue
    @EnumValue
    private int value;

    JobStatus(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    @JSONField
    public int getValue() {
        return value;
    }
}
