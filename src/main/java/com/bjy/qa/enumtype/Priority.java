package com.bjy.qa.enumtype;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * 用例优先级 枚举类
 */
public enum Priority {
    P0("P0", 0),
    P1("P0", 1),
    P2("P0", 2),
    P3("P3", 3);

    private String name;

    @JsonValue
    @EnumValue
    private int value;

    Priority(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    @JSONField
    public int getValue() {
        return value;
    }
}
