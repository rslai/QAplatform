package com.bjy.qa.enumtype;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * 执行状态 枚举类
 */
public enum RunStatus {
    WAITING("待运行", 1),
    RUNNING("运行中", 2),
    INFO("信息", 3),
    PASSED("通过", 4),
    WARN("警告", 5),
    SKIPPED("跳过", 11),
    BROKEN("中断", 12),
    UNKNOWN("未知", 13),
    ERROR("错误", 21),
    FAILED("失败", 22);

    private String name; // 状态名称
    @JsonValue
    @EnumValue
    private int value; // 状态值

    RunStatus(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    @JSONField
    public int getValue() {
        return value;
    }
}
