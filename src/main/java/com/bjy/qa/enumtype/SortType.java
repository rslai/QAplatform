package com.bjy.qa.enumtype;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 *  排序类型 枚举类
 */
public enum SortType {
    BEFORE("目标节点前", 1),
    AFTER("目标节点后", 2),
    INNER("目标节点内-子节点", 3);

    private String name;

    @JsonValue
    @EnumValue
    private int value;

    SortType(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }
}
