package com.bjy.qa.enumtype;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * 压测模式 枚举类
 */
public enum LoadTestMode {
    CONCURRENT("并发模式", 1), // 并发模式
    ROUND_ROBIN("轮次模式", 2), // 轮次模式
    RAMP_UP("阶梯模式", 3), // 阶梯模式
    ERROR_RATE("错误率模式", 4), // 错误率模式
    RESPONSE_TIME("响应时间模式", 5), // 响应时间模式
    THROUGHPUT("每秒应答数模式", 6); // 每秒应答数模式

    private String name;

    @JsonValue
    @EnumValue
    private int value;

    LoadTestMode(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    @JSONField
    public int getValue() {
        return value;
    }
}
