package com.bjy.qa.enumtype;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;

public enum GlobalParamType {
    STATIC_PARAMETER("Static parameter", 0),
    DYNAMIC_PARAMETER("Dynamic parameter", 1),
    USER_PARAMETER("User parameter", 2);

    private String name;

    @JsonValue
    @EnumValue
    private int value;

    GlobalParamType(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getType() {
        return name;
    }

    @JSONField
    public int getValue() {
        return value;
    }
}

