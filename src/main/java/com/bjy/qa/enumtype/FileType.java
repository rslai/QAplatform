package com.bjy.qa.enumtype;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * 文件类型 枚举类
 */
public enum FileType {
    EXCEL("excel", 1),
    XMIND("xmind", 2),
    OPEN_API("OpenAPI（swaggerApi）", 3),
    POSTMAN("Postman（Postman Collection v2.1）", 4);

    private String name;

    @JsonValue
    @EnumValue
    private int value;

    FileType(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    @JSONField
    public int getValue() {
        return value;
    }
}
