package com.bjy.qa.enumtype;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * 执行结果状态枚举类
 */
public enum Status {
    SUCCESS("成功", true),
    FAILED("失败", false);

    private String name;
    @JsonValue
    @EnumValue
    private boolean status;

    Status(String name, boolean status) {
        this.name = name;
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public boolean isStatus() {
        return status;
    }
}
