package com.bjy.qa.enumtype;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * agent 状态枚举类
 */
public enum AgentStatus {
    OFF_LINE("离线", 1),
    ON_LINE("在线", 2);

    private String name;
    
    @JsonValue
    @EnumValue
    private int value;

    AgentStatus(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    @JSONField
    public int getValue() {
        return value;
    }

}
