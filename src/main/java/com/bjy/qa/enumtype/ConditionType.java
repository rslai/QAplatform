package com.bjy.qa.enumtype;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * 条件 枚举类（实现）
 */
public enum ConditionType {
    NONE("none", 0), // 不是 条件步骤
    IF("if", 1), // if 条件步骤
    ELSE_IF("else_if", 2), //else if 条件步骤
    ELSE("else", 3), // else 条件步骤
    WHILE("while", 4), // while 条件步骤
    FOR("for", 5); // for 条件步骤

    @JsonValue
    @EnumValue
    private final int value;

    private final String name;

    ConditionType(String name, int value) {
        this.name = name;
        this.value = value;
    }

    @JSONField
    public Integer getValue() {
        return value;
    }

    public String getName() {
        return name;
    }
}
