package com.bjy.qa.enumtype;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * 错误码枚举类
 */
public enum ErrorCode {
    REQUEST_SUCCEEDED(0, "请求成功"),
    REQUEST_SUCCEEDED_ASYNC(0, "异步执行成功，请稍后查询执行结果!"),
    REQUEST_FAILED(-1, "请求失败"),
    ARGUMENT_NOT_VALID(-100, "参数校验失败"),
    TOKEN_EXCEPTION(-402, "Token验证异常"),
    UNAUTHORIZED(-401, "拒绝访问 - 未授权"),
    FORBIDDEN(-403, "拒绝访问 - 未登录");

    ErrorCode(int code, String msg) {
        this.msg = msg;
        this.code = code;
    }

    private String msg; // 错误信息
    @JsonValue
    @EnumValue
    private int code; // 错误码

    public String getMsg() {
        return msg;
    }

    public int getCode() {
        return code;
    }
}
