package com.bjy.qa.enumtype;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * 定时任务型 枚举类
 */
public enum JobType {
    SYSTEM_JOB("系统任务", 0),
    INTERFACE("接口", 1),
    PERFORMANCE("性能", 2);

    private String name;

    @JsonValue
    @EnumValue
    private int value;

    JobType(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    @JSONField
    public int getValue() {
        return value;
    }
}
