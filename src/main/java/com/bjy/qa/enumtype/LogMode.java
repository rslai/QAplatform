package com.bjy.qa.enumtype;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * 日志模式 枚举类
 */
public enum LogMode {
    OFF("关闭", 1), // 关闭
    FULL("开启-全部日志", 2), // 开启-全部日志
    SUCCESS_ONLY("开启-仅成功日志", 3), // 开启-仅成功日志
    ERROR_ONLY("开启-仅错误日志", 4); // 开启-仅错误日志

    private String name;

    @JsonValue
    @EnumValue
    private int value;

    LogMode(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    @JSONField
    public int getValue() {
        return value;
    }
}
