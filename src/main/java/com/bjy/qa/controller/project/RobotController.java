package com.bjy.qa.controller.project;

import com.bjy.qa.entity.Response;
import com.bjy.qa.entity.manage.Project;
import com.bjy.qa.entity.project.Robot;
import com.bjy.qa.service.project.IRobotService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@Controller
@RequestMapping("/project/robot")
@Validated
@Api(tags = "机器人")
public class RobotController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    IRobotService iRobotService;

    @RequestMapping(value = "/listAll", method = {RequestMethod.GET})
    @ResponseBody
    @ApiOperation(value = "查询机器人列表", notes = "查询机器人列表")
    public Response<List<Robot>> listAll(@NotNull(message = "projectId: projectId 字段必填") Long projectId) throws Exception {
        logger.info("查询定时任列表");
        Response response = Response.success(iRobotService.listAll(projectId));
        logger.info("查询定时任列表，成功。{}", response);
        return response;
    }

    @RequestMapping(value = "/add", method = {RequestMethod.POST})
    @ResponseBody
    @Validated(Project.ProjectGroup.class)
    @ApiOperation(value = "新增机器人信息", notes = "新增机器人信息")
    public Response<Object> add(@RequestBody @Valid Robot robot) throws Exception {
        logger.info("新增机器人信息 {}", robot);
        Response response = Response.success(iRobotService.add(robot));
        logger.info("新增机器人信息，成功 {}", response);
        return response;
    }

    @RequestMapping(value = "/update", method = {RequestMethod.POST})
    @ResponseBody
    @Validated(Project.ProjectGroup.class)
    @ApiOperation(value = "修改机器人信息", notes = "修改机器人信息")
    public Response<Integer> update(@RequestBody @Valid Robot robot) throws Exception {
        logger.info("修改机器人信息 {}", robot);
        Response response = Response.success(iRobotService.update(robot));
        logger.info("修改机器人信息，成功 {}", response);
        return response;
    }

    @RequestMapping(value = "/delete", method = {RequestMethod.GET})
    @ResponseBody
    @ApiOperation(value = "删除机器人信息", notes = "删除机器人信息")
    public Response<Integer> delete(@RequestParam @NotNull(message = "ID: id字段必填") Long id) {
        logger.info("删除机器人信息 ID: " + id);
        Response response = Response.success(iRobotService.delete(id));
        logger.info("删除机器人信息，成功。{}", response);
        return response;
    }
}
