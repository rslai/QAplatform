package com.bjy.qa.controller.project;

import com.bjy.qa.entity.MyPage;
import com.bjy.qa.entity.Response;
import com.bjy.qa.entity.ResponsePagingData;
import com.bjy.qa.entity.project.JobResult;
import com.bjy.qa.service.project.IJobResultService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

@Controller
@RequestMapping("/project/jobResult")
@Validated
@Api(tags = "定时任务执行结果")
public class JobResultController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    IJobResultService iJobResultService;

    @RequestMapping(value = "/list", method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value = "分页查询定时任务执行结果列表", notes = "分页查询定时任务执行结果列表")
    public Response<ResponsePagingData> list(@RequestBody MyPage<JobResult> myPage) throws Exception {
        logger.info("分页查询定时任务执行结果列表 {}", myPage);
        Response response = Response.success(iJobResultService.list(myPage));
        logger.info("分页查询定时任务执行结果列表，成功。{}", response);
        return response;
    }
}
