package com.bjy.qa.controller.project;

import com.bjy.qa.entity.MyPage;
import com.bjy.qa.entity.Response;
import com.bjy.qa.entity.ResponsePagingData;
import com.bjy.qa.entity.functionaltest.TestSuite;
import com.bjy.qa.entity.manage.Project;
import com.bjy.qa.entity.project.Job;
import com.bjy.qa.service.project.IJobService;
import com.bjy.qa.util.security.SecurityUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Controller
@RequestMapping("/project/job")
@Validated
@Api(tags = "定时任务")
public class JobController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    IJobService iJobService;

    @RequestMapping(value = "/list", method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value = "分页查询定时任务列表", notes = "分页查询定时任务列表")
    public Response<ResponsePagingData> list(@RequestBody MyPage<Job> myPage) throws Exception {
        logger.info("分页查询定时任列表 {}", myPage);
        Response response = Response.success(iJobService.list(myPage));
        logger.info("分页查询定时任列表，成功。{}", response);
        return response;
    }

    @RequestMapping(value = "/add", method = {RequestMethod.POST})
    @ResponseBody
    @Validated(Project.ProjectGroup.class)
    @ApiOperation(value = "新增定时任务信息", notes = "新增定时任务信息")
    public Response<Object> add(@RequestBody @Valid Job job) throws Exception {
        logger.info("新增定时任务信息 {}", job);
        Response response = Response.success(iJobService.add(job));
        logger.info("新增定时任务信息，成功 {}", response);
        return response;
    }

    @RequestMapping(value = "/update", method = {RequestMethod.POST})
    @ResponseBody
    @Validated(Project.ProjectGroup.class)
    @ApiOperation(value = "修改定时任务信息", notes = "修改定时任务信息")
    public Response<Integer> update(@RequestBody @Valid Job job) throws Exception {
        logger.info("修改定时任务信息 {}", job);
        Response response = Response.success(iJobService.update(job));
        logger.info("修改定时任务信息，成功 {}", response);
        return response;
    }

    @RequestMapping(value = "/delete", method = {RequestMethod.GET})
    @ResponseBody
    @ApiOperation(value = "删除定时任务信息", notes = "删除定时任务信息")
    public Response<Integer> delete(@RequestParam @NotNull(message = "ID: id字段必填") Long id) {
        logger.info("删除定时任务信息 ID: " + id);
        Response response = Response.success(iJobService.delete(id));
        logger.info("删除定时任务信息，成功。{}", response);
        return response;
    }

    @RequestMapping(value = "/updateStatus", method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value = "更新定时任务状态", notes = "更新定时任务状态")
    public Response<TestSuite> updateStatus(@RequestBody @Valid Job job) throws Exception {
        logger.info("更新定时任务状态 projectId: {}, ID: {}, status: {}", job.getProjectId(), job.getId(), job.getStatus());
        String runUser = SecurityUtils.getCurrentUserName();
        Response response = Response.success(iJobService.updateStatus(job.getProjectId(), job.getId(), job.getStatus()));
        logger.info("更新定时任务状态，成功。{}", response);
        return response;
    }
}
