package com.bjy.qa.controller.performancetest;

import com.bjy.qa.entity.MyPage;
import com.bjy.qa.entity.Response;
import com.bjy.qa.entity.ResponsePagingData;
import com.bjy.qa.entity.performancetest.PerfTestSuite;
import com.bjy.qa.enumtype.CatalogType;
import com.bjy.qa.service.performancetest.IPerfTestSuiteService;
import com.bjy.qa.util.security.SecurityUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@Controller
@RequestMapping("/pt/perfTestSuite")
@Validated
@Api(tags = "性能测试套件")
public class PerfTestSuiteController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    IPerfTestSuiteService iPerfTestSuiteService;

    @RequestMapping(value = "/list", method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value = "分页查询 性能测试套件 信息列表", notes = "分页查询 性能测试套件 信息列表")
    public Response<ResponsePagingData> list(@RequestBody MyPage<PerfTestSuite> myPage) {
        logger.info("分页查询 性能测试套件 信息列表 {}", myPage);
        Response response = Response.success(iPerfTestSuiteService.list(myPage));
        logger.info("分页查询 性能测试套件 信息列表，成功 {}", response);
        return response;
    }

    @RequestMapping(value = "/listAll", method = {RequestMethod.GET})
    @ResponseBody
    @ApiOperation(value = "查询所有 性能测试套件", notes = "查询所有 性能测试套件")
    public Response<List<PerfTestSuite>> listAll(@NotNull(message = "projectId: projectId字段必填") Long projectId) {
        logger.info("查询所有 性能测试套件 projectId: {}", projectId);
        Response response = Response.success(iPerfTestSuiteService.listAll(projectId));
        logger.info("查询所有 性能测试套件，成功 {}", response);
        return response;
    }

    @RequestMapping(value = "/findById", method = {RequestMethod.GET})
    @ResponseBody
    @ApiOperation(value = "查询 性能测试套件 全部信息（包括套件下的所有用例、压力机、场景）", notes = "查询 性能测试套件 全部信息（包括套件下的所有用例、压力机、场景）")
    public Response<PerfTestSuite> findById(@NotNull(message = "ID: id 字段必填") Long id) {
        logger.info("查询 性能测试套件 全部信息（包括套件下的所有用例、压力机、场景） id: " + id);
        Response response = Response.success(iPerfTestSuiteService.findById(id));
        logger.info("查询 性能测试套件 全部信息（包括套件下的所有用例、压力机、场景），成功 {}", response);
        return response;
    }

    @RequestMapping(value = "/add", method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value = "新增 性能测试套件", notes = "新增 性能测试套件")
    public Response<Integer> add(@RequestBody @Valid PerfTestSuite perfTestSuite) {
        logger.info("新增 性能测试套件 {}", perfTestSuite);
        perfTestSuite.setCaseType(CatalogType.PERFORMANCE_TEST_SCRIPT.getValue());
        Response response = Response.success(iPerfTestSuiteService.add(perfTestSuite));
        logger.info("新增 性能测试套件，成功。 {}", response);
        return response;
    }

    @RequestMapping(value = "/update", method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value = "修改 性能测试套件", notes = "修改 性能测试套件")
    public Response<Integer> update(@RequestBody @Valid PerfTestSuite perfTestSuite) {
        logger.info("修改 性能测试套件 {}", perfTestSuite);
        Response response = Response.success(iPerfTestSuiteService.update(perfTestSuite));
        logger.info("修改 性能测试套件，成功。 {}", response);
        return response;
    }

    @RequestMapping(value = "/delete", method = {RequestMethod.GET})
    @ResponseBody
    @ApiOperation(value = "删除 性能测试套件 信息", notes = "删除 性能测试套件 信息")
    public Response<Boolean> delete(@NotNull(message = "ID: id 字段必填") Long id) {
        logger.info("删除 性能测试套件 信息 ID: " + id);
        Response response = Response.success(iPerfTestSuiteService.delete(id));
        logger.info("删除 性能测试套件 信息，成功。 {}", response);
        return response;
    }

    @ResponseBody
    @RequestMapping(value = "/run", method = {RequestMethod.GET})
    @ApiOperation(value = "执行性能测试套件", notes = "执行性能测试套件")
    public Response<Boolean> run(@NotNull(message = "projectId: projectId字段必填") Long projectId, @NotNull(message = "ID: id字段必填！") Long id) throws Exception {
        Long userId = SecurityUtils.getCurrentUserId();
        logger.info("执行性能测试套件 projectId: {} userId: {} id: {}", projectId, userId, id);
        Response response = Response.success(iPerfTestSuiteService.run(projectId, userId, id));
        logger.info("执行测试套件，成功。{}", response);
        return response;
    }

    @ResponseBody
    @RequestMapping(value = "/forceStop", method = {RequestMethod.GET})
    @ApiOperation(value = "停止性能测试套件", notes = "停止性能测试套件")
    public Response<Boolean> forceStop(@NotNull(message = "ID: id字段必填！") Long id) {
        logger.info("停止性能测试套件 ID: {}", id);
        String runUser = SecurityUtils.getCurrentUserName();
        Response response = Response.success(iPerfTestSuiteService.forceStop(id, runUser));
        logger.info("停止性能测试套件，成功。{}", response);
        return response;
    }

    @ResponseBody
    @RequestMapping(value = "/updateBaseLine", method = {RequestMethod.GET})
    @ApiOperation(value = "设为 或 取消 基线", notes = "设为 或 取消 基线")
    public Response<Boolean> updateBaseLine(@NotNull(message = "ID: id字段必填！") Long id) {
        logger.info("设为 或 取消 基线 ID: {}", id);
        Response response = Response.success(iPerfTestSuiteService.updateBaseLine(id));
        logger.info("设为 或 取消 基线，成功。{}", response);
        return response;
    }
}
