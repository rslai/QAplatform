package com.bjy.qa.controller.performancetest;

import com.bjy.qa.entity.Response;
import com.bjy.qa.entity.functionaltest.SearchDTO;
import com.bjy.qa.entity.performancetest.TestScript;
import com.bjy.qa.service.performancetest.impl.TestScriptService;
import com.bjy.qa.util.security.SecurityUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@Controller
@RequestMapping("/pt/testScript")
@Validated
@Api(tags = "性能测试脚本表")
public class TestScriptController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    TestScriptService iTestScriptService;

    @RequestMapping(value = "/add", method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value = "添加性能测试脚本", notes = "添加性能测试脚本")
    public Response<Integer> add(@RequestBody @Valid TestScript testScript) throws Exception {
        testScript.setUserId(SecurityUtils.getCurrentUserId());
        logger.info("添加性能测试脚本：{}", testScript);
        Response response = Response.success(iTestScriptService.add(testScript));
        logger.info("添加性能测试脚本，成功 {}", response);
        return response;
    }

    @RequestMapping(value = "/selectById", method = {RequestMethod.GET})
    @ResponseBody
    @ApiOperation(value = "根据 ID 查询性能测试脚本", notes = "根据 ID 查询性能测试脚本")
    public Response<TestScript> selectById(@NotNull(message = "ID: id字段必填") Long id) {
        logger.info("根据 ID 查询性能测试脚本 id: {}", id);
        Response response = Response.success(iTestScriptService.selectById(id));
        logger.info("根据 ID 查询性能测试脚本，成功 {}", response);
        return response;
    }

    @RequestMapping(value = "/update", method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value = "修改性能测试脚本", notes = "修改性能测试脚本")
    public Response<Integer> update(@RequestBody @Valid TestScript testScript) throws Exception {
        testScript.setUserId(SecurityUtils.getCurrentUserId());
        logger.info("修改性能测试脚本：{}", testScript);
        Response response = Response.success(iTestScriptService.update(testScript));
        logger.info("修改性能测试脚本，成功 {}", response);
        return response;
    }

    @RequestMapping(value = "/delete", method = {RequestMethod.GET})
    @ResponseBody
    @ApiOperation(value = "删除性能测试脚本", notes = "删除性能测试脚本")
    public Response<Integer> delete(@NotNull(message = "ID: id字段必填") Long id) {
        logger.info("删除性能测试脚本，ID：" + id);
        Response response = Response.success(iTestScriptService.delete(id));
        logger.info("删除性能测试脚本，成功 {}", response);
        return response;
    }

    @RequestMapping(value = "/selectByCatalogId", method = {RequestMethod.GET})
    @ResponseBody
    @ApiOperation(value = "根据 目录ID 查询该目录下的性能测试脚本", notes = "根据 目录ID 查询该目录下的性能测试脚本")
    public Response<List<TestScript>> selectByCatalogId(@NotNull(message = "projectId: 项目id字段必填") Long projectId, @NotNull(message = "catalogId: 目录id字段必填") Long catalogId) {
        logger.info("根据 目录ID 查询该目录下的性能测试脚本，projectId：{}, catalogId: {}", projectId, catalogId);
        Response response = Response.success(iTestScriptService.selectByCatalogId(projectId, catalogId));
        logger.info("根据 目录ID 查询该目录下的性能测试脚本，成功 {}", response);
        return response;
    }

    @RequestMapping(value = "/search", method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value = "搜索性能测试脚本", notes = "搜索性能测试脚本")
    public Response<List<TestScript>> search(@RequestBody @Valid SearchDTO searchDTO) {
        logger.info("搜索性能测试脚本，{}", searchDTO);
        Response response = Response.success(iTestScriptService.search(searchDTO));
        logger.info("搜索性能测试脚本，成功 {}", response);
        return response;
    }
}
