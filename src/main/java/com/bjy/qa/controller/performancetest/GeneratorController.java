package com.bjy.qa.controller.performancetest;

import com.bjy.qa.entity.MyPage;
import com.bjy.qa.entity.Response;
import com.bjy.qa.entity.ResponsePagingData;
import com.bjy.qa.entity.performancetest.Generator;
import com.bjy.qa.service.performancetest.IGeneratorService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@Controller
@RequestMapping("/pt/generator")
@Validated
@Api(tags = "压力机（load generators）")
public class GeneratorController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    IGeneratorService iGeneratorService;

    @ApiOperation(value = "查询所有压力机（load generators）", notes = "不分页的压力机（load generators）列表")
    @RequestMapping(value = "/listAll", method = {RequestMethod.GET})
    @ResponseBody
    public Response<List<Generator>> listAll(@NotNull(message = "projectId: projectId 字段必填") Long projectId) {
        logger.info("查询所有压力机（load generators）");
        Response response = Response.success(iGeneratorService.listAll(projectId));
        logger.info("查询所有压力机（load generators），成功 {}", response);
        return response;
    }

    @RequestMapping(value = "/list", method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value = "分页查询压力机（load generators）信息列表", notes = "分页查询压力机（load generators）信息列表")
    public Response<ResponsePagingData> list(@RequestBody MyPage<Generator> myPage) {
        logger.info("分页查询压力机（load generators）信息列表 {}", myPage);
        Response response = Response.success(iGeneratorService.list(myPage));
        logger.info("分页查询压力机（load generators）信息列表，成功 {}", response);
        return response;
    }

    @RequestMapping(value = "/selectById", method = {RequestMethod.GET})
    @ResponseBody
    @ApiOperation(value = "查询压力机（load generators）信息", notes = "查询压力机（load generators）信息")
    public Response<Generator> selectById(@NotNull(message = "ID: id 字段必填") Long id) {
        logger.info("查询压力机（load generators）信息 id: " + id);
        Response response = Response.success(iGeneratorService.selectById(id));
        logger.info("查询压力机（load generators）信息，成功 {}", response);
        return response;
    }

    @RequestMapping(value = "/add", method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value = "新增压力机（load generators）", notes = "新增压力机（load generators）")
    public Response<Integer> add(@RequestBody @Valid Generator generator) {
        logger.info("新增压力机（load generators） {}", generator);
        Response response = Response.success(iGeneratorService.add(generator));
        logger.info("新增压力机（load generators），成功。 {}", response);
        return response;
    }

    @RequestMapping(value = "/update", method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value = "修改压力机（load generators）", notes = "修改压力机（load generators）")
    public Response<Integer> update(@RequestBody @Valid Generator generator) {
        logger.info("修改压力机（load generators） {}", generator);
        Response response = Response.success(iGeneratorService.update(generator));
        logger.info("修改压力机（load generators），成功。 {}", response);
        return response;
    }

    @RequestMapping(value = "/delete", method = {RequestMethod.GET})
    @ResponseBody
    @ApiOperation(value = "删除压力机（load generators）信息", notes = "删除压力机（load generators）信息")
    public Response<Integer> delete(@NotNull(message = "ID: id 字段必填") Long id) {
        logger.info("删除压力机（load generators）信息 ID: " + id);
        Response response = Response.success(iGeneratorService.delete(id));
        logger.info("删除压力机（load generators）信息，成功。 {}", response);
        return response;
    }
}
