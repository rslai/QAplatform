package com.bjy.qa.controller.manage;

import com.bjy.qa.entity.Response;
import com.bjy.qa.service.manage.ICleanDataService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

@Controller
@RequestMapping("/manage/cleanData")
@Validated
@Api(tags = "清除数据")
public class CleanDataController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    ICleanDataService iCleanDataService;

    @RequestMapping(value = "/cleanDeleted", method = {RequestMethod.GET})
    @ResponseBody
    @ApiOperation(value = "清除已删除数据", notes = "清除已删除数据")
    public Response<Object> cleanDeleted() {
        logger.info("清除已删除数据");
        Response response = Response.success(iCleanDataService.cleanDeleted());
        logger.info("清除已删除数据，成功 {}", response);
        return response;
    }
     
    @RequestMapping(value = "/cleanTestResult", method = {RequestMethod.GET})
    @ResponseBody
    @ApiOperation(value = "清除测试报告", notes = "清除测试报告")
    public Response<Object> cleanTestResult() {
        logger.info("清除测试报告");
        Response response = Response.success(iCleanDataService.cleanTestResult());
        logger.info("清除测试报告，成功 {}", response);
        return response;
    }
}
