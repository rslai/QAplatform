package com.bjy.qa.controller.manage;

import com.bjy.qa.entity.MyPage;
import com.bjy.qa.entity.Response;
import com.bjy.qa.entity.ResponsePagingData;
import com.bjy.qa.entity.manage.Company;
import com.bjy.qa.service.manage.ICompanyService;
import com.bjy.qa.util.security.SecurityUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@Controller
@RequestMapping("/manage/company")
@Validated
@Api(tags = "公司管理")
public class CompanyController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    ICompanyService iCompanyService;

    @RequestMapping(value = "/selectAll", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    @ApiOperation(value = "返回所有公司信息信息列表", notes = "返回所有公司信息信息列表")
    public Response<List<Company>> selectAll() {
        Response response = Response.success(iCompanyService.selectAll());
        logger.info("{}", response);
        return response;
    }

    @RequestMapping(value = "/list", method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value = "分页查询公司信息列表", notes = "分页查询公司信息列表")
    public Response<ResponsePagingData> list(@RequestBody MyPage<Company> myPage) throws Exception {
        logger.info("{}", myPage);
        Response response = Response.success(iCompanyService.list(myPage));
        logger.info("{}", response);
        return response;
    }

    @RequestMapping(value = "/selectById", method = {RequestMethod.POST})
    @ResponseBody
    @Validated(Company.IdGroup.class)
    @ApiOperation(value = "查询公司信息", notes = "查询公司信息")
    public Response<Company> selectById(@RequestBody @Valid Company company) {
        logger.info("CompanyId: " + company.getId());
        Response response = Response.success(iCompanyService.selectById(company.getId()));
        logger.info("{}", response);
        return response;
    }

    @RequestMapping(value = "/getUserCompanyInfo", method = {RequestMethod.GET})
    @ResponseBody
    @Validated(Company.IdGroup.class)
    @ApiOperation(value = "获得用户所属公司信息", notes = "获得用户所属公司信息")
    public Response<Company> getUserCompanyInfo() {
        Long companyId = SecurityUtils.getCurrentUserCompanyId();
        logger.info("获得用户所属公司信息 companyId:{}", companyId);
        Response response = Response.success(iCompanyService.selectById(companyId));
        logger.info("获得用户所属公司信息，成功 {}", response);
        return response;
    }

    @RequestMapping(value = "/add", method = {RequestMethod.POST})
    @ResponseBody
    @Validated(Company.CompanyGroup.class)
    @ApiOperation(value = "新增公司信息", notes = "新增公司信息")
    public Response<Integer> add(@RequestBody @Valid Company company) throws Exception {
        logger.info("{}", company);
        Response response = Response.success(iCompanyService.add(company));
        logger.info("{}", response);
        return response;
    }

    @RequestMapping(value = "/update", method = {RequestMethod.POST})
    @ResponseBody
    @Validated(Company.CompanyGroup.class)
    @ApiOperation(value = "修改公司信息", notes = "修改公司信息")
    public Response<Integer> update(@RequestBody @Valid Company company) throws Exception {
        logger.info("{}", company);
        Response response = Response.success(iCompanyService.update(company));
        logger.info("{}", response);
        return response;
    }

    @RequestMapping(value = "/delete", method = {RequestMethod.GET})
    @ResponseBody
    @ApiOperation(value = "删除公司信息", notes = "删除公司信息")
    public Response<Integer> delete(@NotNull(message = "ID: id字段必填") Long id) {
        logger.info("ID: " + id);
        Response response = Response.success(iCompanyService.delete(id));
        logger.info("{}", response);
        return response;
    }
}
