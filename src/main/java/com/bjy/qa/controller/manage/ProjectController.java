package com.bjy.qa.controller.manage;

import com.bjy.qa.entity.MyPage;
import com.bjy.qa.entity.Response;
import com.bjy.qa.entity.ResponsePagingData;
import com.bjy.qa.entity.manage.Project;
import com.bjy.qa.service.manage.IProjectService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@Controller
@RequestMapping("/manage/project")
@Validated
@Api(tags = "项目管理")
public class ProjectController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    IProjectService iProjectService;

    @RequestMapping(value = "/selectAll", method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value = "返回所有项目信息列表", notes = "返回所有项目信息列表")
    public Response<List<Project>> selectAll() {
        Response response = Response.success(iProjectService.selectAll());
        logger.info("{}", response);
        return response;
    }

    @RequestMapping(value = "/list", method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value = "分页查询项目信息列表", notes = "分页查询项目信息列表")
    public Response<ResponsePagingData> list(@RequestBody MyPage<Project> myPage) throws Exception {
        logger.info("{}", myPage);
        Response response = Response.success(iProjectService.list(myPage));
        logger.info("{}", response);
        return response;
    }

    @RequestMapping(value = "/selectById", method = {RequestMethod.POST})
    @ResponseBody
    @Validated(Project.IdGroup.class)
    @ApiOperation(value = "查询项目信息", notes = "查询项目信息")
    public Response<Project> selectById(@RequestBody @Valid Project project) {
        logger.info("CompanyId: " + project.getId());
        Response response = Response.success(iProjectService.selectById(project.getId()));
        logger.info("{}", response);
        return response;
    }

    @RequestMapping(value = "/add", method = {RequestMethod.POST})
    @ResponseBody
    @Validated(Project.ProjectGroup.class)
    @ApiOperation(value = "新增项目信息", notes = "新增项目信息")
    public Response<Integer> add(@RequestBody @Valid Project project) throws Exception {
        logger.info("{}", project);
        Response response = Response.success(iProjectService.add(project));
        logger.info("{}", response);
        return response;
    }

    @RequestMapping(value = "/update", method = {RequestMethod.POST})
    @ResponseBody
    @Validated(Project.ProjectGroup.class)
    @ApiOperation(value = "修改项目信息", notes = "修改项目信息")
    public Response<Integer> update(@RequestBody @Valid Project project) throws Exception {
        logger.info("{}", project);
        Response response = Response.success(iProjectService.update(project));
        logger.info("{}", response);
        return response;
    }

    @RequestMapping(value = "/delete", method = {RequestMethod.GET})
    @ResponseBody
    @ApiOperation(value = "删除项目信息", notes = "删除项目信息")
    public Response<Integer> delete(@NotNull(message = "ID: id字段必填") Long id) {
        logger.info("ID: " + id);
        Response response = Response.success(iProjectService.delete(id));
        logger.info("{}", response);
        return response;
    }
}
