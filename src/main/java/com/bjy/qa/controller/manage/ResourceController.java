package com.bjy.qa.controller.manage;

import com.bjy.qa.entity.MyPage;
import com.bjy.qa.entity.Response;
import com.bjy.qa.entity.ResponsePagingData;
import com.bjy.qa.service.manage.IResourceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;

@org.springframework.stereotype.Controller
@RequestMapping("/manage/resource")
@Validated
@Api(tags = "资源管理")
public class ResourceController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    IResourceService iResourceService;

    @RequestMapping(value = "/list", method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value = "分页查询资源列表", notes = "分页查询资源列表")
    public Response<ResponsePagingData> list(@RequestBody MyPage<com.bjy.qa.entity.manage.Resource> myPage) throws Exception {
        logger.info("分页查询资源列表 {}", myPage);
        Response response = Response.success(iResourceService.list(myPage));
        logger.info("分页查询资源列表，成功。{}", response);
        return response;
    }

    @RequestMapping(value = "/listAll", method = {RequestMethod.GET})
    @ResponseBody
    @ApiOperation(value = "查询资源列表", notes = "查询资源列表")
    public Response<Object> listAll() throws Exception {
        logger.info("查询资源列表");
        Response response = Response.success(iResourceService.listAll());
        logger.info("查询资源列表，成功。{}", response);
        return response;
    }

    @RequestMapping(value = "/delete", method = {RequestMethod.GET})
    @ResponseBody
    @ApiOperation(value = "删除资源信息", notes = "删除资源信息")
    public Response<Integer> delete(@RequestParam @NotNull(message = "ID: id字段必填") Long id) {
        logger.info("删除资源信息 ID: " + id);
        Response response = Response.success(iResourceService.delete(id));
        logger.info("删除资源信息，成功。{}", response);
        return response;
    }

    @RequestMapping(value = "/sync", method = {RequestMethod.GET})
    @ResponseBody
    @ApiOperation(value = "同步资源信息", notes = "同步资源信息")
    public Response<Integer> sync() throws Exception {
        logger.info("同步资源信息");
        Response response = Response.success(iResourceService.sync());
        logger.info("同步资源信息，成功 {}", response);
        return response;
    }
}
