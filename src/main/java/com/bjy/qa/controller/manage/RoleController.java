package com.bjy.qa.controller.manage;

import com.bjy.qa.entity.MyPage;
import com.bjy.qa.entity.Response;
import com.bjy.qa.entity.ResponsePagingData;
import com.bjy.qa.entity.manage.Role;
import com.bjy.qa.entity.manage.RolePermissionDTO;
import com.bjy.qa.service.manage.IRoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Controller
@RequestMapping("/manage/role")
@Validated
@Api(tags = "角色管理")
public class RoleController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    IRoleService iRoleService;

    @RequestMapping(value = "/getPermission", method = {RequestMethod.GET})
    @ResponseBody
    @ApiOperation(value = "得到所有权限（Route 菜单 + Permission 按钮）", notes = "得到所有权限（Route 菜单 + Permission 按钮）")
    public Response<Object> getPermission() {
        logger.info("得到所有权限");
        Response response = Response.success(iRoleService.getPermission());
        logger.info("得到所有权限，成功。{}", response);
        return response;
    }

    @RequestMapping(value = "/getRolePermission", method = {RequestMethod.GET})
    @ResponseBody
    @ApiOperation(value = "得到角色具有的权限（Route 菜单 + Permission 按钮）", notes = "得到角色具有的权限（Route 菜单 + Permission 按钮）")
    public Response<Object> getRolePermission(@RequestParam @NotNull(message = "ID: id字段必填") Long id) {
        logger.info("得到角色具有的权限 roleId: {}", id);
        Response response = Response.success(iRoleService.getRolePermission(id));
        logger.info("得到角色具有的权限，成功。{}", response);
        return response;
    }

    @RequestMapping(value = "/saveRolePermission", method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value = "保存角色具有的权限（Route 菜单 + Permission 按钮）", notes = "保存角色具有的权限（Route 菜单 + Permission 按钮）")
    public Response<Object> saveRolePermission(@RequestBody RolePermissionDTO rolePermission) {
        logger.info("保存角色具有的权限 {}", rolePermission);
        Response response = Response.success(iRoleService.saveRolePermission(rolePermission));
        logger.info("保存角色具有的权限，成功。{}", response);
        return response;
    }

    @RequestMapping(value = "/list", method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value = "分页查询角色列表", notes = "分页查询角色列表")
    public Response<ResponsePagingData> list(@RequestBody MyPage<Role> myPage) throws Exception {
        logger.info("分页查询角色列表 {}", myPage);
        Response response = Response.success(iRoleService.list(myPage));
        logger.info("分页查询角色列表，成功。{}", response);
        return response;
    }

    @RequestMapping(value = "/listAll", method = {RequestMethod.GET})
    @ResponseBody
    @ApiOperation(value = "查询所有角色列表", notes = "查询所有角色列表")
    public Response<Object> listAll() throws Exception {
        logger.info("查询所有角色列表");
        Response response = Response.success(iRoleService.listAll());
        logger.info("查询所有角色列表，成功。{}", response);
        return response;
    }

    @RequestMapping(value = "/listAllProjectRoles", method = {RequestMethod.GET})
    @ResponseBody
    @ApiOperation(value = "查询所有项目角色", notes = "查询所有项目角色")
    public Response<Object> listAllProjectRoles() {
        logger.info("查询所有项目角色");
        Response response = Response.success(iRoleService.listAllProjectRoles());
        logger.info("查询所有项目角色，成功。{}", response);
        return response;
    }

    @RequestMapping(value = "/add", method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value = "新增角色信息", notes = "新增角色信息")
    public Response<Object> add(@RequestBody @Valid Role role) throws Exception {
        logger.info("新增角色信息 {}", role);
        Response response = Response.success(iRoleService.add(role));
        logger.info("新增角色信息，成功 {}", response);
        return response;
    }

    @RequestMapping(value = "/update", method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value = "修改角色信息", notes = "修改角色信息")
    public Response<Integer> update(@RequestBody @Valid Role role) throws Exception {
        logger.info("修改角色信息 {}", role);
        Response response = Response.success(iRoleService.update(role));
        logger.info("修改角色信息，成功 {}", response);
        return response;
    }

    @RequestMapping(value = "/delete", method = {RequestMethod.GET})
    @ResponseBody
    @ApiOperation(value = "删除角色信息", notes = "删除角色信息")
    public Response<Integer> delete(@RequestParam @NotNull(message = "ID: id字段必填") Long id) {
        logger.info("删除角色信息 ID: " + id);
        Response response = Response.success(iRoleService.delete(id));
        logger.info("删除角色信息，成功。{}", response);
        return response;
    }
}
