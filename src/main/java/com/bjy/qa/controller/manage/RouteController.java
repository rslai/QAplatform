package com.bjy.qa.controller.manage;

import com.bjy.qa.entity.Response;
import com.bjy.qa.entity.functionaltest.Tree;
import com.bjy.qa.entity.manage.Route;
import com.bjy.qa.service.manage.IRouteService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@org.springframework.stereotype.Controller
@RequestMapping("/manage/route")
@Validated
@Api(tags = "菜单（路由）管理")
public class RouteController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    IRouteService iRouteService;

    @ApiOperation(value = "查询路由 tree 数据", notes = "查询路由 tree 的所有数据")
    @ResponseBody
    @RequestMapping(value = "/listTree", method = {RequestMethod.GET})
    public Response<List<Tree>> listTree() throws Exception {
        logger.info("查询路由 tree 数据");
        Response response = Response.success(iRouteService.listTree());
        logger.info("查询路由 tree 数据，成功。{}", response);
        return response;
    }

    @RequestMapping(value = "/selectById", method = {RequestMethod.GET})
    @ResponseBody
    @ApiOperation(value = "查询路由信息", notes = "查询路由信息")
    public Response<Object> selectById(@RequestParam @NotNull(message = "ID: id字段必填") Long id) {
        logger.info("查询路由信息 ID: {}", id);
        Response response = Response.success(iRouteService.selectById(id));
        logger.info("查询路由信息，成功。{}", response);
        return response;
    }

    @RequestMapping(value = "/add", method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value = "新增路由信息", notes = "新增路由信息")
    public Response<Object> add(@RequestBody @Valid Route route) throws Exception {
        logger.info("新增路由信息 {}", route);
        Response response = Response.success(iRouteService.add(route));
        logger.info("新增路由信息，成功 {}", response);
        return response;
    }

    @RequestMapping(value = "/update", method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value = "修改路由信息", notes = "修改路由信息")
    public Response<Integer> update(@RequestBody @Valid Route route) throws Exception {
        logger.info("修改路由信息 {}", route);
        Response response = Response.success(iRouteService.update(route));
        logger.info("修改路由信息，成功 {}", response);
        return response;
    }

    @RequestMapping(value = "/delete", method = {RequestMethod.GET})
    @ResponseBody
    @ApiOperation(value = "删除路由信息", notes = "删除路由信息")
    public Response<Integer> delete(@RequestParam @NotNull(message = "ID: id字段必填") Long id) {
        logger.info("删除路由信息 ID: " + id);
        Response response = Response.success(iRouteService.delete(id));
        logger.info("删除路由信息，成功。{}", response);
        return response;
    }
}
