package com.bjy.qa.controller.mock;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.bjy.qa.entity.Response;
import com.bjy.qa.entity.functionaltest.Agent;
import com.bjy.qa.entity.mock.http.MockRequest;
import com.bjy.qa.service.mock.IMockService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.Map;

@Controller
@RequestMapping("/mock")
@Validated
@Api(tags = "mock")
public class MockController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    IMockService iMockService;

    @RequestMapping(value = "/getMockResponse", method = {RequestMethod.POST})
    @ResponseBody
    @Validated(Agent.AgentAgentSignOnGroup.class)
    @ApiOperation(value = "获取 mock 请求的响应信息", notes = "获取 mock 请求的响应信息")
    public Response<Map<String, Object>> getMockResponse(@RequestBody String str) {
        JSONObject jsonObject = JSONObject.parseObject(str);
        MockRequest mockRequest = JSON.parseObject(jsonObject.getString("mockRequest"), MockRequest.class);
        logger.info("获取 mock 请求的响应信息，{}", mockRequest);

        Response response = Response.success(iMockService.getMockResponse(mockRequest));

        logger.info("获取 mock 请求的响应信息，成功 {}", response);
        return response;
    }
}
