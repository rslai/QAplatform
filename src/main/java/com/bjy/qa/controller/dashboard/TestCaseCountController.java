package com.bjy.qa.controller.dashboard;

import com.bjy.qa.entity.Response;
import com.bjy.qa.service.dashboard.ITestCaseCountService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;
import java.text.ParseException;
import java.util.Map;

@Controller
@RequestMapping("/dashboard")
@Validated
@Api(tags = "数据统计-用户对应测试用例维度")
public class TestCaseCountController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    ITestCaseCountService iTestCaseCountService;

    @RequestMapping(value = "/getTestCaseCount", method = {RequestMethod.GET})
    @ApiOperation(value = "数据统计-返回用户对应测试用例维度", notes = "数据统计-返回用户对应测试用例维度")
    @ResponseBody
    public Response<Map<String, Object>> getTestCaseCount(@NotNull(message = "projectId: projectId 字段必填") Long projectId, @NotNull(message = "startDate: startDate 字段必填") String startDate, @NotNull(message = "endDate: endDate 字段必填") String endDate) throws ParseException {
        logger.info("数据统计-返回用户对应测试用例维度，projectId:{}, startDate:{}, endDate:{}", projectId, startDate, endDate);
        Response response = Response.success(iTestCaseCountService.getTestCaseCount(projectId, startDate, endDate));
        logger.info("数据统计-返回用户对应测试用例维度，成功 {}", response);
        return response;
    }

    @RequestMapping(value = "/getTotalCount", method = {RequestMethod.GET})
    @ApiOperation(value = "数据统计-返回项目下的用例总数和套件总数", notes = "数据统计-返回项目下的用例总数和套件总数")
    @ResponseBody
    public Response<Map<String, Object>> getTotalCount(@NotNull(message = "projectId: projectId 字段必填") Long projectId) throws ParseException {
        logger.info("数据统计-返回用例总数和套件总数，projectId:{}", projectId);
        Response response = Response.success(iTestCaseCountService.getTotalCount(projectId));
        logger.info("数据统计-返回用例总数和套件总数，成功 {}", response);
        return response;
    }

}
