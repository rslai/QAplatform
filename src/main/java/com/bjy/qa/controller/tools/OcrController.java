package com.bjy.qa.controller.tools;

import com.bjy.qa.entity.MyPage;
import com.bjy.qa.entity.Response;
import com.bjy.qa.entity.ResponsePagingData;
import com.bjy.qa.entity.tools.Ocr;
import com.bjy.qa.service.tools.IOcrService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Controller
@RequestMapping("/tools/ocr")
@Validated
@Api(tags = "OCR 图像文字识别")
public class OcrController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    IOcrService iOcrService;

    @RequestMapping(value = "/getText", method = {RequestMethod.POST})
    @ResponseBody
    @Validated(Ocr.AddOcr.class)
    @ApiOperation(value = "添加 OCR 图像文字识别", notes = "添加 OCR 图像文字识别 - 异步操作")
    public Response<String> getText(@RequestBody @Valid Ocr ocr) throws Exception {
        logger.info("{}", ocr);
        iOcrService.add(ocr); // 异步ocr
        Response response = Response.successAsync("");
        logger.info("{}", response);
        return response;
    }

    @RequestMapping(value = "/list", method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value = "分页查询 OCR 图像文字识别列表", notes = "分页查询 OCR 图像文字识别列表")
    public Response<ResponsePagingData> list(@RequestBody MyPage<Ocr> myPage) throws Exception {
        logger.info("{}", myPage);
        Response response = Response.success(iOcrService.list(myPage));
        logger.info("{}", response);
        return response;
    }

    @RequestMapping(value = "/select", method = {RequestMethod.GET})
    @ResponseBody
    @ApiOperation(value = "查询 OCR 图像文字识别信息", notes = "根据 ID 查询 OCR 图像文字识别信息")
    public Response<Ocr> select(@NotNull(message = "ID: id字段必填") Long id) {
        logger.info("ID: " + id);
        Response response = Response.success(iOcrService.select(id));
        logger.info("{}", response);
        return response;
    }

    @RequestMapping(value = "/delete", method = {RequestMethod.GET})
    @ResponseBody
    @ApiOperation(value = "删除 OCR 图像文字识别信息", notes = "删除 OCR 图像文字识别信息")
    public Response<Integer> delete(@NotNull(message = "ID: id字段必填") Long id) {
        logger.info("ID: " + id);
        Response response = Response.success(iOcrService.delete(id));
        logger.info("{}", response);
        return response;
    }

}
