package com.bjy.qa.controller.tools;

import com.bjy.qa.entity.MyPage;
import com.bjy.qa.entity.Response;
import com.bjy.qa.entity.ResponsePagingData;
import com.bjy.qa.entity.tools.Upload;
import com.bjy.qa.service.tools.IUploadService;
import com.bjy.qa.util.file.FileTools;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.IOException;

/**
 * 文件上传
 */
@Controller
@RequestMapping("/tools/upload")
@Validated
@Api(tags = "文件上传")
public class UploadController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    IUploadService iUploadService;
    @Autowired
    private FileTools fileTool;

    @RequestMapping(value = "/fileUpload", method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value = "上传文件", notes = "上传文件")
    public Response<Upload> uploadFiles(@RequestParam(name = "file") MultipartFile file) throws IOException {
        Upload upload = fileTool.upload(file);
        if (upload.getUrl().isBlank()) {
            return Response.fail("文件上传错误");
        } else {
            return Response.success(upload);
        }
    }

    @RequestMapping(value = "/list", method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value = "分页查询文件列表", notes = "分页查询文件列表")
    public Response<ResponsePagingData> list(@RequestBody MyPage<Upload> myPage) throws Exception {
        logger.info("{}", myPage);
        Response response = Response.success(iUploadService.list(myPage));
        logger.info("{}", response);
        return response;
    }

    @RequestMapping(value = "/select", method = {RequestMethod.GET})
    @ResponseBody
    @ApiOperation(value = "根据 ID 查询上传文件信息", notes = "根据 ID 查询上传文件信息")
    public Response<Upload> select(@NotNull(message = "ID: id字段必填") Long id) {
        logger.info("ID: " + id);
        Response response = Response.success(iUploadService.select(id));
        logger.info("{}", response);
        return response;
    }

    @RequestMapping(value = "/add", method = {RequestMethod.POST})
    @ResponseBody
    @Validated(Upload.AddUpload.class)
    @ApiOperation(value = "添加 文件上 传信息", notes = "添加 文件上 传信息")
    public Response<Integer> add(@RequestBody @Valid Upload upload) throws Exception {
        logger.info("{}", upload);
        Response response = Response.success(iUploadService.add(upload));
        logger.info("{}", response);
        return response;
    }

    @RequestMapping(value = "/update", method = {RequestMethod.POST})
    @ResponseBody
    @Validated(Upload.AddUpload.class)
    @ApiOperation(value = "修改 文件上 传信息", notes = "修改 文件上 传信息")
    public Response<Integer> update(@RequestBody @Valid Upload upload) throws Exception {
        logger.info("{}", upload);
        Response response = Response.success(iUploadService.update(upload));
        logger.info("{}", response);
        return response;
    }

    @RequestMapping(value = "/delete", method = {RequestMethod.GET})
    @ResponseBody
    @ApiOperation(value = "删除 文件上 传信息", notes = "删除 文件上 传信息")
    public Response<Integer> delete(@NotNull(message = "ID: id字段必填") Long id) {
        logger.info("ID: " + id);
        Response response = Response.success(iUploadService.delete(id));
        logger.info("{}", response);
        return response;
    }

}
