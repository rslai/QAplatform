package com.bjy.qa.controller.tools;

import com.bjy.qa.entity.Response;
import com.bjy.qa.entity.functionaltest.Catalog;
import com.bjy.qa.entity.tools.Task;
import com.bjy.qa.enumtype.TaskStatus;
import com.bjy.qa.service.tools.ITaskManagerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;

@Controller
@RequestMapping("/tools/taskManager")
@Validated
@Api(tags = "任务管理器")
public class TaskManager {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    ITaskManagerService iTaskManagerService;

    @ApiOperation(value = "查询任务状态", notes = "查找对应 taskId 的状态信息")
    @ResponseBody
    @RequestMapping(value = "/getTaskStatus", method = {RequestMethod.GET})
    public Response<Catalog> getTaskStatus(@NotNull(message = "taskId: taskId字段必填") String taskId) {
        logger.info("查询任务状态 taskId: " + taskId);

        Task task = iTaskManagerService.getTask(taskId);
        if (task.getStatus().getValue() >= TaskStatus.END_BREAK.getValue()) {
            iTaskManagerService.deleteTask(taskId);
        }

        Response response = Response.success(task);

        logger.info("{}", response);
        return response;
    }
}
