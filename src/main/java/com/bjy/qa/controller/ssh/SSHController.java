package com.bjy.qa.controller.ssh;

import com.bjy.qa.entity.Response;
import com.bjy.qa.entity.ssh.Results;
import com.bjy.qa.entity.ssh.SSHServer;
import com.bjy.qa.service.ssh.ISSHService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

@Controller
@RequestMapping("/ssh")
@Api(tags = "SSH")
public class SSHController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    ISSHService isshService;

    /**
     * 执行ssh命令
     * url http://localhost:8080/ssh/executeCmd
     *     post
     * body:
     *  {
     * 	    "host":"jumpserver.baijiayun.com",
     * 	    "userName":"xxxxx",
     * 	    "password":"xxxx",
     * 	    "port":2223,
     * 	    "jumpServer":true,
     * 	    "cmdList":[
     * 	    	"43.132.31.31",
     *             "cat /opt/vdeploy/projects/msms/msms_1/configs/config.json && echo "
     * 	    ]
     *  }
     * @param sshServer
     * @return
     */
    @RequestMapping(value = "/executeCmd", method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value = "执行 ssh 命令", notes = "执行 ssh 命令")
    public Response<List<Results>> executeCmd(@RequestBody SSHServer sshServer) {
        logger.info("{}", sshServer);
        Response response;
        try {
            response = Response.success(isshService.executeCmd(sshServer));
        } catch (Exception e) {
            response = Response.fail(e.getMessage());
        }
        logger.info("{}", response);
        return response;
    }

}
