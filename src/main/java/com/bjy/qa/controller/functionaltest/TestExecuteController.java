package com.bjy.qa.controller.functionaltest;

import com.bjy.qa.entity.Response;
import com.bjy.qa.entity.functionaltest.FullTree;
import com.bjy.qa.entity.functionaltest.TestExecuteDataDTO;
import com.bjy.qa.entity.functionaltest.TestSuite;
import com.bjy.qa.entity.functionaltest.TreeQuery;
import com.bjy.qa.service.functionaltest.ITestExecuteService;
import com.bjy.qa.util.security.SecurityUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

@Validated
@Controller
@RequestMapping("/ft/testExecute")
@Api(tags = "测试执行")
public class TestExecuteController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    ITestExecuteService iTestExecuteService;

    @ApiOperation(value = "测试执行中查询 tree 完整路径，只返回在测试套件中被选中的 测试用例", notes = "测试执行中查询 tree 完整路径，只返回在测试套件中被选中的 测试用例")
    @ResponseBody
    @RequestMapping(value = "/listTree", method = {RequestMethod.POST})
    public Response<List<FullTree>> listTree(@Valid @RequestBody TreeQuery treeQuery) throws Exception {
        logger.info("查询 tree 完整路径 {}", treeQuery);
        Response response = Response.success(iTestExecuteService.listTree(treeQuery.getType(), treeQuery.getCaseType(), treeQuery.getProjectId(), treeQuery.getTestSuitesId(), treeQuery.getTestResultId() , treeQuery.getParentId()));
        logger.info("{}", response);
        return response;
    }

    @ResponseBody
    @RequestMapping(value = "/updateCaseStatus", method = {RequestMethod.POST})
    @ApiOperation(value = "更新测试用例执行状态", notes = "更新测试用例执行状态")
    public Response<TestSuite> updateCaseStatus(@RequestBody @Valid TestExecuteDataDTO testExecuteDataDTO) throws Exception {
        logger.info("执行测试计划：{}", testExecuteDataDTO);
        String runUser = SecurityUtils.getCurrentUserName();
        Response response = Response.success(iTestExecuteService.updateCaseStatus(testExecuteDataDTO, runUser));
        logger.info("{}", response);
        return response;
    }
}
