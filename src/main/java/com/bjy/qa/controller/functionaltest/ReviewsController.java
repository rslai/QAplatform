package com.bjy.qa.controller.functionaltest;

import com.bjy.qa.entity.MyPage;
import com.bjy.qa.entity.Response;
import com.bjy.qa.entity.ResponsePagingData;
import com.bjy.qa.entity.functionaltest.Reviews;
import com.bjy.qa.service.functionaltest.IReviewsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Controller
@RequestMapping("/ft/reviews")
@Validated
@Api(tags = "用例评审")
public class ReviewsController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    IReviewsService iReviewsService;

    @ResponseBody
    @RequestMapping(value = "/list", method = {RequestMethod.POST})
    @ApiOperation(value = "分页查询评审计划列表", notes = "分页查询评审计划列表信息")
    public Response<ResponsePagingData> list(@RequestBody MyPage<Reviews> myPage) throws Exception {
        logger.info("分页查询评审计划：{}", myPage);
        Response response = Response.success(iReviewsService.list(myPage));
        logger.info("分页查询评审计划列表，成功{}", response);
        return response;
    }

    @RequestMapping(value = "/add", method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value = "创建评审", notes = "创建评审")
    public Response<Integer> add(@RequestBody @Valid Reviews reviews) throws Exception {
        logger.info("创建评审， {}", reviews);
        Response response = Response.success(iReviewsService.add(reviews));
        logger.info("创建评审，成功 {}", response);
        return response;
    }

    @RequestMapping(value = "/update", method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value = "修改评审", notes = "修改评审")
    public Response<Integer> update(@RequestBody @Valid Reviews reviews) throws Exception {
        logger.info("修改评审， {}", reviews);
        Response response = Response.success(iReviewsService.update(reviews));
        logger.info("修改评审，成功 {}", response);
        return response;
    }

    @ResponseBody
    @RequestMapping(value = "/delete", method = {RequestMethod.GET})
    @ApiOperation(value = "删除评审计划", notes = "删除评审计划")
    public Response<Integer> delete(@NotNull(message = "ID: id字段必填！") Long id) {
        logger.info("删除评审计划，ID: {}", id);
        Response response = Response.success(iReviewsService.delete(id));
        logger.info("删除评审计划，成功{}", response);
        return response;
    }

    @ResponseBody
    @RequestMapping(value = "/findById", method = {RequestMethod.GET})
    @ApiOperation(value = "查询评审计划信息", notes = "查询评审计划信息")
    public Response<Reviews> findById(@NotNull(message = "ID: id字段必填！") Long id) {
        logger.info("查询评审计划信息，ID: {}", id);
        Response response = Response.success(iReviewsService.findById(id));
        logger.info("查询评审计划信息，成功{}", response);
        return response;
    }

    @ResponseBody
    @RequestMapping(value = "/stop", method = {RequestMethod.GET})
    @ApiOperation(value = "结束评审", notes = "结束评审")
    public Response<Object> stop(@NotNull(message = "ID: id字段必填！") Long id) {
        logger.info("结束评审，ID: {}", id);
        Response response = Response.success(iReviewsService.stop(id));
        logger.info("结束评审，成功{}", response);
        return response;
    }
}
