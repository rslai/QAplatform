package com.bjy.qa.controller.functionaltest;

import com.bjy.qa.entity.MyPage;
import com.bjy.qa.entity.Response;
import com.bjy.qa.entity.ResponsePagingData;
import com.bjy.qa.entity.functionaltest.TestResultCase;
import com.bjy.qa.entity.functionaltest.TestResultStep;
import com.bjy.qa.entity.functionaltest.TestResultSuite;
import com.bjy.qa.entity.functionaltest.TestSuite;
import com.bjy.qa.enumtype.ErrorCode;
import com.bjy.qa.service.functionaltest.ITestResultCaseService;
import com.bjy.qa.service.functionaltest.ITestResultSuiteService;
import com.bjy.qa.service.user.IUserCompanyProjectService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;

@Validated
@Controller
@RequestMapping("/ft/testResult")
@Api(tags = "测试结果")
public class TestResultController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final static ConcurrentLinkedQueue<TestResultStep> funcTestResultStepLogQueue = new ConcurrentLinkedQueue<>(); // 功能测试结果日志 队列
    private static long funcTestResultStepLogQueueLock = 0; // 功能测试结果日志 队列 锁

    @Resource
    IUserCompanyProjectService iUserCompanyProjectService;

    @Resource
    ITestResultSuiteService iTestResultSuiteService;

    @Resource
    ITestResultCaseService iTestResultCaseService;

    @ResponseBody
    @RequestMapping(value = "/list", method = {RequestMethod.POST})
    @ApiOperation(value = "分页查询测试结果列表", notes = "分页查询测试结果列表")
    public Response<ResponsePagingData> list(@RequestBody MyPage<TestResultSuite> myPage) throws Exception {
        logger.info("分页查询测试结果列表：{}", myPage);
        Response response = Response.success(iTestResultSuiteService.list(myPage));
        logger.info("{}", response);
        return response;
    }

    @ResponseBody
    @RequestMapping(value = "/getResultInfoById", method = {RequestMethod.GET})
    @ApiOperation(value = "根据id查询测试套件", notes = "根据id查询测试套件")
    public Response<TestSuite> getSuiteInfoById(@NotNull(message = "projectId: 字段必填！") Long projectId, @NotNull(message = "ID: id字段必填！") Long id) {
        logger.info("根据id查询测试套件，projectId: {}, ID: {}", projectId, id);

        Response response;
        if (!iUserCompanyProjectService.hasUserProjectPermissions(null, projectId)) {
            response = Response.fail(ErrorCode.UNAUTHORIZED, String.format("您没有当前项目（projectId: %d）权限，请刷新页面后重试！", projectId));
        } else {
            response = Response.success(iTestResultSuiteService.getResultInfoById(id));
        }

        logger.info("根据id查询测试套件，成功 {}", response);
        return response;
    }

    @ResponseBody
    @RequestMapping(value = "/getCaseResultDetails", method = {RequestMethod.GET})
    @ApiOperation(value = "查询测试结果中的某个测试用例结果", notes = "查询测试结果中的某个测试用例结果")
    public Response<Map<String, TestResultCase>> getCaseResultDetails(@NotNull(message = "cid: 测试结果 ID 字段必填！") Long cid, @NotNull(message = "rid: 测试结果 ID 字段必填！") Long rid) {
        logger.info("查询测试结果中的某个测试用例结果 cid: {}, rid: {}", cid, rid);
        Response response = Response.success(iTestResultCaseService.getCaseResultDetails(cid, rid));
        logger.info("查询测试结果中的某个测试用例结果，成功。{}", response);
        return response;
    }

    @RequestMapping(value = "/log", method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value = "测试结果上报", notes = "测试结果上报")
    public Response<String> log(@RequestBody @Valid TestResultStep testResultStep) {
        logger.info("测试结果上报: {}", testResultStep);

        testResultStep.setUpdatedAt(new Date());
        testResultStep.setCreatedAt(new Date());
        funcTestResultStepLogQueue.add(testResultStep); // 将日志添加到 功能测试结果日志 队列 中

        Response response = Response.success("测试结果处理成功");
        logger.info("测试结果上报，成功 {}", response);
        return response;
    }

    /**
     * 保存功能测试结果日志 任务
     */
    @Scheduled(fixedRate = 200) // 每 200 毫秒执行一次
    public void funcLogTask() {
        if (funcTestResultStepLogQueueLock == 0) {
            TestResultStep testResultStep = funcTestResultStepLogQueue.poll();
            if (testResultStep != null) {
                try {
                    logger.info("保存从 功能测试结果日志队列 中获取到的日志");
                    funcTestResultStepLogQueueLock = System.currentTimeMillis() + 3000; // 加锁（非 0 上锁。用当前时间 + 3s 当超时时间）

                    iTestResultCaseService.handlerResutl(testResultStep);
                } catch (Exception e) {
                    e.printStackTrace();
                    logger.error("保存从 功能测试结果日志队列 中获取到的日志 错误：" + e.getMessage());
                } finally {
                    funcTestResultStepLogQueueLock = 0; // 解锁
                }
            }
        } else {
            if (funcTestResultStepLogQueueLock < System.currentTimeMillis()) {
                logger.error("功能测试结果日志队列锁 超时，强制解锁");
                funcTestResultStepLogQueueLock = 0; // 锁定超时后，解锁
            }
        }
    }

    @ResponseBody
    @RequestMapping(value = "/getCaseResult", method = {RequestMethod.POST})
    @ApiOperation(value = "获取用例结果", notes = "获取用例结果")
    public Response<List<TestResultCase>> getCaseResult(@RequestBody Map<String, Object> params ) {
        logger.info("获取用例结果, {}", params);
        List<Long> cid = (List<Long>)params.get("cid");
        Long sid = Long.valueOf(params.get("sid").toString());
        Response response = Response.success(iTestResultCaseService.getCaseResult(cid, sid));
        logger.info("{}", response);
        return response;
    }
}
