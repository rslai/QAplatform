package com.bjy.qa.controller.functionaltest;

import com.bjy.qa.entity.MyPage;
import com.bjy.qa.entity.Response;
import com.bjy.qa.entity.ResponsePagingData;
import com.bjy.qa.entity.functionaltest.Agent;
import com.bjy.qa.service.functionaltest.IAgentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/ft/agent")
@Validated
@Api(tags = "代理管理")
public class AgentController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    IAgentService iAgentService;

    @RequestMapping(value = "/agentSignOn", method = {RequestMethod.GET})
    @ResponseBody
    @Validated(Agent.AgentAgentSignOnGroup.class)
    @ApiOperation(value = "agent 登录并返回 agent 的配置信息", notes = "agent 登录并返回 agent 的配置信息")
    public Response<Map<String, Object>> agentSignOn(@Valid Agent agent) {
        logger.info("agent 登录并返回 agent 的配置信息，agent: {}", agent);
        Response response = Response.success(iAgentService.agentSignOn(agent));
        logger.info("agent 登录并返回 agent 的配置信息，成功 {}", response);
        return response;
    }

    @ApiOperation(value = "查询所有代理", notes = "不分页的代理列表")
    @RequestMapping(value = "/listAll", method = {RequestMethod.GET})
    @ResponseBody
    public Response<List<Agent>> listAll() {
        logger.info("查询所有 agent");
        Response response = Response.success(iAgentService.listAll());
        logger.info("查询所有 agent，成功 {}", response);
        return response;
    }

    @RequestMapping(value = "/list", method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value = "分页查询 agent 列表", notes = "分页查询 agent 列表")
    public Response<ResponsePagingData> list(@RequestBody MyPage<Agent> myPage) throws Exception {
        logger.info("分页查询 agent 列表 {}", myPage);
        Response response = Response.success(iAgentService.list(myPage));
        logger.info("分页查询 agent 列表，成功。{}", response);
        return response;
    }

    @RequestMapping(value = "/add", method = {RequestMethod.POST})
    @ResponseBody
    @Validated(Agent.AgentAddGroup.class)
    @ApiOperation(value = "新增 agent", notes = "新增 agent")
    public Response<Object> add(@RequestBody @Valid Agent agent) throws Exception {
        logger.info("新增 agent， {}", agent);
        Response response = Response.success(iAgentService.add(agent));
        logger.info("新增 agent，成功 {}", response);
        return response;
    }

    @RequestMapping(value = "/update", method = {RequestMethod.POST})
    @ResponseBody
    @Validated(Agent.AgentAddGroup.class)
    @ApiOperation(value = "修改 agent", notes = "修改 agent")
    public Response<Integer> update(@RequestBody @Valid Agent agent) throws Exception {
        logger.info("修改 agent，{}", agent);
        Response response = Response.success(iAgentService.update(agent));
        logger.info("修改 agent，成功 {}", response);
        return response;
    }

    @RequestMapping(value = "/reboot", method = {RequestMethod.GET})
    @ResponseBody
    @ApiOperation(value = "重启 agent", notes = "重启 agent")
    public Response<Integer> reboot(@NotBlank(message = "key: agentKey 字段必填") String key) {
        logger.info("重启 agent， key:{}", key);
        Response response = Response.success(iAgentService.reboot(key));
        logger.info("重启 agent，成功 {}", response);
        return response;
    }
}
