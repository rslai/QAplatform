package com.bjy.qa.controller.functionaltest;

import com.bjy.qa.entity.Response;
import com.bjy.qa.service.functionaltest.IApiParamService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;

@Controller
@RequestMapping("/ft/apiParams")
@Validated
@Api(tags = "接口参数")
public class ApiParamsController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    IApiParamService iApiParamService;

    @RequestMapping(value = "/deleteById", method = {RequestMethod.GET})
    @ResponseBody
    @ApiOperation(value = "删除接口参数", notes = "删除接口参数")
    public Response<Integer> deleteById(@NotNull(message = "ID: id字段必填") Long id) {
        logger.info("删除参数，ID：" + id);
        Response response = Response.success(iApiParamService.removeById(id));
        logger.info("{}", response);
        return response;
    }
}
