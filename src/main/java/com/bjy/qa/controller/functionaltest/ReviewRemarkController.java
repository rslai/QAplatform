package com.bjy.qa.controller.functionaltest;

import com.bjy.qa.entity.Response;
import com.bjy.qa.entity.functionaltest.BatchReviewsRemarkDTO;
import com.bjy.qa.entity.functionaltest.ReviewsRemark;
import com.bjy.qa.service.functionaltest.IReviewsRemarkService;
import com.bjy.qa.util.security.SecurityUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;

@Validated
@Controller
@RequestMapping("/ft/reviewRemark")
@Api(tags = "评审备注")
public class ReviewRemarkController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    IReviewsRemarkService iReviewsRemarkService;

    @ResponseBody
    @RequestMapping(value = "/getReviewRemark", method = {RequestMethod.GET})
    @ApiOperation(value = "获取评审中给用例添加的备注信息", notes = "获取评审中给用例添加的备注信息")
    public Response<ReviewsRemark> getCaseResultDetails(@NotNull(message = "ID: id字段必填！") Long reviewsId, Long cid) {
        logger.info("获取评审中给用例添加的备注信息 {}", cid, reviewsId);
        Response response = Response.success(iReviewsRemarkService.getCaseRemark(reviewsId, cid));
        logger.info("获取评审中给用例添加的备注信息，成功 {}", response);
        return response;
    }

    @ResponseBody
    @RequestMapping(value = "/add", method = {RequestMethod.POST})
    @ApiOperation(value = "添加评审备注", notes = "添加评审备注")
    public Response<ReviewsRemark> add(@RequestBody ReviewsRemark reviewsRemark) throws Exception {
        logger.info("添加评审备注 {}", reviewsRemark);
        Response response = Response.success(iReviewsRemarkService.add(reviewsRemark));
        logger.info("添加评审备注，成功 {}", response);
        return response;
    }

    @ResponseBody
    @RequestMapping(value = "/batchAdd", method = {RequestMethod.POST})
    @ApiOperation(value = "批量添加评审备注", notes = "批量添加评审备注")
    public Response<String> batchAdd(@RequestBody BatchReviewsRemarkDTO batchReviewsRemarkDTO) {
        logger.info("批量添加评审备注 {}", batchReviewsRemarkDTO);
        Response response = Response.success(iReviewsRemarkService.batchAdd(batchReviewsRemarkDTO, SecurityUtils.getCurrentUserId()));
        logger.info("批量添加评审备注，成功 {}", response);
        return response;
    }
}
