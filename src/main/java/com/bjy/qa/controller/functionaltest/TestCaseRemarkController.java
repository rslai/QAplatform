package com.bjy.qa.controller.functionaltest;

import com.bjy.qa.entity.Response;
import com.bjy.qa.entity.functionaltest.TestCaseRemark;
import com.bjy.qa.entity.functionaltest.TestResultCase;
import com.bjy.qa.service.functionaltest.ITestCaseRemarkService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Validated
@Controller
@RequestMapping("/ft/remark")
@Api(tags = "备注")
public class TestCaseRemarkController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    ITestCaseRemarkService iTestCaseRemarkService;

    @ResponseBody
    @RequestMapping(value = "/add", method = {RequestMethod.POST})
    @ApiOperation(value = "添加备注", notes = "添加备注")
    public Response<TestCaseRemark> add(@RequestBody @Valid TestCaseRemark testCaseRemark) throws Exception {
        logger.info("添加备注，{}", testCaseRemark);
        Response response = Response.success(iTestCaseRemarkService.add(testCaseRemark));
        logger.info("添加备注成功，{}", response);
        return response;
    }

    @ResponseBody
    @RequestMapping(value = "/getCaseRemark", method = {RequestMethod.GET})
    @ApiOperation(value = "获取用例备注信息", notes = "获取用例备注信息")
    public Response<TestResultCase> getCaseResultDetails(@NotNull(message = "ID: id字段必填！") Long sid, Long cid) {
        logger.info("查询用例详情, {}", cid, sid);
        Response response = Response.success(iTestCaseRemarkService.getCaseRemark(sid, cid));
        logger.info("获取用例备注信息成功，{}", response);
        return response;
    }

}
