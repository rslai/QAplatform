package com.bjy.qa.controller.functionaltest;

import com.bjy.qa.entity.Response;
import com.bjy.qa.entity.functionaltest.TestCaseHistory;
import com.bjy.qa.service.functionaltest.IApiHistoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;
import java.util.List;

@Controller
@RequestMapping("/ft/apiHistory")
@Validated
@Api(tags = "API 定义历史记录")
public class ApiHistoryController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    IApiHistoryService iApiHistoryService;

    @RequestMapping(value = "/selectByApiId", method = {RequestMethod.GET})
    @ResponseBody
    @ApiOperation(value = "根据 apiId 查询历史记录列表", notes = "根据 apiId 查询历史记录列表")
    public Response<List<TestCaseHistory>> selectByApiId(@NotNull(message = "projectId: projectId 字段必填") Long projectId, @NotNull(message = "apiId: apiId 字段必填") Long apiId) {
        logger.info("根据 apiId 查询历史记录列表: projectId:{}, apiId:{}", projectId, apiId);
        Response response = Response.success(iApiHistoryService.selectByApiId(projectId, apiId));
        logger.info("根据 apiId 查询历史记录列表，成功 {}", response);
        return response;
    }
}
