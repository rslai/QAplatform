package com.bjy.qa.controller.functionaltest;

import com.bjy.qa.entity.Response;
import com.bjy.qa.entity.functionaltest.*;
import com.bjy.qa.enumtype.CatalogType;
import com.bjy.qa.service.functionaltest.ICatalogService;
import com.bjy.qa.util.EnumUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@Controller
@RequestMapping("/ft/catalog")
@Validated
@Api(tags = "分类管理")
public class CatalogController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    private ICatalogService iCatalogService;

    @RequestMapping(value = "/list", method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value = "查找分类列表", notes = "查找对应项目id和对应父分类id的子分类列表")
    public Response<List<Catalog>> findAll(@RequestBody Catalog catalog) {
        logger.info("查找分类列表，type: {}, projectId:{}, parentId:{}", catalog.getType(), catalog.getProjectId(), catalog.getParentId());
        Response response = Response.success(iCatalogService.listCatalogByParentId(catalog.getType(), catalog.getProjectId(), catalog.getParentId()));
        logger.info("{}", response);
        return response;
    }

    @ApiOperation(value = "根据当前元素id，递归查找控件元素以及父分类列表 ", notes = "例如：一个用例A他在分类A下，分类A又在分类B下。那返回就是 [分类B, 分类A 用例A]")
    @ResponseBody
    @RequestMapping(value = "/getElementParent", method = {RequestMethod.POST})
    public Response<List<Tree>> getElementParent(@RequestBody Catalog catalog) {
        logger.info("查找元素父分类下的所有子元素列表，type: {}，ID: {}", catalog.getType(), catalog.getId());
        Response response = Response.success(iCatalogService.getElementParent(catalog.getType(), catalog.getId()));
        logger.info("{}", response);
        return response;
    }

    @ApiOperation(value = "查询树节点数据", notes = "查询 tree 上某个子节点的所有数据")
    @ResponseBody
    @RequestMapping(value = "/listTreeContent", method = {RequestMethod.POST})
    public Response<List<Tree>> listTreeContent(@Valid @RequestBody TreeQuery treeQuery) {
        logger.info("查找分类列表 {}", treeQuery);
        Response response = Response.success(iCatalogService.listTreeContent(treeQuery.getType(), treeQuery.getProjectId(), treeQuery.getParentId()));
        logger.info("{}", response);
        return response;
    }

    @ApiOperation(value = "查询 tree 完整路径", notes = "查询 tree 完整路径")
    @ResponseBody
    @RequestMapping(value = "/listTree", method = {RequestMethod.POST})
    public Response<List<FullTree>> listTree(@Valid @RequestBody TreeQuery treeQuery) {
        logger.info("查询 tree 完整路径 {}", treeQuery);
        Response response = Response.success(iCatalogService.listTree(treeQuery.getType(), treeQuery.getCaseType(), treeQuery.getProjectId(), treeQuery.getParentId()));
        logger.info("查询 tree 完整路径， 成功 {}", response);
        return response;
    }

    @ApiOperation(value = "查询 tree 完整路径 - 只包含目录", notes = "查询 tree 完整路径 - 只包含目录")
    @ResponseBody
    @RequestMapping(value = "/listTreeCatalog", method = {RequestMethod.POST})
    public Response<List<FullTree>> listTreeCatalog(@Valid @RequestBody TreeQuery treeQuery) {
        logger.info("查询 tree 完整路径 - 只包含目录 {}", treeQuery);
        Response response = Response.success(iCatalogService.listTreeCatalog(treeQuery.getType(), treeQuery.getCaseType(), treeQuery.getProjectId(), treeQuery.getParentId()));
        logger.info("查询 tree 完整路径 - 只包含目录，成功 {}", response);
        return response;
    }

    @ApiOperation(value = "tree 节点排序", notes = "树节点排序")
    @ResponseBody
    @RequestMapping(value = "/treeSort", method = {RequestMethod.POST})
    public Response<List<Tree>> treeSort(@RequestBody @Valid TreeSort treeSort) {
        logger.info("tree 节点排序: {}", treeSort);
        Response response = Response.success(iCatalogService.treeSort(treeSort.getProjectId(), treeSort));
        logger.info("{}", response);
        return response;
    }

    @ApiOperation(value = "查找分类详情", notes = "查找对应id的，分类详细信息")
    @ResponseBody
    @RequestMapping(value = "/selectById", method = {RequestMethod.GET})
    public Response<Catalog> findById(@NotNull(message = "ID: id字段必填") Long id) {
        logger.info("查找分类详情，id: " + id);
        Response response = Response.success(iCatalogService.selectById(id));
        logger.info("{}", response);
        return response;
    }

    @ApiOperation(value = "新增分类信息", notes = "新增分类信息")
    @RequestMapping(value = "/add", method = {RequestMethod.POST})
    @ResponseBody
    @Validated(Catalog.CatalogAddGroup.class)
    public Response<Integer> add(@RequestBody @Valid Catalog catalog) {
        logger.info("新增分类信息：{}", catalog);
        Response response = Response.success(iCatalogService.add(catalog));
        logger.info("{}", response);
        return response;
    }

    @ApiOperation(value = "更新分类信息", notes = "新增或更新分类信息，id为0时新增，否则为更新对应id的信息")
    @RequestMapping(value = "/update", method = {RequestMethod.POST})
    @Validated(Catalog.CatalogUpdateGroup.class)
    @ResponseBody
    public Response<Catalog> update(@Valid @RequestBody Catalog catalog) {
        logger.info("更新分类信息, {}", catalog);
        Response response = Response.success(iCatalogService.update(catalog));
        logger.info("{}", response);
        return response;
    }

    @ApiOperation(value = "删除分类信息", notes = "删除对应 ID 的分类，分类 ID 存在子节点，删除失败")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name = "type", value = "分类类型", required = true, paramType = "query", example = "1", dataTypeClass = Integer.class),
            @ApiImplicitParam(name = "id", value = "分类id", required = true, paramType = "query", example = "1", dataTypeClass = Long.class)
    })
    @ResponseBody
    @RequestMapping(value = "/delete", method = {RequestMethod.GET})
    public Response<Integer> delete(@NotNull(message = "type: type字段必填")  Integer type,
                                    @NotNull(message = "ID: id字段必填") Long id, @NotNull(message = "项目id: 项目id必填") Long projectId) {
        logger.info("删除分类信息，type:{}，projectId{}，ID:{}", type, projectId, id);
        Response response = Response.success(iCatalogService.delete(EnumUtil.valueOf(CatalogType.class, type.intValue()), projectId, id));
        logger.info("{}", response);
        return response;
    }

    @ApiOperation(value = "递归复制分类信息", notes = "递归复制分类信息")
    @RequestMapping(value = "/copy", method = {RequestMethod.GET})
    @ResponseBody
    public Response<Tree> copy(@NotNull(message = "type: type字段必填")  Integer type, @NotNull(message = "项目id: 项目id必填") Long projectId, @NotNull(message = "ID: id字段必填") Long id) {
        logger.info("递归复制分类信息：type:{}，projectId{}，ID:{}", type, projectId, id);
        Response response = Response.success(iCatalogService.copy(EnumUtil.valueOf(CatalogType.class, type.intValue()), projectId, id));
        logger.info("递归复制分类信息，成功。{}", response);
        return response;
    }
}
