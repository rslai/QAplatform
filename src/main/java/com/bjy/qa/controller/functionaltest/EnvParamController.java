package com.bjy.qa.controller.functionaltest;

import com.bjy.qa.entity.Response;
import com.bjy.qa.entity.functionaltest.EnvParam;
import com.bjy.qa.service.functionaltest.IEnvParamService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@Controller
@RequestMapping("/ft/envParam")
@Validated
@Api(tags = "环境参数")
public class EnvParamController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    @Resource
    IEnvParamService iEnvParamsService;

    @RequestMapping(value = "/addEnv",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value = "添加环境", notes = "添加环境")
    @Validated(EnvParam.AddEnvGroup.class)
    public Response<Integer> addEnv(@RequestBody EnvParam envParam) {
        logger.info("添加环境参数-环境 envParams: {}", envParam);
        Response response = Response.success(iEnvParamsService.saveOrUpdateEnv(envParam));
        logger.info("{}", response);
        return response;
    }

    @RequestMapping(value = "/addServer",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value = "添加服务", notes = "添加服务")
    @Validated(EnvParam.AddServerGroup.class)
    public Response<Integer> addServer(@RequestBody @Valid List<EnvParam> envParams) {
        logger.info("添加环境参数-服务 envParams: {}", envParams);
        Response response = new Response<>();
        for (EnvParam envParam : envParams) {
            response = Response.success(iEnvParamsService.saveOrUpdateServer(envParam));
            logger.info("{}", response);
        }
        return response;
    }

    @RequestMapping(value = "/selectIdByEnvName",method = {RequestMethod.GET})
    @ResponseBody
    @ApiOperation(value = "查询环境ID", notes = "查询环境ID")
    public Response<EnvParam> selectIdByEnvName(Long projectId, String name) {
        logger.info("查询环境ID projectId: {}, name: {}", projectId, name);
        Response response = Response.success(iEnvParamsService.selectIdByEnvName(projectId, name));
        logger.info("{}", response);
        return response;
    }

    @RequestMapping(value = "/deleteEnvByName",method = {RequestMethod.GET})
    @ResponseBody
    @ApiOperation(value = "根据环境名删除环境信息", notes = "根据环境名删除环境信息")
    public Response<EnvParam> deleteEnvByName(Long projectId, String envName) {
        logger.info("根据环境名除环境信息 projectId: {}, envName: {}", projectId, envName);
        Response response = Response.success(iEnvParamsService.deleteEnvByName(projectId, envName));
        logger.info("{}", response);
        return response;
    }

    @RequestMapping(value = "/deleteServerById",method = {RequestMethod.GET})
    @ResponseBody
    @ApiOperation(value = "删除当前服务", notes = "通过ID删除当前服务")
    public Response<Integer> deleteServerById(@NotNull(message = "ID: id字段必填") Long id) {
        logger.info("删除当前服务 id：{}", id);
        Response response = Response.success(iEnvParamsService.removeById(id));
        logger.info("{}", response);
        return response;
    }

    @RequestMapping(value = "/findEnvListParamsByName", method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value = "查询某个环境名下的所有服务名", notes = "查询某个环境名下的所有服务名")
    public Response<List<EnvParam>> findEnvListParamsByName(@RequestBody EnvParam envParam) {
        logger.info("查询某个环境名下的所有服务名 projectId:{}, envName:{}", envParam.getProjectId(), envParam.getEnvName());
        Response response = Response.success(iEnvParamsService.listEnvParamsByName(envParam.getProjectId(), envParam.getEnvName()));
        logger.info("{}", response);
        return response;
    }
}
