package com.bjy.qa.controller.functionaltest;

import com.bjy.qa.entity.Response;
import com.bjy.qa.entity.functionaltest.*;
import com.bjy.qa.service.functionaltest.IReviewExecuteService;
import com.bjy.qa.util.security.SecurityUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

@Validated
@Controller
@RequestMapping("/ft/reviewExecute")
@Api(tags = "执行评审")
public class ReviewExecuteController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    IReviewExecuteService iReviewExecuteService;

    @ApiOperation(value = "用例评审中查询 tree 完整路径，只返回在用例评审中被选中的 测试用例", notes = "用例评审中查询 tree 完整路径，只返回在用例评审中被选中的 测试用例")
    @ResponseBody
    @RequestMapping(value = "/listTree", method = {RequestMethod.POST})
    public Response<List<FullTree>> listTree(@Valid @RequestBody TreeQuery treeQuery) throws Exception {
        logger.info("用例评审中查询 tree 完整路径 {}", treeQuery);
        Response response = Response.success(iReviewExecuteService.listTree(treeQuery.getType(), treeQuery.getCaseType(), treeQuery.getProjectId(), treeQuery.getReviewsId(), treeQuery.getParentId()));
        logger.info("用例评审中查询 tree 完整路径，成功 {}", response);
        return response;
    }

    @ResponseBody
    @RequestMapping(value = "/updateCaseStatus", method = {RequestMethod.POST})
    @ApiOperation(value = "更新测试用例编写状态", notes = "更新测试用例编写状态")
    public Response<String> updateCaseStatus(@RequestBody @Valid ReviewsExecuteDataDTO reviewsExecuteDataDTO) throws Exception {
        logger.info("更新测试用例编写状态 {}", reviewsExecuteDataDTO);
        String runUser = SecurityUtils.getCurrentUserName();
        Response response = Response.success(iReviewExecuteService.updateCaseStatus(reviewsExecuteDataDTO, runUser));
        logger.info("更新测试用例编写状态，成功 {}", response);
        return response;
    }

    @ResponseBody
    @RequestMapping(value = "/batchUpdateCaseStatus", method = {RequestMethod.POST})
    @ApiOperation(value = "批量更新测试用例编写状态", notes = "批量更新测试用例编写状态")
    public Response<String> batchUpdateCaseStatus(@RequestBody @Valid BatchReviewsExecuteDataDTO batchReviewsExecuteDataDTO) {
        logger.info("批量更新测试用例编写状态 {}", batchReviewsExecuteDataDTO);
        String runUser = SecurityUtils.getCurrentUserName();
        Response response = Response.success(iReviewExecuteService.batchUpdateCaseStatus(batchReviewsExecuteDataDTO, runUser));
        logger.info("批量更新测试用例编写状态，成功 {}", response);
        return response;
    }
}
