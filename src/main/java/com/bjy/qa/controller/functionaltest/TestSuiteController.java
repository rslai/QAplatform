package com.bjy.qa.controller.functionaltest;

import com.bjy.qa.entity.MyPage;
import com.bjy.qa.entity.Response;
import com.bjy.qa.entity.ResponsePagingData;
import com.bjy.qa.entity.functionaltest.TestResultSuite;
import com.bjy.qa.entity.functionaltest.TestSuite;
import com.bjy.qa.service.functionaltest.ITestSuiteService;
import com.bjy.qa.util.security.SecurityUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Validated
@Controller
@RequestMapping("/ft/testSuite")
@Api(tags = "测试套件")
public class TestSuiteController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    ITestSuiteService iTestSuiteService;

    @ResponseBody
    @RequestMapping(value = "/add", method = {RequestMethod.POST})
    @Validated(TestSuite.TestSuiteGroup.class)
    @ApiOperation(value = "创建测试套件", notes = "创建测试套件")
    public Response<TestSuite> add(@RequestBody @Valid TestSuite testSuite) throws Exception {
        logger.info("创建测试套件: {}", testSuite);
        Response response = Response.success(iTestSuiteService.add(testSuite));
        logger.info("{创建测试套件成功}", testSuite);
        return response;
    }

    @ResponseBody
    @RequestMapping(value = "/delete", method = {RequestMethod.GET})
    @ApiOperation(value = "删除测试套件", notes = "删除测试套件")
    public Response<Integer> delete(@NotNull(message = "ID: id字段必填！") Long id) {
        logger.info("删除测试套件，ID: {}", id);
        Response response = Response.success(iTestSuiteService.delete(id));
        logger.info("{}", response);
        return response;
    }

    @ResponseBody
    @RequestMapping(value = "/findById", method = {RequestMethod.GET})
    @ApiOperation(value = "根据id查询测试套件", notes = "根据id查询测试套件")
    public Response<TestSuite> findById(@NotNull(message = "ID: id字段必填！") Long id) {
        logger.info("查询测试套件，ID: {}", id);
        Response response = Response.success(iTestSuiteService.findById(id));
        logger.info("{}", response);
        return response;
    }

    @ResponseBody
    @RequestMapping(value = "/list", method = {RequestMethod.POST})
    @ApiOperation(value = "分页查询套件列表", notes = "分页查询套件列表信息")
    public Response<ResponsePagingData> list(@RequestBody MyPage<TestSuite> myPage) throws Exception {
        logger.info("分页查询测试套件：{}", myPage);
        Response response = Response.success(iTestSuiteService.list(myPage));
        logger.info("{}", response);
        return response;
    }

    @ResponseBody
    @RequestMapping(value = "/selectAll", method = {RequestMethod.POST})
    @ApiOperation(value = "查询套件列表", notes = "查询套件列表")
    public Response<Object> selectAll(@RequestBody TestSuite testSuite) throws Exception {
        logger.info("查询套件列表 {}", testSuite);
        Response response = Response.success(iTestSuiteService.selectAll(testSuite));
        logger.info("查询套件列表，成功 {}", response);
        return response;
    }

    @ResponseBody
    @RequestMapping(value = "/update", method = {RequestMethod.POST})
    @Validated(TestSuite.TestSuiteGroup.class)
    @ApiOperation(value = "更新测试套件", notes = "更新测试套件")
    public Response<TestSuite> update(@RequestBody @Valid TestSuite testSuite) throws Exception {
        logger.info("更新测试套件：{}", testSuite);
        Response response = Response.success(iTestSuiteService.update(testSuite));
        logger.info("{}", testSuite);
        return response;
    }

    @ResponseBody
    @RequestMapping(value = "/runSuite", method = {RequestMethod.GET})
    @ApiOperation(value = "执行测试套件", notes = "执行测试套件")
    public Response<TestSuite> runSuite(@NotNull(message = "ID: id字段必填！") Long id) throws Exception {
        logger.info("执行测试套件：{}", id);
        String runUser = SecurityUtils.getCurrentUserName();
        Response response = Response.success(iTestSuiteService.runSuite(id, null, runUser));
        logger.info("执行测试套件，成功。{}", response);
        return response;
    }

    @ResponseBody
    @RequestMapping(value = "/forceStopSuite", method = {RequestMethod.GET})
    @ApiOperation(value = "停止测试套件", notes = "停止测试套件")
    public Response<TestSuite> forceStopSuite(@NotNull(message = "ID: id字段必填！") Long id) {
        logger.info("停止测试套件 ID: {}", id);
        String runUser = SecurityUtils.getCurrentUserName();
        Response response = Response.success(iTestSuiteService.forceStopSuite(id, runUser));
        logger.info("停止测试套件，成功。{}", response);
        return response;
    }

    @ResponseBody
    @RequestMapping(value = "/updateReserved", method = {RequestMethod.GET})
    @ApiOperation(value = "设为 或 取消 保留", notes = "设为 或 取消 保留")
    public Response<TestResultSuite> updateReserved(@NotNull(message = "ID: id字段必填！") Long id) {
        logger.info("设为 或 取消 保留 ID: {}", id);
        Response response = Response.success(iTestSuiteService.updateReserved(id));
        logger.info("设为 或 取消 保留，成功。{}", response);
        return response;
    }
}
