package com.bjy.qa.controller.functionaltest;

import com.bjy.qa.entity.Response;
import com.bjy.qa.service.functionaltest.IStepService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/ft/step")
@Validated
@Api(tags = "测试步骤")
public class StepController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    @Resource
    IStepService iStepService;

    @RequestMapping(value = "/deleteStepById", method = {RequestMethod.GET})
    @ResponseBody
    @ApiOperation(value = "删除测试步骤", notes = "删除测试步骤")
    public Response<Integer> deleteStepById(@NotNull(message = "ID: id字段必填") Long id) {
        logger.info("删除测试步骤，ID：" + id);
        List<Long> ids = new ArrayList<>();
        ids.add(id);
        Response response = Response.success(iStepService.deletePhysically(ids));
        logger.info("{}", response);
        return response;
    }

    @RequestMapping(value = "/selectById", method = {RequestMethod.GET})
    @ResponseBody
    @ApiOperation(value = "查询测试步骤", notes = "查询测试步骤")
    public Response<Integer> selectById(@NotNull(message = "ID: id字段必填") Long id) {
        logger.info("查询测试步骤，ID：" + id);
        Response response = Response.success(iStepService.getById(id));
        logger.info("{}", response);
        return response;
    }
}
