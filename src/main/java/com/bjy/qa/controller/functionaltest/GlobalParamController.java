package com.bjy.qa.controller.functionaltest;

import com.bjy.qa.entity.Response;
import com.bjy.qa.entity.functionaltest.GlobalParam;
import com.bjy.qa.service.functionaltest.IGlobalParamService;
import com.bjy.qa.util.security.SecurityUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;
import java.util.List;

@Controller
@RequestMapping("/ft/globalParam")
@Validated
@Api(tags = "全局参数")
public class GlobalParamController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Value("${qa-platform.secret-key}")
    String secretKey;

    @Resource
    IGlobalParamService iGlobalParamService;

    @RequestMapping(value = "/add",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value = "添加全局参数", notes = "添加全局参数")
    public Response<Integer> add(@RequestBody GlobalParam globalParam) throws Exception {
        logger.info("添加全局参数 globalParam: {}", globalParam);
        Response response = Response.success(iGlobalParamService.createOrUpdate(globalParam));
        logger.info("{}", response);
        return response;
    }

    @RequestMapping(value = "/update",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value = "更新全局参数", notes = "更新全局参数")
    public Response<GlobalParam> update(@RequestBody GlobalParam globalParam) throws Exception {
        logger.info("更新全局参数: {}", globalParam);
        Response response = Response.success(iGlobalParamService.createOrUpdate(globalParam));
        logger.info("{}", response);
        return response;
    }


    @RequestMapping(value = "/deleteById",method = {RequestMethod.GET})
    @ResponseBody
    @ApiOperation(value = "删除当前全局参数", notes = "通过ID删除当前全局参数")
    public Response<Integer> deleteById(@NotNull(message = "ID: id字段必填") Long id) {
        logger.info("删除当前全局参数 id：{}", id);
        Response response = Response.success(iGlobalParamService.removeById(id));
        logger.info("{}", response);
        return response;
    }

    @RequestMapping(value = "/selectAll",method = {RequestMethod.GET})
    @ResponseBody
    @ApiOperation(value = "查找当前项目全部全局参数", notes = "通过项目ID查找当前项目全部有效全局参数")
    public Response<List<GlobalParam>> selectAll(Long projectId) throws Exception {
        logger.info("查找当前项目全部全局参数 项目id：{}", projectId);
        Response response = Response.success(iGlobalParamService.list(projectId, SecurityUtils.getCurrentUserId()));
        logger.info("{}", response);
        return response;
    }
}
