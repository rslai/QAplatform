package com.bjy.qa.util;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang.StringUtils;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class StringUtil {

    /**
     * json 对象转 json 字符串
     * @param obj json 对象
     * @return json 字符串
     */
    public static String toJsonStr(Object obj) {
        return JSONObject.toJSONString(obj);
    }

    /**
     * 根据Byte utf-8编码的长度截取字符串
     * @param str 待结局的字符串
     * @param len 截取的长度
     * @return
     * @throws IOException
     */
    public static String subStringByByteUTF8(String str, int len) throws IOException {
        byte[] b = str.getBytes("utf-8"); // 先把字符串转换为字符数组

        if (b.length <= len) {
            return str;
        }

        // 从最后往前找，第一个不是中文的字符，且记录往前了多少个。（一个中文转成utf-8的byte数组时，长度为3且三个byte都小于0，所以遇到一个大于0的则是英文字符）
        int count = 0;
        for (int i = len - 1; i >= 0; i--) {
            if (b[i] < 0)
                count++;
            else
                break;
        }

        if (count % 3 == 0) {
            // count是3的倍数，那就是一个完整的汉字。直接按照len截取并返回
            return new String(b, 0, len, "utf-8");
        } else if (count % 3 == 1) {
            // 余数是1，代表最后一个中文被截断了，剩下1个byte。就减去1个字节截取并返回
            return new String(b, 0, len - 1, "utf-8");
        } else {
            // 余数是2，代表最后一个中文被截断了，剩下2个byte。就减去2个字节截取并返回
            return new String(b, 0, len - 2, "utf-8");
        }
    }

    /**
     * 按照mysql数据库text类型的最大长度截取字符串
     * @param str 待截取的字符串
     * @return
     * @throws IOException
     */
    public static String subStringByMysqlText(String str) throws IOException {
        return subStringByByteUTF8(str, 65534);
    }

    /**
     * 压缩 JSON 字符串
     * @param json 需要被压缩的JSON字符串
     * @return 压缩后的JSON字符串
     */
    public static String compressJson(String json) {
        if (json == null || "".equals(json)) {
            return "";
        }

        StringBuilder sb = new StringBuilder();
        boolean isKeyOrValue = false; // 是 key 或 value(也就是被 " 号包裹的内容，" 号包裹中的空格会被保留）
        int length = json.length();
        for (int i = 0; i < length; i++) {
            char currentChar = json.charAt(i);
            if (currentChar == '"') {
                isKeyOrValue = !isKeyOrValue;
            }
            if (!isKeyOrValue && (currentChar == ' ' || currentChar == '\t' || currentChar == '\r' || currentChar == '\n')) {
                continue;
            }
            sb.append(currentChar);
        }
        return sb.toString();
    }
}
