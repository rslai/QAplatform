package com.bjy.qa.util;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang.StringUtils;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

/**
 * url 工具类
 */
public class UrlUtil {
    /**
     * 从完整的 url 中拆出 host 并返回
     * @param urlString 完整的 url
     * @return host，例如：https://www.baidu.com/abc/def?g=1 返回 https://www.baidu.com
     */
    public static String getUri(String urlString) {
        try {
            URL url = new URL(urlString);
            if (StringUtils.isEmpty(url.getPath().replace("/", ""))) { // 如果 path 为空，直接返回 /
                return "/";
            } else { // 如果 path 不为空，返回 path
                return url.getPath();
            }
        } catch (MalformedURLException e) {
        }
        return "/";
    }

    /**
     * 从完整的 url 中拆出 uri 并返回
     * @param urlString 完整的 url
     * @return uri，例如：https://www.baidu.com/abc/def?g=1 返回 /abc/def
     */
    public static String getHost(String urlString) {
        try {
            URL url = new URL(urlString);
            if (StringUtils.isEmpty(url.getPath().replace("/", ""))) {
                if (StringUtils.isEmpty(url.getQuery())) {
                    if (urlString.indexOf("?") >= 0) { // url 中没有 path 也没有 query 但有 ? 号的，用 ? 之前的内容当 host 返回
                        return StringUtils.substringBefore(urlString, "?");
                    } else { // url 中没有 path 也没有 query，直接返回
                        return urlString;
                    }
                } else {
                    if (StringUtils.isEmpty(url.getPath())) { // url 中没有 path，但有 query 的。用 query 之前的内容当 host 返回
                        return StringUtils.substringBefore(urlString, "?" + url.getQuery());
                    } else { // url 中有 path，也有 query 的。用 path 之前的内容当 host 返回
                        return StringUtils.substringBefore(urlString, url.getPath() + "?" + url.getQuery());
                    }
                }
            } else { // url 中有 path，用 path 之前的内容当 host 返回
                return StringUtils.substringBefore(urlString, url.getPath());
            }
        } catch (MalformedURLException e) {
        }
        return "";
    }

    /**
     * 从完整的 url 中拆出 query 并返回
     * @param urlString 完整的 url
     * @return query，例如：https://www.baidu.com/abc/def?g=1 返回 g=1
     */
    public static String getQuery(String urlString) {
        try {
            URL url = new URL(urlString);
            if (StringUtils.isEmpty(url.getQuery())) {
                return "";
            } else {
                return url.getQuery().replace("?", "");
            }
        } catch (MalformedURLException e) {
        }
        return "";
    }

    /**
     * 将 key、value 的字符串形式参数转为 List<KeyValueStore> 形式
     * @param paramStr key、value 的字符串形式参数
     * @return
     */
    public static List<KeyValueStore> param2KeyValueStore(String paramStr) {
        List<KeyValueStore> keyValueStoreList = new ArrayList<>();

        for (String one : paramStr.split("&")) {
            String[] arrayOne = one.split("=");
            if (arrayOne.length == 2) {
                String value = "";
                try {
                    value = URLDecoder.decode(arrayOne[1], "utf-8");
                } catch (UnsupportedEncodingException e) {
                }
                keyValueStoreList.add(new KeyValueStore(arrayOne[0], value));
            } else if (arrayOne.length == 1 && one.indexOf("=") > 0) {
                keyValueStoreList.add(new KeyValueStore(arrayOne[0], ""));
            }
        }

        return keyValueStoreList;
    }

    /**
     * 将 key、value 的字符串形式参数转为 json 形式
     * @param paramStr key、value 的字符串形式参数
     * @return
     */
    public static String param2JsonStr(String paramStr) {
        JSONObject jsonObject = new JSONObject();
        for (KeyValueStore keyValueStore : UrlUtil.param2KeyValueStore(paramStr)) {
            jsonObject.put(keyValueStore.getName(), keyValueStore.getValue());
        }
        return JSONObject.toJSONString(jsonObject);
    }


    /**
     * 将 cookie 的字符串形式参数转为 List<KeyValueStore> 形式
     * @param cookieStr cookie 的字符串形式参数
     * @return
     */
    public static List<KeyValueStore> cookie2KeyValueStore(String cookieStr) {
        List<KeyValueStore> keyValueStoreList = new ArrayList<>();

        for (String one : cookieStr.split(";")) {
            String[] arrayOne = one.split("=");
            if (arrayOne.length == 2) {
                String value = "";
                try {
                    value = URLDecoder.decode(arrayOne[1], "utf-8");
                } catch (UnsupportedEncodingException e) {
                }
                keyValueStoreList.add(new KeyValueStore(arrayOne[0].trim(), value));
            } else if (arrayOne.length == 1 && one.indexOf("=") > 0) {
                keyValueStoreList.add(new KeyValueStore(arrayOne[0].trim(), ""));
            }
        }

        return keyValueStoreList;
    }

    /**
     * 将 cookie 的字符串形式参数转为 json 形式
     * @param cookieStr cookie 的字符串形式参数
     * @return
     */
    public static String cookie2JsonStr(String cookieStr) {
        JSONObject jsonObject = new JSONObject();
        for (KeyValueStore keyValueStore : UrlUtil.cookie2KeyValueStore(cookieStr)) {
            jsonObject.put(keyValueStore.getName(), keyValueStore.getValue());
        }
        return JSONObject.toJSONString(jsonObject);
    }

    /**
     * KeyValueStore 模型（用于保存 query、body 的参数）
     */
    public static class KeyValueStore {
        String name; // key
        Object value; // value

        /**
         * 构造方法
         * @param name  name
         * @param value value
         */
        public KeyValueStore(String name, Object value) {
            this.name = name;
            this.value = value;
        }

        /**
         * 返回 name
         * @return name
         */
        public String getName() {
            return this.name;
        }

        /**
         * 返回 value
         * @return value
         */
        public Object getValue() {
            return this.value;
        }

        /**
         * @param value 设置 value
         */
        public void setValue(Object value) {
            this.value = value;
        }

        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder("KeyValueStore{");
            sb.append("name='").append(name).append('\'');
            sb.append(", value=").append(value);
            sb.append('}');
            return sb.toString();
        }
    }
}
