package com.bjy.qa.util.security;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;

/**
 * CustomFilterInvocationSecurityMetadataSource 中用到的匹配起
 * 由于 spring cache 在当前类不生效，所以抽出来一个类
 */
@Component
public class CustomFilterInvocationSecurityMetadataSourceMatcher {
    /**
     * 判断当前请求的 URL 是否需要权限验证，如果是返回角色信息（目前是现实的黑名单，未匹配到都放行）
     * @param url
     * @return
     */
    @Cacheable(value = "qap:matcherUrlRole", key = "#url")
    public String matcherUrlRole(String url) throws IllegalArgumentException {
        AntPathMatcher antPathMatcher = new AntPathMatcher(); // Spring URL 匹配器，支持 Ant 风格的路径匹配表达式（支持通配符 * 和 **。`*匹配单路径段,**`匹配多路径段。）
        for (MenuRole menu : CustomFilterInvocationSecurityMetadataSource.menuRoleList) {
            if (antPathMatcher.match(menu.getUrl(), url)) {
                return menu.getRole(); // 匹配到了，需要验证权限
            }
        }
        return "ROLE_ANONYMOUS"; // 没有匹配到，不需要验证权限
    }
}