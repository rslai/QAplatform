package com.bjy.qa.util.security;

import java.io.Serializable;

public class MenuRole implements Serializable {
    String url;
    String role;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
