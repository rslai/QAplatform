package com.bjy.qa.util.security;

import com.bjy.qa.entity.user.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Data
public class LoginUser implements UserDetails {
    private User user; // 用户对象

    private List<String> permissions; // 权限列表（数据库中保存的 list）

    @JsonIgnore
    private List<SimpleGrantedAuthority> authorities; // Spring Security 中用到的权限列表（SimpleGrantedAuthority 类型）

    public LoginUser() {

    }

    public LoginUser(User user, List<String> permissions) {
        this.user = user;
        this.permissions = permissions;
    }

    /**
     * 返回当前用户所拥有的权限信息
     * @return
     */
    @JsonIgnore
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        // 如果已经转过，直接返回
        if (authorities != null) {
            return authorities;
        }

        // 把数据库中的权限列表（permissions）转为 Spring Security 中用到的权限列表（authorities）。String 转 SimpleGrantedAuthority
        authorities = permissions.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList());
        return authorities;
    }

    /**
     * 获取密码
     * @return
     */
    @JsonIgnore
    @Override
    public String getPassword() {
        return "{noop}" + user.getCode();
    }

    /**
     * 获取账号名称
     * @return
     */
    @JsonIgnore
    @Override
    public String getUsername() {
        return user.getAccount();
    }

    /**
     * 账号是否过期
     * @return true：未过期；false：已过期
     */
    @JsonIgnore
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    /**
     * 账号是否被锁定
     * @return true：未锁定；false：已锁定
     */
    @JsonIgnore
    @Override
    public boolean isAccountNonLocked() {
        return !user.isLocked();
    }

    /**
     * 密码是否过期
     * @return true：未过期；false：已过期
     */
    @JsonIgnore
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    /**
     * 是否可用
     * @return treu：可用；false：不可用
     */
    @JsonIgnore
    @Override
    public boolean isEnabled() {
        return true;
    }
}
