package com.bjy.qa.util;

import java.text.DecimalFormat;

/**
 * 数字 工具类
 */
public class NumberUtils {
    /**
     * 格式化数字
     * @param pattern 格式化模式
     * @param value 数字
     * @return
     */
    public static String decimalFormat(String pattern, double value) {
        DecimalFormat myFormat = new DecimalFormat(pattern); // 实例化 DecimalFormat 对象
        return myFormat.format(value); // 将数字进行格式化
    }
}
