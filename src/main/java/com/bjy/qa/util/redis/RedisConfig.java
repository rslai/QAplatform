package com.bjy.qa.util.redis;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

/**
 * Redis 配置类
 */
@Configuration
public class RedisConfig {
    @Bean
    public RedisTemplate<String, Object> redisTemplate(LettuceConnectionFactory lettuceConnectionFactory) {

        RedisTemplate<String, Object> redisTemplate = new RedisTemplate<>();
        redisTemplate.setKeySerializer(new StringRedisSerializer()); // 为 string 类型的 key 设置序列化
        redisTemplate.setValueSerializer(new GenericJackson2JsonRedisSerializer()); // 为 string 类型的 value 设置序列化
        redisTemplate.setHashKeySerializer(new StringRedisSerializer()); // 为 hash 类型的 key 设置序列化
        redisTemplate.setHashValueSerializer(new StringRedisSerializer()); // 为 hash 类型的 value 设置序列化
        redisTemplate.setConnectionFactory(lettuceConnectionFactory);
        return redisTemplate;
    }
}