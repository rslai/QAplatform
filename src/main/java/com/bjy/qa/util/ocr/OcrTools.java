package com.bjy.qa.util.ocr;

import org.sikuli.script.OCR;
import org.sikuli.script.Pattern;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OcrTools {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * 通过 sikuli 的 ocr 库识别图片中的文字
     * @param picFileName 待识别的图片文件路径+文件名
     * @return
     */
    public String getTextBySikuliOcr(String picFileName) {
        OCR.globalOptions().language("chi_sim"); // 设置 ocr 识别语言为-中文
        logger.info("OCR识别，fineName: " + picFileName + ", language: " + OCR.globalOptions().language());

        String text;
        try {
            Pattern pattern = new Pattern(picFileName);
            text = pattern.getImage().text();
        } catch (ExceptionInInitializerError e) {
            e.printStackTrace();
            throw new ExceptionInInitializerError(e.getException().toString());
        } catch (NoClassDefFoundError e) {
            e.printStackTrace();
            throw new NoClassDefFoundError(e.getMessage() + " " + e.getCause().toString());
        }

        return text;
    }
}
