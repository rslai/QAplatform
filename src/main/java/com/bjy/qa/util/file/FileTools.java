package com.bjy.qa.util.file;

import com.bjy.qa.entity.tools.FileInfo;
import com.bjy.qa.entity.tools.Upload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.UUID;

@Component
public class FileTools {
    private final Logger logger = LoggerFactory.getLogger(FileTools.class);

    private static String host;
    @Value("${qa-platform.host}")
    public void setHost(String host) {
        this.host = host;
    }

    /**
     * 文件上传
     * @param file file
     * @return
     * @throws IOException
     */
    public Upload upload(MultipartFile file) throws IOException {
        return this.upload(file, true);
    }

    /**
     * 文件上传
     * @param file file
     * @param rename 是否重新命名。true: 产生新的随机名称，文件不会重复 false: 保持原始名称，第二次上传会覆盖
     * @return
     * @throws IOException
     */
    public Upload upload(MultipartFile file, boolean rename) throws IOException {
        FileInfo local;
        if (rename) {
            local = createFile(file.getOriginalFilename(), true); // 重新生成文件名，防止文件重名
        } else {
            local = createFile(file.getOriginalFilename(), false); // 保持源文件名，不重新产生文件名，文件存在会覆盖
        }

        try {
            file.transferTo(local.getFile().getAbsoluteFile());
        } catch (FileAlreadyExistsException e) {
            logger.error(e.getMessage());
        }

        Upload upload = new Upload();
        upload.setType(local.getType());
        upload.setFileName(local.getFileName());
        upload.setServerFileName(local.getAbsolutePath());
        upload.setUrl(local.getUrl());
        return upload;
    }

    /**
     * 创建文件
     * @param fileName 文件名。当 rename=true，会用这个当文件名。当 rename=false 时会用这个文件的后缀随机产生一个文件。
     * @param rename 是否重新命名。true: 产生新的随机名称，文件不会重复。 false: 保持原始名称，会覆盖之前的文件
     * @return
     */
    public static FileInfo createFile(String fileName, boolean rename) {
        // 每天创建一个文件夹，文件夹名为日期
        SimpleDateFormat sf = new SimpleDateFormat("yyyyMMdd");
        File folder = new File("upload" + File.separator + sf.format(Calendar.getInstance().getTimeInMillis()));
        if (!folder.exists()) {
            folder.mkdirs();
        }

        FileInfo fileInfo = new FileInfo();

        // 设置文件类型
        if (fileName.lastIndexOf(".") >= 0) {
            fileInfo.setType(fileName.substring(fileName.lastIndexOf(".")));
        } else {
            fileInfo.setType("");
        }

        // 设置文件
        File local;
        if (rename) {
            // 重新生成文件名，防止文件重名
            local = new File(folder.getPath() + File.separator + UUID.randomUUID() + fileInfo.getType());
        } else {
            // 保持源文件名，不重新产生文件名，文件存在会覆盖
            local = new File(folder.getPath() + File.separator + fileName);
        }
        fileInfo.setFile(local);

        fileInfo.setFileName(local.getName()); // 设置文件名
        fileInfo.setAbsolutePath(local.getAbsolutePath()); // 设置文件绝对路径
        fileInfo.setUrl(host.replace(":80/", "/") + "/folder/" + local.getPath().replaceAll("\\\\", "/")); // 设置文件 url

        return fileInfo;
    }

}
