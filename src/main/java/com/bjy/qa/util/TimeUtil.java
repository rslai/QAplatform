package com.bjy.qa.util;

import java.util.Date;

public class TimeUtil {

    /**
     * 获取时间差，输出格式为：x天x小时x分x秒
     *
     * @param startTime
     * @param endTime
     * @return
     */
    public static String getTimeDiff(Date startTime, Date endTime) {
        long diff = endTime.getTime() - startTime.getTime(); // 结束 和 开始 时间相差毫秒数

        long day = diff / (24 * 60 * 60 * 1000); // 天数
        long remainder = diff % (24 * 60 * 60 * 1000); // 计算完 天数 后剩余毫秒数

        long hour = remainder / (60 * 60 * 1000); // 小时
        remainder %= (60 * 60 * 1000); // 计算完 小时 后剩余毫秒数

        long minute = remainder / (60 * 1000); // 分钟
        remainder %= (60 * 1000); // 计算完 分钟 后剩余毫秒数
        long second = remainder / 1000; // 秒

        if (day != 0) {
            return day + "天" + hour + "小时" + minute + "分钟" + second + "秒";
        } else if (hour != 0) {
            return hour + "小时" + minute + "分钟" + second + "秒";
        } else if (minute != 0) {
            return minute + "分钟" + second + "秒";
        } else if (second != 0) {
            return second + "秒";
        } else {
            return "0秒";
        }
    }

    /**
     * 产生指定长度的时间戳类型参数值
     * 时间戳
     * @param length 返回时间戳长度（len=0 默认长度，默认13位毫秒值。len < 13 从后向前截取，超过13前边补0）
     * @param salt 盐值（由于使用了时间戳，短时间产生多个值会出现重复值，所以通过此参数避免重复。需要调用此方法时传入不同值）
     * @return
     */
    public static String generateTimestampParameter(int length, int salt) {
        long millis = System.currentTimeMillis() + salt;
        String timestamp = String.valueOf(millis);
        if (length == 0) {
            return timestamp;
        }
        if (length <= timestamp.length()) {
            return timestamp.substring(timestamp.length() - length);
        } else {
            return String.format("%0" + length + "d", millis);
        }
    }
}
