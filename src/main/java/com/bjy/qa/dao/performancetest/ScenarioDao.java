package com.bjy.qa.dao.performancetest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjy.qa.entity.performancetest.Scenario;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface ScenarioDao extends BaseMapper<Scenario> {

}
