package com.bjy.qa.dao.performancetest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjy.qa.entity.performancetest.PerfTestResultLog;
import com.bjy.qa.enumtype.PerfLogType;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface PerfTestResultLogDao extends BaseMapper<PerfTestResultLog> {
    /**
     * 查询性能测试结果中的某个测试脚本的日志
     * @param resultId 测试结果
     * @param testScriptId 测试脚本 ID
     * @return
     */
    @Select("SELECT * FROM pt_test_result_log WHERE result_id=#{resultId} AND test_script_id=#{testScriptId} AND msg='step' AND is_delete=false")
    List<PerfTestResultLog> selecByCaseIdAndResultId(Long resultId, Long testScriptId);

    /**
     * 根据 PerfLogType 查询数据
     * @param resultId 测试结果
     * @param testScriptId 测试脚本 ID
     * @return
     */
    @Select("SELECT * FROM pt_test_result_log WHERE result_id=#{resultId} AND test_script_id=#{testScriptId} AND log_type=#{perfLogType} AND msg='step' AND is_delete=false")
    List<PerfTestResultLog> getByPerfLogType(PerfLogType perfLogType, Long resultId, Long testScriptId);

    /**
     * 得到最新的数据
     * @param perfLogType 性能测试日志类型
     * @param resultId 测试结果
     * @param testScriptId 测试脚本 ID
     * @return
     */
    @Select("SELECT * FROM pt_test_result_log WHERE result_id=#{resultId} AND test_script_id=#{testScriptId} AND log_type=#{perfLogType} AND is_delete=false ORDER BY id DESC LIMIT 1")
    PerfTestResultLog getLatestData(PerfLogType perfLogType, Long resultId, Long testScriptId);
}
