package com.bjy.qa.dao.performancetest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjy.qa.entity.performancetest.PerfTestResult;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface PerfTestResultDao extends BaseMapper<PerfTestResult> {
    /**
     * 查询设置为基线的 性能测试结果
     * @param suiteId 测试套件 ID
     * @return
     */
    @Select("SELECT id,suite_id,suite_name,case_type,project_id,run_user,status,base_line,total_time,end_at,is_delete,updated_at,created_at FROM pt_test_result WHERE is_delete=false AND (suite_id = #{suiteId} AND base_line = true)")
    List<PerfTestResult> selectPerfTestResultBaseLine(Long suiteId);
}
