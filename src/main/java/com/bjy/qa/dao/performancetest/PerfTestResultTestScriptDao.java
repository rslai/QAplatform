package com.bjy.qa.dao.performancetest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjy.qa.entity.performancetest.PerfTestResultTestScript;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface PerfTestResultTestScriptDao extends BaseMapper<PerfTestResultTestScript> {
    /**
     * 根据 性能测试结果 ID 和 测试脚本 ID 查询 性能测试结果对应测试脚本
     * @param resultId 测试结果 ID
     * @param testCaseId 测试脚本 ID
     * @return
     */
    @Select("SELECT * FROM pt_test_result_test_script WHERE test_script_id = #{testCaseId} AND result_id=#{resultId} AND is_delete=false")
    PerfTestResultTestScript getTestResultTestScript(Long resultId, Long testCaseId);

    /**
     * 获取当前套件最终状态
     * @param resultId 测试结果 id
     * @return
     */
    @Select("SELECT MAX(status) FROM pt_test_result_test_script WHERE result_id=#{resultId} limit 1")
    int getTestResultStatus(long resultId);

    /**
     * 查询当前套件 运行中的 测试脚本总数（2 <= status <= 3）
     * @param resultId 测试结果 id
     * @return
     */
    @Select("SELECT COUNT(*) FROM pt_test_result_test_script WHERE result_id=#{resultId} AND `status`<=3 AND is_delete=FALSE")
    int countRunning(Long resultId);

    /**
     * 查询当前套件 通过的 测试脚本总数（status = 4）
     * @param resultId 测试结果 id
     * @return
     */
    @Select("SELECT COUNT(*) FROM pt_test_result_test_script WHERE result_id=#{resultId} AND `status`=4 AND is_delete=FALSE")
    int countPassed(Long resultId);

    /**
     * 查询当前套件 警告的 测试脚本总数（status = 5）
     * @param resultId 测试结果 id
     * @return
     */
    @Select("SELECT COUNT(*) FROM pt_test_result_test_script WHERE result_id=#{resultId} AND `status`=5 AND is_delete=FALSE")
    int countWarn(Long resultId);

    /**
     * 查询当前套件 跳过的 测试脚本总数（11 <= status <= 13）
     * @param resultId 测试结果 id
     * @return
     */
    @Select("SELECT COUNT(*) FROM pt_test_result_test_script WHERE result_id=#{resultId} AND `status`>=11 AND `status`<=13 AND is_delete=FALSE")
    int countSkipped(Long resultId);

    /**
     * 查询当前套件 失败的 测试脚本总数（21 <= status <= 22）
     * @param resultId 测试结果 id
     * @return
     */
    @Select("SELECT COUNT(*) FROM pt_test_result_test_script WHERE result_id=#{resultId} AND `status`>=21 AND `status`<=22 AND is_delete=FALSE")
    int countFailed(Long resultId);
}
