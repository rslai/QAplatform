package com.bjy.qa.dao.performancetest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjy.qa.entity.performancetest.PerfTestResultReport;
import com.bjy.qa.enumtype.PerfLogType;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface PerfTestResultReportDao extends BaseMapper<PerfTestResultReport> {
    /**
     * 查询测试结果 - 测试报告
     * @param resultId 测试结果
     * @param testScriptId 测试脚本 ID
     * @return
     */
    @Select("SELECT * FROM pt_test_result_report WHERE result_id=#{resultId} AND test_script_id=#{testScriptId} AND (log_type=20 OR log_type=21 OR log_type=22 OR log_type=40) AND is_delete=false")
    List<PerfTestResultReport> getReport(Long resultId, Long testScriptId);

    /**
     * 根据 PerfLogType 查询数据
     * @param perfLogType 性能测试日志类型
     * @param resultId 测试结果
     * @param testScriptId 测试脚本 ID
     * @return
     */
    @Select("SELECT * FROM pt_test_result_report WHERE result_id=#{resultId} AND test_script_id=#{testScriptId} AND log_type=#{perfLogType} AND is_delete=false")
    PerfTestResultReport getByPerfLogType(PerfLogType perfLogType, Long resultId, Long testScriptId);
}
