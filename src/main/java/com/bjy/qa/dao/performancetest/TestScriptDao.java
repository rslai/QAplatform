package com.bjy.qa.dao.performancetest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjy.qa.entity.performancetest.TestScript;
import com.bjy.qa.enumtype.CatalogType;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface TestScriptDao extends BaseMapper<TestScript> {
    /**
     * 根据 目录ID 查询该目录下的 性能测试脚本
     * @param projectId 项目ID
     * @param catalogType 目录类型
     * @param catalogId 目录ID
     * @return
     */
    @Select("SELECT * FROM pt_test_script WHERE catalog_id=#{catalogId} AND project_id=#{projectId} AND case_type=#{catalogType} AND is_delete=false ORDER BY sort ASC, created_at ASC")
    List<TestScript> selectByCatalogId(Long projectId, CatalogType catalogType, Long catalogId);

    /**
     * 搜索 性能测试脚本
     * @param projectId 项目 id
     * @param type 性能测试脚本类型 CaseType
     * @param id 性能测试脚本 id（就是 搜索内容）
     * @param findStr 搜索内容
     * @return
     */
    @Select("SELECT tc.* FROM " +
            "   pt_test_script AS tc " +
            "   LEFT JOIN ft_step s " +
            "   ON tc.id = s.test_case_id " +
            "WHERE tc.project_id=#{projectId} AND tc.type=#{type} AND (tc.id=#{id} OR tc.name LIKE #{findStr} OR s.desc LIKE #{findStr} OR s.extra1 LIKE #{findStr} OR s.extra2 LIKE #{findStr}) AND tc.is_delete=false " +
            "GROUP BY tc.id")
    List<TestScript> search(@Param("projectId") Long projectId, @Param("type") CatalogType type, @Param("id") String id, @Param("findStr") String findStr);

    /**
     * 查找测试脚本（包含已删除测试脚本），比如在测试报告中也应该能显示出来
     * @param id 测试脚本 id
     * @return
     */
    @Select("SELECT * FROM pt_test_script WHERE id=#{id}")
    TestScript findById(Long id);
}
