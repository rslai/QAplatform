package com.bjy.qa.dao.performancetest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjy.qa.entity.performancetest.PerfTestResultTestScript;
import com.bjy.qa.entity.performancetest.PerfTestSuiteTestScript;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface PerfTestSuiteTestScriptDao extends BaseMapper<PerfTestSuiteTestScript> {
    /**
     * 查询当前 性能测试结果 id 对应的性能测试脚本列表
     * @param resultId 测试结果 id
     * @return
     */
    @Select("select * from pt_test_result_test_script where result_id=#{rid} and is_delete=false")
    List<PerfTestResultTestScript> selecByResultId(Long resultId);
}
