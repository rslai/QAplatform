package com.bjy.qa.dao.performancetest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjy.qa.entity.performancetest.PerfTestSuite;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface PerfTestSuiteDao extends BaseMapper<PerfTestSuite> {
    /**
     * 根据 id 查询 性能测试套件 全部信息（包括套件下的所有用例、压力机、场景）
     * @param id 套件 id
     * @return
     */
    PerfTestSuite findById(Long id);
}
