package com.bjy.qa.dao.tools;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjy.qa.entity.tools.Upload;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface UploadDao extends BaseMapper<Upload> {
}
