package com.bjy.qa.dao.tools;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjy.qa.entity.tools.Ocr;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface OcrDao extends BaseMapper<Ocr> {
}
