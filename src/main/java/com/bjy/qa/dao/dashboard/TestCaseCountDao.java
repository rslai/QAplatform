package com.bjy.qa.dao.dashboard;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjy.qa.entity.dashboard.TestCaseCount;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Select;

import java.util.Date;
import java.util.List;
import java.util.Map;


public interface TestCaseCountDao extends BaseMapper<TestCaseCount> {
    /**
     * 删除当天的数据
     */
    @Delete("DELETE FROM dash_test_case_count WHERE DATE(updated_at) = #{date}")
    void deleteByDate(String date);

    /**
     * 数据统计-返回用户对应测试用例维度
     * @param projectId 项目 ID
     * @param startDate 开始日期
     * @param endDate 结束日期
     * @return
     */
    @Select("SELECT * FROM dash_test_case_count WHERE project_id = #{projectId} AND created_at >= #{startDate} and created_at <= #{endDate}")
    List<Map<String, Object>> selectTestCaseCount(Long projectId, Date startDate, Date endDate);
}
