package com.bjy.qa.dao.functionaltest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjy.qa.entity.functionaltest.Agent;
import com.bjy.qa.entity.functionaltest.TestSuiteAgent;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface TestSuiteAgentDao extends BaseMapper<TestSuiteAgent> {

    /**
     * 通过测试套件id来查找与其相关联的agent
     * @param testSuiteId 套件id
     * @return
     */
    @Select("SELECT fagent.* FROM ft_test_suite_agents tsagent " +
            "            INNER JOIN ft_agent fagent ON fagent.id = tsagent.agent_id " +
            "            WHERE tsagent.test_suites_id = #{testSuiteId} " +
            "            AND  tsagent.is_delete=false ")
    public List<Agent> findByTestSuiteId(long testSuiteId);

    /**
     * 通过测试套件id来查找与其相关联的agent
     * @param testSuiteId 套件id
     * @return
     */
    @Select("SELECT tsagent.* FROM ft_test_suite_agents tsagent " +
            "            INNER JOIN ft_agent fagent ON fagent.id = tsagent.agent_id " +
            "            WHERE tsagent.test_suites_id = #{testSuiteId} " +
            "            AND  tsagent.is_delete=false ")
    public List<TestSuiteAgent> selectByTestSuiteId(long testSuiteId);

}
