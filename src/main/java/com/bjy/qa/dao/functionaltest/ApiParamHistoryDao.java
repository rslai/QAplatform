package com.bjy.qa.dao.functionaltest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjy.qa.entity.functionaltest.ApiParamHistory;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface ApiParamHistoryDao extends BaseMapper<ApiParamHistory> {
    /**
     * 根据 api 历史记录 id 查询 api 参数历史记录列表
     * @param apiHistoryId api 历史记录 id
     * @return
     */
    @Select("SELECT * FROM ft_api_param_history WHERE api_history_id=#{apiHistoryId} AND is_delete=false ORDER BY sort ASC")
    List<ApiParamHistory> selectByApiHistoryId(long apiHistoryId);
}
