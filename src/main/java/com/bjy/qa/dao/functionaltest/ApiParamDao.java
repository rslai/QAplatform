package com.bjy.qa.dao.functionaltest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjy.qa.entity.functionaltest.ApiParam;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface ApiParamDao extends BaseMapper<ApiParam> {
    /**
     * 物理删除一条数据
     * @param ids ID
     * @return
     */
    @Delete("<script>" +
            "DELETE FROM ft_api_param WHERE id in (" +
            "<foreach collection='ids' item='item' separator=', '> " +
            "#{item}" +
            "</foreach> " +
            ")" +
            "</script>")
    int deletePhysically(List<Long> ids);

    /**
     * 根据 api id ，获得 apiParam 列表
     * @param apiId 接口 id
     */
    @Select("SELECT id FROM ft_api_param WHERE api_id = #{apiId}")
    List<Long> getListByApiId(Long apiId);

    @Select("SELECT * FROM ft_api_param WHERE api_id=#{apiId} AND is_delete=false ORDER BY sort ASC")
    public List<ApiParam> findByApiId(long apiId);
}
