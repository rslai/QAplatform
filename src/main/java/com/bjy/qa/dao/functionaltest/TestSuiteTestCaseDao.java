package com.bjy.qa.dao.functionaltest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjy.qa.entity.functionaltest.TestCase;
import com.bjy.qa.entity.functionaltest.TestSuiteTestCase;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface TestSuiteTestCaseDao extends BaseMapper<TestSuiteTestCase> {

    /**
     * 根据 suite id 查找待运行的 test case（但不包含测试用例中的 steps）
     * @param testSuiteId 套件 ID
     * @return
     */
    @Select("SELECT ftc.* FROM ft_test_suite_test_case tstc " +
            "            INNER JOIN ft_test_case ftc ON ftc.id = tstc.test_cases_id " +
            "            WHERE tstc.test_suites_id = #{testSuiteId} " +
            "            AND  tstc.is_delete=false ORDER BY sort ASC ")
    List<TestCase> findByTestSuiteId(long testSuiteId);

    /**
     * 根据 suite id 查找待运行的 test case 和 steps
     * @param testSuiteId 套件 ID
     * @return
     */
    List<TestCase> findTestCaseAndStepByTestSuiteId(long testSuiteId);

    /**
     * 通过测试套件id来查找与其相关联的test case
     * @param testSuiteId 套件id
     * @return TestSuiteTestCase 数组
     */
    @Select("SELECT tstc.* FROM ft_test_suite_test_case tstc " +
            "            INNER JOIN ft_test_case ftc ON ftc.id = tstc.test_cases_id " +
            "            WHERE tstc.test_suites_id = #{testSuiteId} " +
            "            AND  tstc.is_delete=false ORDER BY sort ASC ")
    List<TestSuiteTestCase> selectByTestSuiteId(long testSuiteId);


    /**
     * 根据用例的id列表，批量更新ft_test_suite_test_case的状态
     * @param cidList
     */
    @Update({"<script>",
                "<foreach collection='cidList' item='item' separator=';'> ",
                    "UPDATE ft_test_suite_test_case SET is_delete=true WHERE test_cases_id=#{item}",
                "</foreach> ",
            "</script>"
    })
    void updateByCidList(List<Long> cidList);
}
