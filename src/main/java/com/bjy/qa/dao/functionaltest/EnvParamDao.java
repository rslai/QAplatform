package com.bjy.qa.dao.functionaltest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjy.qa.entity.functionaltest.EnvParam;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface EnvParamDao extends BaseMapper<EnvParam> {
    @Select("<script>" +
            "SELECT * FROM ft_env_param WHERE project_id=#{projectId} AND env_name=#{envName} AND server_name IS NULL AND value IS NULL <when test='id!=null and id!=0'> AND id != #{id} </when> AND is_delete=false" +
            "</script>")
    EnvParam selectIdByEnvName(Long projectId, Long id, String envName);

    @Select("<script>" +
            "SELECT * FROM ft_env_param WHERE project_id=#{projectId} AND env_name=#{envName} AND env_name=#{envName} AND server_name=#{serverName} <when test='id!=null and id!=0'> AND id != #{id} </when> AND is_delete=false" +
            "</script>")
    EnvParam selectServerByName(Long projectId, Long id, String envName, String serverName);

    List<EnvParam> listEnvParamsByName(Long projectId, String envName);
}
