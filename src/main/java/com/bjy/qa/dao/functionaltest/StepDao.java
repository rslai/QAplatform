package com.bjy.qa.dao.functionaltest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjy.qa.entity.functionaltest.Step;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface StepDao extends BaseMapper<Step> {
    /**
     * 物理删除一条数据
     * @param ids ID
     * @return
     */
    @Delete("<script>" +
            "DELETE FROM ft_step WHERE id in (" +
            "<foreach collection='ids' item='item' separator=', '> " +
            "#{item}" +
            "</foreach> " +
            ")" +
            "</script>")
    int deletePhysically(List<Long> ids);

    /**
     * 根据 父 ID 物理删除一条数据
     * @param parentId 父 ID
     * @return
     */
    @Delete("DELETE FROM ft_step WHERE parent_id = #{parent_id}")
    int deletePhysicallyByParentId(Long parentId);

    /**
     * 根据测试用例 id 和 父 id，获得 step 的 id 列表
     * @param testCaseId 测试用例 id
     * @param parentId step 的 父 ID
     * @return
     */
    @Select("SELECT id FROM ft_step WHERE test_case_id = #{testCaseId} AND parent_id = #{parentId}")
    List<Long> getListByTestCaseId(Long testCaseId, Long parentId);

    /**
     * 根据测试用例 id，获得 step 列表
     * @param testCaseId 测试用例 id
     * @return
     */
    @Results(id = "findByTestCaseId", value = {
            @Result(property = "id", column = "id"),
            @Result(property = "childSteps", column = "id", many = @Many(select = "com.bjy.qa.dao.functionaltest.StepDao.findByStepParentId"))
    })
    @Select("SELECT * FROM ft_step WHERE test_case_id=#{testCaseId} AND parent_id=0 AND is_delete=false ORDER BY sort ASC")
    List<Step> findByTestCaseId(long testCaseId);

    /**
     * 根据 step 的 父 id，获得 step 列表
     * @param parentId step 的 父 id
     * @return
     */
    @Results(id = "findByStepParentId", value = {
            @Result(property = "id", column = "id"),
            @Result(property = "childSteps", column = "id", many = @Many(select = "com.bjy.qa.dao.functionaltest.StepDao.findByStepParentId"))
    })
    @Select("SELECT * FROM ft_step WHERE parent_id=#{parentId} AND is_delete=false ORDER BY sort ASC")
    List<Step> findByStepParentId(long parentId);

    /**
     * 根据用例的id列表，批量更新step的状态
     * @param cidList
     */
    @Update({"<script>",
                "<foreach collection='cidList' item='item' separator=';'> ",
                    "UPDATE ft_step SET is_delete=true WHERE test_case_id=#{item}",
                "</foreach> ",
            "</script>"
    })
    void updateByCidList(List<Long> cidList);

    /**
     * 根据 apiId 更新 extra1 字段内容
     * @param apiId apiId
     * @param extra1 要更新 extra1 的内容
     */
    @Update("UPDATE ft_step SET extra1=#{extra1} WHERE api_id=#{apiId}")
    void updateExtra1ByApiId(Long apiId, String extra1);
}
