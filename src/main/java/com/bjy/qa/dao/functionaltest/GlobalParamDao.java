package com.bjy.qa.dao.functionaltest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjy.qa.entity.functionaltest.GlobalParam;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface GlobalParamDao extends BaseMapper<GlobalParam> {
    /**
     * 查询指定用户下的所有全局参数
     * @param userId 用户id
     * @param projectId 项目ID
     * @return
     */
    @Select("SELECT * FROM ft_global_param WHERE project_id=#{projectId} AND ((global_param_type=2 and user_id=#{userId}) OR (global_param_type in(0,1) AND user_id=0)) AND is_delete=false")
    public List<GlobalParam> list(Long projectId, Long userId);

    /**
     * 通过全局参数名称查询指定项目，指定用户下全局参数
     * @param projectId 项目ID
     * @param userId 用户id
     * @param paramName 全局参数名称
     * @return
     */
    @Select("<script>" +
            "SELECT * FROM ft_global_param WHERE project_id=#{projectId} AND param_name=#{paramName} AND ((global_param_type=2 and user_id=#{userId}) OR (global_param_type !=2 AND user_id=0)) <when test='id!=null and id!=0'> AND id != #{id} </when> AND is_delete=false" +
            "</script>")
    GlobalParam selectUserParamByName(Long projectId, Long id, String paramName, Long userId);
}
