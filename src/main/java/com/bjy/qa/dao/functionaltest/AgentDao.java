package com.bjy.qa.dao.functionaltest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjy.qa.entity.functionaltest.Agent;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface AgentDao extends BaseMapper<Agent> {
    /**
     * 根据 agent key 更新 agent 状态
     * @param key key
     * @param status 状态
     * @return
     */
    @Update("UPDATE ft_agent SET status=#{status} WHERE `key`=#{key} AND is_delete=false")
    Integer updateStatusByAgentKey(@Param("key") String key, @Param("status") int status);

    /**
     * 更新所有 agent 状态
     * @param status 状态
     * @return
     */
    @Update("UPDATE ft_agent SET status=#{status} WHERE is_delete=false")
    Integer updateStatus(@Param("status") int status);
}
