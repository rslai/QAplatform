package com.bjy.qa.dao.functionaltest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjy.qa.entity.functionaltest.TestSuite;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface TestSuiteDao extends BaseMapper<TestSuite> {

    /**
     * 数据统计-返回当前项目下指定时间段内套件总数
     * @param project_id 项目 ID
     * @return
     */
    @Select("SELECT count(*) " +
            "FROM ft_test_suite " +
            "WHERE project_id = #{project_id}")
    int testSuiteCount(Long project_id);
}
