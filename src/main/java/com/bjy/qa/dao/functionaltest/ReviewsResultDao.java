package com.bjy.qa.dao.functionaltest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjy.qa.entity.functionaltest.ReviewsResult;

public interface ReviewsResultDao extends BaseMapper<ReviewsResult> {

}
