package com.bjy.qa.dao.functionaltest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjy.qa.entity.functionaltest.Api;
import com.bjy.qa.enumtype.CatalogType;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface ApiDao extends BaseMapper<Api> {
    @Select("SELECT * FROM ft_api WHERE is_delete = false")
    List<Api> selectAll();

    @Results(id = "selectById", value = {
            @Result(property = "id", column = "id"),
            @Result(property = "apiParams", column = "id", many = @Many(select = "com.bjy.qa.dao.functionaltest.ApiParamDao.findByApiId"))
    })
    @Select("SELECT * FROM ft_api WHERE id=#{id} AND type=#{type} AND is_delete=false")
    Api selectById(@Param("type") int type, @Param("id") long id);

    @Select("SELECT *, CONCAT(IFNULL(host, ''), uri) AS url FROM ft_api WHERE id=#{id} AND is_delete=false")
    Api findById(@Param("id") long id);

    /**
     * 根据 URI 查询 API
     * @param projectId 项目 ID
     * @param method http method
     * @param uri URI
     * @return
     */
    @Select("SELECT * FROM ft_api WHERE project_id=#{projectId} AND uri=#{uri} AND method=#{method} AND is_delete=false")
    List<Api> selectByUri(@Param("projectId") Long projectId, @Param("method") String method, @Param("uri") String uri);

    /**
     * 查询 URI 中带参数的 API
     * @param projectId 项目 ID
     * @param method http method
     * @return
     */
    @Select("SELECT * FROM ft_api WHERE project_id=#{projectId} AND method=#{method} AND uri LIKE '%${%' AND is_delete=false")
    List<Api> selectByUriParam(@Param("projectId") Long projectId, @Param("method") String method);

    /**
     * 搜索 api 定义信息
     * @param projectId 项目 id
     * @param type 接口类型 CatalogType
     * @param id 接口id（就是 搜索内容）
     * @param findStr 搜索内容
     * @return
     */
    @Select("SELECT api.* FROM " +
            "   ft_api AS api " +
            "   LEFT JOIN ft_api_param AS param " +
            "   ON api.id = param.api_id " +
            "WHERE api.project_id=#{projectId} AND api.type=#{type} AND (api.id=#{id} OR api.name LIKE #{findStr} OR param.name LIKE #{findStr} OR param.des LIKE #{findStr} OR param.example LIKE #{findStr}) AND api.is_delete=false " +
            "GROUP BY api.id")
    List<Api> search(@Param("projectId") Long projectId, @Param("type") CatalogType type, @Param("id") String id, @Param("findStr") String findStr);

    /**
     * 查询当前项目下的接口总数
     * @param project_id 项目 ID
     * @return
     */
    @Select("SELECT count(*) " +
            "FROM ft_api " +
            "WHERE project_id = #{project_id}")
    int apiCount(Long project_id);

    /**
     * 获取新增接口数
     * @param designer 用户名
     * @param projectId 项目ID
     * @param date 创建日期
     * @return
     */
    @Select("SELECT count(*) " +
            "FROM ft_api " +
            "WHERE designer = #{designer} AND project_id = #{projectId} and  DATE(created_at) = #{date}")
    int getAddApiCount(@Param("designer") String designer, @Param("projectId") Long projectId, @Param("date") String date);

    /**
     * 获取修改接口数
     * @param designer 用户名
     * @param projectId 项目ID
     * @param date 创建日期
     * @return
     */
    @Select("SELECT count(*) " +
            "FROM ft_api " +
            "WHERE designer = #{designer} AND project_id = #{projectId} AND DATE(created_at) = #{date} AND updated_at > created_at")
    int getUpdateApiCount(@Param("designer") String designer, @Param("projectId") Long projectId, @Param("date") String date);


}
