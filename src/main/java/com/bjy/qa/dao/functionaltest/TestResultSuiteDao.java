package com.bjy.qa.dao.functionaltest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjy.qa.entity.functionaltest.TestResultSuite;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface TestResultSuiteDao extends BaseMapper<TestResultSuite> {
    /**
     * 查询当前套件 待运行中的 测试用例总数（status = 1）
     * @param rid 测试结果 id
     * @return
     */
    @Select("SELECT COUNT(*) FROM ft_test_result_case WHERE rid=#{rid} and `status`=1 and is_delete=FALSE")
    int countWaitingTestcase(Long rid);

    /**
     * 查询当前套件 运行中的 测试用例总数（2 <= status <= 3）
     * @param rid 测试结果 id
     * @return
     */
    @Select("SELECT COUNT(*) FROM ft_test_result_case WHERE rid=#{rid} and `status`<=3 and is_delete=FALSE")
    int countRunningTestcase(Long rid);

    /**
     * 查询当前套件 通过的 测试用例总数（status = 4）
     * @param rid 测试结果 id
     * @return
     */
    @Select("SELECT COUNT(*) FROM ft_test_result_case WHERE rid=#{rid} and `status`=4 and is_delete=FALSE")
    int countPassedTestcase(Long rid);

    /**
     * 查询当前套件 警告的 测试用例总数（status = 5）
     * @param rid 测试结果 id
     * @return
     */
    @Select("SELECT COUNT(*) FROM ft_test_result_case WHERE rid=#{rid} and `status`=5 and is_delete=FALSE")
    int countWarnTestcase(Long rid);

    /**
     * 查询当前套件 跳过的 测试用例总数（11 <= status <= 13）
     * @param rid 测试结果 id
     * @return
     */
    @Select("SELECT COUNT(*) FROM ft_test_result_case WHERE rid=#{rid} and `status`>=11 and `status`<=13 and is_delete=FALSE")
    int countSkippedTestcase(Long rid);

    /**
     * 查询当前套件 失败的 测试用例总数（21 <= status <= 22）
     * @param rid 测试结果 id
     * @return
     */
    @Select("SELECT COUNT(*) FROM ft_test_result_case WHERE rid=#{rid} and `status`>=21 and `status`<=22 and is_delete=FALSE")
    int countFailedTestcase(Long rid);

    /**
     * 获取当前套件最终状态
     * @param rid 测试结果 id
     * @return
     */
    @Select("select MAX(status) from ft_test_result_case where rid=#{rid} limit 1")
    int getTestResultStatus(long rid);

    /**
     * 查找测试套件
     * @param projectId
     * @param sid
     * @return
     */
    @Select("SELECT * FROM ft_test_result_suite" +
            "    WHERE suite_id=#{sid} AND project_id=#{projectId} AND is_delete=FALSE")
    TestResultSuite getTestResultSuiteByProjectIdAndSid(Long projectId, Long sid);
}
