package com.bjy.qa.dao.functionaltest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjy.qa.entity.functionaltest.TestResultStep;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface TestResultStepDao extends BaseMapper<TestResultStep> {

    /**
     * 得到测试结果中的测试用例详情
     * @param resultId
     * @param cid
     * @return
     */
    @Select("select * from ft_test_result_step where rid=#{resultId} and cid=#{cid} and is_delete=false;")
    List<TestResultStep> selecByCaseIdAndResultId(long resultId, long cid);

}
