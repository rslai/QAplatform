package com.bjy.qa.dao.functionaltest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjy.qa.entity.functionaltest.ApiHistory;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface ApiHistoryDao extends BaseMapper<ApiHistory> {
    /**
     * 根据 apiId 查询历史记录列表
     * @param projectId 项目 id
     * @param apiId 接口 id
     * @return
     */
    @Results(id = "selectByApiId", value = {
            @Result(property = "id", column = "id"),
            @Result(property = "apiParamHistories", column = "id", many = @Many(select = "com.bjy.qa.dao.functionaltest.ApiParamHistoryDao.selectByApiHistoryId"))
    })
    @Select("SELECT * FROM ft_api_history WHERE api_id=#{apiId} AND project_id=#{projectId} AND is_delete=false ORDER BY created_at DESC")
    List<ApiHistory> selectByApiId(Long projectId, Long apiId);
}
