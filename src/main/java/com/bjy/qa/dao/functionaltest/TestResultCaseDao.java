package com.bjy.qa.dao.functionaltest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjy.qa.entity.functionaltest.TestResultCase;
import com.bjy.qa.entity.functionaltest.TestResultSuite;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface TestResultCaseDao extends BaseMapper<TestResultCase> {

    /**
     * 根据 测试结果id 和 测试用例id 查询 测试用例结果
     * @param rid 测试结果
     * @param cid 测试用例id
     * @return
     */
    @Select("select * from ft_test_result_case where cid=#{cid} and rid=#{rid} and is_delete=false;")
    TestResultCase getTestResultCase(Long rid, Long cid);

    /**
     * 查询当前 测试结果id 对应的测试用例列表
     * @param rid 测试结果 id
     * @return
     */
    @Select("select * from ft_test_result_case where rid=#{rid} and is_delete=false;")
    List<TestResultCase> selecByResultId(Long rid);

    /**
     * 通过case id list 查找对应测试用例集
     * @param cids
     * @param sid
     * @return
     */
    @Select({"<script>",
                "select * from ft_test_result_case ",
                    "<where>",
                        "<foreach collection='cids' item='item' separator='OR'> ",
                         " is_delete=FALSE AND cid=#{item} AND rid=(select id from ft_test_result_suite where suite_id=#{sid})",
                        "</foreach> ",
                    "</where>",
            "</script>"
    })
    List<TestResultCase> getTestResultCaseByCidList(List<Long> cids, Long sid);
}
