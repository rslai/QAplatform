package com.bjy.qa.dao.functionaltest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjy.qa.entity.functionaltest.ReviewsRemark;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface ReviewsRemarkDao extends BaseMapper<ReviewsRemark> {
    /**
     * 获取用例评审的备注信息
     * @param reviewsId 评审计划 ID
     * @param cid 用例 ID
     * @return
     */
    @Select("SELECT * FROM ft_reviews_remark" +
            "   WHERE test_case_id=#{cid} and reviews_id=#{reviewsId} and is_delete=false ORDER BY updated_at DESC")
    List<ReviewsRemark> getCaseRemark(Long reviewsId, Long cid);
}
