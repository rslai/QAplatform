package com.bjy.qa.dao.functionaltest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjy.qa.entity.functionaltest.StepHistory;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface StepHistoryDao extends BaseMapper<StepHistory> {
    /**
     * 根据测试用例历史记录 id 查询步骤历史记录列表
     * @param testCaseHistoryId 测试用例历史记录 id
     * @return
     */
    @Results(id = "findByTestCaseId", value = {
            @Result(property = "id", column = "id"),
            @Result(property = "childSteps", column = "id", many = @Many(select = "com.bjy.qa.dao.functionaltest.StepHistoryDao.findByStepHistoryParentId"))
    })
    @Select("SELECT * FROM ft_step_history WHERE test_case_history_id=#{testCaseHistoryId} AND history_parent_id=0 AND is_delete=false ORDER BY sort ASC")
    List<StepHistory> selectByTestCaseHistoryId(long testCaseHistoryId);

    /**
     * 根据 step_history 的 父 id，获得 StepHistory 列表
     * @param historyParentId step_history 历史记录的 父 id
     * @return
     */
    @Results(id = "findByStepHistoryParentId", value = {
            @Result(property = "id", column = "id"),
            @Result(property = "childSteps", column = "id", many = @Many(select = "com.bjy.qa.dao.functionaltest.StepHistoryDao.findByStepHistoryParentId"))
    })
    @Select("SELECT * FROM ft_step_history WHERE history_parent_id=#{historyParentId} AND is_delete=false ORDER BY sort ASC")
    List<StepHistory> findByStepHistoryParentId(long historyParentId);
}
