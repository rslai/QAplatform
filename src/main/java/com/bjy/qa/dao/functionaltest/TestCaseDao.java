package com.bjy.qa.dao.functionaltest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjy.qa.entity.functionaltest.TestCase;
import com.bjy.qa.enumtype.CatalogType;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface TestCaseDao extends BaseMapper<TestCase> {
    @Results(id = "selectById", value = {
            @Result(property = "id", column = "id"),
            @Result(property = "steps", column = "id", many = @Many(select = "com.bjy.qa.dao.functionaltest.StepDao.findByTestCaseId"))
    })
    @Select("SELECT * FROM ft_test_case WHERE id=#{id} AND is_delete=false")
    List<TestCase> selectById(long id);

    @Results(id = "selectByCid", value = {
            @Result(property = "id", column = "id"),
            @Result(property = "steps", column = "id", many = @Many(select = "com.bjy.qa.dao.functionaltest.StepDao.findByTestCaseId"))
    })
    @Select("SELECT * FROM ft_test_case WHERE catalog_id=#{catalogId} AND project_id=#{projectId} AND case_type=#{catalogType} AND is_delete=false ORDER BY sort ASC, created_at ASC")
    List<TestCase> selectByCatalogId(Long projectId, CatalogType catalogType, Long catalogId);

    /**
     * 查找测试用例（包含已删除测试用例），比如在测试报告中也应该能显示出来
     * @param id 测试用例 id
     * @return
     */
    @Select("SELECT * FROM ft_test_case WHERE id=#{id}")
    TestCase findById(Long id);

    /**
     * 搜索测试用例
     * @param projectId 项目 id
     * @param type 测试用例类型 CaseType
     * @param id 测试用例 id（就是 搜索内容）
     * @param findStr 搜索内容
     * @return
     */
    @Select("SELECT tc.* FROM " +
            "   ft_test_case AS tc " +
            "   LEFT JOIN ft_step s " +
            "   ON tc.id = s.test_case_id " +
            "WHERE tc.project_id=#{projectId} AND tc.type=#{type} AND (tc.id=#{id} OR tc.name LIKE #{findStr} OR s.desc LIKE #{findStr} OR s.extra1 LIKE #{findStr} OR s.extra2 LIKE #{findStr}) AND tc.is_delete=false " +
            "GROUP BY tc.id")
    List<TestCase> search(@Param("projectId") Long projectId, @Param("type") CatalogType type, @Param("id") String id, @Param("findStr") String findStr);

    /**
     * 获取新增用例数
     * @param designer 用户名
     * @param projectId 项目ID
     * @param caseType 用例类型
     * @param date 创建日期
     * @return
     */
    @Select("SELECT COUNT(*) AS count  FROM ft_test_case WHERE designer = #{designer} AND case_type = #{caseType} AND project_id = #{projectId} AND DATE(created_at) = #{date}")
    int getAddCount(@Param("designer") String designer, @Param("projectId") Long projectId, @Param("caseType") CatalogType caseType, @Param("date") String date);

    /**
     * 获取修改用例数
     * @param designer 用户名
     * @param projectId 项目ID
     * @param caseType 用例类型
     * @param date 修改日期
     * @return
     */
    @Select("SELECT COUNT(*) AS count  FROM ft_test_case WHERE designer = #{designer} AND case_type = #{caseType} AND project_id = #{projectId} AND DATE(created_at) = #{date} AND updated_at > created_at")
    int getUpdateCount(@Param("designer") String designer, @Param("projectId") Long projectId, @Param("caseType") CatalogType caseType, @Param("date") String date);

    /**
     * 数据统计-当前项目下的指定时间段内功能和接口用例总数
     * @param catalogType 用例类型
     * @param project_id 项目 ID
     * @return
     */
    @Select("SELECT count(*) " +
            "FROM ft_test_case " +
            "WHERE case_type = #{catalogType} AND project_id = #{project_id}")
    int testCaseCount(CatalogType catalogType, Long project_id);


}
