package com.bjy.qa.dao.functionaltest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjy.qa.entity.functionaltest.ReviewTestCase;
import com.bjy.qa.entity.functionaltest.TestCase;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface ReviewTestCaseDao extends BaseMapper<ReviewTestCase> {
    /**
     * 通过评审计划 id 来找对应的测试用例
     * @param reviewsId
     * @return
     */
    @Select("SELECT ftc.* FROM ft_reviews_test_case rtc  " +
            "INNER JOIN ft_test_case ftc ON ftc.id = rtc.test_case_id " +
            "WHERE rtc.reviews_id = #{reviewsId}  " +
            "AND  rtc.is_delete=false ORDER BY sort ASC ")
    List<TestCase> findByReviewId(long reviewsId);

    /**
     * 通过通过评审计划 id 来查找与其相关联的 test case
     * @param  reviewsId
     * @return
     */
    @Select("SELECT rtc.* FROM ft_reviews_test_case rtc " +
            "INNER JOIN ft_test_case ftc ON ftc.id = rtc.test_case_id " +
            "WHERE rtc.reviews_id = #{reviewsId} " +
            "AND  rtc.is_delete=false ORDER BY sort ASC ")
    List<ReviewTestCase> selectByReviewId(long reviewsId);
}
