package com.bjy.qa.dao.functionaltest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjy.qa.entity.functionaltest.ReviewsUser;
import com.bjy.qa.entity.user.User;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface ReviewsUserDao extends BaseMapper<ReviewsUser> {
    /**
     * 通过评审计划 id 来查找与其对应的 user
     * @param reviewId
     * @return
     */
    @Select("SELECT u.*  " +
            "FROM ft_reviews_user ruser  " +
            "INNER JOIN user u ON u.id = ruser.reviewer_user_id  " +
            "WHERE ruser.reviews_id = #{reviews_id} AND ruser.is_delete = 0")
    List<User> findByReviewId(long reviewId);

}
