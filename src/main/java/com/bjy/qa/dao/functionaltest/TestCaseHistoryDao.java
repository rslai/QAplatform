package com.bjy.qa.dao.functionaltest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjy.qa.entity.functionaltest.TestCaseHistory;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface TestCaseHistoryDao extends BaseMapper<TestCaseHistory> {
    /**
     * 根据测试用例 id 查询历史记录列表
     * @param projectId 项目 id
     * @param testCaseId 测试用例 id
     * @return
     */
    @Results(id = "selectByTestCaseId", value = {
            @Result(property = "id", column = "id"),
            @Result(property = "stepHistories", column = "id", many = @Many(select = "com.bjy.qa.dao.functionaltest.StepHistoryDao.selectByTestCaseHistoryId"))
    })
    @Select("SELECT * FROM ft_test_case_history WHERE test_case_id=#{testCaseId} AND project_id=#{projectId} AND is_delete=false ORDER BY created_at DESC")
    List<TestCaseHistory> selectByTestCaseId(Long projectId, Long testCaseId);
}
