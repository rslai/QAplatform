package com.bjy.qa.dao.functionaltest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjy.qa.entity.functionaltest.TestCaseRemark;
import org.apache.ibatis.annotations.Select;

import java.util.Date;
import java.util.List;

public interface TestCaseRemarkDao extends BaseMapper<TestCaseRemark> {

    @Select("SELECT * FROM ft_test_case_remark" +
            "   WHERE cid=#{cid} and suite_id=#{sid} and is_delete=false ORDER BY updated_at DESC")
    List<TestCaseRemark> getCaseRemark(Long sid, Long cid);

    /**
     * 获取用户执行手工测试用例的时间列表
     * @param designer 用户
     * @param projectId 项目ID
     * @param date 日期
     * @return
     */
    @Select("SELECT created_at FROM ft_test_case_remark WHERE operator = #{designer} AND project_id = #{projectId} AND DATE(created_at) = #{date}")
    List<Date> selectDate(String designer, Long projectId, String date);

}
