package com.bjy.qa.dao.manage;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjy.qa.entity.manage.Project;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface ProjectDao extends BaseMapper<Project> {
    /**
     * 查找不包含当前 id 的 mock 前置 url
     * @param id id
     * @param mockPreUrl mock 前置 url
     * @return
     */
    @Select("SELECT id,company_id,name,`desc`,mock,mock_pre_url,is_delete,updated_at,created_at FROM project WHERE mock_pre_url = #{mockPreUrl} AND id <> #{id} LIMIT 1")
    Project selectMockPreUrl(@Param("id") Long id, @Param("mockPreUrl") String mockPreUrl);
}
