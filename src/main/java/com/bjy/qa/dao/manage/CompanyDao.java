package com.bjy.qa.dao.manage;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjy.qa.entity.manage.Company;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface CompanyDao extends BaseMapper<Company> {

}
