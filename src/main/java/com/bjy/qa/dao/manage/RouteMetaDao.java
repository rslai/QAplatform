package com.bjy.qa.dao.manage;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjy.qa.entity.manage.RouteMeta;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface RouteMetaDao extends BaseMapper<RouteMeta> {

}
