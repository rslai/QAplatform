package com.bjy.qa.dao.manage;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjy.qa.entity.manage.RoleRoute;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface RoleRouteDao extends BaseMapper<RoleRoute> {
    /**
     * 根据 路由 ID 删除角色权限信息
     * @param routeId 路由 ID
     * @return
     */
    @Update("UPDATE role_route SET is_delete=true WHERE route_id=#{routeId}")
    int deleteByRouteId(@Param("routeId") Long routeId);
}
