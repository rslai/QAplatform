package com.bjy.qa.dao.user;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjy.qa.entity.user.UserRole;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface UserRoleDao extends BaseMapper<UserRole> {
    @Results(id = "selectById", value = {
            @Result(property = "id", column = "id"),
            @Result(property = "roleId", column = "role_id"),
            @Result(property = "role", column = "role_id", one=@One(select = "com.bjy.qa.dao.manage.RoleDao.selectById")),
    })
    @Select("SELECT * FROM user_role WHERE user_id=#{userId} AND is_delete=false")
    List<UserRole> selectByUserId(@Param("userId") Long userId);
}
