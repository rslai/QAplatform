package com.bjy.qa.dao.user;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjy.qa.entity.user.UserSshServerInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface UserSshServerInfoDao extends BaseMapper<UserSshServerInfo> {

    @Select("SELECT * FROM user_ssh_server_info WHERE user_id=#{userId} AND ssh_server_info_id=#{userSshServerInfo} AND is_delete=false")
    public UserSshServerInfo selectOne(Long userId, Long userSshServerInfo);

}
