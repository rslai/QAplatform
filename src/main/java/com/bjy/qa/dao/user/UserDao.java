package com.bjy.qa.dao.user;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjy.qa.entity.user.User;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface UserDao extends BaseMapper<User> {

    @Select("SELECT " +
            "   `user`.*, " +
            "   company.NAME AS company_name, " +
            "   project.NAME AS project_name " +
            "FROM " +
            "   `user` " +
            "   LEFT JOIN company ON `user`.company_id = company.id " +
            "   LEFT JOIN project ON `user`.project_id = project.id " +
            "WHERE " +
            "   `user`.id = #{id}")
    public User selectById(Long id);

    @Select("SELECT * FROM user WHERE account=#{account} AND code=#{code} AND is_delete=false")
    public User selectByAcountAndCode(@Param("account") String account, @Param("code") String code);

    @Select("SELECT * FROM user WHERE account=#{account} AND is_delete=false")
    public User selectByAcount(@Param("account") String account);

    /**
     * 查询用户完整信息，包括公司及项目列表（需要关联多个表一般不要使用）
     * @param id 用户id
     * @return 用户数据
     */
    public User selectWholeInfoById(@Param("userId") Long id);

    /**
     * 通过 项目 ID 查询所有拥有该项目权限的用户
     * @param projectId 项目 ID
     * @return
     */
    @Select("SELECT * " +
            "FROM user " +
            "LEFT JOIN user_company_project ON user.id = user_company_project.user_id " +
            "WHERE user_company_project.project_id=#{projectId} AND user.is_delete=false AND user_company_project.is_delete=false")
    List<User> selectUserByProjectId(Long projectId);

    /**
     * 通过 项目 ID 查询所有不包含该项目权限的用户
     * @param projectId 项目 ID
     * @return
     */
    @Select("SELECT * " +
            "FROM user " +
            "WHERE id NOT IN ( " +
            "   SELECT user.id " +
            "   FROM user " +
            "   LEFT JOIN user_company_project ON user.id = user_company_project.user_id " +
            "   WHERE user_company_project.project_id=#{projectId} AND user.is_delete=false AND user_company_project.is_delete=false" +
            ") AND is_delete=false")
    List<User> selectUserByNotIncludeProjectId(Long projectId);

    /**
     * 根据用户 ID 查询用户信息（总条数）
     * @param projectId 项目 ID
     * @param account 用户账号
     * @param name 用户姓名
     * @return
     */
    Long countListByProjectId(@Param("projectId") Long projectId, @Param("account") String account, @Param("name") String name);

    /**
     * 根据用户 ID 查询用户信息
     * @param projectId 项目 ID
     * @param account 用户账号
     * @param name 用户姓名
     * @param column 排序字段
     * @param asc 是否升序
     * @param offset 偏移量
     * @param limit 限制条数
     * @return
     */
    List<User> listByProjectId(@Param("projectId") Long projectId, @Param("account") String account, @Param("name") String name, @Param("column") String column, @Param("asc") boolean asc, @Param("offset") Long offset, @Param("limit") Long limit);
}
