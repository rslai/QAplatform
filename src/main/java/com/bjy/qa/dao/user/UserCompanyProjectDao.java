package com.bjy.qa.dao.user;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjy.qa.entity.user.UserCompanyProject;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface UserCompanyProjectDao extends BaseMapper<UserCompanyProject> {
    /**
     * 获取所有用户和他们关联的项目信息
     * @return
     */
    @Select("SELECT ucp.project_id, u.name " +
            "   FROM user u " +
            "JOIN user_company_project ucp ON u.id = ucp.user_id")
    List<Map<String, Object>> getUserCompanyProjects();
}
