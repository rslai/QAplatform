package com.bjy.qa.dao.project;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjy.qa.entity.project.Job;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface JobDao extends BaseMapper<Job> {

}
