package com.bjy.qa.dao.project;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjy.qa.entity.project.JobResult;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface JobResultDao extends BaseMapper<JobResult> {

}
