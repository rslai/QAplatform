package com.bjy.qa.dao.project;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjy.qa.entity.project.Robot;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface RobotDao extends BaseMapper<Robot> {

}
