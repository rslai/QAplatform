package com.bjy.qa.quartz;

import com.bjy.qa.service.project.IJobService;
import org.quartz.SchedulerException;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 初始化系统定时任务
 */
@Component
public class QuartzLaunchTool implements ApplicationRunner {
    @Resource
    private IJobService iJobService;

    @Override
    public void run(ApplicationArguments args) throws SchedulerException {
        iJobService.initSystemJob();
    }
}