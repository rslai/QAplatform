package com.bjy.qa.quartz;

import com.bjy.qa.dao.project.JobResultDao;
import com.bjy.qa.entity.Response;
import com.bjy.qa.entity.project.JobResult;
import com.bjy.qa.enumtype.JobType;
import com.bjy.qa.enumtype.TaskStatus;
import com.bjy.qa.exception.MyException;
import com.bjy.qa.service.dashboard.ITestCaseCountService;
import com.bjy.qa.service.functionaltest.ITestSuiteService;
import com.bjy.qa.service.manage.ICleanDataService;
import com.bjy.qa.service.project.IJobService;
import com.bjy.qa.util.EnumUtil;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 定时任务实现类
 */
@Component
public class QuartzJob extends QuartzJobBean implements Job {
    private final Logger logger = LoggerFactory.getLogger(QuartzJob.class);
    @Resource
    IJobService iJobService;

    @Resource
    ITestSuiteService iTestSuiteService;

    @Resource
    JobResultDao jobResultDao;

    @Resource
    ICleanDataService iCleanDataService;

    @Resource
    ITestCaseCountService iTestCaseCountService;

    /**
     * 执行定时任务，回调此方法
     * @param jobExecutionContext
     */
    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) {
        JobDataMap dataMap = jobExecutionContext.getJobDetail().getJobDataMap();
        JobType type = EnumUtil.valueOf(JobType.class, dataMap.getInt("type"));

        com.bjy.qa.entity.project.Job job = iJobService.getById(dataMap.getLong("id"));
        switch (type) {
            case SYSTEM_JOB: {
                if (job != null) {
                    JobResult jobResult = new JobResult();
                    jobResult.setJobId(job.getId());
                    jobResult.setProjectId(job.getProjectId());
                    try {
                        Response response = null;
                        switch (job.getRunUser()) {
                            case "cleanDeleted":
                                response = Response.success(iCleanDataService.cleanDeleted());
                                break;
                            case "cleanTestResult":
                                response = Response.success(iCleanDataService.cleanTestResult());
                                break;
                            case "testCaseCount":
                                response = Response.success(iTestCaseCountService.testCaseCount());
                                break;
                        }
                        if (response == null) {
                            throw new MyException("系统定时任务执行失败，返回执行结果为 null！");
                        } else {
                            jobResult.setResult(response.toString());
                            jobResult.setStatus(TaskStatus.END_SUCCESS.getValue());

                            logger.info("系统定时任务完成。{}", response);
                        }
                    } catch (Exception e) {
                        jobResult.setResult(e.getMessage());
                        jobResult.setStatus(TaskStatus.END_FAILED.getValue());
                    }
                    jobResultDao.insert(jobResult);
                } else {
                    logger.info("定时任务执行失败，找不到任务 ID: " + dataMap.getLong("id") + " ！");
                }
                break;
            }
            case INTERFACE: {
                if (job != null) {
                    JobResult jobResult = new JobResult();
                    jobResult.setJobId(job.getId());
                    jobResult.setProjectId(job.getProjectId());
                    try {
                        Response response = Response.success(iTestSuiteService.runSuite(job.getTaskId(), job.getUserId(), "JOB-" + job.getRunUser()));
                        jobResult.setResult(response.toString());
                        jobResult.setStatus(TaskStatus.END_SUCCESS.getValue());

                        logger.info("接口定时任务开始，任务 ID：" + job.getTaskId() + " " + response);
                    } catch (Exception e) {
                        jobResult.setResult(e.getMessage());
                        jobResult.setStatus(TaskStatus.END_FAILED.getValue());
                    }
                    jobResultDao.insert(jobResult);
                } else {
                    logger.info("定时任务执行失败，找不到任务 ID: " + dataMap.getLong("id") + " ！");
                }
                break;
            }
            case PERFORMANCE: {
                logger.info("性能测试定时任务，敬请期待！！！！");
                break;
            }
        }
    }
}
