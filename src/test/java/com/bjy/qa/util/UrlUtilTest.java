package com.bjy.qa.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class UrlUtilTest {
    /**
     * url 中 host 部分带参数，且不带 path
     */
    @Test
    void getHostUri_01() {
        String url = "http://${xxx}//?aa=bb&cc=dd";
        Assertions.assertEquals("http://${xxx}", UrlUtil.getHost(url));
        Assertions.assertEquals("/", UrlUtil.getUri(url));
        Assertions.assertEquals("aa=bb&cc=dd", UrlUtil.getQuery(url));
        Assertions.assertEquals("[KeyValueStore{name='aa', value=bb}, KeyValueStore{name='cc', value=dd}]", UrlUtil.param2KeyValueStore(UrlUtil.getQuery(url)).toString());
        Assertions.assertEquals("{\"aa\":\"bb\",\"cc\":\"dd\"}", UrlUtil.param2JsonStr(UrlUtil.getQuery(url)).toString());
    }

    /**
     * url 中 uri 中带多个 ?
     */
    @Test
    void getHostUri_02() {
        String url = "http://${xxx}//??aa=bb&cc=dd";
        Assertions.assertEquals("http://${xxx}", UrlUtil.getHost(url));
        Assertions.assertEquals("/", UrlUtil.getUri(url));
        Assertions.assertEquals("aa=bb&cc=dd", UrlUtil.getQuery(url));
        Assertions.assertEquals("[KeyValueStore{name='aa', value=bb}, KeyValueStore{name='cc', value=dd}]", UrlUtil.param2KeyValueStore(UrlUtil.getQuery(url)).toString());
        Assertions.assertEquals("{\"aa\":\"bb\",\"cc\":\"dd\"}", UrlUtil.param2JsonStr(UrlUtil.getQuery(url)).toString());
    }

    /**
     * url 中 uri 部分不带 path 只有一个 ?
     */
    @Test
    void getHostUri_03() {
        String url = "http://${xxx}?aa=bb&cc=dd";
        Assertions.assertEquals("http://${xxx}", UrlUtil.getHost(url));
        Assertions.assertEquals("/", UrlUtil.getUri(url));
        Assertions.assertEquals("aa=bb&cc=dd", UrlUtil.getQuery(url));
        Assertions.assertEquals("[KeyValueStore{name='aa', value=bb}, KeyValueStore{name='cc', value=dd}]", UrlUtil.param2KeyValueStore(UrlUtil.getQuery(url)).toString());
        Assertions.assertEquals("{\"aa\":\"bb\",\"cc\":\"dd\"}", UrlUtil.param2JsonStr(UrlUtil.getQuery(url)).toString());
    }

    /**
     * url 中只有 host 部分
     */
    @Test
    void getHostUri_04() {
        String url = "http://ww.example.com:8080";
        Assertions.assertEquals("http://ww.example.com:8080", UrlUtil.getHost(url));
        Assertions.assertEquals("/", UrlUtil.getUri(url));
        Assertions.assertEquals("", UrlUtil.getQuery(url));
        Assertions.assertEquals("[]", UrlUtil.param2KeyValueStore(UrlUtil.getQuery(url)).toString());
        Assertions.assertEquals("{}", UrlUtil.param2JsonStr(UrlUtil.getQuery(url)).toString());
    }

    /**
     * url 中有 host、path
     */
    @Test
    void getHostUri_05() {
        String url = "http://ww.example.com:8080/path";
        Assertions.assertEquals("http://ww.example.com:8080", UrlUtil.getHost(url));
        Assertions.assertEquals("/path", UrlUtil.getUri(url));
        Assertions.assertEquals("", UrlUtil.getQuery(url));
        Assertions.assertEquals("[]", UrlUtil.param2KeyValueStore(UrlUtil.getQuery(url)).toString());
        Assertions.assertEquals("{}", UrlUtil.param2JsonStr(UrlUtil.getQuery(url)).toString());
    }

    /**
     * url 中有 host、path、参数
     */
    @Test
    void getHostUri_06() {
        String url = "http://ww.example.com:8080/path?aa=bb&cc=dd";
        Assertions.assertEquals("http://ww.example.com:8080", UrlUtil.getHost(url));
        Assertions.assertEquals("/path", UrlUtil.getUri(url));
        Assertions.assertEquals("aa=bb&cc=dd", UrlUtil.getQuery(url));
        Assertions.assertEquals("[KeyValueStore{name='aa', value=bb}, KeyValueStore{name='cc', value=dd}]", UrlUtil.param2KeyValueStore(UrlUtil.getQuery(url)).toString());
        Assertions.assertEquals("{\"aa\":\"bb\",\"cc\":\"dd\"}", UrlUtil.param2JsonStr(UrlUtil.getQuery(url)).toString());
    }

    /**
     * url 中有 host、path、没有参数但有一个 ?
     */
    @Test
    void getHostUri_07() {
        String url = "http://ww.example.com:8080/path?";
        Assertions.assertEquals("http://ww.example.com:8080", UrlUtil.getHost(url));
        Assertions.assertEquals("/path", UrlUtil.getUri(url));
        Assertions.assertEquals("", UrlUtil.getQuery(url));
        Assertions.assertEquals("[]", UrlUtil.param2KeyValueStore(UrlUtil.getQuery(url)).toString());
        Assertions.assertEquals("{}", UrlUtil.param2JsonStr(UrlUtil.getQuery(url)).toString());
    }

    /**
     * url 中有 host，没有 path、没有参数但有一个 ?
     */
    @Test
    void getHostUri_08() {
        String url = "http://ww.example.com:8080?";
        Assertions.assertEquals("http://ww.example.com:8080", UrlUtil.getHost(url));
        Assertions.assertEquals("/", UrlUtil.getUri(url));
        Assertions.assertEquals("", UrlUtil.getQuery(url));
        Assertions.assertEquals("[]", UrlUtil.param2KeyValueStore(UrlUtil.getQuery(url)).toString());
        Assertions.assertEquals("{}", UrlUtil.param2JsonStr(UrlUtil.getQuery(url)).toString());
    }

    /**
     * url 中 path 中存在多个 /
     */
    @Test
    void getHostUri_09() {
        String url = "http://ww.example.com:8080/path//xxx/?aa=bb&cc=dd";
        Assertions.assertEquals("http://ww.example.com:8080", UrlUtil.getHost(url));
        Assertions.assertEquals("/path//xxx/", UrlUtil.getUri(url));
        Assertions.assertEquals("aa=bb&cc=dd", UrlUtil.getQuery(url));
        Assertions.assertEquals("[KeyValueStore{name='aa', value=bb}, KeyValueStore{name='cc', value=dd}]", UrlUtil.param2KeyValueStore(UrlUtil.getQuery(url)).toString());
        Assertions.assertEquals("{\"aa\":\"bb\",\"cc\":\"dd\"}", UrlUtil.param2JsonStr(UrlUtil.getQuery(url)).toString());
    }

    /**
     * url 中没有端口
     */
    @Test
    void getHostUri_10() {
        String url = "https://ww.example.com";
        Assertions.assertEquals("https://ww.example.com", UrlUtil.getHost(url));
        Assertions.assertEquals("/", UrlUtil.getUri(url));
        Assertions.assertEquals("", UrlUtil.getQuery(url));
        Assertions.assertEquals("[]", UrlUtil.param2KeyValueStore(UrlUtil.getQuery(url)).toString());
        Assertions.assertEquals("{}", UrlUtil.param2JsonStr(UrlUtil.getQuery(url)).toString());
    }

    /**
     * url 中参数中有多个 ?
     */
    @Test
    void getHostUri_11() {
        String url = "https://www.baidu.com/s?wd=url%20uri&rsv_spt=1&rsv_iqid=0xc5214f770025594b&issp=1&f=8&rsv_bp=1&rsv_idx=2&ie=utf-8&tn=baiduhome_pg&rsv_dl=tb&rsv_enter=1&rsv_sug3=2&rsv_sug1=1&rsv_sug7=100&rsv_sug2=0&rsv_btype=i&prefixsug=d&rsp=5&inputT=1086&rsv_sug4=1237?aa=bb";
        Assertions.assertEquals("https://www.baidu.com", UrlUtil.getHost(url));
        Assertions.assertEquals("/s", UrlUtil.getUri(url));
        Assertions.assertEquals("wd=url%20uri&rsv_spt=1&rsv_iqid=0xc5214f770025594b&issp=1&f=8&rsv_bp=1&rsv_idx=2&ie=utf-8&tn=baiduhome_pg&rsv_dl=tb&rsv_enter=1&rsv_sug3=2&rsv_sug1=1&rsv_sug7=100&rsv_sug2=0&rsv_btype=i&prefixsug=d&rsp=5&inputT=1086&rsv_sug4=1237aa=bb", UrlUtil.getQuery(url));
        Assertions.assertEquals("[KeyValueStore{name='wd', value=url uri}, KeyValueStore{name='rsv_spt', value=1}, KeyValueStore{name='rsv_iqid', value=0xc5214f770025594b}, KeyValueStore{name='issp', value=1}, KeyValueStore{name='f', value=8}, KeyValueStore{name='rsv_bp', value=1}, KeyValueStore{name='rsv_idx', value=2}, KeyValueStore{name='ie', value=utf-8}, KeyValueStore{name='tn', value=baiduhome_pg}, KeyValueStore{name='rsv_dl', value=tb}, KeyValueStore{name='rsv_enter', value=1}, KeyValueStore{name='rsv_sug3', value=2}, KeyValueStore{name='rsv_sug1', value=1}, KeyValueStore{name='rsv_sug7', value=100}, KeyValueStore{name='rsv_sug2', value=0}, KeyValueStore{name='rsv_btype', value=i}, KeyValueStore{name='prefixsug', value=d}, KeyValueStore{name='rsp', value=5}, KeyValueStore{name='inputT', value=1086}]", UrlUtil.param2KeyValueStore(UrlUtil.getQuery(url)).toString());
        Assertions.assertEquals("{\"rsv_spt\":\"1\",\"rsv_sug7\":\"100\",\"rsv_btype\":\"i\",\"inputT\":\"1086\",\"f\":\"8\",\"rsv_dl\":\"tb\",\"rsv_sug3\":\"2\",\"rsv_sug2\":\"0\",\"rsv_sug1\":\"1\",\"prefixsug\":\"d\",\"rsv_bp\":\"1\",\"wd\":\"url uri\",\"rsp\":\"5\",\"rsv_enter\":\"1\",\"issp\":\"1\",\"rsv_iqid\":\"0xc5214f770025594b\",\"tn\":\"baiduhome_pg\",\"rsv_idx\":\"2\",\"ie\":\"utf-8\"}", UrlUtil.param2JsonStr(UrlUtil.getQuery(url)).toString());
    }

    /**
     * url 中 参数只有 key + =
     */
    @Test
    void getHostUri_12() {
        String url = "http://${xxx}//?aa=bb&cc=";
        Assertions.assertEquals("http://${xxx}", UrlUtil.getHost(url));
        Assertions.assertEquals("/", UrlUtil.getUri(url));
        Assertions.assertEquals("aa=bb&cc=", UrlUtil.getQuery(url));
        Assertions.assertEquals("[KeyValueStore{name='aa', value=bb}, KeyValueStore{name='cc', value=}]", UrlUtil.param2KeyValueStore(UrlUtil.getQuery(url)).toString());
        Assertions.assertEquals("{\"aa\":\"bb\",\"cc\":\"\"}", UrlUtil.param2JsonStr(UrlUtil.getQuery(url)).toString());
    }

    /**
     * url 中 参数只有 key 没有 value
     */
    @Test
    void getHostUri_13() {
        String url = "http://${xxx}//?aa=bb&cc";
        Assertions.assertEquals("http://${xxx}", UrlUtil.getHost(url));
        Assertions.assertEquals("/", UrlUtil.getUri(url));
        Assertions.assertEquals("aa=bb&cc", UrlUtil.getQuery(url));
        Assertions.assertEquals("[KeyValueStore{name='aa', value=bb}]", UrlUtil.param2KeyValueStore(UrlUtil.getQuery(url)).toString());
        Assertions.assertEquals("{\"aa\":\"bb\"}", UrlUtil.param2JsonStr(UrlUtil.getQuery(url)).toString());
    }

    /**
     * url 中 参数中没有 key 没有 value 只有 =
     */
    @Test
    void getHostUri_14() {
        String url = "http://${xxx}//?aa=bb&=";
        Assertions.assertEquals("http://${xxx}", UrlUtil.getHost(url));
        Assertions.assertEquals("/", UrlUtil.getUri(url));
        Assertions.assertEquals("aa=bb&=", UrlUtil.getQuery(url));
        Assertions.assertEquals("[KeyValueStore{name='aa', value=bb}]", UrlUtil.param2KeyValueStore(UrlUtil.getQuery(url)).toString());
        Assertions.assertEquals("{\"aa\":\"bb\"}", UrlUtil.param2JsonStr(UrlUtil.getQuery(url)).toString());
    }

    /**
     * cookie 中包含 ;
     */
    @Test
    void getCookie_01() {
        String cookieStr = "COOKIE_SESSION=3393_2_8_9_22_21_1_0_8_7_18_0_3389_0_4_24_1701255522_1701252149_1701255518%7C9%23285902_383_1701252125%7C9; ab_sr=1.0.1_NjIyNzJlZTMxNjRiNTI0YTA0ODFkYjQxNTgyNGQ0NTc5OWIxMjFmMDAyMmI3ZWY1M2I5ZWU1NGY4Y2Q2ZjA1OTIwNzg3ODM5OTYzMWU0NzEwODRkYWIxZjM2YmQ5OTM0OTRmZTYxNGY0YmIxMTBhZWVhNzNiZDkwMGQ0YTY1ZmFlMTkwODcxOTZjMzY0ZTJkZDQ5NDVkYjQ5MWIzY2JjYmVkMDY4NTU0ZDkzNmE2NGM5ODQ1MTAyYzExYTIxYzI3; H_PS_645EC=9fc1R1eEq6ew7bbs1jYcMqylsIMR9Ko2EAfkiwpbiKEweudFlk3EcQXxOi8%2BRaJPzqgB; BDSVRTM=0";
        Assertions.assertEquals("[KeyValueStore{name='COOKIE_SESSION', value=3393_2_8_9_22_21_1_0_8_7_18_0_3389_0_4_24_1701255522_1701252149_1701255518|9#285902_383_1701252125|9}, KeyValueStore{name='ab_sr', value=1.0.1_NjIyNzJlZTMxNjRiNTI0YTA0ODFkYjQxNTgyNGQ0NTc5OWIxMjFmMDAyMmI3ZWY1M2I5ZWU1NGY4Y2Q2ZjA1OTIwNzg3ODM5OTYzMWU0NzEwODRkYWIxZjM2YmQ5OTM0OTRmZTYxNGY0YmIxMTBhZWVhNzNiZDkwMGQ0YTY1ZmFlMTkwODcxOTZjMzY0ZTJkZDQ5NDVkYjQ5MWIzY2JjYmVkMDY4NTU0ZDkzNmE2NGM5ODQ1MTAyYzExYTIxYzI3}, KeyValueStore{name='H_PS_645EC', value=9fc1R1eEq6ew7bbs1jYcMqylsIMR9Ko2EAfkiwpbiKEweudFlk3EcQXxOi8+RaJPzqgB}, KeyValueStore{name='BDSVRTM', value=0}]", UrlUtil.cookie2KeyValueStore(cookieStr).toString());
        Assertions.assertEquals("{\"COOKIE_SESSION\":\"3393_2_8_9_22_21_1_0_8_7_18_0_3389_0_4_24_1701255522_1701252149_1701255518|9#285902_383_1701252125|9\",\"BDSVRTM\":\"0\",\"ab_sr\":\"1.0.1_NjIyNzJlZTMxNjRiNTI0YTA0ODFkYjQxNTgyNGQ0NTc5OWIxMjFmMDAyMmI3ZWY1M2I5ZWU1NGY4Y2Q2ZjA1OTIwNzg3ODM5OTYzMWU0NzEwODRkYWIxZjM2YmQ5OTM0OTRmZTYxNGY0YmIxMTBhZWVhNzNiZDkwMGQ0YTY1ZmFlMTkwODcxOTZjMzY0ZTJkZDQ5NDVkYjQ5MWIzY2JjYmVkMDY4NTU0ZDkzNmE2NGM5ODQ1MTAyYzExYTIxYzI3\",\"H_PS_645EC\":\"9fc1R1eEq6ew7bbs1jYcMqylsIMR9Ko2EAfkiwpbiKEweudFlk3EcQXxOi8+RaJPzqgB\"}", UrlUtil.cookie2JsonStr(cookieStr).toString());
    }

    /**
     * cookie 中的 key 只有 = 没有 value
     */
    @Test
    void getCookie_02() {
        String cookieStr = "COOKIE_SESSION=3393; ab_sr=";
        Assertions.assertEquals("[KeyValueStore{name='COOKIE_SESSION', value=3393}, KeyValueStore{name='ab_sr', value=}]", UrlUtil.cookie2KeyValueStore(cookieStr).toString());
        Assertions.assertEquals("{\"COOKIE_SESSION\":\"3393\",\"ab_sr\":\"\"}", UrlUtil.cookie2JsonStr(cookieStr).toString());
    }

    /**
     * cookie 中只有 key 没有 =
     */
    @Test
    void getCookie_03() {
        String cookieStr = "COOKIE_SESSION=3393; ab_sr";
        Assertions.assertEquals("[KeyValueStore{name='COOKIE_SESSION', value=3393}]", UrlUtil.cookie2KeyValueStore(cookieStr).toString());
        Assertions.assertEquals("{\"COOKIE_SESSION\":\"3393\"}", UrlUtil.cookie2JsonStr(cookieStr).toString());
    }
}