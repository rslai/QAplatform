package com.smartentities.json.generator;

import org.everit.json.schema.Schema;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;

/**
 *  JSON Generator，JSON-Schema 生成 json 测试类
 */
class JsonGeneratorTest {
    /**
     * 测试生成空 json
     * 例如：{}
     * @throws FileNotFoundException
     */
    @Test
    void generate01() throws FileNotFoundException {
        String schemaStr = "{\"type\":\"object\",\"properties\":{}}";

        JSONObject jsonSchema = new JSONObject(new JSONTokener(schemaStr));
        Schema schema = SchemaLoader.load(jsonSchema);

        GeneratorConfig generatorConfig= GeneratorConfig.fromJsonSchema(schema);
        JsonGenerator jsonGenerator = new JsonGenerator(generatorConfig);
        String actual = jsonGenerator.generate();

        String expected = "{}";
        Assertions.assertEquals(expected, actual);
    }

    /**
     * 测试生成带有一个属性的 json
     * 例如：{"field_d5389":"xxxxxx"}，属性类型为 string，value 为随机字符串
     * @throws FileNotFoundException
     */
    @Test
    void generate02() throws FileNotFoundException {
        String schemaStr = "{\"type\":\"object\",\"properties\":{\"field_d5389\":{\"type\":\"string\"}},\"required\":[\"field_d5389\"]}";

        JSONObject jsonSchema = new JSONObject(new JSONTokener(schemaStr));
        Schema schema = SchemaLoader.load(jsonSchema);

        GeneratorConfig generatorConfig= GeneratorConfig.fromJsonSchema(schema);
        JsonGenerator jsonGenerator = new JsonGenerator(generatorConfig);
        String actual = jsonGenerator.generate();

        String expected = "{\"field_d5389\":\"";
        Assertions.assertTrue(actual.indexOf(expected) == 0, "actual: " + actual + ", expected: " + expected);
    }

    /**
     * 测试生成带一个带默认值的 String 属性
     * 例如：{"field_925a8":"ddd"}
     * @throws FileNotFoundException
     */
    @Test
    void generate03() throws FileNotFoundException {
        String schemaStr = "{\"type\":\"object\",\"properties\":{\"field_925a8\":{\"type\":\"string\",\"default\":\"ddd\"}},\"required\":[\"field_925a8\"]}";

        JSONObject jsonSchema = new JSONObject(new JSONTokener(schemaStr));
        Schema schema = SchemaLoader.load(jsonSchema);

        GeneratorConfig generatorConfig= GeneratorConfig.fromJsonSchema(schema);
        JsonGenerator jsonGenerator = new JsonGenerator(generatorConfig);
        String actual = jsonGenerator.generate();

        String expected = "{\"field_925a8\":\"ddd\"}";
        Assertions.assertEquals(expected, actual);
    }

    /**
     * 测试生成带一个带默认值的 number 属性的（整数）
     * 例如：{"field_925a8":12}
     * @throws FileNotFoundException
     */
    @Test
    void generate04() throws FileNotFoundException {
        String schemaStr = "{\"type\":\"object\",\"properties\":{\"field_925a8\":{\"type\":\"number\",\"default\":\"12\"}},\"required\":[\"field_925a8\"]}";

        JSONObject jsonSchema = new JSONObject(new JSONTokener(schemaStr));
        Schema schema = SchemaLoader.load(jsonSchema);

        GeneratorConfig generatorConfig= GeneratorConfig.fromJsonSchema(schema);
        JsonGenerator jsonGenerator = new JsonGenerator(generatorConfig);
        String actual = jsonGenerator.generate();

        String expected = "{\"field_925a8\":12}";
        Assertions.assertEquals(expected, actual);
    }

    /**
     * 测试生成带一个带默认值的 number 属性的（小数）
     * 例如：{"field_925a8":12.001}
     * @throws FileNotFoundException
     */
    @Test
    void generate05() throws FileNotFoundException {
        String schemaStr = "{\"type\":\"object\",\"properties\":{\"field_925a8\":{\"type\":\"number\",\"default\":\"12.001\"}},\"required\":[\"field_925a8\"]}";

        JSONObject jsonSchema = new JSONObject(new JSONTokener(schemaStr));
        Schema schema = SchemaLoader.load(jsonSchema);

        GeneratorConfig generatorConfig= GeneratorConfig.fromJsonSchema(schema);
        JsonGenerator jsonGenerator = new JsonGenerator(generatorConfig);
        String actual = jsonGenerator.generate();

        String expected = "{\"field_925a8\":12.001}";
        Assertions.assertEquals(expected, actual);
    }

    /**
     * 测试生成带一个带默认值的 integer 属性
     * 例如：{"field_925a8":12.001}
     * @throws FileNotFoundException
     */
    @Test
    void generate06() throws FileNotFoundException {
        String schemaStr = "{\"type\":\"object\",\"properties\":{\"field_925a8\":{\"type\":\"integer\",\"default\":\"12.001\"}},\"required\":[\"field_925a8\"]}";

        JSONObject jsonSchema = new JSONObject(new JSONTokener(schemaStr));
        Schema schema = SchemaLoader.load(jsonSchema);

        GeneratorConfig generatorConfig= GeneratorConfig.fromJsonSchema(schema);
        JsonGenerator jsonGenerator = new JsonGenerator(generatorConfig);
        String actual = jsonGenerator.generate();

        String expected = "{\"field_925a8\":12.001}";
        Assertions.assertEquals(expected, actual);
    }

    /**
     * 测试生成带一个带默认值的 boolean 属性
     * 例如：{"field_925a8":true}
     * @throws FileNotFoundException
     */
    @Test
    void generate07() throws FileNotFoundException {
        String schemaStr = "{\"type\":\"object\",\"properties\":{\"field_925a8\":{\"type\":\"boolean\",\"default\":\"true\"}},\"required\":[\"field_925a8\"]}";

        JSONObject jsonSchema = new JSONObject(new JSONTokener(schemaStr));
        Schema schema = SchemaLoader.load(jsonSchema);

        GeneratorConfig generatorConfig= GeneratorConfig.fromJsonSchema(schema);
        JsonGenerator jsonGenerator = new JsonGenerator(generatorConfig);
        String actual = jsonGenerator.generate();

        String expected = "{\"field_925a8\":true}";
        Assertions.assertEquals(expected, actual);
    }

    /**
     * 测试生成带一个带默认值的 boolean 属性（转 boolean 异常）
     * 例如：{"field_925a8":false}
     * @throws FileNotFoundException
     */
    @Test
    void generate08() throws FileNotFoundException {
        String schemaStr = "{\"type\":\"object\",\"properties\":{\"field_925a8\":{\"type\":\"boolean\",\"default\":\"abc\"}},\"required\":[\"field_925a8\"]}";

        JSONObject jsonSchema = new JSONObject(new JSONTokener(schemaStr));
        Schema schema = SchemaLoader.load(jsonSchema);

        GeneratorConfig generatorConfig= GeneratorConfig.fromJsonSchema(schema);
        JsonGenerator jsonGenerator = new JsonGenerator(generatorConfig);
        String actual = jsonGenerator.generate();

        String expected = "{\"field_925a8\":false}";
        Assertions.assertEquals(expected, actual);
    }
}