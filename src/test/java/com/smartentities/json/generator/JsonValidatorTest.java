package com.smartentities.json.generator;

import org.everit.json.schema.Schema;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.jupiter.api.Test;

/**
 *  JSON Generator，JSON-Schema validator 测试类
 */
class JsonValidatorTest {
    /**
     * 测试验证带有一个属性的 json
     */
    @Test
    void validator01() {
        String schemaStr = "{\"type\":\"object\",\"properties\":{\"field_d5389\":{\"type\":\"string\"}},\"required\":[\"field_d5389\"]}";
        JSONObject jsonSchema = new JSONObject(new JSONTokener(schemaStr));
        Schema schema = SchemaLoader.load(jsonSchema);

        String jsonSubjectStr = "{\"field_d5389\":\"djudhj\"}";
        JSONObject jsonSubject = new JSONObject(new JSONTokener(jsonSubjectStr));

        schema.validate(jsonSubject);
    }
}