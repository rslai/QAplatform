# Docker 基础库打包

#### 介绍
1. 由于 docker 镜像中添加了 openCV 库，可以分别打不同 cpu 架构的包，也可以在不同 cpu 架构中打特定的包，这里使用的是后者。
2. 另外 linux 版的 openCV 安装后，没有初始化对应 so 库，这样第一次调用 openCV 库就会抛错。为了避免首次运行报错，特意在最后运行了一个 openCV 的程序，以便让 openCV 初始化 so 库。

#### 打包说明

1. 进入 linux 系统，pull 下完整项目
2. 进入 docker
3. 解压 InitSikuli.zip 文件
   ```
   unzip InitSikuli.zip
   ```
4. 进入 InitSikuli 目录后 mvn 编译
   ```
   cd InitSikuli
   mvn clean package
   ```
5. 这时在 target 目录下就编译好了 init-sikuli.jar 文件
6. 返回 docker 目录（存在 InitSikuli.zip 文件的目录）
7. 编译 docker 镜像
   ```
   docker build -t registry.cn-hangzhou.aliyuncs.com/netlrs/jdk:jdk15-sikulix205-opencv430 .
   ```
8. 推送 docker 镜像到仓库
   ```
   docker push registry.cn-hangzhou.aliyuncs.com/netlrs/jdk:jdk15-sikulix205-opencv430
   ```

   ```
   如果只是本地 build 使用则不需要修改如下内容，如果要推到远程仓库则需要根据你的仓库地址修改如下内容
   1. registry.cn-hangzhou.aliyuncs.com : 阿里云加速 docker 仓库
   2. netlrs/jdk : 实例 + 仓库名称
   3. jdk15-sikulix205-opencv430 : 镜像文件版本
   ```
